package com.clickshopmerchant.shop.main.model.ordernew;

public class RequestBodyOrderNow {
    String merchantId, orderStatus, sessionId, mobileNumber;

    public RequestBodyOrderNow(String mobileNumber, String merchantId, String orderStatus, String sessionId) {
        this.mobileNumber = mobileNumber;
        this.merchantId = merchantId;
        this.orderStatus = orderStatus;
        this.sessionId = sessionId;
    }
}
