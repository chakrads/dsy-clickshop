package com.clickshopmerchant.shop.appconfig;
import com.clickshopmerchant.shop.main.model.ordernew.OrderNew;
import com.clickshopmerchant.shop.main.model.ordernew.RequestBodyOrderNow;
import com.clickshopmerchant.shop.main.model.resetpassword.RequestBodyResetPassword;
import com.clickshopmerchant.shop.main.model.resetpassword.ResetPasswords;
import com.clickshopmerchant.shop.main.model.selecteditemmodel.RequestBodySelectedItem;
import com.clickshopmerchant.shop.main.model.selecteditemmodel.SelectedItemM;
import com.clickshopmerchant.shop.main.model.signout.RequestBodySignOut;
import com.clickshopmerchant.shop.main.model.signout.SignOut;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;
public interface ApiInterface {

//    @GET("findItemCategoryRandomly")
//    Call<CatagoryListing> getCatagory();


    @POST("merchant/getStoreItem")
    Call<SelectedItemM> getItems(@Body RequestBodySelectedItem requestBodySelectedItem);

    @POST("merchant/searchMerchantOrder")
    Call<OrderNew> getOrders (@Body RequestBodyOrderNow requestBodyOrderNow);

    @POST("merchant/signout")
    Call<SignOut> signOut (@Body RequestBodySignOut requestBodySignOut);

    @POST("merchant/resetPassword")
    Call<ResetPasswords> resetPassword (@Body RequestBodyResetPassword requestBodyResetPassword);



}



