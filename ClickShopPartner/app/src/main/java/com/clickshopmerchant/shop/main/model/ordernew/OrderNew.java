
package com.clickshopmerchant.shop.main.model.ordernew;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OrderNew {

    @SerializedName("responseCode")
    @Expose
    private Integer responseCode;
    @SerializedName("responseMessage")
    @Expose
    private String responseMessage;
    @SerializedName("orderSearchList")
    @Expose
    private List<OrderSearchList> orderSearchList = null;

    public Integer getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(Integer responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public List<OrderSearchList> getOrderSearchList() {
        return orderSearchList;
    }

    public void setOrderSearchList(List<OrderSearchList> orderSearchList) {
        this.orderSearchList = orderSearchList;
    }

}
