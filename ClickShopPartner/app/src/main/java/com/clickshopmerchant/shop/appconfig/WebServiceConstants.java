package com.clickshopmerchant.shop.appconfig;

public class WebServiceConstants {


    public static String RESPONSE_STATUS_CODE = "responseStatusCode";
    public static String RESPONSE_STATUS_MESSAGE = "loginStatusMessage";
    public static String RESPONSE_MERCHANT_ID = "merchantId";
    public static String RESPONSE_PHONE_NUMBER = "mobileNumber";
    public static String RESPONSE_FIRST_NAME = "firstName";
    public static String RESPONSE_LAST_NAME = "lastName";
    public static String NULL = "null";

    // LoginActivity
    public static String FLAG = "flag";


    public static String EXISTING_RESPONSE_CODE= "responseCode";
    public static String EXISTING_RESPONSE_MESSAGE= "responseMessage";
    public static String DATA= "data";
    public static String OFFER_LIST = "offerList";
    public static String FETCH_DATA= "itemList";
    public static String EXISTING_ITEM_ID= "itemId";
    public static String EXISTING_ITEM_NAME= "itemName";
    public static String EXISTING_ITEM_PRICE= "itemPrice";
    public static String EXISTING_ITEM_IMAGE= "itemImage";
    public static String EXISTING_ITEM_WEIGHT= "itemWeight";
    public static String EXISTING_ITEM_STRING_EMPTY= "";
    public static int EXISTING_ITEM_INTEGER_NONE= 0;

    public static String FETCH_FLAG = "flag";


// Store Profile Set Up


    public static String STORE_PROFILE_STORE_ID= "storeId";

    public static String STORE_PROFILE_STORE_NAME= "storeName";
    public static String STORE_PROFILE_MERCHANT_ID= "merchantId";
    public static String STORE_PROFILE_DELIVERY_DURATION= "deliveryDuration";
    public static String STORE_PROFILE_WORKING_HOURS_FROM= "workingHourFrom";
    public static String STORE_PROFILE_WORKING_HOURS_TO= "workingHourTo";
    public static String STORE_PROFILE_DELIVERY_RANGE= "deliveryRange";
    public static String STORE_PROFILE_LATITTUDE= "latitude";
    public static String STORE_PROFILE_LONGITUDE= "longitude";
    public static String STORE_PROFILE_ADDRESS= "address";
    public static String STORE_PROFILE_CITY= "city";
    public static String STORE_PROFILE_STATE= "state";
    public static String STORE_PROFILE_COUNTRY= "country";
    public static String STORE_PROFILE_PIN_CODE= "pinCode";
    public static String STORE_DELIVERY_CHARGE= "deliveryCharge";
    public static String STORE_DELIVERY_CHARGE_UPTO= "deliveryFreeAbove";


  // GetFilterCategory
    public static String GET_FILTER_CATEGORY_ID = "itemCategoryId";
    public static String GET_FILTER_CATEGORY_NAME= "itemCategoryName";
    public static String GET_FILTER_CATEGORY_IMAGE= "itemCategoryImage";




    // GetOfferList
    public static String OFFER_ID = "offerId";
    public static String DISCOUNT = "discount";
    public static String PRICE_AFTER_DISCOUNT= "priceAfterDiscount";


   //Profile
    public static String PROFILE_IMAGE = "profilePic";





}
