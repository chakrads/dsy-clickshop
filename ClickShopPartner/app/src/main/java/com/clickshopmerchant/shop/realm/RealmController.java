package com.clickshopmerchant.shop.realm;
import android.app.Activity;
import android.app.Application;
import android.support.v4.app.Fragment;
import io.realm.Realm;
import io.realm.RealmResults;


public class RealmController {
    private static RealmController instance;
    public static Realm realm;

    public RealmController(Application application) {
        realm = Realm.getDefaultInstance();
    }

    public static RealmController with(Fragment fragment) {

        if (instance == null) {
            instance = new RealmController(fragment.getActivity().getApplication());
        }
        return instance;
    }

    public static RealmController with(Activity activity) {

        if (instance == null) {
            instance = new RealmController(activity.getApplication());
        }
        return instance;
    }

    public static RealmController with(Application application) {

        if (instance == null) {
            instance = new RealmController(application);
        }
        return instance;
    }

    public static RealmController getInstance() {

        return instance;
    }

    public Realm getRealm() {

        return realm;
    }

    //Refresh the realm istance
    public void refresh() {

        realm.refresh();
    }

    //clear all objects from Store.class
    public static void clearAll() {

        realm.beginTransaction();
        realm.clear(Store.class);
        realm.commitTransaction();
    }

    //find all objects in the Store.class
    public RealmResults<Store> getBooks() {

        return realm.where(Store.class).findAll();
    }

    //query a single item with the given id
    public Store getStore(String id) {

        return realm.where(Store.class).equalTo("id", id).findFirst();
    }

    //check if Store.class is empty
    public boolean hasStore() {

        return !realm.allObjects(Store.class).isEmpty();
    }




//    //query example
//    public RealmResults<Book> queryedBooks() {
//
//        return realm.where(Book.class)
//                .contains("author", "Author 0")
//                .or()
//                .contains("title", "Realm")
//                .findAll();
//
//    }
}
