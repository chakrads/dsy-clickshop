package com.clickshopmerchant.shop.appconfig;

public interface EndPoint {

     String USER_APP_URL = "http://18.222.124.117:9091/";
   //  String MERCHANT_APP_URL = "http://3.17.138.169:9091/";
     String MERCHANT_APP_URL = "http://18.222.124.117:9091/";
//
////
//
//    String USER_APP_URL = "http://192.168.0.108:9091/";
//    String MERCHANT_APP_URL = "http://192.168.0.108:8080/";


    String SIGNIN = MERCHANT_APP_URL + "merchant/signin"; // working after session
    String REGISTRATION = MERCHANT_APP_URL + "merchant/signup";  // working

    String EXISTING_ITEMS = MERCHANT_APP_URL + "clickshop/getAllItems";
    String ADD_SELECTED_ITEM = MERCHANT_APP_URL + "merchant/saveItem";    // working with session id
    String DELETE_SELECTED_ITEM =  MERCHANT_APP_URL + "merchant/deleteItem"; // working with session id
    String UPDATE_SELECTED_ITEM = MERCHANT_APP_URL + "merchant/updateItem";  // working with session id
    String FETCH_STORE_SET_UP_DATA = MERCHANT_APP_URL + "store/getStoreSetup"; // working with session Id
    String CATEGORY_LISTING =  MERCHANT_APP_URL + "clickshop/getItemsCategory";  // Since this is GET method session id not required
    String GET_FILTERED_ITEM = MERCHANT_APP_URL + "clickshop/getItemsByCategory"; //
    String SAVE_INITIAL_SETUP_ADDRESS = MERCHANT_APP_URL + "store/saveStoreSetup"; // working with session id
    String UPDATE_PROFILE_SETUP = MERCHANT_APP_URL + "store/updateStoreSetup"; // working with session id
    String ADD_NEW_OFFER = MERCHANT_APP_URL + "clickshop/saveOffer"; // working with session id
    String GET_EXISITING_OFFER_LIST = MERCHANT_APP_URL + "clickshop/getOffer";  // working with session id
    String DELETE_EXISITING_OFFER = MERCHANT_APP_URL + "clickshop/deleteOffer";  // working with session id
    String PROFILE_IMAGE_UPLOAD = MERCHANT_APP_URL + "clickshop/saveProfileImage";
    String PROFILE_IMAGE_DOWNLOAD = MERCHANT_APP_URL + "clickshop/getProfileImage"; // Working Since this is GET method no session id is passed
    String MERCHANT_ORDER = MERCHANT_APP_URL + "merchant/searchMerchantOrder"; // Working with session id
    String SAVE_MERCHANT_ORDER_STATUS = MERCHANT_APP_URL + "merchant/saveMerchantOrderStatus"; // Working with session id
    String GET_MERCHANT_ORDER_ARCHIVE = MERCHANT_APP_URL + "merchant/searchMerchantOrderArchive"; // working
    String ORDER_ITEMS = MERCHANT_APP_URL + "merchant/searchMerchantOrderDetails";  // working with session id
    String WALLET_SYSTEM = MERCHANT_APP_URL + "merchant/getMerchantWalletAmount"; // working with session id
    String SEND_CONTACT_US = MERCHANT_APP_URL + "merchant/saveContactUs"; // working with session id
    String FORGOT_PASSWORD = MERCHANT_APP_URL + "merchant/forgetPassword";




}





