package com.clickshopmerchant.shop.main.adapter;
import android.app.ProgressDialog;
import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.clickshopmerchant.shop.R;
import com.clickshopmerchant.shop.appconfig.AppUtils;
import com.clickshopmerchant.shop.appconfig.EndPoint;
import com.clickshopmerchant.shop.appconfig.PreferenceConstants;
import com.clickshopmerchant.shop.appconfig.SessionManager;
import com.clickshopmerchant.shop.appconfig.WebServiceConstants;
import com.clickshopmerchant.shop.main.model.GetOfferList;
import com.clickshopmerchant.shop.network.NetworkUtils;
import com.clickshopmerchant.shop.storage.Preference;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.HashMap;
import java.util.List;

public class OfferCardAdapter extends RecyclerView.Adapter<OfferCardAdapter.ViewHolder> {

    private static final String TAG = OfferCardAdapter.class.getSimpleName();
    Context context;
    List<GetOfferList> listInAss;
    private AlertDialog alert;
    private ProgressDialog progress;
    String mOfferId;


    public OfferCardAdapter(List<GetOfferList> listInAss, Context context) {
        super();
        this.listInAss = listInAss;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.offer_card_adapter, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public int getItemCount() {
        return listInAss.size();
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final GetOfferList getOfferList = listInAss.get(position);
        holder.textViewDiscount.setText(getOfferList.getMDiscount());
        holder.textViewPriceAfterDiscount.setText(getOfferList.getMPriceAfterDiscount());
        holder.imageViewDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOfferId = String.valueOf(getOfferList.getOfferId());
                AreYourSureMessage("Are You Sure You Want To Delete This Item?");
            }
        });
    }


    class ViewHolder extends RecyclerView.ViewHolder {
        TextView textViewDiscount;
        TextView textViewPriceAfterDiscount;
        ImageView imageViewDelete;

        public ViewHolder(View itemView) {
            super(itemView);
            textViewDiscount = itemView.findViewById(R.id._discount);
            textViewPriceAfterDiscount = itemView.findViewById(R.id._price_afte);
            imageViewDelete = itemView.findViewById(R.id._delete_item);
            this.setIsRecyclable(false);
        }
    }



    public void AreYourSureMessage(String message) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View promptView = layoutInflater.inflate(R.layout.alert_message_are_your_sure, null);
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setView(promptView);
        TextView textViewMessage = promptView.findViewById(R.id.alert_message_display);
        textViewMessage.setText(message);
        Button buttonblock = promptView.findViewById(R.id.alert_ok_buton);
        buttonblock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (AppUtils.internetConnectionAvailable(2000)) {
                    deleteOffer();
                } else {
                    Toast.makeText(context, "No Network Connection",
                            Toast.LENGTH_SHORT).show();
                }

                alert.dismiss();
            }
        });

        Button buttoncancel = promptView.findViewById(R.id.alert_cancel);
        buttoncancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alert.dismiss();

            }
        });
        alert = alertDialogBuilder.create();
        alert.show();
    }




    public void deleteOffer() {
        progress = ProgressDialog.show(context, "", "Please wait...", false, false);
        SessionManager sessionManager = new SessionManager(context);
        HashMap<String, String> hashMap = sessionManager.getUserDetails();
        String mSessionId = hashMap.get(SessionManager.KEY_SESSION_ID);
        String mMobile = hashMap.get(SessionManager.KEY_PHONE_NUMBER);


        HashMap<String, String> params = new HashMap<String, String>();
        params.put("merchantId", Preference.getDefaults(PreferenceConstants.MERCHANT_ID, context));
        params.put("offerid", mOfferId);
        params.put("sessionId", mSessionId);
        params.put("mobileNumber", mMobile);

        Log.d(TAG, "params: " + params);
        JsonObjectRequest request_json = new JsonObjectRequest(EndPoint.DELETE_EXISITING_OFFER, new JSONObject(params),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        progress.dismiss();
                        try {
                            Integer mStatusCode = response.getInt(WebServiceConstants.EXISTING_RESPONSE_CODE);
                            if (mStatusCode == 400) {
                                AreYourSureMessage("Delete Failed. Please Retry..");
                            } else if (mStatusCode == 200) {
                                AreYourSureMessage("Delete Successful");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progress.dismiss();
                NetworkUtils.UnableToReachServer(context);
            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(request_json);
    }
}



