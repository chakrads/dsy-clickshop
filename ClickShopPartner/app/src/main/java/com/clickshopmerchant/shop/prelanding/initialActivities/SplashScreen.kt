package com.clickshopmerchant.shop.prelanding.initialActivities
import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.view.WindowManager
import com.clickshopmerchant.shop.R
import com.clickshopmerchant.shop.appconfig.PreferenceConstants
import com.clickshopmerchant.shop.appconfig.SessionManager
import com.clickshopmerchant.shop.main.activity.MainActivity
import com.clickshopmerchant.shop.storage.Preference
import java.util.ArrayList
import java.util.HashMap

class SplashScreen : AppCompatActivity() {
    internal var PERMISSIONS = arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.CAMERA)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)
        if (Build.VERSION.SDK_INT >= 21) {
            val window = window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            window.statusBarColor = resources.getColor(R.color.md_black_1000)
        }

        if (checkAndRequestPermissions()) {

                Handler().postDelayed({
                    val sessionManager = SessionManager(applicationContext);

                    if (sessionManager.isLoggedIn){

                        var mLoginFlag = Preference.getDefaults(PreferenceConstants.LOGIN_FLAG, applicationContext);
                        if (mLoginFlag.equals("0")){

                            val i = Intent(this@SplashScreen, SetUpMainActivity::class.java)
                            startActivity(i)
                            finish()

                        } else if (mLoginFlag.equals("1")){

                            val i = Intent(this@SplashScreen, MainActivity::class.java)
                            startActivity(i)
                            finish()

                        }

                    } else {
                        val i = Intent(this@SplashScreen, LoginActivity::class.java)
                        startActivity(i)
                        finish()
                    }




                }, SPLASH_TIME_OUT.toLong())
            }


    }

    private fun checkAndRequestPermissions(): Boolean {
        val neededPermissions = ArrayList<String>()
        for (permission in PERMISSIONS) {
            if (ContextCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {
                neededPermissions.add(permission)


            }
        }

        if (!neededPermissions.isEmpty()) {
            ActivityCompat.requestPermissions(this, neededPermissions.toTypedArray(), REQUEST_MULTIPLE_PERMISSIONS)
            return false
        }
        return true
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            REQUEST_MULTIPLE_PERMISSIONS -> {
                val perms = HashMap<String, Int>()
                for (permission in permissions) {
                    perms[permission] = PackageManager.PERMISSION_GRANTED
                }

                if (grantResults.size > 0) {
                    for (i in permissions.indices)
                        perms[permissions[i]] = grantResults[i]

                    var allPermissionsGranted = true
                    for (permission1 in permissions) {
                        allPermissionsGranted = allPermissionsGranted && perms[permission1] == PackageManager.PERMISSION_GRANTED
                    }

                    if (allPermissionsGranted) {
                        val i = Intent(this@SplashScreen, LoginActivity::class.java)
                        startActivity(i)
                        finish()

                    } else {
                        for (permission2 in perms.keys)
                            if (perms[permission2] == PackageManager.PERMISSION_GRANTED)
                                perms.remove(permission2)

                        val message = StringBuilder("The app has not been granted permissions:\\n\\n")
                        for (permission in perms.keys) {
                            message.append(permission)
                            message.append("\n")
                        }

                        message.append("\nHence, it cannot function properly." + "\nPlease consider granting it this permission in phone Settings.")

                        val builder = AlertDialog.Builder(this)
                        builder.setTitle("Permission Required")
                                .setMessage(message)
                                .setPositiveButton("Grant") { dialogInterface, i -> dialogInterface.cancel() }
                        builder.setNegativeButton("Cancel") { dialogInterface, i -> dialogInterface.cancel() }
                        val alertDialog = builder.create()
                        alertDialog.show()
                        //builder.show();
                    }

                }
            }
        }
    }

    companion object {
        private val SPLASH_TIME_OUT = 3000
        private val REQUEST_MULTIPLE_PERMISSIONS = 111
    }

}






