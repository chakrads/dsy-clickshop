package com.clickshopmerchant.shop.main.offerFragments;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.clickshopmerchant.shop.R;
import com.clickshopmerchant.shop.appconfig.AppUtils;
import com.clickshopmerchant.shop.appconfig.EndPoint;
import com.clickshopmerchant.shop.appconfig.PreferenceConstants;
import com.clickshopmerchant.shop.appconfig.SessionManager;
import com.clickshopmerchant.shop.appconfig.WebServiceConstants;
import com.clickshopmerchant.shop.main.adapter.OfferCardAdapter;
import com.clickshopmerchant.shop.main.model.GetOfferList;
import com.clickshopmerchant.shop.network.NetworkUtils;
import com.clickshopmerchant.shop.storage.Preference;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;


public class ExistingOffer extends Fragment {

    private static final String TAG = ExistingOffer.class.getSimpleName();
    boolean mUserVisibleHint = true;

    public RelativeLayout relativeLayoutLoading;
    public SwipeRefreshLayout swipeRefreshLayout;
    public RecyclerView recyclerView;
    public List<GetOfferList> arrayOffer;
    public OfferCardAdapter adapter;
    public LinearLayoutManager linearLayoutManager;

    TextView textViewDiscount;
    TextView textViewPriceAfterDiscount;
    ImageView imageViewDelete;

    private ProgressDialog progress;

    String mActualPrice, mDiscountPercentage, mDiscountAmount, mPriceAfterDiscount;
    Integer mofferId;

    private AlertDialog alert;
    CardView cardView;
    RelativeLayout relativeLayoutNotAvailalbe;


    public ExistingOffer() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setUserVisibleHint(false);
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_existing_offer, container, false);
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {


//        relativeLayoutLoading = getActivity().findViewById(R.id._loadingS);
//        swipeRefreshLayout = getActivity().findViewById(R.id.offer_item_refresh);
//        swipeRefreshLayout.setVisibility(View.GONE);
//        swipeRefreshLayout.setOnRefreshListener(this);
//        recyclerView = getActivity().findViewById(R.id.offer_recyclerview);
//        linearLayoutManager = new LinearLayoutManager(getActivity());
//        recyclerView.setLayoutManager(linearLayoutManager);
//        arrayOffer = new ArrayList<>();


        relativeLayoutNotAvailalbe = getActivity().findViewById(R.id._no_offers_added);
        relativeLayoutNotAvailalbe.setVisibility(View.GONE);

        cardView = getActivity().findViewById(R.id._card_view);
        cardView.setVisibility(View.GONE);
        textViewDiscount = getActivity().findViewById(R.id._discount);
        textViewPriceAfterDiscount = getActivity().findViewById(R.id._price_afte);
        imageViewDelete = getActivity().findViewById(R.id._delete_item);
        imageViewDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AreYourSureMessage("Are You Sure You Want To Delete Offer");
            }
        });
        super.onActivityCreated(savedInstanceState);
    }


    public void Message(String message) {
        LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
        View promptView = layoutInflater.inflate(R.layout.alert_message, null);
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setView(promptView);
        TextView textViewMessage = promptView.findViewById(R.id.alert_message_display);
        textViewMessage.setText(message);
        Button buttonblock = promptView.findViewById(R.id.alert_ok_buton);
        buttonblock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alert.dismiss();
            }
        });
        alert = alertDialogBuilder.create();
        alert.show();
    }


    public void AreYourSureMessage(String message) {
        LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
        View promptView = layoutInflater.inflate(R.layout.alert_message_are_your_sure, null);
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setView(promptView);
        TextView textViewMessage = promptView.findViewById(R.id.alert_message_display);
        textViewMessage.setText(message);
        Button buttonblock = promptView.findViewById(R.id.alert_ok_buton);
        buttonblock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (AppUtils.internetConnectionAvailable(2000)) {
                    deleteOffer();
                } else {
                    Toast.makeText(getActivity(), "No Network Connection",
                            Toast.LENGTH_SHORT).show();
                }
                alert.dismiss();
            }
        });

        Button buttoncancel = promptView.findViewById(R.id.alert_cancel);
        buttoncancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alert.dismiss();

            }
        });

        alert = alertDialogBuilder.create();
        alert.show();
    }


    public void deleteOffer() {
        progress = ProgressDialog.show(getActivity(), "", "Please wait...", false, false);
        SessionManager sessionManager = new SessionManager(getActivity());
        HashMap<String, String> hashMap = sessionManager.getUserDetails();
        String mSessionId = hashMap.get(SessionManager.KEY_SESSION_ID);
        String mMobile = hashMap.get(SessionManager.KEY_PHONE_NUMBER);

        HashMap<String, String> params = new HashMap<String, String>();

        params.put("merchantId", Preference.getDefaults(PreferenceConstants.MERCHANT_ID, getActivity()));
        params.put("itemId", Preference.getDefaults(PreferenceConstants.SELECTED_ITEM_ID, getActivity()));
        params.put("sessionId", mSessionId);
        params.put("mobileNumber", mMobile);


        Log.d(TAG, "params: " + params);
        JsonObjectRequest request_json = new JsonObjectRequest(EndPoint.DELETE_EXISITING_OFFER, new JSONObject(params),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        progress.dismiss();
                        try {
                            Integer mStatusCode = response.getInt(WebServiceConstants.EXISTING_RESPONSE_CODE);
                            if (mStatusCode == 400) {
                                Message("Delete Failed. Please Retry..");
                            } else if (mStatusCode == 200) {

                                Message("Delete Successful");
                                cardView.setVisibility(View.GONE);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progress.dismiss();
                NetworkUtils.UnableToReachServer(getActivity());
            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(request_json);
    }


    @Override
    public void setMenuVisibility(final boolean visible) {
        super.setMenuVisibility(visible);
        if (visible) {
            servicecall();
        }
    }


    public void servicecall() {
        if (AppUtils.internetConnectionAvailable(2000)) {
            getAllOfferList();
        } else {
            Toast.makeText(getActivity(), "No Network Connection",
                    Toast.LENGTH_SHORT).show();
        }

    }


    public void getAllOfferList() {
        progress = ProgressDialog.show(getActivity(), "", "Please wait...", false, false);
        SessionManager sessionManager = new SessionManager(getActivity());
        HashMap<String, String> hashMap = sessionManager.getUserDetails();
        String mSessionId = hashMap.get(SessionManager.KEY_SESSION_ID);
        String mMobile = hashMap.get(SessionManager.KEY_PHONE_NUMBER);

        HashMap<String, String> params = new HashMap<String, String>();
        params.put("merchantId", Preference.getDefaults(PreferenceConstants.MERCHANT_ID, getActivity()));
        params.put("itemId", Preference.getDefaults(PreferenceConstants.SELECTED_ITEM_ID, getActivity()));
        params.put("sessionId", mSessionId);
        params.put("mobileNumber", mMobile);

        Log.d(TAG, "params: " + params);

        JsonObjectRequest request_json = new JsonObjectRequest(EndPoint.GET_EXISITING_OFFER_LIST, new JSONObject(params),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        progress.dismiss();
                        //     recyclerView.setVisibility(View.VISIBLE);
                        Log.d(TAG, "Offer Response: " + response);
                        try {
                            int mStatusCode = response.getInt(WebServiceConstants.EXISTING_RESPONSE_CODE);
                            if (mStatusCode == 200) {
                                relativeLayoutNotAvailalbe.setVisibility(View.GONE);
                                cardView.setVisibility(View.VISIBLE);
                                mofferId = response.getInt("offerId");
                                Integer mMerchantId = response.getInt("merchantId");
                                Integer itemId = response.getInt("itemId");
                                mActualPrice = response.getString("actualPrice");
                                mDiscountPercentage = response.getString("discountPercentage");
                                mDiscountAmount = response.getString("discountAmount");
                                mPriceAfterDiscount = response.getString("priceAfterDiscount");
                                textViewDiscount.setText(mDiscountAmount);
                                textViewPriceAfterDiscount.setText(mPriceAfterDiscount);
                            } else {
                                cardView.setVisibility(View.GONE);
                                relativeLayoutNotAvailalbe.setVisibility(View.VISIBLE);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progress.dismiss();
                NetworkUtils.UnableToReachServer(getActivity());
            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(request_json);
    }



    // Offer : Earlier we thought of showing order in List view and later only one offer price was assigned per product.


    private void parseData(JSONArray array) {
        for (int i = 0; i < array.length(); i++) {
            GetOfferList offerList = new GetOfferList();
            JSONObject json;
            try {
                json = array.getJSONObject(i);
                if (json.getString(WebServiceConstants.OFFER_ID) != null && !json.getString(WebServiceConstants.OFFER_ID).isEmpty()
                        && !json.getString(WebServiceConstants.OFFER_ID).equals(WebServiceConstants.NULL)) {
                    offerList.setOfferId(Integer.valueOf(json.getString(WebServiceConstants.OFFER_ID)));
                } else {
                    offerList.setOfferId(0);
                }
                if (json.getString(WebServiceConstants.DISCOUNT) != null && !json.getString(WebServiceConstants.DISCOUNT).isEmpty()
                        && !json.getString(WebServiceConstants.DISCOUNT).equals(WebServiceConstants.NULL)) {
                    offerList.setMDiscount((json.getString(WebServiceConstants.DISCOUNT)));
                } else {
                    offerList.setMDiscount("");
                }
                if (json.getString(WebServiceConstants.PRICE_AFTER_DISCOUNT) != null && !json.getString(WebServiceConstants.PRICE_AFTER_DISCOUNT).isEmpty()
                        && !json.getString(WebServiceConstants.PRICE_AFTER_DISCOUNT).equals(WebServiceConstants.NULL)) {
                    offerList.setMPriceAfterDiscount((json.getString(WebServiceConstants.PRICE_AFTER_DISCOUNT)));
                } else {
                    offerList.setMPriceAfterDiscount("");
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
            arrayOffer.add(offerList);
            // Collections.sort(recordsList);
        }
        adapter = new OfferCardAdapter(arrayOffer, getActivity());
        recyclerView.setAdapter(adapter);
        swipeRefreshLayout.setRefreshing(false);
        adapter.notifyDataSetChanged();
    }


}
