package com.clickshopmerchant.shop.main.model

class GetFilteredItems {

  var item_id : Int? = 0;
  var item_image_url: String? = null
  var item_name : String? = null
  var item_price : String? = null
  var item_weight : String? = null

  var item_weight_unit : String? = null
  var brand_name : String? = null
  var company_name : String? = null
  var itemCategory : String? = null
  var itemSubcategory : String? = null
  var family : String? = null
  var priceUnit : String? = null
  var barcode : String? = null


  var isSelected: Boolean = false


}