package com.clickshopmerchant.shop.main.adapter;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.clickshopmerchant.shop.R;
import com.clickshopmerchant.shop.appconfig.AppUtils;
import com.clickshopmerchant.shop.appconfig.EndPoint;
import com.clickshopmerchant.shop.appconfig.PreferenceConstants;
import com.clickshopmerchant.shop.appconfig.SessionManager;
import com.clickshopmerchant.shop.appconfig.WebServiceConstants;
import com.clickshopmerchant.shop.main.activity.OrderItemActivity;
import com.clickshopmerchant.shop.main.fragment.hometabfragment.ProgressOrderRefresh;
import com.clickshopmerchant.shop.main.model.ProgressPo;
import com.clickshopmerchant.shop.storage.Preference;

import org.json.JSONException;
import org.json.JSONObject;
import java.util.HashMap;
import java.util.List;

public class ProgressCardAdapter extends RecyclerView.Adapter<ProgressCardAdapter.ViewHolder> {

    private static final String TAG = ProgressCardAdapter.class.getSimpleName();
    Context context;
    List<ProgressPo> listInAss;
    ProgressOrderRefresh progressOrderRefresh;
    public AlertDialog alertMessage;
    String mOrderId;
    private ProgressDialog progress;
    int mCheck;

    public ProgressCardAdapter(List<ProgressPo> listInAss, Context context, ProgressOrderRefresh progressOrderRefresh) {
        super();
        this.listInAss = listInAss;
        this.context = context;
        this.progressOrderRefresh = progressOrderRefresh;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_adapter_progress, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public int getItemCount() {
        return listInAss.size();
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final ProgressPo progressPo = listInAss.get(position);
        holder.textViewOrderId.setText(progressPo.getOrderId());
        holder.textViewAddress.setText(progressPo.getShippingAddress());
        holder.textViewPaymentMode.setText(progressPo.getPaymentMode());
        holder.textViewOrderUsername.setText(progressPo.getUserName());
        holder.textViewPhoneNumber.setText(progressPo.getMobileNumber());
        holder.textViewTotalAmount.setText(new StringBuilder("Rs. ").append(progressPo.getOrderAmount()));

        holder.buttonDeliveryDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCheck = 1;
                mOrderId = progressPo.getOrderId();
                Message("Delivery Done ?");


            }
        });


        holder.buttonShowItems.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCheck = 2;
                mOrderId = progressPo.getOrderId();

                Intent intent = new Intent(context, OrderItemActivity.class);
                intent.putExtra("OrderItems", mOrderId);
                context.startActivity(intent);
            }
        });






        holder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String mPhoneNumber = progressPo.getMobileNumber();
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:"+mPhoneNumber));
                context.startActivity(intent);


            }
        });


    }


    class ViewHolder extends RecyclerView.ViewHolder {
        TextView textViewOrderId;
        TextView textViewAddress;
        TextView textViewPaymentMode;
        TextView textViewTotalAmount;
        Button buttonDeliveryDone;
        Button buttonShowItems;
        TextView textViewOrderUsername;
        TextView textViewPhoneNumber;
        LinearLayout linearLayout;

        private ViewHolder(View itemView) {
            super(itemView);
            textViewOrderId = itemView.findViewById(R.id._order_id);
            textViewAddress = itemView.findViewById(R.id._deliver_to);
            textViewPaymentMode = itemView.findViewById(R.id._payment_mode);
            textViewTotalAmount = itemView.findViewById(R.id._total_amount);
            buttonDeliveryDone = itemView.findViewById(R.id._delivery_done);
            buttonShowItems = itemView.findViewById(R.id._show_items);
            textViewOrderUsername = itemView.findViewById(R.id._order_user_name);
            textViewPhoneNumber = itemView.findViewById(R.id._order_user_phonenumber);
            linearLayout = itemView.findViewById(R.id.phone_calling);
            this.setIsRecyclable(false);
        }
    }


    public void Message(String message) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View promptView = layoutInflater.inflate(R.layout.alert_message, null);
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setView(promptView);
        TextView textViewMessage = promptView.findViewById(R.id.alert_message_display);
        textViewMessage.setText(message);
        Button buttonblock = promptView.findViewById(R.id.alert_ok_buton);
        buttonblock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (AppUtils.internetConnectionAvailable(2000)) {
                    AcceptRejectStatusUpdate();
                } else {
                    Toast.makeText(context, "No Network Connection",
                            Toast.LENGTH_SHORT).show();
                }

                alertMessage.dismiss();
            }
        });
        alertMessage = alertDialogBuilder.create();
        alertMessage.show();
    }

    private void AcceptRejectStatusUpdate() {
        progress = ProgressDialog.show(context, "", "Please wait...", false, false);


        SessionManager sessionManager = new SessionManager(context);
        HashMap<String, String> hashMap = sessionManager.getUserDetails();
        String mSessionId = hashMap.get(SessionManager.KEY_SESSION_ID);
        String mMobile = hashMap.get(SessionManager.KEY_PHONE_NUMBER);


        HashMap<String, String> params = new HashMap<String, String>();
        params.put("mobileNumber", mMobile);
        params.put("sessionId", mSessionId);
        params.put("orderId", mOrderId);
        params.put("merchantId", Preference.getDefaults(PreferenceConstants.MERCHANT_ID, context));

        if (mCheck == 1){
            params.put("orderStatus", "Finished");
        }

        Log.d(TAG, "params: " + params);
        JsonObjectRequest request_json = new JsonObjectRequest(EndPoint.SAVE_MERCHANT_ORDER_STATUS, new JSONObject(params),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        progress.dismiss();
                        Log.d(TAG, "Slot Response: " + response);
                        try {
                            int mStatusCode = response.getInt(WebServiceConstants.EXISTING_RESPONSE_CODE);
                            if (mStatusCode == 200) {
                                progressOrderRefresh.progressOrderRE();
                                Toast.makeText(context, "Submit Successful",
                                        Toast.LENGTH_LONG).show();

                            } else if (mStatusCode == 400) {
                                Toast.makeText(context, "Update Failed",
                                        Toast.LENGTH_LONG).show();
                            } else {


                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                try {
                    progress.dismiss();
                } catch (NullPointerException e) {
                    Toast.makeText(context, "Unable Reach Remote Server. Please try later..",
                            Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                } catch (IllegalStateException e) {
                    Toast.makeText(context, "Unable Reach Remote Server. Please try later..",
                            Toast.LENGTH_LONG).show();
                }
            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(request_json);
    }












}
