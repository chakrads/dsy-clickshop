package com.clickshopmerchant.shop.prelanding.setupFragments;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.CursorIndexOutOfBoundsException;
import android.database.SQLException;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.TimePicker;
import android.widget.Toast;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.clickshopmerchant.shop.R;
import com.clickshopmerchant.shop.appconfig.AppUtils;
import com.clickshopmerchant.shop.appconfig.EndPoint;
import com.clickshopmerchant.shop.appconfig.GPSTracker;
import com.clickshopmerchant.shop.appconfig.PreferenceConstants;
import com.clickshopmerchant.shop.appconfig.SessionManager;
import com.clickshopmerchant.shop.appconfig.WebServiceConstants;
import com.clickshopmerchant.shop.main.fragment.menufragments.SelectedItems;
import com.clickshopmerchant.shop.network.NetworkUtils;
import com.clickshopmerchant.shop.places.PlacesActivity;
import com.clickshopmerchant.shop.storage.Preference;
import com.clickshopmerchant.shop.volley.VolleyMultipartRequest;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Method;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


public class SetUpFragment extends Fragment implements View.OnClickListener {
    private static final String TAG = SetUpFragment.class.getSimpleName();
    private int CalendarHour, CalendarMinute;
    String format;
    Calendar calendar;
    TimePickerDialog timepickerdialog;

    String timeformat;



    static Dialog d ;

    final Calendar c = Calendar.getInstance();

    final int hour = c.get(Calendar.HOUR_OF_DAY);

    final int minute = c.get(Calendar.MINUTE);

    int year = Calendar.getInstance().get(Calendar.YEAR);


    ProgressDialog progress, downloadprogress;
    EditText editTextDeliveryDuration, editTextWorkingFrom, editTextWorkingTo,
            editTextDeliveryRange, editTextLocation, editTextStoreName;
    Button buttonNext;
    ImageView imageViewUploadButton;
    ImageView networkImageViewStorePhoto;
    Uri outPutfileUri;
    String mDeliveryDuration, mWorkingHourFromTime, mWorkingHourToTime, mDeliveryRange, mStoreName;
    String postlatitude;
    String postlongitude;
    String postaddress;
    String postcity;
    String poststate;
    String postcountry;
    String postpinCode;
    int mTimeCheck;
    private AlertDialog alert;
    GPSTracker gpsTracker;
    private int PICK_IMAGE_REQUEST= 1;
    Bitmap decodeimage;
    String uri;
    Bitmap bitmap;
    Cursor cursor;
    String path;
    String filepathshort;
    EditText editTextGoogleLocation, editTextDeliveryCharge, editTextDeliveryUpto;
    Button buttonFetchLocation;
    int mCheckCamaraOrLocation;
    String mDeliveryAmount, mFreeDelivery;
    public SetUpFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_set_up, container, false);
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        editTextStoreName = getActivity().findViewById(R.id._store_name_enter);
        editTextDeliveryDuration = getActivity().findViewById(R.id.delivery_duration);
        editTextWorkingFrom = getActivity().findViewById(R.id.working_from_input);
        editTextWorkingTo = getActivity().findViewById(R.id.working_to_input);
        editTextDeliveryRange = getActivity().findViewById(R.id.delivery_range);
        editTextLocation = getActivity().findViewById(R.id.your_location);
        buttonNext = getActivity().findViewById(R.id.btn_next);
        editTextDeliveryCharge = getActivity().findViewById(R.id._edit_delivery_charge);
        editTextDeliveryUpto = getActivity().findViewById(R.id._edit_free_delivery_above);
        networkImageViewStorePhoto = getActivity().findViewById(R.id._store_image);
        imageViewUploadButton = getActivity().findViewById(R.id._image_upload_button);
        imageViewUploadButton.setOnClickListener(this);
        editTextGoogleLocation = getActivity().findViewById(R.id._edit_text_gogole_location);
        buttonFetchLocation = getActivity().findViewById(R.id._button_get_location);
        buttonFetchLocation.setOnClickListener(this);
        gpsTracker = new GPSTracker(getActivity());
        editTextDeliveryDuration.setOnClickListener(this);
        editTextWorkingFrom.setOnClickListener(this);
        editTextWorkingTo.setOnClickListener(this);
        editTextDeliveryRange.setOnClickListener(this);
        buttonNext.setOnClickListener(this);

        if (AppUtils.internetConnectionAvailable(2000)) {
            getProfileImage();
        } else {
            Toast.makeText(getActivity(), "No Network Connection",
                    Toast.LENGTH_SHORT).show();
        }

        if (Build.VERSION.SDK_INT >= 24) {
            try {
                Method m = StrictMode.class.getMethod("disableDeathOnFileUriExposure");
                m.invoke(null);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        SharedPreferences preferences1 = PreferenceManager.getDefaultSharedPreferences(getActivity());
        SharedPreferences.Editor editor1 = preferences1.edit();
        editor1.putString("flag","0");
        editor1.apply();
        super.onActivityCreated(savedInstanceState);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_next:
                String mWorkFrom = editTextWorkingFrom.getText().toString().trim();
                String mWrokTo = editTextWorkingTo.getText().toString().trim();
                String mDr = editTextDeliveryDuration.getText().toString().trim();
                String mDlRg = editTextDeliveryRange.getText().toString().trim();
                try {
                    SimpleDateFormat FFFFF = new SimpleDateFormat("HH:mm");
                    Date date1 = FFFFF.parse(mWorkFrom);
                    Date date2 = FFFFF.parse(mWrokTo);
                    System.out.println("From Time : " + FFFFF.format(date1));
                    System.out.println("To Time : " + FFFFF.format(date2));

                    if (date1.compareTo(date2) > 0) {
                        mTimeCheck = 0;
                        System.out.println("Time From is after Time To");

                    } else if (date1.compareTo(date2) < 0) {
                        mTimeCheck = 1;
                        System.out.println("Time From is before Time To");

                    } else if (date1.compareTo(date2) == 0) {
                        mTimeCheck = 0;
                        System.out.println("Time From is equal to Time to");

                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }

                if (editTextStoreName.getText().length() == 0) {
                    Toast.makeText(getActivity(), "Enter Store Name",
                            Toast.LENGTH_SHORT).show();
                } else if (editTextDeliveryDuration.getText().length() == 0) {
                    Toast.makeText(getActivity(), "Enter Delivery Duration",
                            Toast.LENGTH_SHORT).show();
                } else if (mDr.equalsIgnoreCase("00:00") || (mDr.equalsIgnoreCase("0:0"))) {
                    Toast.makeText(getActivity(), "Enter Correct Time Required for Delivery",
                            Toast.LENGTH_SHORT).show();
                } else if (editTextWorkingFrom.getText().length() == 0) {
                    Toast.makeText(getActivity(), "Enter Working From",
                            Toast.LENGTH_SHORT).show();
                } else if (mWorkFrom.equalsIgnoreCase("00:00") || (mWorkFrom.equalsIgnoreCase("0:0"))) {
                    Toast.makeText(getActivity(), "Enter Correct From Time",
                            Toast.LENGTH_SHORT).show();
                } else if (editTextWorkingTo.getText().length() == 0) {
                    Toast.makeText(getActivity(), "Enter Working To",
                            Toast.LENGTH_SHORT).show();
                } else if (mWrokTo.equalsIgnoreCase("00:00") || (mWrokTo.equalsIgnoreCase("0:0"))) {
                    Toast.makeText(getActivity(), "Enter Correct To Time",
                            Toast.LENGTH_SHORT).show();
                } else if (editTextDeliveryRange.getText().length() == 0) {
                    Toast.makeText(getActivity(), "Enter Delivery Range",
                            Toast.LENGTH_SHORT).show();
                } else if (mDlRg.equalsIgnoreCase("0")) {
                    Toast.makeText(getActivity(), "Enter Correct Delivery Distance",
                            Toast.LENGTH_SHORT).show();
                } else if (editTextLocation.getText().length() == 0) {
                    Toast.makeText(getActivity(), "Enter Address",
                            Toast.LENGTH_SHORT).show();
                } else if (editTextGoogleLocation.getText().length() == 0) {
                    Toast.makeText(getActivity(), "Fetch Store Location",
                            Toast.LENGTH_SHORT).show();
                } else if (editTextDeliveryCharge.getText().length() == 0) {
                    Toast.makeText(getActivity(), "Enter Delivery Charge",
                            Toast.LENGTH_SHORT).show();
                } else if (editTextDeliveryUpto.getText().length() == 0) {
                    Toast.makeText(getActivity(), "Enter Free Delivery Upto",
                            Toast.LENGTH_SHORT).show();


                } else {
                    SetUpFragment();
                    SavingInPreference(); // Saving to Post in Next Fragment
                }
                break;
            case R.id.delivery_duration:
               // deliveryduration();
                showYearDialog(R.id.delivery_duration);
                break;
            case R.id.working_to_input:
                showYearDialog(R.id.working_to_input);
                break;
            case R.id.working_from_input:
              //  WorkingHourFromTime();
                showYearDialog(R.id.working_from_input);
                break;
            case R.id.delivery_range:
                mDeliveryRange = editTextDeliveryRange.getText().toString().trim();
                break;
            case R.id.your_location:


                break;
            case R.id._image_upload_button:
                mCheckCamaraOrLocation = 1;
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.putExtra(MediaStore.EXTRA_OUTPUT, outPutfileUri);
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
                break;

            case R.id._button_get_location:
                mCheckCamaraOrLocation = 0;
                if (AppUtils.internetConnectionAvailable(2000)) {
                    Intent i = new Intent(getActivity(), PlacesActivity.class);
                    startActivityForResult(i, 50);
                } else {
                    Toast.makeText(getActivity(), "No Network Connection",
                            Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    public void SavingInPreference() {
        mStoreName = editTextStoreName.getText().toString().trim();
        mDeliveryDuration = editTextDeliveryDuration.getText().toString().trim();
        mDeliveryRange = editTextDeliveryRange.getText().toString().trim();
        mWorkingHourFromTime = editTextWorkingFrom.getText().toString().trim();
        mWorkingHourToTime = editTextWorkingTo.getText().toString().trim();
        postaddress = editTextLocation.getText().toString().trim();
        mDeliveryAmount = editTextDeliveryCharge.getText().toString().trim();
        mFreeDelivery = editTextDeliveryUpto.getText().toString().trim();
        if (Preference.getDefaults(PreferenceConstants.POST_LATTITUDE, getActivity()) != null) {
            Preference.deletePrefs(PreferenceConstants.POST_STORE_NAME, getActivity());
            Preference.deletePrefs(PreferenceConstants.POST_WORKING_HOUR_FROM_TIME, getActivity());
            Preference.deletePrefs(PreferenceConstants.POST_WORKING_HOUR_TO_TIME, getActivity());
            Preference.deletePrefs(PreferenceConstants.POST_DELIVERY_DURATION, getActivity());
            Preference.deletePrefs(PreferenceConstants.POST_DELIVERY_RANGE, getActivity());
            Preference.deletePrefs(PreferenceConstants.POST_ADDRESS, getActivity());
            Preference.deletePrefs(PreferenceConstants.POST_CITY, getActivity());
            Preference.deletePrefs(PreferenceConstants.POST_COUNTRY, getActivity());
            Preference.deletePrefs(PreferenceConstants.POST_STATE, getActivity());
            Preference.deletePrefs(PreferenceConstants.POST_PINCODE, getActivity());
            Preference.deletePrefs(PreferenceConstants.POST_LATTITUDE, getActivity());
            Preference.deletePrefs(PreferenceConstants.POST_LONGITUDE, getActivity());
            Preference.deletePrefs(PreferenceConstants.POST_DELIVERY_AMOUNT, getActivity());
            Preference.deletePrefs(PreferenceConstants.POST_FREE_DELIVERY, getActivity());
        }
        Preference.setDefaults(PreferenceConstants.POST_STORE_NAME, mStoreName, getActivity());
        Preference.setDefaults(PreferenceConstants.POST_WORKING_HOUR_FROM_TIME, mWorkingHourFromTime, getActivity());
        Preference.setDefaults(PreferenceConstants.POST_WORKING_HOUR_TO_TIME, mWorkingHourToTime, getActivity());
        Preference.setDefaults(PreferenceConstants.POST_DELIVERY_DURATION, mDeliveryDuration, getActivity());
        Preference.setDefaults(PreferenceConstants.POST_DELIVERY_RANGE, mDeliveryRange, getActivity());
        Preference.setDefaults(PreferenceConstants.POST_ADDRESS, postaddress, getActivity());
        Preference.setDefaults(PreferenceConstants.POST_CITY, postcity, getActivity());
        Preference.setDefaults(PreferenceConstants.POST_COUNTRY, postcountry, getActivity());
        Preference.setDefaults(PreferenceConstants.POST_STATE, poststate, getActivity());
        Preference.setDefaults(PreferenceConstants.POST_PINCODE, postpinCode, getActivity());
        Preference.setDefaults(PreferenceConstants.POST_LATTITUDE, postlatitude, getActivity());
        Preference.setDefaults(PreferenceConstants.POST_LONGITUDE, postlongitude, getActivity());
        Preference.setDefaults(PreferenceConstants.POST_DELIVERY_AMOUNT, mDeliveryAmount, getActivity());
        Preference.setDefaults(PreferenceConstants.POST_FREE_DELIVERY, mFreeDelivery, getActivity());
    }

    public void deliveryduration() {


        calendar = Calendar.getInstance();
        CalendarHour = calendar.get(Calendar.HOUR_OF_DAY);
        CalendarMinute = calendar.get(Calendar.MINUTE);


        timepickerdialog = new TimePickerDialog(getActivity(),
                new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {

                        if (hourOfDay == 0) {

                            hourOfDay += 12;

                            format = "AM";
                        }
                        else if (hourOfDay == 12) {

                            format = "PM";

                        }
                        else if (hourOfDay > 12) {

                            hourOfDay -= 12;

                            format = "PM";

                        }
                        else {

                            format = "AM";
                        }


                        if(format=="PM" && hourOfDay==1){
                            hourOfDay=13;
                        }else
                        if(format=="PM" && hourOfDay==2){
                            hourOfDay=14;
                        }
                        else
                        if(format=="PM" && hourOfDay==3){
                            hourOfDay=15;
                        }
                        else
                        if(format=="PM" && hourOfDay==4){
                            hourOfDay=16;
                        }
                        else
                        if(format=="PM" && hourOfDay==5){
                            hourOfDay=17;
                        }
                        else
                        if(format=="PM" && hourOfDay==6){
                            hourOfDay=18;
                        }
                        else
                        if(format=="PM" && hourOfDay==7){
                            hourOfDay=19;
                        }
                        else
                        if(format=="PM" && hourOfDay==8){
                            hourOfDay=20;
                        }
                        else
                        if(format=="PM" && hourOfDay==9){
                            hourOfDay=21;
                        }
                        else
                        if(format=="PM" && hourOfDay==10){
                            hourOfDay=22;
                        }
                        else
                        if(format=="PM" && hourOfDay==11){
                            hourOfDay=23;
                        }
                        else
                        if(format=="PM" && hourOfDay==12){
                            hourOfDay=24;
                        }
                        editTextDeliveryDuration.setText(hourOfDay + ":" + minute);
                        mDeliveryDuration = String.format("%02d:%02d", hourOfDay, minute);
                    }
                }, CalendarHour, CalendarMinute, false);
        timepickerdialog.show();

//        Toast.makeText(getActivity(), "TimePicker", Toast.LENGTH_SHORT).show();

    }



//        Calendar mcurrentTimeb = Calendar.getInstance();
//        int hourb = mcurrentTimeb.get(Calendar.HOUR_OF_DAY);
//        int minuteb = mcurrentTimeb.get(Calendar.MINUTE);
//        TimePickerDialog mTimePickerb;
//        mTimePickerb = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
//            @Override
//            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
//                editTextDeliveryDuration.setText(String.format("%02d:%02d", selectedHour, selectedMinute));
//                mDeliveryDuration = String.format("%02d:%02d", selectedHour, selectedMinute);
//            }
//        }, hourb, minuteb, true);//Yes 24 hour time
//        mTimePickerb.show();
    // }

    public void WorkingHourFromTime() {
//        Calendar mcurrentTimeb = Calendar.getInstance();
//        int hourb = mcurrentTimeb.get(Calendar.HOUR_OF_DAY);
//        int minuteb = mcurrentTimeb.get(Calendar.MINUTE);
//        TimePickerDialog mTimePickerb;
//        mTimePickerb = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
//            @Override
//            public void onTimeSet(TimePicker timePicker, int selectedHourFromTime, int selectedMinuteFromTime) {
//                editTextWorkingFrom.setText(String.format("%02d:%02d", selectedHourFromTime, selectedMinuteFromTime));
//                mWorkingHourFromTime = String.format("%02d:%02d", selectedHourFromTime, selectedMinuteFromTime);
//            }
//        }, hourb, minuteb, true);//Yes 24 hour time
//        mTimePickerb.show();


        calendar = Calendar.getInstance();
        CalendarHour = calendar.get(Calendar.HOUR_OF_DAY);
        CalendarMinute = calendar.get(Calendar.MINUTE);


        timepickerdialog = new TimePickerDialog(getActivity(),
                new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {

                        if (hourOfDay == 0) {

                            hourOfDay += 12;

                            format = "AM";
                        }
                        else if (hourOfDay == 12) {

                            format = "PM";

                        }
                        else if (hourOfDay > 12) {

                            hourOfDay -= 12;

                            format = "PM";

                        }
                        else {

                            format = "AM";
                        }


                        if(format=="PM" && hourOfDay==1){
                            hourOfDay=13;
                        }else
                        if(format=="PM" && hourOfDay==2){
                            hourOfDay=14;
                        }
                        else
                        if(format=="PM" && hourOfDay==3){
                            hourOfDay=15;
                        }
                        else
                        if(format=="PM" && hourOfDay==4){
                            hourOfDay=16;
                        }
                        else
                        if(format=="PM" && hourOfDay==5){
                            hourOfDay=17;
                        }
                        else
                        if(format=="PM" && hourOfDay==6){
                            hourOfDay=18;
                        }
                        else
                        if(format=="PM" && hourOfDay==7){
                            hourOfDay=19;
                        }
                        else
                        if(format=="PM" && hourOfDay==8){
                            hourOfDay=20;
                        }
                        else
                        if(format=="PM" && hourOfDay==9){
                            hourOfDay=21;
                        }
                        else
                        if(format=="PM" && hourOfDay==10){
                            hourOfDay=22;
                        }
                        else
                        if(format=="PM" && hourOfDay==11){
                            hourOfDay=23;
                        }
                        else
                        if(format=="PM" && hourOfDay==12){
                            hourOfDay=24;
                        }
                        editTextWorkingFrom.setText(hourOfDay + ":" + minute);
                        mWorkingHourFromTime = String.format("%02d:%02d", hourOfDay, minute);
                    }
                }, CalendarHour, CalendarMinute, false);
        timepickerdialog.show();

    }





    public void showYearDialog(int str)

    {


        final int string=str;


        final Dialog d = new Dialog(getActivity());

        d.setTitle("Year Picker");

        d.setContentView(R.layout.timedialog);

        Button set = (Button) d.findViewById(R.id.button1);

        Button cancel = (Button) d.findViewById(R.id.button2);

//        TextView year_text=(TextView)d.findViewById(R.id.title_text);
//
//        title_text.setText(hour+":"+minute);

        final NumberPicker nopicker = (NumberPicker) d.findViewById(R.id.numberPicker1);

        final NumberPicker nopicker1 = (NumberPicker) d.findViewById(R.id.numberPicker2);
        final NumberPicker nopicker2 = (NumberPicker) d.findViewById(R.id.numberPicker3);

        nopicker.setMaxValue(12);

        nopicker.setMinValue(0);

        nopicker.setWrapSelectorWheel(false);

        nopicker.setValue(hour);

        nopicker.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);





        nopicker1.setMaxValue(60);

        nopicker1.setMinValue(minute);

        nopicker1.setWrapSelectorWheel(false);

        nopicker1.setValue(minute);

        nopicker1.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);




        final String[] values = {"AM", "PM"};
        nopicker2.setMinValue(0);
        nopicker2.setMaxValue(values.length - 1);
        nopicker2.setDisplayedValues(values);
        nopicker2.setWrapSelectorWheel(true);



        if(string==R.id.delivery_duration){

            nopicker2.setVisibility(View.GONE);
        }
        else
            nopicker2.setVisibility(View.VISIBLE);


        nopicker2.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {



            }
        });

        nopicker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {

            @Override

            public void onValueChange(NumberPicker picker, int oldVal, int newVal){

                //Display the newly selected number from picker

                //  tv.setText("Selected Number : " + newVal);

                if(nopicker.getValue()==hour)

                {

                    nopicker1.setMinValue(minute);

                }else {

                    nopicker1.setMinValue(0);

                }

            }

        });



        set.setOnClickListener(new View.OnClickListener()

        {

            @Override            public void onClick(View v) {


                if(nopicker2.getValue()==0){
                    timeformat="AM";
                }
                else
                    timeformat="PM";


                if(string==R.id.working_from_input)
                editTextWorkingFrom.setText(String.valueOf(nopicker.getValue()+":"+nopicker1.getValue())+ " "+timeformat);
                else if(string==R.id.working_to_input)
                    editTextWorkingTo.setText(String.valueOf(nopicker.getValue()+":"+nopicker1.getValue())+ " "+timeformat);
                else
                    editTextDeliveryDuration.setText(String.valueOf(nopicker.getValue()+":"+nopicker1.getValue()));
                d.dismiss();

            }

        });

        cancel.setOnClickListener(new View.OnClickListener()

        {

            @Override            public void onClick(View v) {

                d.dismiss();

            }

        });

        d.show();

    }


    public void WorkingHourToTime() {
//        Calendar mcurrentTimeb = Calendar.getInstance();
//        int hourb = mcurrentTimeb.get(Calendar.HOUR_OF_DAY);
//        int minuteb = mcurrentTimeb.get(Calendar.MINUTE);
//        TimePickerDialog mTimePickerb;
//        mTimePickerb = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
//            @Override
//            public void onTimeSet(TimePicker timePicker, int selectedHourToTime, int selectedMinuteToTime) {
//
//                editTextWorkingTo.setText(String.valueOf(selectedHourToTime) + ":" + String.valueOf(selectedMinuteToTime));
//                mWorkingHourToTime = String.format("%02d:%02d", selectedHourToTime, selectedMinuteToTime);
//            }
//        }, hourb, minuteb, true);//Yes 24 hour time
//        mTimePickerb.show();


        calendar = Calendar.getInstance();
        CalendarHour = calendar.get(Calendar.HOUR_OF_DAY);
        CalendarMinute = calendar.get(Calendar.MINUTE);


        timepickerdialog = new TimePickerDialog(getActivity(),
                new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {

                        if (hourOfDay == 0) {

                            hourOfDay += 12;

                            format = "AM";
                        }
                        else if (hourOfDay == 12) {

                            format = "PM";

                        }
                        else if (hourOfDay > 12) {

                            hourOfDay -= 12;

                            format = "PM";

                        }
                        else {

                            format = "AM";
                        }


                        if(format=="PM" && hourOfDay==1){
                            hourOfDay=13;
                        }else
                        if(format=="PM" && hourOfDay==2){
                            hourOfDay=14;
                        }
                        else
                        if(format=="PM" && hourOfDay==3){
                            hourOfDay=15;
                        }
                        else
                        if(format=="PM" && hourOfDay==4){
                            hourOfDay=16;
                        }
                        else
                        if(format=="PM" && hourOfDay==5){
                            hourOfDay=17;
                        }
                        else
                        if(format=="PM" && hourOfDay==6){
                            hourOfDay=18;
                        }
                        else
                        if(format=="PM" && hourOfDay==7){
                            hourOfDay=19;
                        }
                        else
                        if(format=="PM" && hourOfDay==8){
                            hourOfDay=20;
                        }
                        else
                        if(format=="PM" && hourOfDay==9){
                            hourOfDay=21;
                        }
                        else
                        if(format=="PM" && hourOfDay==10){
                            hourOfDay=22;
                        }
                        else
                        if(format=="PM" && hourOfDay==11){
                            hourOfDay=23;
                        }
                        else
                        if(format=="PM" && hourOfDay==12){
                            hourOfDay=24;
                        }
                        editTextWorkingTo.setText(hourOfDay + ":" + minute );
                        mWorkingHourToTime = String.format("%02d:%02d", hourOfDay, minute);
                    }
                }, CalendarHour, CalendarMinute, false);
        timepickerdialog.show();

    }


    public void SetUpFragment() {
        Fragment fragment = new SelectedItems();
        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        ft.addToBackStack("SetUpFragment");
        ft.replace(R.id.content_frame, fragment);
        ft.commit();
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (mCheckCamaraOrLocation == 0) {
            if (resultCode == 50) {
                Double mLatitude = Double.valueOf(data.getStringExtra("latitude"));
                Double mLongitude = Double.valueOf(data.getStringExtra("longitude"));
                postlatitude = String.valueOf(mLatitude);
                postlongitude = String.valueOf(mLongitude);
                editTextGoogleLocation.setText("Location Received");
            }
            if (resultCode == Activity.RESULT_CANCELED) {
            }
        }

        if (mCheckCamaraOrLocation == 1) {
            if (resultCode == Activity.RESULT_OK) {
                if (requestCode == PICK_IMAGE_REQUEST) {
                    onSelectFromGalleryResult(data);
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Toast.makeText(getActivity(), "Photo Cancelled.",
                        Toast.LENGTH_SHORT).show();
            }
        }
    }


    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {
        try {
            if (data != null) {
                String ddd = getImagePath(data.getData());
                outPutfileUri = Uri.parse(ddd);
                try {
                    Bitmap convbit = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), data.getData());
                    String imagebit = getStringImage(convbit);
                    bitmap = StringToBitMap(imagebit);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        networkImageViewStorePhoto.setImageBitmap(bitmap);
        if (AppUtils.internetConnectionAvailable(2000)) {
            profileImageUpload();
        } else {
            Toast.makeText(getActivity(), "No Network Connection",
                    Toast.LENGTH_SHORT).show();
        }
    }



    public void getProfileImage() {
        downloadprogress = ProgressDialog.show(getActivity(), "", "Please wait...", false, false);
        String mId = Preference.getDefaults(PreferenceConstants.MERCHANT_ID, getActivity());
        String mMerchantId = "http://18.222.124.117:9091/clickshop/getProfileImage?merchantId=" + mId;

//        String mMerchantId = "http://18.222.124.117:8080/clickshop/getProfileImage?merchantId=" + mId  ;
        StringRequest stringRequest = new StringRequest(Request.Method.GET, mMerchantId,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d(TAG, "Photoresponse: " + response);
                        downloadprogress.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            int mStatusCode = jsonObject.getInt(WebServiceConstants.EXISTING_RESPONSE_CODE);
                            if (mStatusCode == 200) {
                                decodeimage = decodeBase64(jsonObject.getString(WebServiceConstants.PROFILE_IMAGE));
                                networkImageViewStorePhoto.setImageBitmap(decodeimage);
                            }else if (mStatusCode == 400){
                                Toast.makeText(getActivity(), "Store Photo Not Available",
                                        Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        downloadprogress.dismiss();
                        NetworkUtils.UnableToReachServer(getActivity());
                    }
                });
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(stringRequest);
    }

    public static Bitmap decodeBase64(String input) {
        byte[] decodedBytes = Base64.decode(input, 0);
        return BitmapFactory.decodeByteArray(decodedBytes, 0, decodedBytes.length);
    }


    private void profileImageUpload() {
        progress = ProgressDialog.show(getActivity(), "", "Please wait...", false, false);
        VolleyMultipartRequest multipartRequest = new VolleyMultipartRequest(Request.Method.POST,
                EndPoint.PROFILE_IMAGE_UPLOAD, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                try {
                    String jsonresp = new String(response.data, HttpHeaderParser.parseCharset(response.headers));

                    Log.d(TAG, "jsonresp  " + jsonresp);
                    try {
                        JSONObject jsonObject = new JSONObject(jsonresp);
                        String mMessage = jsonObject.getString("responseCode");
                        if (mMessage.equalsIgnoreCase("200")) {

                            Toast.makeText(getActivity(), "Store Photo Uploaded Successfully",
                                    Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(getActivity(), "Store Photo Upload Failed",
                                    Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) { e.printStackTrace();
                    }
                } catch (UnsupportedEncodingException e) { e.printStackTrace();
                }
                progress.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progress.dismiss();

            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                SessionManager sessionManager = new SessionManager(getActivity());
                HashMap<String, String> hashMap = sessionManager.getUserDetails();
                String sessionId = hashMap.get(SessionManager.KEY_SESSION_ID);
                params.put("merchantId", Preference.getDefaults(PreferenceConstants.MERCHANT_ID, getActivity()));
//                params.put("sessionId", sessionId);

                Log.d(TAG, "params image upload  " + params);
                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                String file_path = (outPutfileUri.toString()).substring(outPutfileUri.toString().lastIndexOf("/"));
                filepathshort = removeFirstOne(file_path);
                String compressfile = compressImage(outPutfileUri.toString());
                Bitmap bitmaps = BitmapFactory.decodeFile(compressfile);
                Bitmap bitmaparray = Bitmap.createScaledBitmap(bitmaps, 350, 200, false);
                ByteArrayOutputStream bosarray = new ByteArrayOutputStream();
                bitmaparray.compress(Bitmap.CompressFormat.JPEG, 80, bosarray);
                byte[] imageBytesarray = bosarray.toByteArray();
                params.put("file", new DataPart(filepathshort, imageBytesarray, "image/jpeg"));
                Log.d(TAG, "Data Params   " + params);
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(multipartRequest);
    }

    public String removeFirstOne(String input) {
        return input.substring(1);
    }

    public String getImagePath(Uri uri) {
        try {
            cursor = getActivity().getContentResolver().query(uri, null, null, null, null);
            cursor.moveToFirst();
            String document_id = cursor.getString(0);
            document_id = document_id.substring(document_id.lastIndexOf(":") + 1);
            cursor.close();
            cursor = getActivity().getContentResolver().query(
                    android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                    null, MediaStore.Images.Media._ID + " = ? ", new String[]{document_id}, null);
            cursor.moveToFirst();
            path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
            cursor.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (CursorIndexOutOfBoundsException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        return path;
    }

    public Bitmap StringToBitMap(String encodedString) {
        try {
            byte[] encodeByte = Base64.decode(encodedString, Base64.DEFAULT);
            Bitmap bitmap = BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
            return bitmap;
        } catch (Exception e) {
            e.getMessage();
            return null;
        }
    }

    public String getStringImage(Bitmap bmp) {
        int newWidth = 800;
        int newHeight = 600;

        Bitmap resized = Bitmap.createScaledBitmap(bmp, newWidth, newHeight, true);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        resized.compress(Bitmap.CompressFormat.JPEG, 90, baos);
        byte[] imageBytes = baos.toByteArray();
        String input = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return input;
    }


    // Whatsapp like image compression
    public String compressImage(String imageUri) {
        String filePath = getRealPathFromURI(imageUri);
        Bitmap scaledBitmap = null;
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        Bitmap bmp = BitmapFactory.decodeFile(filePath, options);
        int actualHeight = options.outHeight;
        int actualWidth = options.outWidth;
        float maxHeight = 816.0f;
        float maxWidth = 812.0f;
        float imgRatio = actualWidth / actualHeight;
        float maxRatio = maxWidth / maxHeight;
        if (actualHeight > maxHeight || actualWidth > maxWidth) {
            if (imgRatio < maxRatio) {
                imgRatio = maxHeight / actualHeight;
                actualWidth = (int) (imgRatio * actualWidth);
                actualHeight = (int) maxHeight;
            } else if (imgRatio > maxRatio) {
                imgRatio = maxWidth / actualWidth;
                actualHeight = (int) (imgRatio * actualHeight);
                actualWidth = (int) maxWidth;
            } else {
                actualHeight = (int) maxHeight;
                actualWidth = (int) maxWidth;

            }
        }
        options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight);
        options.inJustDecodeBounds = false;
        options.inPurgeable = true;
        options.inInputShareable = true;
        options.inTempStorage = new byte[16 * 1024];

        try {
            bmp = BitmapFactory.decodeFile(filePath, options);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();
        }
        try {
            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();
        }

        float ratioX = actualWidth / (float) options.outWidth;
        float ratioY = actualHeight / (float) options.outHeight;
        float middleX = actualWidth / 2.0f;
        float middleY = actualHeight / 2.0f;
        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);
        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2, middleY - bmp.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));
        try {
            ExifInterface ei = new ExifInterface(filePath);
            int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_UNDEFINED);
            bitmap = null;
            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_90:
                    bitmap = rotateImage(bitmap, 90);
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    bitmap = rotateImage(bitmap, 180);
                    break;
                case ExifInterface.ORIENTATION_ROTATE_270:
                    bitmap = rotateImage(bitmap, 270);
                    break;
                case ExifInterface.ORIENTATION_NORMAL:
                default:
                    bitmap = bitmap;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        FileOutputStream out = null;
        String filename = getFilename();
        try {
            out = new FileOutputStream(filename);
            scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return filename;
    }


    private static Bitmap rotateImage(Bitmap img, int degree) {
        Matrix matrix = new Matrix();
        matrix.postRotate(degree);
        Bitmap rotatedImg = Bitmap.createBitmap(img, 0, 0, img.getWidth(), img.getHeight(), matrix, true);
        img.recycle();
        return rotatedImg;
    }

    public String getFilename() {
        File file = new File(Environment.getExternalStorageDirectory().getPath(), "MyFolder/Images");
        if (!file.exists()) {
            file.mkdirs();
        }
        String uriSting = (file.getAbsolutePath() + "/" + System.currentTimeMillis() + ".jpg");
        return uriSting;
    }

    private String getRealPathFromURI(String contentURI) {
        Uri contentUri = Uri.parse(contentURI);
        Cursor cursor = getActivity().getContentResolver().query(contentUri, null, null, null, null);
        if (cursor == null) {
            return contentUri.getPath();
        } else {
            cursor.moveToFirst();
            int index = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(index);
        }
    }

    public int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }
        final float totalPixels = width * height;
        final float totalReqPixelsCap = reqWidth * reqHeight * 2;
        while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
            inSampleSize++;
        }

        return inSampleSize;
    }


}


