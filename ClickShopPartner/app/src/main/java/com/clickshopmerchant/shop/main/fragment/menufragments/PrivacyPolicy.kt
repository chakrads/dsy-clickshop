package com.clickshopmerchant.shop.main.fragment.menufragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.clickshopmerchant.shop.R
import java.io.ByteArrayOutputStream
import java.io.IOException

class PrivacyPolicy : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?  {
        return inflater.inflate(R.layout.fragment_privacy_policy, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?)  {
        super.onViewCreated(view, savedInstanceState)
        activity?.title = "Privacy Policy"
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {


        val myTextView = activity?.findViewById(R.id.textView) as TextView

        val inputStream = resources.openRawResource(R.raw.privacypolicy)
        val byteArrayOutputStream = ByteArrayOutputStream()

        var myText = ""
        var `in`: Int
        try {
            `in` = inputStream.read()
            while (`in` != -1) {
                byteArrayOutputStream.write(`in`)
                `in` = inputStream.read()
            }
            inputStream.close()

            myText = byteArrayOutputStream.toString()
        } catch (e: IOException) {
            e.printStackTrace()
        }


        myTextView.text = myText







        super.onActivityCreated(savedInstanceState)
    }


}
