package com.clickshopmerchant.shop.prelanding.setupFragments;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.clickshopmerchant.shop.R;


public class AddYourItemFragment extends Fragment {

    public AddYourItemFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_add_your_item, container, false);
    }



}
