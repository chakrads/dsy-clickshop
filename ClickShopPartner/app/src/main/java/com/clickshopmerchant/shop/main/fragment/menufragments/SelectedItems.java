package com.clickshopmerchant.shop.main.fragment.menufragments;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.clickshopmerchant.shop.R;
import com.clickshopmerchant.shop.appconfig.ApiClient;
import com.clickshopmerchant.shop.appconfig.ApiInterface;
import com.clickshopmerchant.shop.appconfig.AppUtils;
import com.clickshopmerchant.shop.appconfig.EndPoint;
import com.clickshopmerchant.shop.appconfig.PreferenceConstants;
import com.clickshopmerchant.shop.appconfig.SessionManager;
import com.clickshopmerchant.shop.appconfig.WebServiceConstants;
import com.clickshopmerchant.shop.fonts.CorbelTextView;
import com.clickshopmerchant.shop.main.activity.MainActivity;
import com.clickshopmerchant.shop.main.adapter.SelectedItemCardAdapter;
import com.clickshopmerchant.shop.main.model.selecteditemmodel.ItemList;
import com.clickshopmerchant.shop.main.model.selecteditemmodel.RequestBodySelectedItem;
import com.clickshopmerchant.shop.main.model.selecteditemmodel.SelectedItemM;
import com.clickshopmerchant.shop.realm.RealmController;
import com.clickshopmerchant.shop.realm.Store;
import com.clickshopmerchant.shop.storage.Preference;
import com.clickshopmerchant.shop.volley.AppController;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;


public class SelectedItems extends Fragment implements SwipeRefreshLayout.OnRefreshListener, SelectedItemRefresh {
    private static final String TAG = SelectedItems.class.getSimpleName();
    private EditText editTextSearch;
    private RelativeLayout relativeLayoutLoading;
    private SwipeRefreshLayout swipeRefreshLayout;
    private RecyclerView recyclerView;
    private SelectedItemCardAdapter adapter;
    private LinearLayoutManager linearLayoutManager;
    private String newquery, mSubString;
    private Button buttonSelectItem;
    private RelativeLayout relativeLayoutNOItemSeletecd;
    private CorbelTextView corbelTextView;
    private AlertDialog alert;
    private LinearLayout linearLayoutStepper;
    private RelativeLayout relativeLayoutbtnLone;
    private Button buttonDone;
    private ProgressDialog progress;
    public static int index = -1;
    public static int top = -1;
    public List<ItemList> datum;
    private Realm realm;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getActivity().setTitle("Store Items");
        setHasOptionsMenu(true);
        //get realm instance

        return inflater.inflate(R.layout.selected_items, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        this.realm = RealmController.with(AppController.getInstance()).getRealm();


        Views();
        servicecall();
        super.onActivityCreated(savedInstanceState);
    }

    public void Views() {
        // This is because it is used in two places
        linearLayoutStepper = getActivity().findViewById(R.id._set_up_stepper);
        relativeLayoutbtnLone = getActivity().findViewById(R.id._button_done_layout);
        buttonDone = getActivity().findViewById(R.id.btn_done);
        String mFlag = Preference.getDefaults(PreferenceConstants.LOGIN_FLAG, getActivity());
        if (mFlag.equalsIgnoreCase("1")) {
            linearLayoutStepper.setVisibility(View.GONE);
            relativeLayoutbtnLone.setVisibility(View.GONE);
        }
        editTextSearch = getActivity().findViewById(R.id._search);
        editTextSearch.setVisibility(View.GONE);
        editTextSearch.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES); // Keypad First Letter Caps
        relativeLayoutNOItemSeletecd = getActivity().findViewById(R.id._select_item);
        relativeLayoutNOItemSeletecd.setVisibility(View.GONE);
        relativeLayoutLoading = getActivity().findViewById(R.id._loading);
        relativeLayoutLoading.setVisibility(View.VISIBLE);
        swipeRefreshLayout = getActivity().findViewById(R.id.existing_items_refresh);
        swipeRefreshLayout.setVisibility(View.GONE);
        swipeRefreshLayout.setOnRefreshListener(this);
        recyclerView = getActivity().findViewById(R.id.existing_recyclerview_refresh);
        linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(linearLayoutManager);
        editTextSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence query, int start, int before, int count) {
                if (count >= 2) {
                    mSubString = String.valueOf(query);
                    newquery = mSubString.substring(0, 1).toUpperCase() + mSubString.substring(1);
                } else {
                    mSubString = String.valueOf(query);
                    newquery = query.toString().toUpperCase();
                }

                final List<ItemList> filteredList = new ArrayList<>();

                if (datum != null) {
                    for (int i = 0; i < datum.size(); i++) {
                        try {
                            final String text = datum.get(i).getItemName();
                            if (text.contains(newquery)) {
                                filteredList.add(datum.get(i));
                                updateRecyclerView(filteredList, false);
                            }
                        } catch (NullPointerException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });


        buttonSelectItem = getActivity().findViewById(R.id.btn_select);
        buttonSelectItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                existingFragment();
            }
        });
        buttonDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (AppUtils.internetConnectionAvailable(2000)) {
                    doneGoToLogin();
                } else {
                    Toast.makeText(getActivity(), "No Network Connection",
                            Toast.LENGTH_SHORT).show();
                }


            }
        });
    }


    public void callSelectedItemRefresh() {
        saving();
        servicecall();
    }


    @Override
    public void onPause() {
        super.onPause();
        //read current recyclerview position
        index = linearLayoutManager.findFirstVisibleItemPosition();
        View v = recyclerView.getChildAt(0);
        top = (v == null) ? 0 : (v.getTop() - recyclerView.getPaddingTop());
    }


    @Override
    public void onResume() {
        super.onResume();
        //set recyclerview position
        if (index != -1) {
            linearLayoutManager.scrollToPositionWithOffset(index, top);
        }
    }


    public void updateRecyclerView(List<ItemList> lst, boolean swipeStatus) {
        recyclerView = getActivity().findViewById(R.id.existing_recyclerview_refresh);
        recyclerView.setHasFixedSize(true);
        linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(linearLayoutManager);
        adapter = new SelectedItemCardAdapter(lst, getActivity(), SelectedItems.this);
        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        swipeRefreshLayout.setRefreshing(swipeStatus);
    }


    public void servicecall() {
        if (datum != null) {
            datum.clear();
        }
        if (AppUtils.internetConnectionAvailable(2000)) {
            getSelectedItems();
        } else {
            Toast.makeText(getActivity(), "No Network Connection",
                    Toast.LENGTH_SHORT).show();
        }

    }


    @Override
    public void onRefresh() {
        relativeLayoutLoading.setVisibility(View.GONE);
        if (datum != null) {
            datum.clear();
        }
        recyclerView.setVisibility(View.GONE);
        servicecall();
    }


    public void saving() {
        //read current recyclerview position
        index = linearLayoutManager.findFirstVisibleItemPosition();
        View v = recyclerView.getChildAt(0);
        top = (v == null) ? 0 : (v.getTop() - recyclerView.getPaddingTop());
    }

    public void retreiving() {
        if (index != -1) {
            linearLayoutManager.scrollToPositionWithOffset(index, top);
        }
    }


    public void getSelectedItems() {
        SessionManager sessionManager = new SessionManager(getActivity());
        final HashMap<String, String> hashMap = sessionManager.getUserDetails();
        String mSessionId = hashMap.get(SessionManager.KEY_SESSION_ID);
        String mMobile = hashMap.get(SessionManager.KEY_PHONE_NUMBER);

        final ApiInterface apiInterfacec = ApiClient.getApiClient().create(ApiInterface.class);
        String mMerchantId = Preference.getDefaults(PreferenceConstants.MERCHANT_ID, getActivity());
        Call<SelectedItemM> call = apiInterfacec.getItems(new RequestBodySelectedItem(mMobile, mMerchantId, mSessionId));
        call.enqueue(new Callback<SelectedItemM>() {
            @Override
            public void onResponse(Call<SelectedItemM> call, final retrofit2.Response<SelectedItemM> response) {
                if (response.isSuccessful()) {
                    if (response.body().getResponseCode() == 200) {

                        Log.e("TAG", "SelectedItem: " + new Gson().toJson(response.body()));

                        relativeLayoutLoading.setVisibility(View.GONE);
                        relativeLayoutNOItemSeletecd.setVisibility(View.GONE);
                        swipeRefreshLayout.setVisibility(View.VISIBLE);
                        recyclerView.setVisibility(View.VISIBLE);
                        editTextSearch.setVisibility(View.VISIBLE);
                        datum = response.body().getItemList();

                        try {
                            JSONObject jsonObject = new JSONObject(new Gson().toJson(response.body()));
                            JSONArray jsonArray = jsonObject.getJSONArray("itemList");
                            if (jsonArray != null) {
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    String mItemCatagory = jsonArray.getJSONObject(i).getString("itemCategory");
                                    String mItemId = jsonArray.getJSONObject(i).getString("itemId");
                                    String mItemImage = jsonArray.getJSONObject(i).getString("itemImage");
                                    String mItemName = jsonArray.getJSONObject(i).getString("itemName");
                                    String mItemPrice = jsonArray.getJSONObject(i).getString("itemPrice");
                                    String mItemWeight = jsonArray.getJSONObject(i).getString("itemWeight");
                                    String mItemWeightUnit = jsonArray.getJSONObject(i).getString("itemWeightUnit");



                                    ArrayList<Store> storeArrayList = new ArrayList<>();
                                    Store stores = new Store();
                                    stores.setmItemCatagory(mItemCatagory);
                                    stores.setmItemId(mItemId);
                                    stores.setmItemImage(mItemImage);
                                    stores.setmItemName(mItemName);
                                    stores.setmItemPrice(mItemPrice);
                                    stores.setmItemWeight(mItemWeight);
                                    stores.setmItemWeightUnit(mItemWeightUnit);
                                    storeArrayList.add(stores);
                                    RealmController.clearAll();

                                    for (Store b : storeArrayList) {
                                        realm.beginTransaction();
                                        realm.copyToRealm(b);
                                        realm.commitTransaction();
                                    }
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        adapter = new SelectedItemCardAdapter(datum, getActivity(), SelectedItems.this);
                        recyclerView.setAdapter(adapter);
                        swipeRefreshLayout.setRefreshing(false);
                        retreiving();
                    } else {
                        relativeLayoutLoading.setVisibility(View.GONE);
                        swipeRefreshLayout.setVisibility(View.GONE);
                        relativeLayoutNOItemSeletecd.setVisibility(View.VISIBLE);
                        swipeRefreshLayout.setRefreshing(false);
                        Toast.makeText(getActivity(), "No Data",
                                Toast.LENGTH_LONG).show();
                    }
                } else {
                    relativeLayoutLoading.setVisibility(View.GONE);
                    swipeRefreshLayout.setVisibility(View.GONE);
                    relativeLayoutNOItemSeletecd.setVisibility(View.VISIBLE);
                    swipeRefreshLayout.setRefreshing(false);
                    Toast.makeText(getActivity(), "No Data",
                            Toast.LENGTH_LONG).show();
                }
            }


            @Override
            public void onFailure(Call<SelectedItemM> call, Throwable t) {
                swipeRefreshLayout.setRefreshing(false);
                relativeLayoutLoading.setVisibility(View.GONE);
                Toast.makeText(getActivity(), t.toString(),
                        Toast.LENGTH_SHORT).show();
            }
        });


    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.selected_itme_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.action_add:
                existingFragment();
                break;


            default:
                break;
        }

        return true;
    }


    public void existingFragment() {
        Fragment fragment = new CategoryListing();
        FragmentManager fm = getActivity().getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.content_frame, fragment, "CategoryListing");
        ft.addToBackStack("SelectedItems");
        ft.commit();

    }


    public void UnableToReachServer() {
        LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
        View promptView = layoutInflater.inflate(R.layout.alert_unable_to_reach_server, null);
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setView(promptView);

        Button buttonok = promptView.findViewById(R.id.alert_ok_buton);
        buttonok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alert.dismiss();
            }
        });

        alert = alertDialogBuilder.create();
        alert.show();
    }


    // Used only in Initial Set UP - One Time
    public void doneGoToLogin() {
        progress = ProgressDialog.show(getActivity(), "", "Please wait...", false, false);

        String mPostStoreName = Preference.getDefaults(PreferenceConstants.POST_STORE_NAME, getActivity());
        String mPostWorkingFromTime = Preference.getDefaults(PreferenceConstants.POST_WORKING_HOUR_FROM_TIME, getActivity());
        String mPostWorkingToTime = Preference.getDefaults(PreferenceConstants.POST_WORKING_HOUR_TO_TIME, getActivity());
        String mPostDeliveryDuration = Preference.getDefaults(PreferenceConstants.POST_DELIVERY_DURATION, getActivity());
        String mPostDeliverRange = Preference.getDefaults(PreferenceConstants.POST_DELIVERY_RANGE, getActivity());
        String mPostAddress = Preference.getDefaults(PreferenceConstants.POST_ADDRESS, getActivity());
        String mPostCity = Preference.getDefaults(PreferenceConstants.POST_CITY, getActivity());
        String mPostCountry = Preference.getDefaults(PreferenceConstants.POST_COUNTRY, getActivity());
        String mPostState = Preference.getDefaults(PreferenceConstants.POST_STATE, getActivity());
        String mPinCode = Preference.getDefaults(PreferenceConstants.POST_PINCODE, getActivity());
        String mLatitude = Preference.getDefaults(PreferenceConstants.POST_LATTITUDE, getActivity());
        String mLongitude = Preference.getDefaults(PreferenceConstants.POST_LONGITUDE, getActivity());

        String mDeliveryAmount = Preference.getDefaults(PreferenceConstants.POST_DELIVERY_AMOUNT, getActivity());
        String mFreeDeliveryAbove = Preference.getDefaults(PreferenceConstants.POST_FREE_DELIVERY, getActivity());


        SessionManager sessionManager = new SessionManager(getActivity());
        HashMap<String, String> hashMap = sessionManager.getUserDetails();
        String mSessionId = hashMap.get(SessionManager.KEY_SESSION_ID);
        String mMobile = hashMap.get(SessionManager.KEY_PHONE_NUMBER);


        HashMap<String, String> params = new HashMap<String, String>();
        params.put("merchantId", Preference.getDefaults(PreferenceConstants.MERCHANT_ID, getActivity()));
        params.put("storeName", mPostStoreName);
        params.put("deliveryDuration", mPostDeliveryDuration);
        params.put("workingHourFrom", mPostWorkingFromTime);
        params.put("workingHourTo", mPostWorkingToTime);
        params.put("deliveryRange", mPostDeliverRange);
        params.put("latitude", mLatitude);
        params.put("longitude", mLongitude);
        params.put("address", mPostAddress);
        params.put("city", mPostCity);
        params.put("state", mPostState);
        params.put("country", mPostCountry);
        params.put("pinCode", mPinCode);
        params.put("flag", "1"); // FLAG FOR PRE - LOGIN
        params.put("sessionId", mSessionId);
        params.put("mobileNumber", mMobile);

        params.put("deliveryCharge", mDeliveryAmount);
        params.put("deliveryFreeAbove", mFreeDeliveryAbove);





        Log.d(TAG, "params: " + params);

        JsonObjectRequest request_json = new JsonObjectRequest(EndPoint.SAVE_INITIAL_SETUP_ADDRESS, new JSONObject(params),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        progress.dismiss();
                        Log.d(TAG, "Slot Response: " + response);
                        try {
                            int mStatusCode = response.getInt(WebServiceConstants.EXISTING_RESPONSE_CODE);
                            if (mStatusCode == 200) {

                                // LOGIN FLAG - FLAG USED TO DETECT NAVIGATION
                                String mFlag = response.getString(WebServiceConstants.FLAG);
                                Preference.setDefaults(PreferenceConstants.LOGIN_FLAG, mFlag, getActivity());

                                Intent intent = new Intent(getActivity(), MainActivity.class);
                                startActivity(intent);
                                getActivity().finish();
                            } else {
                                Message("Request Failed. Please Try Later");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progress.dismiss();

                UnableToReachServer();
            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(request_json);
    }


    public void Message(String message) {
        LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
        View promptView = layoutInflater.inflate(R.layout.alert_message, null);
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setView(promptView);
        TextView textViewMessage = promptView.findViewById(R.id.alert_message_display);
        textViewMessage.setText(message);
        Button buttonblock = promptView.findViewById(R.id.alert_ok_buton);
        buttonblock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alert.dismiss();
            }
        });
        alert = alertDialogBuilder.create();
        alert.show();
    }


}