package com.clickshopmerchant.shop.main.activity;
import android.content.Intent;
import android.os.Build;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.clickshopmerchant.shop.R;
import com.clickshopmerchant.shop.appconfig.AppUtils;
import com.clickshopmerchant.shop.appconfig.EndPoint;
import com.clickshopmerchant.shop.appconfig.PreferenceConstants;
import com.clickshopmerchant.shop.appconfig.SessionManager;
import com.clickshopmerchant.shop.fonts.CorbelTextView;
import com.clickshopmerchant.shop.main.adapter.OrderItemCardAdapter;
import com.clickshopmerchant.shop.main.model.OrderItems;
import com.clickshopmerchant.shop.network.NetworkUtils;
import com.clickshopmerchant.shop.storage.Preference;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class OrderItemActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {
    private static final String TAG = OrderItemActivity.class.getSimpleName();

    Toolbar toolbar;
    CorbelTextView corbelTextView;
    public RelativeLayout relativeLayoutLoading;
    public SwipeRefreshLayout swipeRefreshLayout;
    public RecyclerView recyclerView;
    public List<OrderItems> orderItems;
    public OrderItemCardAdapter adapter;
    public LinearLayoutManager linearLayoutManager;
    String mOrderId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_item);

        // Showing Status bar
        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }

        toolbar = findViewById(R.id.order_item_toolbar);
        setSupportActionBar(toolbar);
        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_keyboard_backspace_black_24dp));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        corbelTextView = findViewById(R.id.order_title);
        corbelTextView.setText("Order Items");
        relativeLayoutLoading = findViewById(R.id._loading_layout);
        swipeRefreshLayout = findViewById(R.id._order_item_swipe);
        swipeRefreshLayout.setOnRefreshListener(this);
        recyclerView = findViewById(R.id._order_item_recyclerview);
        linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        orderItems = new ArrayList<>();
        getInent();

        if (NetworkUtils.isInternetAvailable(true, getApplicationContext())) {
            gettingOrderItems();
        }
    }

    public void getInent() {
        Intent intent = getIntent();
        try {
            mOrderId = intent.getStringExtra("OrderItems");
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRefresh() {
        if (orderItems != null) {
            orderItems.clear();
        }

        if (AppUtils.internetConnectionAvailable(2000)) {
            gettingOrderItems();
        } else {
            Toast.makeText(getApplicationContext(), "No Network Connection",
                    Toast.LENGTH_SHORT).show();
        }

    }

    public void gettingOrderItems() {
        SessionManager sessionManager = new SessionManager(getApplicationContext());
        HashMap<String, String> hashMap = sessionManager.getUserDetails();
        String mSessionId = hashMap.get(SessionManager.KEY_SESSION_ID);
        String mMobile = hashMap.get(SessionManager.KEY_PHONE_NUMBER);


        HashMap<String, String> params = new HashMap<String, String>();
        params.put("merchantId", Preference.getDefaults(PreferenceConstants.MERCHANT_ID, getApplicationContext()));
        params.put("sessionId", mSessionId);
        params.put("orderId", mOrderId);
        params.put("mobileNumber", mMobile);

        Log.d(TAG, "OrderItemsParams: " + params);

        JsonObjectRequest request_json = new JsonObjectRequest(EndPoint.ORDER_ITEMS, new JSONObject(params),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        Log.d(TAG, "OrderId Response: " + response);
                        try {
                            int mStatusCode = response.getInt("responseCode");
                            if (mStatusCode == 200) {
                                relativeLayoutLoading.setVisibility(View.GONE);
                                recyclerView.setVisibility(View.VISIBLE);
                                JSONArray array = response.optJSONArray("orderDetailsList");
                                parseData(array);
                            } else {
                                relativeLayoutLoading.setVisibility(View.GONE);
                                swipeRefreshLayout.setRefreshing(false);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkUtils.UnableToReachServer(getApplicationContext());
            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(request_json);
    }


    private void parseData(JSONArray array) {
        for (int i = 0; i < array.length(); i++) {
            OrderItems orderItems1 = new OrderItems();
            JSONObject json;
            try {
                json = array.getJSONObject(i);
                if (json.getString("orderId") != null && !json.getString("orderId").isEmpty()
                        && !json.getString("orderId").equals("orderId")) {
                    orderItems1.setOrderId((json.getString("orderId")));
                } else {
                    orderItems1.setOrderId((""));
                }
                if (json.getString("itemId") != null && !json.getString("itemId").isEmpty()
                        && !json.getString("itemId").equals("itemId")) {
                    orderItems1.setItemId((json.getInt("itemId")));
                } else {
                    orderItems1.setItemId(0);
                }
                if (json.getString("itemName") != null && !json.getString("itemName").isEmpty()
                        && !json.getString("itemName").equals("itemName")) {
                    orderItems1.setItemName((json.getString("itemName")));
                } else {
                    orderItems1.setItemName((""));
                }
                if (json.getString("itemImage") != null && !json.getString("itemImage").isEmpty()
                        && !json.getString("itemImage").equals("itemImage")) {
                    orderItems1.setItemImage((json.getString("itemImage")));
                } else {
                    orderItems1.setItemImage((""));
                }
                if (json.getString("itemPrice") != null && !json.getString("itemPrice").isEmpty()
                        && !json.getString("itemPrice").equals("itemPrice")) {
                    orderItems1.setItemPrice((json.getString("itemPrice")));
                } else {
                    orderItems1.setItemPrice((""));
                }
                if (json.getString("itemQuantity") != null && !json.getString("itemQuantity").isEmpty()
                        && !json.getString("itemQuantity").equals("itemQuantity")) {
                    orderItems1.setItemQuantity((json.getString("itemQuantity")));
                } else {
                    orderItems1.setItemQuantity((""));
                }
                if (json.getString("totalPrice") != null && !json.getString("totalPrice").isEmpty()
                        && !json.getString("totalPrice").equals("totalPrice")) {
                    orderItems1.setTotalPrice((json.getString("totalPrice")));
                } else {
                    orderItems1.setTotalPrice((""));
                }
                if (json.getString("itemWeight") != null && !json.getString("itemWeight").isEmpty()
                        && !json.getString("itemWeight").equals("itemWeight")) {
                    orderItems1.setItemWeight((json.getString("itemWeight")));
                } else {
                    orderItems1.setItemWeight((""));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            orderItems.add(orderItems1);
            // Collections.sort(recordsList);
        }
        adapter = new OrderItemCardAdapter(orderItems, this);
        recyclerView.setAdapter(adapter);
        swipeRefreshLayout.setRefreshing(false);
        adapter.notifyDataSetChanged();
    }


}
