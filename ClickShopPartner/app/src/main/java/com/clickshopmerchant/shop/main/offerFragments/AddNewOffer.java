package com.clickshopmerchant.shop.main.offerFragments;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.Volley;
import com.clickshopmerchant.shop.R;
import com.clickshopmerchant.shop.appconfig.AppUtils;
import com.clickshopmerchant.shop.appconfig.EndPoint;
import com.clickshopmerchant.shop.appconfig.PreferenceConstants;
import com.clickshopmerchant.shop.appconfig.SessionManager;
import com.clickshopmerchant.shop.appconfig.WebServiceConstants;
import com.clickshopmerchant.shop.network.NetworkUtils;
import com.clickshopmerchant.shop.storage.Preference;
import com.clickshopmerchant.shop.volley.CustomVolleyRequest;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.HashMap;


public class AddNewOffer extends Fragment {
    private static final String TAG = AddNewOffer.class.getSimpleName();
    ProgressDialog progress;
    private AlertDialog alert;
    NetworkImageView imageView;
    private ImageLoader imageLoader;
    private TextView textView, textViewSelectedPrice;
    private EditText editTextPercentage;
    private TextView editTextDiscount, editTextPrice;
    private TextView textViewDiscountAmount;
    private Button buttonSave;
    private String mDiscountPercentage, mPriceAfterDisc;
    private String mItmePrice, mDiscountAmount;

    public AddNewOffer() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_add_new_offer, container, false);
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        imageView = getActivity().findViewById(R.id.selected_item);
        textView = getActivity().findViewById(R.id._selected_item_name);
        textViewSelectedPrice = getActivity().findViewById(R.id._selected_item_price);
        editTextDiscount = getActivity().findViewById(R.id._price_discount);
        editTextPercentage = getActivity().findViewById(R.id.percentage_input);
        editTextPrice = getActivity().findViewById(R.id._pricing);
        textViewDiscountAmount = getActivity().findViewById(R.id._discount_amount);
        mItmePrice = Preference.getDefaults(PreferenceConstants.ITEM_PRICE, getActivity());
        editTextPrice.setText(mItmePrice);
        editTextPercentage.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (editTextPrice.getText().length() == 0) {
                    Toast.makeText(getActivity(), "Enter Price", Toast.LENGTH_SHORT).show();
                } else if (editTextPercentage.getText().length() == 0) {
                    Toast.makeText(getActivity(), "Enter Percent", Toast.LENGTH_SHORT).show();

                    editTextDiscount.setText("");
                    textViewDiscountAmount.setText("");
                } else {
                    int mDiscount = Integer.valueOf(String.valueOf(s));
                    int mPrice = Integer.valueOf(editTextPrice.getText().toString().trim());
                    int mDiscAmount = mPrice * mDiscount / 100;
                    textViewDiscountAmount.setText(String.valueOf(mDiscAmount));
                    int mPriceAfterDiscount = mPrice - mDiscAmount;
                    editTextDiscount.setText(String.valueOf(mPriceAfterDiscount));
                    mDiscountAmount = String.valueOf(mDiscAmount);
                    mDiscountPercentage = String.valueOf(mDiscount);
                    mPriceAfterDisc = String.valueOf(mPriceAfterDiscount);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        buttonSave = getActivity().findViewById(R.id.btn_save);
        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editTextPrice.getText().length() == 0) {
                    Toast.makeText(getActivity(), "Enter Price", Toast.LENGTH_SHORT).show();
                } else if (editTextPercentage.getText().length() == 0) {
                    Toast.makeText(getActivity(), "Enter Percentage", Toast.LENGTH_SHORT).show();
                } else {
                    if (AppUtils.internetConnectionAvailable(2000)) {
                        saveOffer();
                    } else {
                        Toast.makeText(getActivity(), "No Network Connection",
                                Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
        String Itemaname = Preference.getDefaults(PreferenceConstants.ITEM_NAME, getActivity());
        String mItemWeight = Preference.getDefaults(PreferenceConstants.ITEM_WEIGHT, getActivity());
        String mItemImageUrl = Preference.getDefaults(PreferenceConstants.ITEM_IMAGE, getActivity());
        imageLoader = CustomVolleyRequest.getInstance(getActivity()).getImageLoader();
        imageLoader.get(mItemImageUrl, ImageLoader.getImageListener(imageView, R.drawable.noimage,
                android.R.drawable.ic_dialog_alert));
        imageView.setImageUrl(mItemImageUrl, imageLoader);
        textView.setText(Itemaname);
        textViewSelectedPrice.setText(new StringBuilder("Rs. ").append(mItmePrice).append(" - ").append(mItemWeight));
        super.onActivityCreated(savedInstanceState);
    }



    public void saveOffer() {
        progress = ProgressDialog.show(getActivity(), "", "Please wait...", false, false);
        SessionManager sessionManager = new SessionManager(getActivity());
        HashMap<String, String> hashMap = sessionManager.getUserDetails();
        String mSessionId = hashMap.get(SessionManager.KEY_SESSION_ID);
        String mMobile = hashMap.get(SessionManager.KEY_PHONE_NUMBER);

        HashMap<String, String> params = new HashMap<String, String>();
        params.put("merchantId", Preference.getDefaults(PreferenceConstants.MERCHANT_ID, getActivity()));
        params.put("itemId", Preference.getDefaults(PreferenceConstants.SELECTED_ITEM_ID, getActivity()));
        params.put("actualPrice", mItmePrice);
        params.put("discountAmount", mDiscountAmount);
        params.put("discountPercentage", mDiscountPercentage);
        params.put("priceAfterDiscount", mPriceAfterDisc);
        params.put("sessionId", mSessionId);
        params.put("mobileNumber", mMobile);



        JsonObjectRequest request_json = new JsonObjectRequest(EndPoint.ADD_NEW_OFFER, new JSONObject(params),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        progress.dismiss();
                        try {
                            Integer mStatusCode = response.getInt(WebServiceConstants.EXISTING_RESPONSE_CODE);
                            if (mStatusCode == 400) {
                                Message("Offer Already Added. Please Delete The Current Offer And Proceed.");
                            } else if (mStatusCode == 200) {
                                Message1("Submit Successful");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progress.dismiss();
                NetworkUtils.UnableToReachServer(getActivity());
            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(request_json);
    }


    public void Message(String message) {
        LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
        View promptView = layoutInflater.inflate(R.layout.alert_message, null);
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setView(promptView);
        TextView textViewMessage = promptView.findViewById(R.id.alert_message_display);
        textViewMessage.setText(message);
        Button buttonblock = (Button) promptView.findViewById(R.id.alert_ok_buton);
        buttonblock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alert.dismiss();
            }
        });
        alert = alertDialogBuilder.create();
        alert.show();
    }
    public void Message1(String message) {
        LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
        View promptView = layoutInflater.inflate(R.layout.alert_message, null);
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setView(promptView);
        TextView textViewMessage = promptView.findViewById(R.id.alert_message_display);
        textViewMessage.setText(message);
        Button buttonblock = (Button) promptView.findViewById(R.id.alert_ok_buton);
        buttonblock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alert.dismiss();
                getActivity().finish();
            }
        });
        alert = alertDialogBuilder.create();
        alert.show();
    }


}
