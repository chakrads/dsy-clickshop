package com.clickshopmerchant.shop.fonts;
import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;

import com.clickshopmerchant.shop.volley.AppController;

/**
 * Created by Yashpal on 24-03-2018.
 */


public class CorbelTextView extends AppCompatTextView {

    public CorbelTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public CorbelTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CorbelTextView(Context context) {
        super(context);
        init();
    }

    private void init() {
        if (!isInEditMode()) {
            Typeface custom_font = Typeface.createFromAsset(AppController.getInstance().getAssets(), "corbel-1361520258.ttf");
            setTypeface(custom_font);
        }
    }
}

