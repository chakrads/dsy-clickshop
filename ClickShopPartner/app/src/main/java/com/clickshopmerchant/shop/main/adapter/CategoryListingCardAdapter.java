package com.clickshopmerchant.shop.main.adapter;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.clickshopmerchant.shop.R;
import com.clickshopmerchant.shop.appconfig.Constants;
import com.clickshopmerchant.shop.main.activity.GetFilteredActivity;
import com.clickshopmerchant.shop.main.model.GetCategory;
import com.clickshopmerchant.shop.storage.Preference;
import com.clickshopmerchant.shop.volley.CustomVolleyRequest;
import java.util.List;

public class CategoryListingCardAdapter extends RecyclerView.Adapter<CategoryListingCardAdapter.ViewHolder> {

    private static final String TAG = CategoryListingCardAdapter.class.getSimpleName();
    Context context;
    List<GetCategory> listInAss;
    public ImageLoader imageLoader;
    private NetworkImageView networkImageView;

    public CategoryListingCardAdapter(List<GetCategory> listInAss, Context context) {
        super();
        this.listInAss = listInAss;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.catagory_card_adapter, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public int getItemCount() {
        return listInAss.size();
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final GetCategory getCategory = listInAss.get(position);
        holder.textViewItemName.setText(getCategory.getItemCategoryName());

        final String image = getCategory.getItemCategoryImage();
        imageLoader = CustomVolleyRequest.getInstance(context).getImageLoader();
        imageLoader.get(image, ImageLoader.getImageListener(networkImageView, R.drawable.noimage,
                android.R.drawable.ic_dialog_alert));
        networkImageView.setImageUrl( image, imageLoader);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String mItemId = String.valueOf(getCategory.getItemCategoryId());
                String mItemName = getCategory.getItemCategoryName();

                if (Preference.getDefaults(Constants.CATAGORY_ID, context) != null){
                    Preference.deletePrefs(Constants.CATAGORY_ID, context);
                }
                Preference.setDefaults(Constants.CATAGORY_ID, mItemId, context);
                Intent intent = new Intent(context, GetFilteredActivity.class);
                intent.putExtra("ItemId", mItemId);
                intent.putExtra("ItemName", mItemName);
                context.startActivity(intent);

            }
        });
    }


    class ViewHolder extends RecyclerView.ViewHolder {
        TextView textViewItemName;
        public ViewHolder(View itemView) {
            super(itemView);
            networkImageView = itemView.findViewById(R.id.category_image);
            textViewItemName = itemView.findViewById(R.id._catogory_image_name);
            this.setIsRecyclable(false);
        }
    }

}



