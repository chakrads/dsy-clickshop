package com.clickshopmerchant.shop.appconfig;

public class PreferenceConstants {

    //Login Page
    public static String MERCHANT_ID = "merchantId";
    public static String LOGIN_FLAG = "login_flag";


    // SaveStoreSetUp
    public static String POST_STORE_ID = "storeid";
    public static String POST_STORE_NAME = "storeName";
    public static String POST_DELIVERY_DURATION = "deliveryduration";
    public static String POST_WORKING_HOUR_FROM_TIME = "workinghourfromtime";
    public static String POST_WORKING_HOUR_TO_TIME = "workinghourtotime";
    public static String POST_DELIVERY_RANGE = "deliveryrange";
    public static String POST_LATTITUDE = "lattitude";
    public static String POST_LONGITUDE = "longitude";
    public static String POST_ADDRESS = "address";
    public static String POST_CITY = "city";
    public static String POST_STATE = "state";
    public static String POST_COUNTRY = "country";
    public static String POST_PINCODE = "pincode";
    public static String POST_DELIVERY_AMOUNT = "delivery_amount";
    public static String POST_FREE_DELIVERY = "free_delivery";


    public static String SELECTED_ITEM_ID = "SELECTED_ITEM_ID";
    public static String ITEM_SELECTED = "ITEM_SELECTED";
    public static String ITEM_NAME = "ITEM_NAME";
    public static String ITEM_DESCRIPTION = "ITEM_DESCRIPTION";
    public static String ITEM_PRICE = "ITEM_PRICE";
    public static String ITEM_WEIGHT = "ITEM_WEIGHT";
    public static String ITEM_IMAGE = "ITEM_IMAGE";


}
