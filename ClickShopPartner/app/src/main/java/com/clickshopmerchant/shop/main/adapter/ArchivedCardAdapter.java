package com.clickshopmerchant.shop.main.adapter;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.clickshopmerchant.shop.R;
import com.clickshopmerchant.shop.main.fragment.hometabfragment.ArchivedOrderRefresh;
import com.clickshopmerchant.shop.main.model.ArchivedPo;
import java.util.List;

public class ArchivedCardAdapter extends RecyclerView.Adapter<ArchivedCardAdapter.ViewHolder> {
    private static final String TAG = ArchivedCardAdapter.class.getSimpleName();
    Context context;
    List<ArchivedPo> listInAss;
    ArchivedOrderRefresh archivedOrderRefresh;

    public ArchivedCardAdapter(List<ArchivedPo> listInAss, Context context, ArchivedOrderRefresh archivedOrderRefresh) {
        super();
        this.listInAss = listInAss;
        this.context = context;
        this.archivedOrderRefresh = archivedOrderRefresh;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_adapter_archived, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public int getItemCount() {
        return listInAss.size();
    }



    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final ArchivedPo archivedPo = listInAss.get(position);
        holder.textViewOrderId.setText(archivedPo.getOrderId());
        holder.textViewAddress.setText(archivedPo.getShippingAddress());
        holder.textViewPaymentMode.setText(archivedPo.getPaymentMode());
        holder.textViewPhoneNumber.setText(archivedPo.getMobileNumber());
        holder.textViewTotalAmount.setText(new StringBuilder("Rs. ").append(archivedPo.getOrderAmount()));
        holder.textViewOrderStatus.setText(archivedPo.getOrderStatus());

        if (archivedPo.getOrderStatus().equalsIgnoreCase("Finished")) {
            holder.textViewOrderStatus.setTextColor(Color.parseColor("#028738"));
        } else if (archivedPo.getOrderStatus().equalsIgnoreCase("Rejected")) {
            holder.textViewOrderStatus.setTextColor(Color.parseColor("#D50000"));
        }





        holder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String mPhoneNumber = archivedPo.getMobileNumber();
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:"+mPhoneNumber));
                context.startActivity(intent);


            }
        });


    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView textViewOrderId;
        TextView textViewAddress;
        TextView textViewPaymentMode;
        TextView textViewTotalAmount;
        TextView textViewOrderStatus;
        LinearLayout linearLayout;
        TextView textViewPhoneNumber;

        private ViewHolder(View itemView) {
            super(itemView);
            textViewOrderId = itemView.findViewById(R.id._order_id);
            textViewAddress = itemView.findViewById(R.id._deliver_to);
            textViewPaymentMode = itemView.findViewById(R.id._payment_mode);
            textViewTotalAmount = itemView.findViewById(R.id._total_amount);
            textViewOrderStatus = itemView.findViewById(R.id._order_status);
            linearLayout = itemView.findViewById(R.id.phone_calling);
            textViewPhoneNumber = itemView.findViewById(R.id._order_user_phonenumber);


            this.setIsRecyclable(false);
        }
    }
}
