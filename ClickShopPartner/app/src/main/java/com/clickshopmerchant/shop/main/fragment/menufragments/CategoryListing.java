package com.clickshopmerchant.shop.main.fragment.menufragments;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.clickshopmerchant.shop.R;
import com.clickshopmerchant.shop.appconfig.AppUtils;
import com.clickshopmerchant.shop.appconfig.EndPoint;
import com.clickshopmerchant.shop.appconfig.PreferenceConstants;
import com.clickshopmerchant.shop.appconfig.SessionManager;
import com.clickshopmerchant.shop.appconfig.WebServiceConstants;
import com.clickshopmerchant.shop.main.activity.MainActivity;
import com.clickshopmerchant.shop.main.adapter.CategoryListingCardAdapter;
import com.clickshopmerchant.shop.main.model.GetCategory;
import com.clickshopmerchant.shop.network.NetworkUtils;
import com.clickshopmerchant.shop.storage.Preference;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class CategoryListing extends Fragment implements SwipeRefreshLayout.OnRefreshListener, CategoryListingRefres {
    private static final String TAG = CategoryListing.class.getSimpleName();
    EditText editTextSearch;
    public RelativeLayout relativeLayoutLoading;
    public SwipeRefreshLayout swipeRefreshLayout;
    public RecyclerView recyclerView;
    Button btngohome;
    public List<GetCategory> allItemlist;
    public CategoryListingCardAdapter adapter;
    public GridLayoutManager gridLayoutManager;
    String newquery, mSubString;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getActivity().setTitle("Select Catagory");

        return inflater.inflate(R.layout.fragment_existing_items, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        Views();
        servicecall();
        super.onActivityCreated(savedInstanceState);
    }

    public void Views() {
        btngohome=getActivity().findViewById(R.id.btngohome);

        btngohome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doneGoToLogin();
            }
        });

        SharedPreferences preferences1 = PreferenceManager.getDefaultSharedPreferences(getActivity());

        if(preferences1.getString("flag","").equals("0")){
         btngohome.setVisibility(View.VISIBLE);
        }
        else
            btngohome.setVisibility(View.GONE);

        editTextSearch = getActivity().findViewById(R.id._search_existing);
        editTextSearch.setVisibility(View.GONE);
        editTextSearch.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES); // Keypad First Letter Caps
        relativeLayoutLoading = getActivity().findViewById(R.id._loadingS);
        swipeRefreshLayout = getActivity().findViewById(R.id.existing_items_refreshs);
        swipeRefreshLayout.setVisibility(View.GONE);
        swipeRefreshLayout.setOnRefreshListener(this);
        recyclerView = getActivity().findViewById(R.id.existing_recyclerview_refreshs);
        gridLayoutManager = new GridLayoutManager(getActivity(), 2);
        recyclerView.setLayoutManager(gridLayoutManager);
        allItemlist = new ArrayList<>();
        editTextSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence query, int start, int before, int count) {
                if (count >= 2) {
                    mSubString = String.valueOf(query);
                    newquery = mSubString.substring(0, 1).toUpperCase() + mSubString.substring(1);
                } else {
                    mSubString = String.valueOf(query);
                    newquery = query.toString().toUpperCase();
                }

                final List<GetCategory> filteredList = new ArrayList<>();

                if (allItemlist != null) {
                    for (int i = 0; i < allItemlist.size(); i++) {
                        try {
                            final String text = allItemlist.get(i).getItemCategoryName();
                            if (text.contains(newquery)) {
                                filteredList.add(allItemlist.get(i));
                                updateRecyclerView(filteredList, false);
                            }
                        } catch (NullPointerException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }


    public void callExisitingItemRefresh() {
        servicecall();
    }


    public void doneGoToLogin() {

        String mPostStoreName = Preference.getDefaults(PreferenceConstants.POST_STORE_NAME, getActivity());
        String mPostWorkingFromTime = Preference.getDefaults(PreferenceConstants.POST_WORKING_HOUR_FROM_TIME, getActivity());
        String mPostWorkingToTime = Preference.getDefaults(PreferenceConstants.POST_WORKING_HOUR_TO_TIME, getActivity());
        String mPostDeliveryDuration = Preference.getDefaults(PreferenceConstants.POST_DELIVERY_DURATION, getActivity());
        String mPostDeliverRange = Preference.getDefaults(PreferenceConstants.POST_DELIVERY_RANGE, getActivity());
        String mPostAddress = Preference.getDefaults(PreferenceConstants.POST_ADDRESS, getActivity());
        String mPostCity = Preference.getDefaults(PreferenceConstants.POST_CITY, getActivity());
        String mPostCountry = Preference.getDefaults(PreferenceConstants.POST_COUNTRY, getActivity());
        String mPostState = Preference.getDefaults(PreferenceConstants.POST_STATE, getActivity());
        String mPinCode = Preference.getDefaults(PreferenceConstants.POST_PINCODE, getActivity());
        String mLatitude = Preference.getDefaults(PreferenceConstants.POST_LATTITUDE, getActivity());
        String mLongitude = Preference.getDefaults(PreferenceConstants.POST_LONGITUDE, getActivity());

        String mDeliveryAmount = Preference.getDefaults(PreferenceConstants.POST_DELIVERY_AMOUNT, getActivity());
        String mFreeDeliveryAbove = Preference.getDefaults(PreferenceConstants.POST_FREE_DELIVERY, getActivity());


        SessionManager sessionManager = new SessionManager(getActivity());
        HashMap<String, String> hashMap = sessionManager.getUserDetails();
        String mSessionId = hashMap.get(SessionManager.KEY_SESSION_ID);
        String mMobile = hashMap.get(SessionManager.KEY_PHONE_NUMBER);


        HashMap<String, String> params = new HashMap<String, String>();
        params.put("merchantId", Preference.getDefaults(PreferenceConstants.MERCHANT_ID, getActivity()));
        params.put("storeName", mPostStoreName);
        params.put("deliveryDuration", mPostDeliveryDuration);
        params.put("workingHourFrom", mPostWorkingFromTime);
        params.put("workingHourTo", mPostWorkingToTime);
        params.put("deliveryRange", mPostDeliverRange);
        params.put("latitude", mLatitude);
        params.put("longitude", mLongitude);
        params.put("address", mPostAddress);
        params.put("city", mPostCity);
        params.put("state", mPostState);
        params.put("country", mPostCountry);
        params.put("pinCode", mPinCode);
        params.put("flag", "1"); // FLAG FOR PRE - LOGIN
        params.put("sessionId", mSessionId);
        params.put("mobileNumber", mMobile);

        params.put("deliveryCharge", mDeliveryAmount);
        params.put("deliveryFreeAbove", mFreeDeliveryAbove);





        Log.d(TAG, "params: " + params);

        JsonObjectRequest request_json = new JsonObjectRequest(EndPoint.SAVE_INITIAL_SETUP_ADDRESS, new JSONObject(params),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        Log.d(TAG, "Slot Response: " + response);
                        try {
                            int mStatusCode = response.getInt(WebServiceConstants.EXISTING_RESPONSE_CODE);
                            if (mStatusCode == 200) {

                                // LOGIN FLAG - FLAG USED TO DETECT NAVIGATION
                                String mFlag = response.getString(WebServiceConstants.FLAG);
                                Preference.setDefaults(PreferenceConstants.LOGIN_FLAG, mFlag, getActivity());

                                Intent intent = new Intent(getActivity(), MainActivity.class);
                                startActivity(intent);
                                getActivity().finish();
                            } else {
                                Toast.makeText(getActivity(), "Request Failed. Please Try Later", Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Toast.makeText(getActivity(), "Unable to Reach Server", Toast.LENGTH_SHORT).show();
            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(request_json);
    }


    public void updateRecyclerView(List<GetCategory> lst, boolean swipeStatus) {
        recyclerView = getActivity().findViewById(R.id.existing_recyclerview_refreshs);
        gridLayoutManager = new GridLayoutManager(getActivity(), 2);
        recyclerView.setLayoutManager(gridLayoutManager);
        adapter = new CategoryListingCardAdapter(lst, getActivity());
        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        swipeRefreshLayout.setRefreshing(swipeStatus);
    }


    public void servicecall() {
        if (allItemlist != null){
            allItemlist.clear();
        }

        if (AppUtils.internetConnectionAvailable(2000)) {
            getExisitingItems();
        } else {
            Toast.makeText(getActivity(), "No Network Connection",
                    Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public void onRefresh() {
        relativeLayoutLoading.setVisibility(View.GONE);
        allItemlist.clear();
        recyclerView.setVisibility(View.GONE);
        servicecall();
    }


    public void getExisitingItems() {
        HashMap<String, String> params = new HashMap<String, String>();
       // params.put("merchantId", Preference.getDefaults(PreferenceConstants.MERCHANT_ID, getActivity()));
        Log.d(TAG, "params: " + params);

        JsonObjectRequest request_json = new JsonObjectRequest(EndPoint.CATEGORY_LISTING, new JSONObject(params),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        recyclerView.setVisibility(View.VISIBLE);
                        Log.d(TAG, "Catagory Listing Response: " + response);
                        try {
                            int mStatusCode = response.getInt(WebServiceConstants.EXISTING_RESPONSE_CODE);
                            if (mStatusCode == 200) {
                                relativeLayoutLoading.setVisibility(View.GONE);
                                swipeRefreshLayout.setVisibility(View.VISIBLE);
                                editTextSearch.setVisibility(View.VISIBLE);
                                JSONArray array = response.optJSONArray(WebServiceConstants.DATA);
                                parseData(array);
                            } else {
                                relativeLayoutLoading.setVisibility(View.GONE);
                                swipeRefreshLayout.setVisibility(View.GONE);
                                swipeRefreshLayout.setRefreshing(false);
                                editTextSearch.setVisibility(View.GONE);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkUtils.UnableToReachServer(getActivity());
            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(request_json);
    }


    private void parseData(JSONArray array) {
        for (int i = 0; i < array.length(); i++) {
            GetCategory getCategory = new GetCategory();
            JSONObject json;
            try {
                json = array.getJSONObject(i);
                if (json.getString(WebServiceConstants.GET_FILTER_CATEGORY_ID) != null && !json.getString(WebServiceConstants.GET_FILTER_CATEGORY_ID).isEmpty()
                        && !json.getString(WebServiceConstants.GET_FILTER_CATEGORY_ID).equals(WebServiceConstants.NULL)) {
                    getCategory.setItemCategoryId((json.getInt(WebServiceConstants.GET_FILTER_CATEGORY_ID)));
                } else {
                    getCategory.setItemCategoryId(0);
                }
                if (json.getString(WebServiceConstants.GET_FILTER_CATEGORY_NAME) != null && !json.getString(WebServiceConstants.GET_FILTER_CATEGORY_NAME).isEmpty()
                        && !json.getString(WebServiceConstants.GET_FILTER_CATEGORY_NAME).equals(WebServiceConstants.NULL)) {
                    getCategory.setItemCategoryName((json.getString(WebServiceConstants.GET_FILTER_CATEGORY_NAME)));
                } else {
                    getCategory.setItemCategoryName("");
                }
                if (json.getString(WebServiceConstants.GET_FILTER_CATEGORY_IMAGE) != null && !json.getString(WebServiceConstants.GET_FILTER_CATEGORY_IMAGE).isEmpty()
                        && !json.getString(WebServiceConstants.GET_FILTER_CATEGORY_IMAGE).equals(WebServiceConstants.NULL)) {

//                    String mPath = EndPoint.CATAGORY;
                    getCategory.setItemCategoryImage(json.getString(WebServiceConstants.GET_FILTER_CATEGORY_IMAGE));

                } else {
                    getCategory.setItemCategoryImage("");
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            allItemlist.add(getCategory);
            // Collections.sort(recordsList);
        }
        adapter = new CategoryListingCardAdapter(allItemlist, getActivity());
        recyclerView.setAdapter(adapter);
        swipeRefreshLayout.setRefreshing(false);
        adapter.notifyDataSetChanged();
    }


}

