
package com.clickshopmerchant.shop.main.model.ordernew;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OrderSearchList {

    @SerializedName("orderId")
    @Expose
    private String orderId;
    @SerializedName("userId")
    @Expose
    private Integer userId;
    @SerializedName("shippingAddress")
    @Expose
    private String shippingAddress;
    @SerializedName("paymentMode")
    @Expose
    private String paymentMode;
    @SerializedName("orderAmount")
    @Expose
    private String orderAmount;
    @SerializedName("merchantId")
    @Expose
    private Integer merchantId;
    @SerializedName("orderStatus")
    @Expose
    private String orderStatus;
    @SerializedName("userName")
    @Expose
    private String userName;
    @SerializedName("mobileNumber")
    @Expose
    private String mobileNumber;

    public String getDeliveryCharge() {
        return deliveryCharge;
    }

    public void setDeliveryCharge(String deliveryCharge) {
        this.deliveryCharge = deliveryCharge;
    }

    @SerializedName("deliveryCharge")
    @Expose
    private String deliveryCharge;






    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getShippingAddress() {
        return shippingAddress;
    }

    public void setShippingAddress(String shippingAddress) {
        this.shippingAddress = shippingAddress;
    }

    public String getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    public String getOrderAmount() {
        return orderAmount;
    }

    public void setOrderAmount(String orderAmount) {
        this.orderAmount = orderAmount;
    }

    public Integer getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(Integer merchantId) {
        this.merchantId = merchantId;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }





}
