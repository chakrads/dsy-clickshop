package com.clickshopmerchant.shop.realm;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Store extends RealmObject {

    @PrimaryKey
    private long id;

    private String mItemId;

    private String mItemName;

    private String mItemCatagory;

    private String mItemWeight;

    private String mItemPrice;

    private String mItemImage;

    private String mItemWeightUnit;


    // Standard getters & setters generated by your IDE…
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }


    public String getmItemId() {
        return mItemId;
    }

    public void setmItemId(String mItemId) {
        this.mItemId = mItemId;
    }



    public String getmItemName() {
        return mItemName;
    }

    public void setmItemName(String mItemName) {
        this.mItemName = mItemName;
    }

    public String getmItemCatagory() {
        return mItemCatagory;
    }

    public void setmItemCatagory(String mItemCatagory) {
        this.mItemCatagory = mItemCatagory;
    }

    public String getmItemWeight() {
        return mItemWeight;
    }

    public void setmItemWeight(String mItemWeight) {
        this.mItemWeight = mItemWeight;
    }


    public String getmItemPrice() {
        return mItemPrice;
    }

    public void setmItemPrice(String mItemPrice) {
        this.mItemPrice = mItemPrice;
    }


    public String getmItemImage() {
        return mItemImage;
    }

    public void setmItemImage(String mItemImage) {
        this.mItemImage = mItemImage;
    }

    public String getmItemWeightUnit() {
        return mItemWeightUnit;
    }

    public void setmItemWeightUnit(String mItemWeightUnit) {
        this.mItemWeightUnit = mItemWeightUnit;
    }


}

