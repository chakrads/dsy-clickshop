package com.clickshopmerchant.shop.main.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.clickshopmerchant.shop.R;
import com.clickshopmerchant.shop.appconfig.AppUtils;
import com.clickshopmerchant.shop.appconfig.EndPoint;
import com.clickshopmerchant.shop.network.NetworkUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class ForgotPassword extends AppCompatActivity {
    EditText editTextPhone, editTextPassword, editTextReEnterPassword;
    Button buttonSubmit;
    private AlertDialog alert;
    private String mPass;
    private String mPassConfirm;
    private ProgressDialog progress;
    private String mPhoneNo;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        editTextPhone = findViewById(R.id.phone_no);
        editTextPassword = findViewById(R.id.password);
        editTextReEnterPassword = findViewById(R.id.reenter_password);
        buttonSubmit = findViewById(R.id.btn_reset_password);
        buttonSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPass = editTextPassword.getText().toString().trim();
                mPassConfirm = editTextReEnterPassword.getText().toString().trim();
                if (editTextPhone.getText().length() == 0) {
                    Message("Enter Phone No");
                } else if (editTextPhone.getText().length() < 10) {
                    Message("Enter A Valid Phone No");
                } else if (!mPass.equals(mPassConfirm)) {
                    Message("Password Do Not Match");
                } else {
                    if (AppUtils.internetConnectionAvailable(2000)) {
                        submitForgotPassword();
                    } else {
                        Message("No Internet Connection");
                    }
                }
            }
        });
    }


    public void submitForgotPassword() {
        progress = ProgressDialog.show(this, "",
                "Please wait...", false, false);
        mPhoneNo = editTextPhone.getText().toString().trim();
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("mobileNumber", mPhoneNo);
        params.put("newPassword", mPass);
        params.put("confirmPassword", mPassConfirm);



        JsonObjectRequest request_json = new JsonObjectRequest(EndPoint.FORGOT_PASSWORD,
                new JSONObject(params),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        progress.dismiss();
                        try {
                            Integer mStatusCode = response.getInt("responseCode");
                            if (mStatusCode == 400) {
                                Message("Reset Password Failed");
                            } else if (mStatusCode == 200) {
                                Message("Reset Password Successful");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progress.dismiss();
                try {
                    NetworkUtils.UnableToReachServer(getApplicationContext());
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(request_json);
    }


    public void Message(String message) {
        LayoutInflater layoutInflater = LayoutInflater.from(this);
        View promptView = layoutInflater.inflate(R.layout.alert_message, null);
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setView(promptView);
        TextView textViewMessage = promptView.findViewById(R.id.alert_message_display);
        textViewMessage.setText(message);
        Button buttonblock = promptView.findViewById(R.id.alert_ok_buton);
        buttonblock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alert.dismiss();
                finish();
            }
        });
        alert = alertDialogBuilder.create();
        alert.show();
    }


}
