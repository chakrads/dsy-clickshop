package com.clickshopmerchant.shop.main.adapter;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.Volley;
import com.clickshopmerchant.shop.R;
import com.clickshopmerchant.shop.appconfig.AppUtils;
import com.clickshopmerchant.shop.appconfig.EndPoint;
import com.clickshopmerchant.shop.appconfig.PreferenceConstants;
import com.clickshopmerchant.shop.appconfig.SessionManager;
import com.clickshopmerchant.shop.appconfig.WebServiceConstants;
import com.clickshopmerchant.shop.main.activity.OfferActivity;
import com.clickshopmerchant.shop.main.fragment.menufragments.SelectedItems;
import com.clickshopmerchant.shop.main.model.selecteditemmodel.ItemList;
import com.clickshopmerchant.shop.storage.Preference;
import com.clickshopmerchant.shop.volley.CustomVolleyRequest;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;

/**
 * @author Yashpal M created on 4/3/18.
 */


public class SelectedItemCardAdapter extends
        RecyclerView.Adapter<SelectedItemCardAdapter.ViewHolder> {

    private static final String TAG = SelectedItemCardAdapter.class.getSimpleName();
    Context context;
    List<ItemList> listInAss;
    public ImageLoader imageLoader;
    private NetworkImageView networkImageView;
    private AlertDialog alert;
    private ProgressDialog progress;
    private String mItemId, mItemName, mItemPrice;
    public SelectedItems selectedItems;




    public SelectedItemCardAdapter(List<ItemList> listInAss, Context context, SelectedItems selectedItems) {
        super();
        this.listInAss = listInAss;
        this.context = context;
        this.selectedItems = selectedItems;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.selected_item_card_adapter, parent, false);

        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public int getItemCount() {
        return listInAss.size();
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {


        final ItemList getAllItems = listInAss.get(position);
        holder.textViewItemName.setText(getAllItems.getItemName());
        holder.textViewItemPrice.setText(new StringBuilder("Rs. ")
                .append(getAllItems.getItemPrice()).append(" / ").append(getAllItems.getItemWeight()).append(" ").append(getAllItems.getItemWeightUnit()));
        final String images = getAllItems.getItemImage();

        if (images != null &&  !images.isEmpty()){
            final String image = modifyDropboxUrl(getAllItems.getItemImage());
            imageLoader = CustomVolleyRequest.getInstance(context).getImageLoader();
            imageLoader.get(image, ImageLoader.getImageListener(networkImageView, R.drawable.noimage,
                    android.R.drawable.ic_dialog_alert));
            networkImageView.setImageUrl(image, imageLoader);
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mItemId = String.valueOf(getAllItems.getItemId());
                    mItemName = getAllItems.getItemName();
                }
            });
        }


        holder.imageButtonDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mItemId = String.valueOf(getAllItems.getItemId());
                mItemName = getAllItems.getItemName();
                AreYourSureMessage("Are You Sure You Want to Delete This Item ?");
            }
        });


        holder.imageButtonUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String mItemId = String.valueOf(getAllItems.getItemId());
                String mItemName = getAllItems.getItemName();
                String mItemPrice = getAllItems.getItemPrice();
                String mItemWeight = getAllItems.getItemWeight();
                addPriceAlert(mItemName, mItemId, mItemPrice, mItemWeight);
            }
        });


        holder.buttonOffer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String mSelectedItemId = String.valueOf(getAllItems.getItemId());
                String mItemName = getAllItems.getItemName();
                String mItemPrice = getAllItems.getItemPrice();
                String mItemWeight = getAllItems.getItemWeight();
                String mItemImage = modifyDropboxUrl(getAllItems.getItemImage());
                if (Preference.getDefaults(PreferenceConstants.SELECTED_ITEM_ID, context) != null) {
                    Preference.deletePrefs(PreferenceConstants.SELECTED_ITEM_ID, context);
                    Preference.deletePrefs(PreferenceConstants.ITEM_NAME, context);
                    Preference.deletePrefs(PreferenceConstants.ITEM_DESCRIPTION, context);
                    Preference.deletePrefs(PreferenceConstants.ITEM_PRICE, context);
                    Preference.deletePrefs(PreferenceConstants.ITEM_WEIGHT, context);
                    Preference.deletePrefs(PreferenceConstants.ITEM_IMAGE, context);
                }
                Preference.setDefaults(PreferenceConstants.SELECTED_ITEM_ID, mSelectedItemId, context);
                Preference.setDefaults(PreferenceConstants.ITEM_NAME, mItemName, context);
                Preference.setDefaults(PreferenceConstants.ITEM_PRICE, mItemPrice, context);
                Preference.setDefaults(PreferenceConstants.ITEM_WEIGHT, mItemWeight, context);
                Preference.setDefaults(PreferenceConstants.ITEM_IMAGE, mItemWeight, context);
                Preference.setDefaults(PreferenceConstants.ITEM_IMAGE, mItemImage, context);
                Intent intentOffer = new Intent(context, OfferActivity.class);
                context.startActivity(intentOffer);
            }
        });











        holder.setIsRecyclable(false);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView textViewItemName;
        TextView textViewItemPrice;
        Button imageButtonDelete, imageButtonUpdate, buttonOffer;

        public ViewHolder(View itemView) {
            super(itemView);
            networkImageView = itemView.findViewById(R.id.existing_item_image);
            textViewItemName = itemView.findViewById(R.id.existing_item_name);
            textViewItemPrice = itemView.findViewById(R.id.existing_item_price);
            imageButtonDelete = itemView.findViewById(R.id._delete_item);
            imageButtonUpdate = itemView.findViewById(R.id._update_item);
            buttonOffer = itemView.findViewById(R.id._item_offer);
        }
    }


    public static String modifyDropboxUrl(String originalUrl) {
        String newUrl = originalUrl.replace("www.dropbox." ,"dl.dropboxusercontent.");
        newUrl = newUrl.replace("dropbox.", "dl.dropboxusercontent.");
        return newUrl;
    }


    private void addPriceAlert(final String mName, final String mItemId, final String mItemPrice, final String mItemWeight) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        final View promptView = layoutInflater.inflate(R.layout.add_price_alert, null);
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setView(promptView);
        TextView textViewDisplay = promptView.findViewById(R.id._selected_display);
        textViewDisplay.setText(new StringBuilder("").append("").append(mName));
        final EditText editTextPrice = promptView.findViewById(R.id._get_price);
        editTextPrice.setText(mItemPrice);

        Button buttonSubmit = promptView.findViewById(R.id.btn_add_price);
        buttonSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editTextPrice.getText().length() == 0) {
                    editTextPrice.setError("Enter Price");
                } else {
                    if (AppUtils.internetConnectionAvailable(2000)) {
                        String mPrice = editTextPrice.getText().toString().trim();
                        submitSelectedItems(mItemId, mPrice, mName, mItemWeight);
                        alert.dismiss();
                    } else {
                        Toast.makeText(context, "No Network Connection",
                                Toast.LENGTH_SHORT).show();
                    }
                }

            }
        });

        alert = alertDialogBuilder.create();
        alert.show();
    }


    public void AreYourSureMessage(String message) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View promptView = layoutInflater.inflate(R.layout.alert_message_are_your_sure, null);
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setView(promptView);
        TextView textViewMessage = promptView.findViewById(R.id.alert_message_display);
        textViewMessage.setText(message);
        Button buttonblock = promptView.findViewById(R.id.alert_ok_buton);
        buttonblock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (AppUtils.internetConnectionAvailable(2000)) {
                    submitDeleteItem();
                } else {
                    Toast.makeText(context, "No Network Connection",
                            Toast.LENGTH_SHORT).show();
                }
                alert.dismiss();
            }
        });

        Button buttoncancel = promptView.findViewById(R.id.alert_cancel);
        buttoncancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alert.dismiss();

            }
        });

        alert = alertDialogBuilder.create();
        alert.show();
    }


    public void submitDeleteItem() {
        progress = ProgressDialog.show(context, "", "Please wait...", false, false);
        SessionManager sessionManager = new SessionManager(context);
        HashMap<String, String> hashMap = sessionManager.getUserDetails();
        String mSessionId = hashMap.get(SessionManager.KEY_SESSION_ID);
        String mMobile = hashMap.get(SessionManager.KEY_PHONE_NUMBER);


        HashMap<String, String> params = new HashMap<String, String>();
        params.put("merchantId", Preference.getDefaults(PreferenceConstants.MERCHANT_ID, context));
        params.put("itemId", mItemId);
        params.put("type", "Non-Brand");
        params.put("sessionId", mSessionId);
        params.put("mobileNumber", mMobile);



        Log.d(TAG, "params: " + params);
        JsonObjectRequest request_json = new JsonObjectRequest(EndPoint.DELETE_SELECTED_ITEM, new JSONObject(params),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        progress.dismiss();
                        try {
                            Integer mStatusCode = response.getInt(WebServiceConstants.EXISTING_RESPONSE_CODE);
                            if (mStatusCode == 400) {
                                Toast.makeText(context, "Delete Failed. Please Retry..",
                                        Toast.LENGTH_SHORT).show();
                            } else if (mStatusCode == 200) {
                                selectedItems.callSelectedItemRefresh();
                                Toast.makeText(context, "Delete Successfully",
                                        Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Log.d(TAG, "Delete Item Response: " + response);

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progress.dismiss();
                VolleyLog.e("Error: ", error.getMessage());
                Toast.makeText(context, "Server Error",
                        Toast.LENGTH_SHORT).show();
            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(request_json);
    }


    public void submitSelectedItems(String mItemId, String mPrice, String mName, String mItemWeight) {
        progress = ProgressDialog.show(context, "", "Please wait...", false, false);
        SessionManager sessionManager = new SessionManager(context);
        HashMap<String, String> hashMap = sessionManager.getUserDetails();
        String mSessionId = hashMap.get(SessionManager.KEY_SESSION_ID);
        String mMobile = hashMap.get(SessionManager.KEY_PHONE_NUMBER);


        HashMap<String, String> params = new HashMap<String, String>();
        params.put("merchantId", Preference.getDefaults(PreferenceConstants.MERCHANT_ID, context));
        params.put("itemId", mItemId);
        params.put("itemPrice", mPrice);
        params.put("itemName", mName);
        params.put("type", "Non-Brand");
        params.put("sessionId", mSessionId);
        params.put("mobileNumber", mMobile);



        Log.d(TAG, "params: " + params);

        JsonObjectRequest request_json = new JsonObjectRequest(EndPoint.UPDATE_SELECTED_ITEM, new JSONObject(params),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        progress.dismiss();
                        try {
                            Integer mStatusCode = response.getInt(WebServiceConstants.EXISTING_RESPONSE_CODE);
                            if (mStatusCode == 400) {
                                Toast.makeText(context, "Update Failed. Please Retry..",
                                        Toast.LENGTH_SHORT).show();
                            } else if (mStatusCode == 200) {
                                selectedItems.callSelectedItemRefresh();
                                Toast.makeText(context, "Update Successfully",
                                        Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Log.d(TAG, "Delete Item Response: " + response);

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progress.dismiss();
                VolleyLog.e("Error: ", error.getMessage());
                Toast.makeText(context, "Server Error",
                        Toast.LENGTH_SHORT).show();
            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(request_json);
    }
}


