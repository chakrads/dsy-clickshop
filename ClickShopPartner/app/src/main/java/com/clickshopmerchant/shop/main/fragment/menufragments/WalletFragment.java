package com.clickshopmerchant.shop.main.fragment.menufragments;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.clickshopmerchant.shop.R;
import com.clickshopmerchant.shop.appconfig.AppUtils;
import com.clickshopmerchant.shop.appconfig.EndPoint;
import com.clickshopmerchant.shop.appconfig.PreferenceConstants;
import com.clickshopmerchant.shop.appconfig.SessionManager;
import com.clickshopmerchant.shop.appconfig.WebServiceConstants;
import com.clickshopmerchant.shop.storage.Preference;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.HashMap;


public class WalletFragment extends Fragment{
    private static final String TAG = WalletFragment.class.getSimpleName();

    public RelativeLayout relativeLayoutLoading;
    public SwipeRefreshLayout swipeRefreshLayout;
    public RecyclerView recyclerView;
    public LinearLayoutManager linearLayoutManager;
    private AlertDialog alert;
    TextView corbelTextViewOrderAmount, corbelTextViewRechargeAmount, corbelTextViewBalanceAmount;
    Double mstr;

    public WalletFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getActivity().setTitle("My Wallet");
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_wallet, container, false);
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        corbelTextViewOrderAmount = getActivity().findViewById(R.id._total_amount_right);
        corbelTextViewRechargeAmount = getActivity().findViewById(R.id._total_recharge_right);
        corbelTextViewBalanceAmount = getActivity().findViewById(R.id._balance_amount);
        corbelTextViewBalanceAmount.setText("0");
        corbelTextViewRechargeAmount.setText("0");
        corbelTextViewOrderAmount.setText("0");
        if (AppUtils.internetConnectionAvailable(2000)) {
            getWalletBalance();
        } else {
            Toast.makeText(getActivity(), "No Network Connection",
                    Toast.LENGTH_SHORT).show();
        }

        super.onActivityCreated(savedInstanceState);
    }





    public void getWalletBalance() {

        SessionManager sessionManager = new SessionManager(getActivity());
        HashMap<String, String> hashMap = sessionManager.getUserDetails();
        String mSessionId = hashMap.get(SessionManager.KEY_SESSION_ID);
        String mMobile = hashMap.get(SessionManager.KEY_PHONE_NUMBER);

        HashMap<String, String> paramsSSS = new HashMap<String, String>();
        paramsSSS.put("merchantId", Preference.getDefaults(PreferenceConstants.MERCHANT_ID, getActivity()));
        paramsSSS.put("sessionId", mSessionId);
        paramsSSS.put("mobileNumber", mMobile);

        Log.d(TAG, "paramsSSS: " + paramsSSS);


        JsonObjectRequest request_jsonS = new JsonObjectRequest(EndPoint.WALLET_SYSTEM, new JSONObject(paramsSSS),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        Log.d(TAG, "response: " + response);


                        try {
                            int mStatusCode = response.getInt(WebServiceConstants.EXISTING_RESPONSE_CODE);
                            if (mStatusCode == 200) {
                                String mTotalPoint = response.getString("totalPoint");
                                corbelTextViewRechargeAmount.setText("Rs ." + mTotalPoint);

                                String mTotalOrderAmount = response.getString("totalOrderAmount");
                                corbelTextViewOrderAmount.setText("Rs. " +mTotalOrderAmount);

                                String mBalancePoint = response.getString("balancePoint");

                                try {

                                    mstr = Double.valueOf(mBalancePoint);

                                }catch (NumberFormatException e){
                                    e.printStackTrace();
                                }

                                double roundOff = Math.round(mstr * 100.0) / 100.0;

                                corbelTextViewBalanceAmount.setText("Rs. " + String.valueOf(roundOff));



                            } else if (mStatusCode == 400) {
                                Toast.makeText(getActivity(), "No Data",
                                        Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getActivity(), "Error",
                        Toast.LENGTH_LONG).show();
                try {
                    swipeRefreshLayout.setRefreshing(false);
                    relativeLayoutLoading.setVisibility(View.GONE);
                } catch (NullPointerException e) {
                    Toast.makeText(getActivity(), "Unable Reach Remote Server. Please try later..",
                            Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                } catch (IllegalStateException e) {
                    Toast.makeText(getActivity(), "Unable Reach Remote Server. Please try later..",
                            Toast.LENGTH_LONG).show();
                }
            }
        });


        request_jsonS.setRetryPolicy(new

                DefaultRetryPolicy(60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(request_jsonS);
    }


    public void Message(String message) {
        LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
        View promptView = layoutInflater.inflate(R.layout.alert_message, null);
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setView(promptView);
        TextView textViewMessage = promptView.findViewById(R.id.alert_message_display);
        textViewMessage.setText(message);
        Button buttonblock = promptView.findViewById(R.id.alert_ok_buton);
        buttonblock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alert.dismiss();
            }
        });
        alert = alertDialogBuilder.create();
        alert.show();
    }


}
