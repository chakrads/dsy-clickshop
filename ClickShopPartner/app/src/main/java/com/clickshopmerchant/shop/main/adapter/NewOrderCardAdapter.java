package com.clickshopmerchant.shop.main.adapter;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.clickshopmerchant.shop.R;
import com.clickshopmerchant.shop.appconfig.AppUtils;
import com.clickshopmerchant.shop.appconfig.EndPoint;
import com.clickshopmerchant.shop.appconfig.PreferenceConstants;
import com.clickshopmerchant.shop.appconfig.SessionManager;
import com.clickshopmerchant.shop.appconfig.WebServiceConstants;
import com.clickshopmerchant.shop.main.activity.OrderItemActivity;
import com.clickshopmerchant.shop.main.fragment.hometabfragment.NewOrderRefresh;
import com.clickshopmerchant.shop.main.model.ordernew.OrderSearchList;
import com.clickshopmerchant.shop.storage.Preference;

import org.json.JSONException;
import org.json.JSONObject;
import java.util.HashMap;
import java.util.List;

public class NewOrderCardAdapter extends RecyclerView.Adapter<NewOrderCardAdapter.ViewHolder> {

    private static final String TAG = NewOrderCardAdapter.class.getSimpleName();
    Context context;
    List<OrderSearchList> listInAss;
    NewOrderRefresh newOrderRefresh;
    public AlertDialog alertMessage;
    String mOrderId;
    private ProgressDialog progress;
    int mCheck;

    public NewOrderCardAdapter(List<OrderSearchList> listInAss, Context context, NewOrderRefresh newOrderRefresh) {
        super();
        this.listInAss = listInAss;
        this.context = context;
        this.newOrderRefresh = newOrderRefresh;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.new_order_card_adapter, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public int getItemCount() {
        return listInAss.size();
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final OrderSearchList newOrderPo = listInAss.get(position);
        holder.textViewOrderId.setText(newOrderPo.getOrderId());
        holder.textViewAddress.setText(newOrderPo.getShippingAddress());
        holder.textViewOrderUsername.setText(newOrderPo.getUserName());
        holder._delivery_charge.setText(newOrderPo.getDeliveryCharge());

        holder.textViewPhoneNumber.setText(newOrderPo.getMobileNumber());
        holder.textViewPaymentMode.setText(newOrderPo.getPaymentMode());
        holder.textViewTotalAmount.setText(new StringBuilder("Rs. ").append(newOrderPo.getOrderAmount()));
        holder.buttonReject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCheck = 1;
                mOrderId = newOrderPo.getOrderId();
                Message("Reject");
              //  reasonToRejectOrder();
            }
        });

        holder.buttonAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCheck = 2;
                mOrderId = newOrderPo.getOrderId();
                Message("Accept the Order ?");
            }
        });

        holder.buttonShowItems.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOrderId = newOrderPo.getOrderId();
                Intent intent = new Intent(context, OrderItemActivity.class);
                intent.putExtra("OrderItems", mOrderId);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_MULTIPLE_TASK); // avoids crashing in lower verion phones lollipop etc
                context.startActivity(intent);
            }
        });






        holder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String mPhoneNumber = newOrderPo.getMobileNumber();
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:"+mPhoneNumber));
                context.startActivity(intent);


            }
        });








    }

    public void Message(String message) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View promptView = layoutInflater.inflate(R.layout.alert_message, null);
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setView(promptView);
        TextView textViewMessage = promptView.findViewById(R.id.alert_message_display);
        textViewMessage.setText(message);
        Button buttonblock = promptView.findViewById(R.id.alert_ok_buton);
        buttonblock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (AppUtils.internetConnectionAvailable(2000)) {
                    AcceptRejectStatusUpdate();
                } else {
                    Toast.makeText(context, "No Network Connection",
                            Toast.LENGTH_SHORT).show();
                }
                alertMessage.dismiss();
            }
        });
        alertMessage = alertDialogBuilder.create();
        alertMessage.show();
    }


    class ViewHolder extends RecyclerView.ViewHolder {
        TextView textViewOrderId;
        TextView textViewAddress;
        TextView textViewPaymentMode;
        TextView textViewTotalAmount;
        TextView textViewOrderUsername;
        TextView textViewPhoneNumber;
        TextView _delivery_charge;
        Button buttonReject, buttonAccept, buttonShowItems;
        LinearLayout linearLayout;

        private ViewHolder(View itemView) {
            super(itemView);
            textViewOrderId = itemView.findViewById(R.id._order_id);
            _delivery_charge = itemView.findViewById(R.id._delivery_charge);
            textViewAddress = itemView.findViewById(R.id._deliver_to);
            textViewPaymentMode = itemView.findViewById(R.id._payment_mode);
            textViewTotalAmount = itemView.findViewById(R.id._total_amount);
            buttonReject = itemView.findViewById(R.id._reject);
            buttonAccept = itemView.findViewById(R.id._accept);
            buttonShowItems = itemView.findViewById(R.id._show_items);
            textViewOrderUsername = itemView.findViewById(R.id._order_user_name);
            textViewPhoneNumber = itemView.findViewById(R.id._order_user_phonenumber);
            linearLayout = itemView.findViewById(R.id.phone_calling);


            this.setIsRecyclable(false);
        }


    }


    private void AcceptRejectStatusUpdate() {
        progress = ProgressDialog.show(context, "", "Please wait...", false, false);
        SessionManager sessionManager = new SessionManager(context);
        HashMap<String, String> hashMap = sessionManager.getUserDetails();
        String mSessionId = hashMap.get(SessionManager.KEY_SESSION_ID);
        String mMobile = hashMap.get(SessionManager.KEY_PHONE_NUMBER);




        HashMap<String, String> params = new HashMap<String, String>();
        params.put("sessionId", mSessionId);
        params.put("orderId", mOrderId);
        params.put("mobileNumber", mMobile);
        params.put("merchantId", Preference.getDefaults(PreferenceConstants.MERCHANT_ID, context));



        if (mCheck == 1){
            params.put("orderStatus", "Rejected");
        }else if (mCheck == 2){
            params.put("orderStatus", "Progress"); // After accepting it will be moved to Progress page
        }
        Log.d(TAG, "params: " + params);

        JsonObjectRequest request_json = new JsonObjectRequest(EndPoint.SAVE_MERCHANT_ORDER_STATUS, new JSONObject(params),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        progress.dismiss();

                        Log.d(TAG, "Slot Response: " + response);
                        try {
                            int mStatusCode = response.getInt(WebServiceConstants.EXISTING_RESPONSE_CODE);
                            if (mStatusCode == 200) {
                                newOrderRefresh.newOrderRefresh();;
                                if (mCheck == 1){
                                    Toast.makeText(context, "Reject Successful",
                                            Toast.LENGTH_LONG).show();
                                }else if (mCheck == 2){
                                    Toast.makeText(context, "Accept Successful",
                                            Toast.LENGTH_LONG).show();
                                }
                            } else if (mStatusCode == 400) {


                            } else {

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                try {
                    progress.dismiss();
                } catch (NullPointerException e) {
                    Toast.makeText(context, "Unable Reach Remote Server. Please try later..",
                            Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                } catch (IllegalStateException e) {
                    Toast.makeText(context, "Unable Reach Remote Server. Please try later..",
                            Toast.LENGTH_LONG).show();
                }
            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(request_json);
    }


}
