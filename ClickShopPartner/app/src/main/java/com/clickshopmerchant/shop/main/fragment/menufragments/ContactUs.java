package com.clickshopmerchant.shop.main.fragment.menufragments;

import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.clickshopmerchant.shop.R;
import com.clickshopmerchant.shop.appconfig.AppUtils;
import com.clickshopmerchant.shop.appconfig.EndPoint;
import com.clickshopmerchant.shop.appconfig.PreferenceConstants;
import com.clickshopmerchant.shop.appconfig.SessionManager;
import com.clickshopmerchant.shop.appconfig.WebServiceConstants;
import com.clickshopmerchant.shop.storage.Preference;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;


public class ContactUs extends Fragment {
    private static final String TAG = ContactUs.class.getSimpleName();
    private AlertDialog alert;

    LinearLayout linearPhone,linearEmail;
    EditText editTextName, editTextEmail, editTextPhone, editTextMessage;
    Button buttonSubmit;
    ProgressDialog progress;


    public ContactUs() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        getActivity().setTitle("Contact Us");
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_contact_us, container, false);
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {

        linearPhone=getActivity().findViewById(R.id.linearPhone);
        linearEmail=getActivity().findViewById(R.id.linearEmail);

        linearEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                try{
                    Intent intent = new Intent (Intent.ACTION_VIEW , Uri.parse("mailto:" + "support@dsysolutions.co.in"));
                    intent.putExtra(Intent.EXTRA_SUBJECT, "");
                    intent.putExtra(Intent.EXTRA_TEXT, "");
                    startActivity(intent);
                }catch(ActivityNotFoundException e){
                    //TODO smth
                }

            }
        });

        linearPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:9980543595"));
                startActivity(intent);


            }
        });


        editTextName = getActivity().findViewById(R.id._contact_name);
        editTextEmail = getActivity().findViewById(R.id._contact_email);
        editTextPhone = getActivity().findViewById(R.id._contact_phone);
        editTextMessage = getActivity().findViewById(R.id._contact_description);
        buttonSubmit = getActivity().findViewById(R.id.btn_submit);
        buttonSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String mEmail = editTextEmail.getText().toString().trim();
                String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";


                if (editTextName.getText().length() == 0) {
                    Toast.makeText(getActivity(), "Enter Name", Toast.LENGTH_SHORT).show();
                } else if (editTextEmail.getText().length() == 0) {
                    Toast.makeText(getActivity(), "Enter E-mail", Toast.LENGTH_SHORT).show();
                } else if (editTextPhone.getText().toString().length() < 10) {
                    Toast.makeText(getActivity(), "Enter Phone", Toast.LENGTH_SHORT).show();
                } else if (!mEmail.matches(emailPattern)) {
                    Toast.makeText(getActivity(), "Enter E-mail", Toast.LENGTH_SHORT).show();
//                } else if (AppGlobal.checkPhoneNumber(editTextPhone.getText().toString().trim())) {
//                    Toast.makeText(getActivity(), "Enter Phone", Toast.LENGTH_SHORT).show();
                } else if (editTextMessage.getText().length() == 0) {
                    Toast.makeText(getActivity(), "Enter Message", Toast.LENGTH_SHORT).show();
                } else {
                    if (AppUtils.internetConnectionAvailable(2000)) {
                        submitContact();
                    } else {
                        Toast.makeText(getActivity(), "No Network Connection",
                                Toast.LENGTH_SHORT).show();
                    }

                }

            }
        });


        super.onActivityCreated(savedInstanceState);
    }


    public void submitContact() {
        progress = ProgressDialog.show(getActivity(), "", "Please wait...", false, false);
        String mName = editTextName.getText().toString().trim();
        String mEmail = editTextEmail.getText().toString().trim();
        String mPhone = editTextPhone.getText().toString().trim();
        String mMessage = editTextMessage.getText().toString().trim();

        SessionManager sessionManager = new SessionManager(getActivity());
        HashMap<String, String> hashMap = sessionManager.getUserDetails();
        String mSessionId = hashMap.get(SessionManager.KEY_SESSION_ID);
        String mMobile = hashMap.get(SessionManager.KEY_PHONE_NUMBER);

        HashMap<String, String> params = new HashMap<String, String>();
        params.put("merchantId", Preference.getDefaults(PreferenceConstants.MERCHANT_ID, getActivity()));
        params.put("contactUsName", mName);
        params.put("contactUsEmail", mEmail);
        params.put("contactUsMobileNo", mPhone);
        params.put("contactUsMessage", mMessage);
        params.put("sessionId", mSessionId);
        params.put("mobileNumber", mMobile);



        Log.d(TAG, "params: " + params);

        JsonObjectRequest request_json = new JsonObjectRequest(EndPoint.SEND_CONTACT_US, new JSONObject(params),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, "ContactResponse: " + response);
                        progress.dismiss();
                        try {
                            int mStatusCode = response.getInt(WebServiceConstants.EXISTING_RESPONSE_CODE);
                            if (mStatusCode == 200) {
                                Message("Submit Successful");
                            } else if (mStatusCode == 400) {
                                Message("Submit Failed. Please Retry..");
                            } else {
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                try {
                    progress.dismiss();
                } catch (NullPointerException e) {
                    Toast.makeText(getActivity(), "Unable Reach Remote Server. Please try later..",
                            Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                } catch (IllegalStateException e) {
                    Toast.makeText(getActivity(), "Unable Reach Remote Server. Please try later..",
                            Toast.LENGTH_LONG).show();
                }
            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(request_json);
    }

    public void Message(String message) {
        LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
        View promptView = layoutInflater.inflate(R.layout.alert_message, null);
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setView(promptView);
        TextView textViewMessage = promptView.findViewById(R.id.alert_message_display);
        textViewMessage.setText(message);
        Button buttonblock = (Button) promptView.findViewById(R.id.alert_ok_buton);
        buttonblock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alert.dismiss();
            }
        });
        alert = alertDialogBuilder.create();
        alert.show();
    }


}
