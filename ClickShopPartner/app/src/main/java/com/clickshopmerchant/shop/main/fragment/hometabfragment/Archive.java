package com.clickshopmerchant.shop.main.fragment.hometabfragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.clickshopmerchant.shop.R;
import com.clickshopmerchant.shop.appconfig.AppUtils;
import com.clickshopmerchant.shop.appconfig.EndPoint;
import com.clickshopmerchant.shop.appconfig.PreferenceConstants;
import com.clickshopmerchant.shop.appconfig.SessionManager;
import com.clickshopmerchant.shop.appconfig.WebServiceConstants;
import com.clickshopmerchant.shop.main.adapter.ArchivedCardAdapter;
import com.clickshopmerchant.shop.main.model.ArchivedPo;
import com.clickshopmerchant.shop.storage.Preference;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Archive extends Fragment implements SwipeRefreshLayout.OnRefreshListener, ArchivedOrderRefresh {
    private static final String TAG = Archive.class.getSimpleName();
    private RelativeLayout relativeLayoutLoading, noOrders;
    private SwipeRefreshLayout swipeRefreshLayout;
    private RecyclerView recyclerView;
    private List<ArchivedPo> allItemlist;
    private ArchivedCardAdapter adapter;
    private LinearLayoutManager linearLayoutManager;
    public boolean visible = true;
    AlertDialog alert;
    SessionManager sessionManager;
    String mSessionId, mMobile;

    public Archive() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setUserVisibleHint(false);

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_archive, container, false);
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {

        Views();
        if (AppUtils.internetConnectionAvailable(2000)) {
            getMerchantOrder();
        } else {
            noOrders.setVisibility(View.VISIBLE);
            relativeLayoutLoading.setVisibility(View.GONE);
            swipeRefreshLayout.setVisibility(View.GONE);
            swipeRefreshLayout.setRefreshing(false);
            Toast.makeText(getActivity(), "No Network Connection",
                    Toast.LENGTH_SHORT).show();
        }

        super.onActivityCreated(savedInstanceState);
    }


    @Override
    public void setMenuVisibility(final boolean visible) {
        super.setMenuVisibility(visible);
        if (visible) {



            if (allItemlist != null) {
                allItemlist.clear();
            }
            if (AppUtils.internetConnectionAvailable(2000)) {
                getMerchantOrder();
            } else {
                noOrders.setVisibility(View.VISIBLE);
                relativeLayoutLoading.setVisibility(View.GONE);
                swipeRefreshLayout.setVisibility(View.GONE);
                swipeRefreshLayout.setRefreshing(false);
                Toast.makeText(getActivity(), "No Network Connection",
                        Toast.LENGTH_SHORT).show();
            }
        }
    }


    @Override
    public void onRefresh() {
        if (allItemlist != null) {
            allItemlist.clear();
        }
        relativeLayoutLoading.setVisibility(View.GONE);
        if (AppUtils.internetConnectionAvailable(2000)) {
            getMerchantOrder();
        } else {
            noOrders.setVisibility(View.VISIBLE);
            relativeLayoutLoading.setVisibility(View.GONE);
            swipeRefreshLayout.setVisibility(View.GONE);
            swipeRefreshLayout.setRefreshing(false);
            Toast.makeText(getActivity(), "No Network Connection",
                    Toast.LENGTH_SHORT).show();
        }
    }


    public void archivedOrderRefresh() {
        if (allItemlist != null) {
            allItemlist.clear();
        }
        recyclerView.setVisibility(View.GONE);
        relativeLayoutLoading.setVisibility(View.GONE);
        if (AppUtils.internetConnectionAvailable(2000)) {
            getMerchantOrder();
        } else {
            noOrders.setVisibility(View.VISIBLE);
            relativeLayoutLoading.setVisibility(View.GONE);
            swipeRefreshLayout.setVisibility(View.GONE);
            swipeRefreshLayout.setRefreshing(false);
            Toast.makeText(getActivity(), "No Network Connection",
                    Toast.LENGTH_SHORT).show();
        }
    }


    public void Views() {
        noOrders = getActivity().findViewById(R.id._no_order_archivedd);
        noOrders.setVisibility(View.GONE);
        relativeLayoutLoading = getActivity().findViewById(R.id._loading_archived);
        swipeRefreshLayout = getActivity().findViewById(R.id.items_refresh_archived);
        swipeRefreshLayout.setOnRefreshListener(this);
        recyclerView = getActivity().findViewById(R.id.recyclerview_new_archived);
        linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(linearLayoutManager);
        allItemlist = new ArrayList<>();

    }


    public void Message(String message) {
        LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
        View promptView = layoutInflater.inflate(R.layout.alert_message, null);
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setView(promptView);
        TextView textViewMessage = promptView.findViewById(R.id.alert_message_display);
        textViewMessage.setText(message);
        Button buttonblock = (Button) promptView.findViewById(R.id.alert_ok_buton);
        buttonblock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alert.dismiss();
            }
        });
        alert = alertDialogBuilder.create();
        alert.show();
    }


    public void getMerchantOrder() {

        try {
            sessionManager = new SessionManager(getActivity());
            HashMap<String, String> hashMap = sessionManager.getUserDetails();
            mSessionId = hashMap.get(SessionManager.KEY_SESSION_ID);
            mMobile = hashMap.get(SessionManager.KEY_PHONE_NUMBER);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }


        HashMap<String, String> params = new HashMap<String, String>();
        params.put("merchantId", Preference.getDefaults(PreferenceConstants.MERCHANT_ID, getActivity()));
        params.put("sessionId", mSessionId);
        params.put("mobileNumber", mMobile);

        Log.d(TAG, "params: " + params);

        JsonObjectRequest request_json = new JsonObjectRequest(EndPoint.GET_MERCHANT_ORDER_ARCHIVE, new JSONObject(params),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        recyclerView.setVisibility(View.VISIBLE);
                        Log.d(TAG, "ARCHIVE Response: " + response);
                        try {
                            int mStatusCode = response.getInt(WebServiceConstants.EXISTING_RESPONSE_CODE);
                            if (mStatusCode == 200) {
                                relativeLayoutLoading.setVisibility(View.GONE);
                                swipeRefreshLayout.setVisibility(View.VISIBLE);
                                noOrders.setVisibility(View.GONE);
                                JSONArray array = response.optJSONArray("orderSearchList");
                                parseData(array);
                            } else if (mStatusCode == 400) {
                                relativeLayoutLoading.setVisibility(View.GONE);
                                noOrders.setVisibility(View.VISIBLE);
                            } else {
                                noOrders.setVisibility(View.VISIBLE);
                                relativeLayoutLoading.setVisibility(View.GONE);
                                swipeRefreshLayout.setVisibility(View.GONE);
                                swipeRefreshLayout.setRefreshing(false);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                try {
                    swipeRefreshLayout.setRefreshing(false);
                    relativeLayoutLoading.setVisibility(View.GONE);
                } catch (NullPointerException e) {
                    Toast.makeText(getActivity(), "Unable Reach Remote Server. Please try later..",
                            Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                } catch (IllegalStateException e) {
                    Toast.makeText(getActivity(), "Unable Reach Remote Server. Please try later..",
                            Toast.LENGTH_LONG).show();
                }
            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(request_json);
    }


    private void parseData(JSONArray array) {
        for (int i = 0; i < array.length(); i++) {
            ArchivedPo archivedPo = new ArchivedPo();
            JSONObject json;
            try {
                json = array.getJSONObject(i);
                if (json.getString("orderId") != null && !json.getString("orderId").isEmpty()
                        && !json.getString("orderId").equals(WebServiceConstants.NULL)) {
                    archivedPo.setOrderId(json.getString("orderId"));
                }

                if (json.getString("userId") != null && !json.getString("userId").isEmpty()
                        && !json.getString("userId").equals(WebServiceConstants.NULL)) {
                    archivedPo.setUserId((json.getInt("userId")));
                } else {
                    archivedPo.setUserId(0);
                }


                if (json.getString("userName") != null && !json.getString("userName").isEmpty()
                        && !json.getString("userName").equals(WebServiceConstants.NULL)) {
                    archivedPo.setUserName((json.getString("userName")));
                } else {
                    archivedPo.setUserName("None");
                }

                if (json.getString("mobileNumber") != null && !json.getString("mobileNumber").isEmpty()
                        && !json.getString("mobileNumber").equals(WebServiceConstants.NULL)) {
                    archivedPo.setMobileNumber((json.getString("mobileNumber")));
                } else {
                    archivedPo.setMobileNumber("None");
                }


                if (json.getString("shippingAddress") != null && !json.getString("shippingAddress").isEmpty()
                        && !json.getString("shippingAddress").equals(WebServiceConstants.NULL)) {
                    archivedPo.setShippingAddress((json.getString("shippingAddress")));
                } else {
                    archivedPo.setShippingAddress("None");
                }

                if (json.getString("paymentMode") != null && !json.getString("paymentMode").isEmpty()
                        && !json.getString("paymentMode").equals(WebServiceConstants.NULL)) {
                    archivedPo.setPaymentMode((json.getString("paymentMode")));
                } else {
                    archivedPo.setPaymentMode("None");
                }

                if (json.getString("orderAmount") != null && !json.getString("orderAmount").isEmpty()
                        && !json.getString("orderAmount").equals(WebServiceConstants.NULL)) {
                    archivedPo.setOrderAmount((json.getString("orderAmount")));
                } else {
                    archivedPo.setOrderAmount("Nill");
                }

                if (json.getString("merchantId") != null && !json.getString("merchantId").isEmpty()
                        && !json.getString("merchantId").equals(WebServiceConstants.NULL)) {
                    archivedPo.setMerchantId((json.getInt("merchantId")));
                } else {
                    archivedPo.setMerchantId(0);
                }

                if (json.getString("orderStatus") != null && !json.getString("orderStatus").isEmpty()
                        && !json.getString("orderStatus").equals(WebServiceConstants.NULL)) {
                    archivedPo.setOrderStatus((json.getString("orderStatus")));
                } else {
                    archivedPo.setOrderStatus("None");
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }
            allItemlist.add(archivedPo);
            // Collections.sort(recordsList);
        }
        adapter = new ArchivedCardAdapter(allItemlist, getActivity(), Archive.this);
        recyclerView.setAdapter(adapter);
        swipeRefreshLayout.setRefreshing(false);
        adapter.notifyDataSetChanged();
    }
}
