package com.clickshopmerchant.shop.places;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.ResultReceiver;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.clickshopmerchant.shop.R;
import com.clickshopmerchant.shop.appconfig.AppUtils;
import com.clickshopmerchant.shop.appconfig.GPSTracker;
import com.clickshopmerchant.shop.main.activity.ToFetchAddressIntentService;
import com.clickshopmerchant.shop.network.NetworkUtils;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class PlacesActivity extends AppCompatActivity implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        com.google.android.gms.location.LocationListener,
        OnMapReadyCallback {

    Toolbar toolbar;
    private String TAG = PlacesActivity.class.getSimpleName();
    private GoogleMap mMap;
    GoogleApiClient mGoogleApiClient;
    int mNullcheck;
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

    LinearLayout GreenPointer;
    LocationManager manager;
    TextView mLocationAddress;
    ImageView mLocationText;

    private static final int REQUEST_CODE_AUTOCOMPLETE = 1;
    Context mContext;
    private AddressResultReceiver mResultReceiver;
    String mFromLatitudePost;
    String mFromLongitudePost;
    String mToLatitudePost;
    String mToLongitudePost;
    private AlertDialog alert;

//////////////////////////////////////////////////////////////////////////////

    private LatLng mTOCenterLatLong;
    Location mLocations;
    Geocoder geocoder;
    List<Address> addresses;
    String postaddress, postcity, poststate, postcountry, postpinCode;
    Button buttonUseThisLocation;
    private double mLatitude;
    private double mLongitude;
    LatLng latLng;
    RelativeLayout mLayout;
    Location locationJ;
    double latitudes;
    double longitudes;

    ProgressDialog progress;

    GPSTracker gpsTracker;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_places);

        // Showing Status bar
        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }

        toolbar = findViewById(R.id._place_activity_toolbar);
        setSupportActionBar(toolbar);
        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_arrow_back_black_24dp));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });




        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();






        MapFragment mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.frag_map);
        mapFragment.getMapAsync(this);

        gpsTracker = new GPSTracker(getApplicationContext());
        // Start Pointer
        GreenPointer = findViewById(R.id.locationMarker);
////////////////////////////////////////////////////////////////////////////////////////////////////

        try {
            manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(myIntent);

            } else {
                mapFragment.getMapAsync(this);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

////////////////////////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////////////////////////////

        // Views for From Address

        mLocationAddress = findViewById(R.id.Frag_Address);
        mLocationText = findViewById(R.id.Frag_Locality);
        mLocationText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (NetworkUtils.isInternetAvailable(true, getApplicationContext())) {
                    openAutocompleteActivity();
                }

            }
        });


        mLayout = findViewById(R.id.layout_FromAdress);
        mLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (NetworkUtils.isInternetAvailable(true, getApplicationContext())) {
                    openAutocompleteActivity();
                }

            }
        });


        buttonUseThisLocation = findViewById(R.id._button_use_this_location);
        buttonUseThisLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mLocationAddress.getText().length() == 0) {
                    Message("You Have Not Selected Any Location. Please Select and Then Proceed.");
                } else {

                        Intent returnIntent = new Intent();
                        returnIntent.putExtra("latitude", String.valueOf(mLatitude));
                        returnIntent.putExtra("longitude", String.valueOf(mLongitude));
                        setResult(50, returnIntent);
                        finish();

                }
            }
        });


        mapFragment.getMapAsync(this);
        mResultReceiver = new AddressResultReceiver(new Handler());

//        if (checkPlayServices()) {
//            // If this check succeeds, proceed with normal processing.
//            // Otherwise, prompt user to get valid Play Services APK.
//            if (!AppUtils.isLocationEnabled(mContext)) {
//                // notify user
//                AlertDialog.Builder dialog = new AlertDialog.Builder(this);
//                dialog.setMessage("Location not enabled!");
//                dialog.setPositiveButton("Open location settings", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
//                        Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
//                        startActivity(myIntent);
//                    }
//                });
//                dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
//
//                    @Override
//                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
//                        // TODO Auto-generated method stub
//
//                    }
//                });
//                dialog.show();
//            }
//            buildGoogleApiClient();
//        } else {
//            Toast.makeText(mContext, "Location not supported in this device", Toast.LENGTH_SHORT).show();
//        }

        if (NetworkUtils.isNetworkAvailable(getApplicationContext())) {
            new FetchLocationTask().execute();
        }


        RelativeLayout relativeLayoutMyLocationButton = findViewById(R.id._current_location_button);
        relativeLayoutMyLocationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (NetworkUtils.isNetworkAvailable(getApplicationContext())) {
                    new FetchLocationTask().execute();
                }

            }
        });
    }


//
//    private void getMyLocation() {
//        Criteria criteria = new Criteria();
//        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
//        String provider = locationManager.getBestProvider(criteria, true);
//        try {
//            locationJ = locationManager.getLastKnownLocation(provider);
//        } catch (SecurityException e) {
//            e.printStackTrace();
//        }
//        try {
//            latitudes = locationJ.getLatitude();
//            longitudes = locationJ.getLongitude();
//        } catch (NullPointerException e) {
//            e.printStackTrace();
//        }
//        LatLng latLng = new LatLng(latitudes, longitudes);
//        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 18);
//        mMap.animateCamera(cameraUpdate);
//        try {
//            locationJ = locationManager.getLastKnownLocation(provider);
//        } catch (SecurityException e) {
//            e.printStackTrace();
//        }
//    }











    public class FetchLocationTask extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            progress = ProgressDialog.show(PlacesActivity.this, "", "Please wait...", false, false);

            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... params) {
            try {
                geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
                Criteria criteria = new Criteria();
                LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
                String provider = locationManager.getBestProvider(criteria, true);
                Location location = locationManager.getLastKnownLocation(provider);
//                latitude = location.getLatitude();
//                longitude = location.getLongitude();



                // Getting Location through GPS Tracker
                if (!gpsTracker.canGetLocation()) {

                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            // this will run in the main thread
                            showSettingsAlert("GPS is Not Enabled. Do You Want To Go To Settings Menu?");
                        }
                    });

                } else {
                    mLatitude = gpsTracker.getLatitude();
                    mLongitude = gpsTracker.getLongitude();
                }





                Log.d(TAG, "postlatitude: " + mLatitude);
                Log.d(TAG, "postlongitude: " + mLongitude);

                addresses = geocoder.getFromLocation(mLatitude, mLongitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                if (addresses != null) {
                    postaddress = addresses.get(0).getAddressLine(0);
                    postcity = addresses.get(0).getLocality();
                    poststate = addresses.get(0).getAdminArea();
                    postcountry = addresses.get(0).getCountryName();
                    postpinCode = addresses.get(0).getPostalCode();
                }
            } catch (SecurityException e) {
                progress.dismiss();
                mNullcheck = 1;
                e.printStackTrace();
            } catch (IndexOutOfBoundsException e) {
                mNullcheck = 1;
                e.printStackTrace();
                progress.dismiss();
            } catch (NullPointerException e) {
                mNullcheck = 1;
                e.printStackTrace();
                progress.dismiss();
            } catch (IOException e) {
                mNullcheck = 1;
                e.printStackTrace();
                progress.dismiss();
            }

            return "";
        }

        @Override
        protected void onPostExecute(String s) {
            progress.dismiss();
            if (mNullcheck == 1) {
                Message("Unable To Fetch Your Current Location. Please Retry..");
            }else {
                LatLng latLng = new LatLng(mLatitude, mLongitude);
                CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 18);
                mMap.animateCamera(cameraUpdate);

            }
        }
    }














    public void showSettingsAlert(String message) {
        LayoutInflater layoutInflater = LayoutInflater.from(this);
        View promptView = layoutInflater.inflate(R.layout.showsettingalert, null);
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setView(promptView);
        TextView textViewMessage = promptView.findViewById(R.id.alert_message_display);
        textViewMessage.setText(message);
        Button buttonblock = promptView.findViewById(R.id.alert_ok_buton);
        buttonblock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
                alert.dismiss();
            }
        });

        Button buttonbcancel = promptView.findViewById(R.id.alert_cancel_buton);
        buttonbcancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                alert.dismiss();
            }
        });

        alert = alertDialogBuilder.create();
        alert.show();
    }


















    public void Message(String message) {
        LayoutInflater layoutInflater = LayoutInflater.from(this);
        View promptView = layoutInflater.inflate(R.layout.alert_message, null);
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setView(promptView);
        TextView textViewMessage = promptView.findViewById(R.id.alert_message_display);
        textViewMessage.setText(message);
        Button buttonblock = (Button) promptView.findViewById(R.id.alert_ok_buton);
        buttonblock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alert.dismiss();
            }
        });
        alert = alertDialogBuilder.create();
        alert.show();
    }


    @Override
    public void onStart() {
        super.onStart();
        try {
            mGoogleApiClient.connect();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        Log.d(TAG, "OnMapReady");
        mMap = googleMap;


        if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        try {
            if (mMap != null) {
                //Map Rotate Gesture
                mMap.getUiSettings().setRotateGesturesEnabled(true);
                //My Location Button
                mMap.getUiSettings().setMyLocationButtonEnabled(false);
                //Compass Functionality
                mMap.getUiSettings().setCompassEnabled(true);
                //Zooming Functionality
                mMap.getUiSettings().setZoomGesturesEnabled(true);
                //Zooming Buttons
                mMap.getUiSettings().setZoomControlsEnabled(true);
                //Showing Current Location
                mMap.setMyLocationEnabled(true);
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(mLatitude, mLongitude), 15.0f));
            } else {
                Toast.makeText(getApplicationContext(), "Sorry! unable to create maps", Toast.LENGTH_SHORT).show();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }


        mMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
            @Override
            public void onCameraChange(CameraPosition cameraPosition) {
                GreenPointer.setVisibility(View.VISIBLE);
                mTOCenterLatLong = cameraPosition.target;
                try {
                    mLocations = new Location("");
                    mLocations.setLatitude(mTOCenterLatLong.latitude);
                    mLocations.setLongitude(mTOCenterLatLong.longitude);

                    startIntentService(mLocations);
//                    mLocationMarkerText.setText("Lat : " + mCenterLatLong.latitude + "," + "Long : " + mCenterLatLong.longitude);

                } catch (Exception e) {
                    e.printStackTrace();
                }

                latLng = new LatLng(mLocations.getLatitude(), mLocations.getLongitude());
                Log.d(TAG, "latLng: " + latLng);

                mLatitude = mLocations.getLatitude();
                mLongitude = mLocations.getLongitude();

//                // SAVING FOR POST REQUEST = CURRENT LOACATION
//                mToLatitudePost = String.valueOf(latLng.latitude);
//                mToLongitudePost = String.valueOf(latLng.longitude);


                geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());

                if (latLng != null) {
                    try {
                        try {
                            addresses = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        if (addresses != null) {
                            postaddress = addresses.get(0).getAddressLine(0);
                            postcity = addresses.get(0).getLocality();
                            poststate = addresses.get(0).getAdminArea();
                            postcountry = addresses.get(0).getCountryName();
                            postpinCode = addresses.get(0).getPostalCode();
                            mLatitude = addresses.get(0).getLatitude();
                            mLongitude = addresses.get(0).getLongitude();
                            mLocationAddress.setText(postaddress);
                        }
                    } catch (IndexOutOfBoundsException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }


    private void openAutocompleteActivity() {

        try {
            // The autocomplete activity requires Google Play Services to be available. The intent
            // builder checks this and throws an exception if it is not the case.
            Intent intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                    .build(this);
            startActivityForResult(intent, REQUEST_CODE_AUTOCOMPLETE);
        } catch (GooglePlayServicesRepairableException e) {
            // Indicates that Google Play Services is either not installed or not up to date. Prompt
            // the user to correct the issue.
            GoogleApiAvailability.getInstance().getErrorDialog(this, e.getConnectionStatusCode(),
                    0 /* requestCode */).show();
        } catch (GooglePlayServicesNotAvailableException e) {
            // Indicates that Google Play Services is not available and the problem is not easily
            // resolvable.
            String message = "Google Play Services is not available: " +
                    GoogleApiAvailability.getInstance().getErrorString(e.errorCode);

            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        }

    }


    /**
     * Receiver for data sent from FetchAddressIntentService.
     */
    @SuppressLint("ParcelCreator")
    class AddressResultReceiver extends ResultReceiver {
        public AddressResultReceiver(Handler handler) {
            super(handler);
        }

        /**
         * Receives data sent from FetchAddressIntentService and updates the UI in MainActivity.
         */
        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {

            //      Log.d(TAG, "mButtonCountForToAddress " + mButtonCountForToAddress);


//                // Display the address string or an error message sent from the intent service.
//                ToAddressOutput = resultData.getString(AppUtils.LocationConstants.RESULT_DATA_KEY);
//                ToAreaOutput = resultData.getString(AppUtils.LocationConstants.LOCATION_DATA_AREA);
//                ToCityOutput = resultData.getString(AppUtils.LocationConstants.LOCATION_DATA_CITY);
//                ToStateOutput = resultData.getString(AppUtils.LocationConstants.LOCATION_DATA_STREET);
//
//                Log.d(TAG, "ToAddressOutput " + ToAddressOutput);
//                Log.d(TAG, "ToAreaOutput " + ToAreaOutput);
//                Log.d(TAG, "ToCityOutput " + ToCityOutput);
//                Log.d(TAG, "ToStateOutput " + ToStateOutput);


            // Show a toast message if an address was found.
            if (resultCode == AppUtils.LocationConstants.SUCCESS_RESULT) {
                //  showToast(getString(R.string.address_found));

            }


        }
    }

    /**
     * Creates an intent, adds location data to it as an extra, and starts the intent service for
     * fetching an address.
     */
    protected void startIntentService(Location mLocation) {

        // Create an intent for passing to the intent service responsible for fetching the address.
        Intent intent = new Intent(PlacesActivity.this, ToFetchAddressIntentService.class);

        // Pass the result receiver as an extra to the service.
        intent.putExtra(AppUtils.LocationConstants.RECEIVER, mResultReceiver);

        // Pass the location data as an extra to the service.
        intent.putExtra(AppUtils.LocationConstants.LOCATION_DATA_EXTRA, mLocation);

        // Start the service. If the service isn't already running, it is instantiated and started
        // (creating a process for it if needed); if it is running then it remains running. The
        // service kills itself automatically once all intents are processed.
        startService(intent);

    }


    /**
     * Called after the autocomplete activity has finished to return its result.
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        // DISABLE FROM ADDRESS BUTTON
        mLocationText.setEnabled(false);
        // Check that the result was from the autocomplete widget.
        if (requestCode == REQUEST_CODE_AUTOCOMPLETE) {
            if (resultCode == Activity.RESULT_OK) {
                // Get the user's selected place from the Intent.
                Place place = PlaceAutocomplete.getPlace(getApplicationContext(), data);
                LatLng latLong = place.getLatLng();
                mLatitude = latLong.latitude;
                mLongitude = latLong.longitude;
                try {
                    try {
                        addresses = geocoder.getFromLocation(latLong.latitude, latLong.longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    if (addresses != null) {
                        postaddress = addresses.get(0).getAddressLine(0);
                        mLocationAddress.setText(postaddress);
                    }
                } catch (IndexOutOfBoundsException e) {
                    e.printStackTrace();
                }


                Log.d(TAG, "Fetching To Address from Selected Pointer : " + place.getName() + "");

                //mLocationText.setText(place.getName() + "");

                CameraPosition cameraPosition = new CameraPosition.Builder()
                        .target(latLong).zoom(15f).tilt(40).build();

                if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                mMap.setMyLocationEnabled(true);
                mMap.animateCamera(CameraUpdateFactory
                        .newCameraPosition(cameraPosition));

                //   displayServiceProvidersMap();

            }
        } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
            Status status = PlaceAutocomplete.getStatus(getApplicationContext(), data);
        } else if (resultCode == Activity.RESULT_CANCELED) {
            // Indicates that the activity closed before a selection was made. For example if
            // the user pressed the back button.
        }


        ////////////////////////////////////////////////////////////////////////////////////////////
    }


    @Override
    public void onConnected(Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        Location mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient);
        if (mLastLocation != null) {
            changeMap(mLastLocation);
            Log.d(TAG, "ON connected");

        } else
            try {
                LocationServices.FusedLocationApi.removeLocationUpdates(
                        mGoogleApiClient, this);

            } catch (Exception e) {
                e.printStackTrace();
            }
        try {
            LocationRequest mLocationRequest = new LocationRequest();
            mLocationRequest.setInterval(10000);
            mLocationRequest.setFastestInterval(5000);
            mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            LocationServices.FusedLocationApi.requestLocationUpdates(
                    mGoogleApiClient, mLocationRequest, this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.i(TAG, "Connection suspended");
        mGoogleApiClient.connect();
    }

    @Override
    public void onLocationChanged(Location location) {
        try {
            if (location != null)
                changeMap(location);
            LocationServices.FusedLocationApi.removeLocationUpdates(
                    mGoogleApiClient, this);

        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }


    @Override
    public void onStop() {
        super.onStop();

        try {
        } catch (RuntimeException e) {
            e.printStackTrace();
        }
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }


    private void changeMap(Location location) {

        Log.d(TAG, "Reaching map" + mMap);
        if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        // check if map is created successfully or not
        if (mMap != null) {
            mMap.getUiSettings().setZoomControlsEnabled(false);
            LatLng latLong;

            latLong = new LatLng(location.getLatitude(), location.getLongitude());

            mLatitude = latLong.latitude;
            mLongitude = latLong.longitude;


            // SAVING FOR POST REQUEST = CURRENT LOACATION
            mFromLatitudePost = String.valueOf(latLong.latitude);
            mFromLongitudePost = String.valueOf(latLong.longitude);


            try {
                try {
                    addresses = geocoder.getFromLocation(latLong.latitude, latLong.longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                } catch (IOException e) {
                    e.printStackTrace();
                }

                if (addresses != null) {
                    postaddress = addresses.get(0).getAddressLine(0);
                    mLatitude = addresses.get(0).getLatitude();
                    mLongitude = addresses.get(0).getLongitude();
//                        postcity = addresses.get(0).getLocality();
//                        poststate = addresses.get(0).getAdminArea();
//                        postcountry = addresses.get(0).getCountryName();
//                        postpinCode = addresses.get(0).getPostalCode();

                    mLocationAddress.setText(postaddress);


                }
            } catch (IndexOutOfBoundsException e) {
                e.printStackTrace();
            }


            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(latLong).zoom(14f).tilt(20).build();

            mMap.setMyLocationEnabled(true);
            mMap.setTrafficEnabled(true);
            mMap.getUiSettings().setMyLocationButtonEnabled(true);
            mMap.animateCamera(CameraUpdateFactory
                    .newCameraPosition(cameraPosition));
            startIntentService(location);

        } else {
            Toast.makeText(getApplicationContext(), "Sorry! unable to create maps", Toast.LENGTH_SHORT).show();
        }

    }


}
