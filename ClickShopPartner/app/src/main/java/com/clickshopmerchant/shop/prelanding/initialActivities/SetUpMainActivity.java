package com.clickshopmerchant.shop.prelanding.initialActivities;
import android.app.ProgressDialog;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;
import com.clickshopmerchant.shop.R;
import com.clickshopmerchant.shop.appconfig.ApiClient;
import com.clickshopmerchant.shop.appconfig.ApiInterface;
import com.clickshopmerchant.shop.appconfig.AppUtils;
import com.clickshopmerchant.shop.appconfig.SessionManager;
import com.clickshopmerchant.shop.main.model.signout.RequestBodySignOut;
import com.clickshopmerchant.shop.main.model.signout.SignOut;
import com.clickshopmerchant.shop.prelanding.setupFragments.SetUpFragment;
import java.util.HashMap;
import retrofit2.Call;
import retrofit2.Callback;

public class SetUpMainActivity extends AppCompatActivity {
    private static ProgressDialog progress;
    Boolean doubleBackToExitPressedOnce = false;
    Fragment fragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_store_set_up);
        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }
        Toolbar toolbar = findViewById(R.id.setup_toolbar);
        setSupportActionBar(toolbar);
        SetUpFragment();
    }

    public void SetUpFragment() {
        fragment = new SetUpFragment();
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.addToBackStack("SetUpFragment");
        ft.replace(R.id.content_frame, fragment);
        ft.commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.setup_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_logout) {
            if (AppUtils.internetConnectionAvailable(2000)) {
                progress = ProgressDialog.show(this, "Logging Out...", "Please wait...", false, false);
                sessionLogOut();
            } else {
                Toast.makeText(getApplicationContext(), "No Network Connection",
                        Toast.LENGTH_SHORT).show();
            }

        }
        return super.onOptionsItemSelected(item);
    }

    public void sessionLogOut() {
        SessionManager sessionManager = new SessionManager(getApplicationContext());
        HashMap<String, String> hashMap = sessionManager.getUserDetails();
        String mPhone = hashMap.get(SessionManager.KEY_PHONE_NUMBER);
        ApiInterface apiInterfacec = ApiClient.getApiClient().create(ApiInterface.class);
        Call<SignOut> call = apiInterfacec.signOut(new RequestBodySignOut(mPhone));
        call.enqueue(new Callback<SignOut>() {
            @Override
            public void onResponse(Call<SignOut> call, retrofit2.Response<SignOut> response) {
                if (response.isSuccessful()) {
                    Toast.makeText(getApplicationContext(), "Successfully Logged Out",
                            Toast.LENGTH_SHORT).show();
                    progress.dismiss();
                    SessionManager.logoutUser();
                    finish();
                }else {
                    Toast.makeText(getApplicationContext(), "Successfully Logged Out\"",
                            Toast.LENGTH_SHORT).show();

                    progress.dismiss();
                    SessionManager.logoutUser();
                    finish();
                }
            }

            @Override
            public void onFailure(Call<SignOut> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Successfully Logged Out\"",
                        Toast.LENGTH_SHORT).show();
                progress.dismiss();
                SessionManager.logoutUser();
                finish();
            }
        });
    }

    @Override
    public void onBackPressed() {
        int fragments = getSupportFragmentManager().getBackStackEntryCount();
        if (fragments == 1) {
            if (doubleBackToExitPressedOnce) {
                System.exit(0);
                return;
            }
            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this, "Please click BACK again to exit",
                    Toast.LENGTH_SHORT).show();
        } else {
            if (getFragmentManager().getBackStackEntryCount() > 1) {
                getFragmentManager().popBackStack();
            } else {
                super.onBackPressed();
            }

        }
    }

}
