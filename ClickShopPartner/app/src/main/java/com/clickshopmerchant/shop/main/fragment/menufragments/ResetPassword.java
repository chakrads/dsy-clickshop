package com.clickshopmerchant.shop.main.fragment.menufragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.clickshopmerchant.shop.R;
import com.clickshopmerchant.shop.appconfig.ApiClient;
import com.clickshopmerchant.shop.appconfig.ApiInterface;
import com.clickshopmerchant.shop.appconfig.AppUtils;
import com.clickshopmerchant.shop.appconfig.PreferenceConstants;
import com.clickshopmerchant.shop.appconfig.SessionManager;
import com.clickshopmerchant.shop.main.model.resetpassword.RequestBodyResetPassword;
import com.clickshopmerchant.shop.main.model.resetpassword.ResetPasswords;
import com.clickshopmerchant.shop.storage.Preference;
import com.google.gson.Gson;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;


public class ResetPassword extends Fragment {
    EditText editTextExistingPassword, editTextNewPassword, editTextConfirmPassword;
    Button buttonSubmit;

    public ResetPassword() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getActivity().setTitle("Reset Password");
        return inflater.inflate(R.layout.fragment_reset_password, container, false);
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {


        editTextExistingPassword = getActivity().findViewById(R.id._enter_exisitng_password);
        editTextNewPassword = getActivity().findViewById(R.id._enter_new_password);
        editTextConfirmPassword = getActivity().findViewById(R.id._enter_confirm_pas);
        buttonSubmit = getActivity().findViewById(R.id.btn_submit);
        buttonSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editTextExistingPassword.getText().length() == 0) {
                    Toast.makeText(getActivity(), "Enter Existing Password",
                            Toast.LENGTH_SHORT).show();
                } else if (editTextNewPassword.getText().length() == 0) {
                    Toast.makeText(getActivity(), "Enter New Password",
                            Toast.LENGTH_SHORT).show();
                } else if (editTextConfirmPassword.getText().length() == 0) {
                    Toast.makeText(getActivity(), "Enter Confirm Password",
                            Toast.LENGTH_SHORT).show();
                } else {
                    if (AppUtils.internetConnectionAvailable(2000)) {
                        resetPassword();
                    } else {
                        Toast.makeText(getActivity(), "No Network Connection",
                                Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });


        super.onActivityCreated(savedInstanceState);
    }


    public void resetPassword() {
        String mExistingPassword = editTextExistingPassword.getText().toString();
        String mNewPassword = editTextNewPassword.getText().toString();
        String mConfirmPassword = editTextConfirmPassword.getText().toString();
        SessionManager sessionManager = new SessionManager(getActivity());
        final HashMap<String, String> hashMap = sessionManager.getUserDetails();
        String mSessionId = hashMap.get(SessionManager.KEY_SESSION_ID);
        String mPhoneNo = hashMap.get(SessionManager.KEY_PHONE_NUMBER);
        String mMerchantId = Preference.getDefaults(PreferenceConstants.MERCHANT_ID, getActivity());

        final ApiInterface apiInterfacec = ApiClient.getApiClient().create(ApiInterface.class);
        Call<ResetPasswords> call = apiInterfacec.resetPassword(new RequestBodyResetPassword(mMerchantId, mPhoneNo, mSessionId, mExistingPassword, mNewPassword, mConfirmPassword));
        call.enqueue(new Callback<ResetPasswords>() {
            @Override
            public void onResponse(Call<ResetPasswords> call, final retrofit2.Response<ResetPasswords> response) {
                Log.e("TAG", "PasswordReset: " + new Gson().toJson(response.body()));

                if (response.isSuccessful()) {
                    if (response.body().getResponseCode() == 200) {


                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                        alertDialogBuilder.setMessage("Password Updated Successfully");



                        AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();


                    } else {

                        AlertDialog alertDialog = null;
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                        alertDialogBuilder.setMessage("Password Update Failed");

                         alertDialog = alertDialogBuilder.create();
                        alertDialog.show();

                    }
                } else {
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                    alertDialogBuilder.setMessage("Password Update Failed");

                    AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.show();

                }
            }
            @Override
            public void onFailure(Call<ResetPasswords> call, Throwable t) {
                Toast.makeText(getActivity(), t.toString(),
                        Toast.LENGTH_SHORT).show();
            }
        });


    }


}
