package com.clickshopmerchant.shop.main.model

class OrderItems {


    var orderId: String? = null
    var itemId: Int? = 0;
    var itemName: String? = null
    var itemImage: String? = null
    var itemPrice: String? = null
    var itemQuantity: String? = null
    var totalPrice: String? = null
    var itemWeight: String? = null


}