package com.clickshopmerchant.shop.main.model.resetpassword;

public class RequestBodyResetPassword {

    String mobileNumber, sessionId,  oldPassword, newPassword, confirmPassword, merchantId;

    public RequestBodyResetPassword(String merchantId,  String mobileNumber, String sessionId, String oldPassword,
                                    String newPassword, String confirmPassword) {
        this.merchantId = merchantId;
        this.mobileNumber = mobileNumber;
        this.sessionId = sessionId;
        this.oldPassword = oldPassword;
        this.newPassword = newPassword;
        this.confirmPassword = confirmPassword;

    }
}
