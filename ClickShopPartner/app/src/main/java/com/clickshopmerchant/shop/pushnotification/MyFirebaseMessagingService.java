package com.clickshopmerchant.shop.pushnotification;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import org.json.JSONObject;
import java.util.HashMap;
import java.util.Map;

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = MyFirebaseMessagingService.class.getSimpleName();
    Context mContext;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);



        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().toString());

            String title = remoteMessage.getNotification().getTitle();
            String body = remoteMessage.getNotification().getBody();


                Map<String, String> params = new HashMap<>();
                params.put("body", body);


                JSONObject object = new JSONObject(params);
                Log.e("body", object.toString());





            // This broadcast is sent to NewOrder.class fragment to refresh the New Order received page
            MyNotificationManager.getInstance(getApplicationContext()).displayNotification(title, body);
            Intent intent = new Intent("broadcast_notifiy");
            intent.putExtra("key", "value");
            LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);

        }

















    }


}
