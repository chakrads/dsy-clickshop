package com.clickshopmerchant.shop.prelanding.initialActivities;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.clickshopmerchant.shop.R;
import com.clickshopmerchant.shop.appconfig.AppUtils;
import com.clickshopmerchant.shop.appconfig.EndPoint;
import com.clickshopmerchant.shop.appconfig.PreferenceConstants;
import com.clickshopmerchant.shop.appconfig.WebServiceConstants;
import com.clickshopmerchant.shop.pushnotification.SharedPrefManager;
import com.clickshopmerchant.shop.storage.Preference;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.HashMap;


public class RegistrationActivity extends AppCompatActivity {
    EditText editTextPhoneNo, editTextGst, editTextStoreNo, editTextFirstName, editTextLastName;
    Button buttonSubmit;
    private ProgressDialog progress;
    AlertDialog alert;
    private static final String TAG = RegistrationActivity.class.getSimpleName();
    TextView corbelTextViewCheck;
    CheckBox checkBoxTermsnConditions;
    String mChecked;
    EditText editTextEmail, editTextPassword, editTextReentryPassword;
    String mPass;
    String mPassConfirm;
    SharedPrefManager sharedPrefManager;
    private TelephonyManager telephonyManager;
    private String deviceId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(getResources().getColor(R.color.colorPrimary));
        }

        sharedPrefManager = SharedPrefManager.getInstance(getApplicationContext());
        initViews();
        FirebaseMessaging.getInstance().subscribeToTopic("topic");
        try {
            String token = FirebaseInstanceId.getInstance().getToken();
            Log.d("TOKEN", token);
        }catch (NullPointerException e){ e.printStackTrace(); }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }


    public void initViews() {
        editTextPhoneNo = findViewById(R.id.registration_phone);
        editTextGst = findViewById(R.id.registration_gst_no);
        editTextStoreNo = findViewById(R.id.registration_store_no);
        editTextFirstName = findViewById(R.id.registration_firstname);
        editTextLastName = findViewById(R.id.registration_lastname);
        checkBoxTermsnConditions = findViewById(R.id._checkbox_terms_conditions);
        editTextEmail = findViewById(R.id.registration_email);
        editTextPassword = findViewById(R.id.registration_password);
        editTextReentryPassword = findViewById(R.id.registration_confirt_password);
        corbelTextViewCheck = findViewById(R.id.check_terms_cond);
        corbelTextViewCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentReg = new Intent(RegistrationActivity.this,
                        TermsAndConditions.class);
                startActivity(intentReg);
            }
        });
        buttonSubmit = findViewById(R.id.button_sign_up);
        buttonSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

                mPass = editTextPassword.getText().toString().trim();
                mPassConfirm = editTextReentryPassword.getText().toString().trim();
                Log.d(TAG, "mPass: " + mPass);
                Log.d(TAG, "mPassConfirm: " + mPassConfirm);
                mChecked = "N";
                if (checkBoxTermsnConditions.isChecked()) {
                    mChecked = "Y";
                }
                if (editTextFirstName.getText().toString().length() == 0) {
                    Message("Enter First Name");
                } else if (editTextLastName.getText().toString().length() == 0) {
                    Message("Enter Last Name");
                } else if (editTextPhoneNo.getText().toString().length() < 10) {
                    Message("Enter Valid Phone No.");
                } else if (editTextPhoneNo.getText().toString().length() == 0) {
                    Message("Enter Phone No.");
                } else if (editTextEmail.getText().toString().length() == 0) {
                    Message("Enter Email");
                } else if (!editTextEmail.getText().toString().matches(emailPattern)) {
                    Toast.makeText(RegistrationActivity.this, "Enter E-mail", Toast.LENGTH_SHORT).show();
                } else if (editTextPassword.getText().toString().length() == 0) {
                    Message("Enter Password");
                } else if (editTextReentryPassword.getText().toString().length() == 0) {
                    Message("Re Enter Password");
                } else if (!mPass.equals(mPassConfirm)) {
                    Message("Password Do Not Match");
//                } else if (editTextGst.getText().toString().length() == 0) {
//                    Message("Enter GST No.");
//                } else if (editTextStoreNo.getText().toString().length() == 0) {
//                    Message("Enter License  No.");
                } else if (mChecked.equalsIgnoreCase("N")) {
                    Message("Accept Terms and Conditions");
                } else {

                    if (AppUtils.internetConnectionAvailable(2000)) {
                        registration();
                    } else {
                        Message("No Network Connection");
                    }

                }
            }
        });



        editTextFirstName.setOnKeyListener(new View.OnKeyListener() {

            public boolean onKey(View v, int keyCode, KeyEvent event) {
                // If the event is a key-down event on the "enter" button
                if ((event.getAction() == KeyEvent.ACTION_DOWN) ||
                        (keyCode == KeyEvent.KEYCODE_ENTER))
                {
                    // Perform action on Enter key press
                    editTextFirstName.clearFocus();
                    editTextLastName.requestFocus();
                    return true;
                }
                return false;
            }
        });

    }


    public void registration() {
        progress = ProgressDialog.show(this, "Submit...", "Please wait...",
                false, false);
        telephonyManager = (TelephonyManager) getSystemService(Context.
                TELEPHONY_SERVICE);
        try {
            deviceId = telephonyManager.getDeviceId();
        }catch (SecurityException e){ e.printStackTrace(); }

        String mFirstName = editTextFirstName.getText().toString();
        String mLastName = editTextLastName.getText().toString();
        String mMobileNumber = editTextPhoneNo.getText().toString();
        String mGSTNo = editTextGst.getText().toString();
        String mStoreNo = editTextStoreNo.getText().toString();
        String mEmail = editTextEmail.getText().toString();
        String mPassword = editTextPassword.getText().toString();
        String mFireBaseToken = sharedPrefManager.getDeviceToken();


        HashMap<String, String> params = new HashMap<String, String>();
        params.put("mobileNumber", mMobileNumber);
        params.put("password", mPassword);
        params.put("gstNumber", mGSTNo);
        params.put("email", mEmail);
        params.put("storeNumber", mStoreNo);
        params.put("firstName", mFirstName);
        params.put("lastName", mLastName);
        params.put("address", "NA");
        params.put("flag", "0");
        params.put("deviceUniqueId", deviceId);
        params.put("deviceType", "1");  // 1 for Android
        params.put("latitude", "0000");
        params.put("longitude", "0000");
        params.put("firebaseToken", mFireBaseToken);



//        params.put("mobileNumber", "1111111111");
//        params.put("password", "1111111111");
//        params.put("gstNumber", "000000000");
//        params.put("email", "5555555555");
//        params.put("storeNumber", "444444444");
//        params.put("firstName", "444444444");
//        params.put("lastName", "44444444");
//        params.put("address", "NA");
//        params.put("flag", "0");
//        params.put("deviceUniqueId", "444444444");
//        params.put("deviceType", "1");  // 1 for Android
//        params.put("latitude", "0000");
//        params.put("longitude", "0000");
//        params.put("firebaseToken","44444444444");
//


        Log.d(TAG, "Registration params: " + params);

        JsonObjectRequest request_json = new JsonObjectRequest(EndPoint.REGISTRATION,
                new JSONObject(params),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        progress.dismiss();
                        Log.d(TAG, "SlotResponse: " + response);

                        try {
                            Integer mStatusCode = response.getInt("responseCode");
                            if (mStatusCode == 400) {
                                String mMessage = response.getString("responseMessage");
                                Message(mMessage);
                            } else if (mStatusCode == 200) {
                                String mLoginFlag = response.getString(WebServiceConstants.FETCH_FLAG);
                                if (Preference.getDefaults(PreferenceConstants.LOGIN_FLAG,
                                        getApplicationContext()) != null) {
                                    Preference.deletePrefs(PreferenceConstants.LOGIN_FLAG,
                                            getApplicationContext());
                                }
                                Preference.setDefaults(PreferenceConstants.
                                        POST_WORKING_HOUR_FROM_TIME, mLoginFlag,
                                        getApplicationContext());
                                Intent intentSuccess = new Intent(
                                        RegistrationActivity.this, LoginActivity.class);
                                startActivity(intentSuccess);
                                finish();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                progress.dismiss();
                VolleyLog.e("Error: ", error.getMessage());
//                Message("Merchant Already Exists. Try logging in.");
            }
        });


        //        {
//            @Override
//            public String getBodyContentType() {
//                return "application/json; charset=utf-8";
//            }
//
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json; charset=utf-8");
//                return headers;
//
//
//            }
//        };



        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(request_json);
    }


    public void Message(String message) {
        LayoutInflater layoutInflater = LayoutInflater.from(this);
        View promptView = layoutInflater.inflate(R.layout.alert_message, null);
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setView(promptView);
        TextView textViewMessage = promptView.findViewById(R.id.alert_message_display);
        textViewMessage.setText(message);
        Button buttonblock = (Button) promptView.findViewById(R.id.alert_ok_buton);
        buttonblock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alert.dismiss();
            }
        });
        alert = alertDialogBuilder.create();
        alert.show();
    }


    @Override
    public void onBackPressed() {
        Intent i = new Intent(getApplicationContext(), LoginActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);
        finish();
    }




}

