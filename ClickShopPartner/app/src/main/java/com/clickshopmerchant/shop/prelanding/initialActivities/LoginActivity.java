package com.clickshopmerchant.shop.prelanding.initialActivities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.clickshopmerchant.shop.R;
import com.clickshopmerchant.shop.appconfig.AppUtils;
import com.clickshopmerchant.shop.appconfig.EndPoint;
import com.clickshopmerchant.shop.appconfig.PreferenceConstants;
import com.clickshopmerchant.shop.appconfig.SessionManager;
import com.clickshopmerchant.shop.appconfig.WebServiceConstants;
import com.clickshopmerchant.shop.main.activity.ForgotPassword;
import com.clickshopmerchant.shop.main.activity.MainActivity;
import com.clickshopmerchant.shop.network.NetworkUtils;
import com.clickshopmerchant.shop.pushnotification.SharedPrefManager;
import com.clickshopmerchant.shop.storage.Preference;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    EditText editTextPhoneNo, editTextPassword;
    Button buttonLogin;
    Button buttonRegister, buttonForgotPassword;
    private static final String TAG = LoginActivity.class.getSimpleName();
    private ProgressDialog progress;
    AlertDialog alert;
    SharedPrefManager sharedPrefManager;
    TextView termsandconditons;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        sharedPrefManager = SharedPrefManager.getInstance(getApplicationContext());

        // Showing Status bar
        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(getResources().getColor(R.color.colorPrimary));
        }
        initViews();
        termsandconditons = findViewById(R.id._termsandcondition);
        termsandconditons.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent termsintent = new Intent(LoginActivity.this, TermsAndConditions.class);
                startActivity(termsintent);
            }
        });
    }

    public void initViews() {
        editTextPhoneNo = findViewById(R.id.login_phone_no);
        editTextPassword = findViewById(R.id.password);
        buttonLogin = findViewById(R.id.btnLogin);
        buttonRegister = findViewById(R.id.btnLinkToRegisterScreen);
        buttonForgotPassword = findViewById(R.id.forgot_password);
        buttonLogin.setOnClickListener(this);
        buttonRegister.setOnClickListener(this);
        buttonForgotPassword.setOnClickListener(this);


    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnLinkToRegisterScreen:
                startActivity(new Intent(LoginActivity.this, RegistrationActivity.class));
                finish();
                break;
            case R.id.btnLogin:
                submit();
                break;

            case R.id.forgot_password:
                Intent intentFor = new Intent(LoginActivity.this, ForgotPassword.class);
                startActivity(intentFor);
                break;



        }
    }


    public void submit() {
        if (editTextPhoneNo.getText().toString().length() <= 9) {
            Toast.makeText(getApplicationContext(), "Enter Phone No",
                    Toast.LENGTH_SHORT).show();
        } else if (editTextPassword.getText().toString().length() == 0) {
            Toast.makeText(getApplicationContext(), "Enter Password ",
                    Toast.LENGTH_SHORT).show();
        } else {
            if (AppUtils.internetConnectionAvailable(2000)) {
                login();
            } else {
                Toast.makeText(getApplicationContext(), "No Network Connection",
                        Toast.LENGTH_SHORT).show();
            }
        }
    }


    public void login() {
        progress = ProgressDialog.show(this, "Signing in...", "Please wait...", false, false);

        String mMobileNumber = editTextPhoneNo.getText().toString();
//        try {
//            mPass = AESCrypt.encrypt(editTextPassword.getText().toString());
//        }catch (Exception e){e.printStackTrace();}

        String mPassword = editTextPassword.getText().toString();
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("mobileNumber", mMobileNumber);
        params.put("password", mPassword);
        String mDeviceToken = sharedPrefManager.getDeviceToken();
        params.put("firebaseToken", mDeviceToken);
        Log.d(TAG, "params: " + params);
        JsonObjectRequest request_json = new JsonObjectRequest(EndPoint.SIGNIN, new JSONObject(params),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        progress.dismiss();
                        Log.d(TAG, "LoginResponse: " + response);
                        try {
                            Integer mStatusCode = response.getInt("responseCode");
                            if (mStatusCode == 400) {
                                String mdonotmacth = response.getString("responseMessage");
                                Message(mdonotmacth);
                            } else if (mStatusCode == 200) {
                                String mMerchantId = String.valueOf(response.getInt(WebServiceConstants.RESPONSE_MERCHANT_ID));
                                Preference.setDefaults(PreferenceConstants.MERCHANT_ID, mMerchantId, getApplicationContext());
                                String mPhoneNumber = response.getString("mobileNumber");
                                String mFirstName = response.getString("firstName");
                                String mLastName = response.getString("lastName");
                                String mSessionId = response.getString("sessionId");

                                SessionManager sessionManager = new SessionManager(getApplicationContext());
                                sessionManager.createLoginSession(mPhoneNumber, mFirstName, mLastName, mSessionId);


                                // LOGIN FLAG - FLAG USED TO DETECT NAVIGATION
                                String mFlag = response.getString("flag");
                                Preference.setDefaults(PreferenceConstants.LOGIN_FLAG, mFlag, getApplicationContext());

                                // If flag is 0 go to MainActivity else go to SetUp Activity
                                if (mFlag.equalsIgnoreCase("0")) {
                                    Intent intentSuccess = new Intent(LoginActivity.this, SetUpMainActivity.class);
                                    startActivity(intentSuccess);
                                    finish();
                                } else if (mFlag.equalsIgnoreCase("1")) {
                                    Intent intentSuccess = new Intent(LoginActivity.this, MainActivity.class);
                                    startActivity(intentSuccess);
                                    finish();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progress.dismiss();
                Message("Login Credentials Do Not Match");
                NetworkUtils.UnableToReachServer(getApplicationContext());
            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(request_json);
    }


    public void Message(String message) {
        LayoutInflater layoutInflater = LayoutInflater.from(this);
        View promptView = layoutInflater.inflate(R.layout.alert_message, null);
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setView(promptView);
        TextView textViewMessage = promptView.findViewById(R.id.alert_message_display);
        textViewMessage.setText(message);
        Button buttonblock = (Button) promptView.findViewById(R.id.alert_ok_buton);
        buttonblock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alert.dismiss();
            }
        });
        alert = alertDialogBuilder.create();
        alert.show();
    }


}
