package com.clickshopmerchant.shop.main.fragment.hometabfragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.clickshopmerchant.shop.R;
import com.clickshopmerchant.shop.appconfig.ApiClient;
import com.clickshopmerchant.shop.appconfig.ApiInterface;
import com.clickshopmerchant.shop.appconfig.AppUtils;
import com.clickshopmerchant.shop.appconfig.PreferenceConstants;
import com.clickshopmerchant.shop.appconfig.SessionManager;
import com.clickshopmerchant.shop.main.adapter.NewOrderCardAdapter;
import com.clickshopmerchant.shop.main.model.ordernew.OrderNew;
import com.clickshopmerchant.shop.main.model.ordernew.OrderSearchList;
import com.clickshopmerchant.shop.main.model.ordernew.RequestBodyOrderNow;
import com.clickshopmerchant.shop.storage.Preference;

import org.jsoup.Jsoup;

import java.util.HashMap;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;

public class NewOrder extends Fragment implements SwipeRefreshLayout.OnRefreshListener, NewOrderRefresh {
    private static final String TAG = NewOrder.class.getSimpleName();
    private RelativeLayout relativeLayoutLoading, noOrders;
    private SwipeRefreshLayout swipeRefreshLayout;
    private RecyclerView recyclerView;
    private NewOrderCardAdapter adapter;
    private LinearLayoutManager linearLayoutManager;
    private AlertDialog alert;
    private TextView textView;
    private BroadcastReceiver receiver;
    List<OrderSearchList> datum;
    String currentVersion;
    SessionManager sessionManager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_new_order, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        textView = getActivity().findViewById(R.id._new_order_received);
        noOrders = getActivity().findViewById(R.id._no_order);
        noOrders.setVisibility(View.GONE);
        relativeLayoutLoading = getActivity().findViewById(R.id._loading_new_loading);
        swipeRefreshLayout = getActivity().findViewById(R.id.items_refresh_new_order);
        swipeRefreshLayout.setOnRefreshListener(this);
        recyclerView = getActivity().findViewById(R.id.recyclerview_new_order);
        recyclerView.setHasFixedSize(true);
        linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(linearLayoutManager);

        sessionManager = new SessionManager(getActivity());


        SharedPreferences preferences1 = PreferenceManager.getDefaultSharedPreferences(getActivity());
        SharedPreferences.Editor editor1 = preferences1.edit();
        editor1.putString("flag","1");
        editor1.apply();


        if (AppUtils.internetConnectionAvailable(2000)) {
//            new GetVersionCode().execute();;
            getMerchantOrder();

        } else {
            noOrders.setVisibility(View.VISIBLE);
            relativeLayoutLoading.setVisibility(View.GONE);
            swipeRefreshLayout.setVisibility(View.GONE);
            swipeRefreshLayout.setRefreshing(false);
            textView.setText(String.valueOf("0"));
            Toast.makeText(getActivity(), "No Network Connection",
                    Toast.LENGTH_SHORT).show();
        }


//        try {
//            currentVersion = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0).versionName;
//            Log.d(TAG, "currentVersion  : " + currentVersion);
//        } catch (PackageManager.NameNotFoundException e) {
//            e.printStackTrace();
//        }



        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String mReceivedData = intent.getStringExtra("key");
                if (mReceivedData.equalsIgnoreCase("value")) {
                    if (datum != null) {
                        datum.clear();
                    }
                    if (AppUtils.internetConnectionAvailable(2000)) {
                        getMerchantOrder();
                    } else {
                        noOrders.setVisibility(View.VISIBLE);
                        relativeLayoutLoading.setVisibility(View.GONE);
                        swipeRefreshLayout.setVisibility(View.GONE);
                        swipeRefreshLayout.setRefreshing(false);
                        textView.setText(String.valueOf("0"));
                        Toast.makeText(getActivity(), "No Network Connection",
                                Toast.LENGTH_SHORT).show();
                    }
                }
            }
        };
        // Registering the Local Broadcast Receiver
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(receiver, new IntentFilter("broadcast_notifiy"));
        super.onActivityCreated(savedInstanceState);
    }


    @Override
    public void onRefresh() {
        if (datum != null) {
            datum.clear();
        }
        recyclerView.setVisibility(View.GONE);
        relativeLayoutLoading.setVisibility(View.GONE);
        if (AppUtils.internetConnectionAvailable(2000)) {
            getMerchantOrder();
        } else {
            noOrders.setVisibility(View.VISIBLE);
            relativeLayoutLoading.setVisibility(View.GONE);
            swipeRefreshLayout.setVisibility(View.GONE);
            swipeRefreshLayout.setRefreshing(false);
            textView.setText(String.valueOf("0"));
            Toast.makeText(getActivity(), "No Network Connection",
                    Toast.LENGTH_SHORT).show();
        }
    }

    public void newOrderRefresh() {
        if (datum != null) {
            datum.clear();
        }
        recyclerView.setVisibility(View.GONE);
        relativeLayoutLoading.setVisibility(View.GONE);
        if (AppUtils.internetConnectionAvailable(2000)) {
            getMerchantOrder();
        } else {
            noOrders.setVisibility(View.VISIBLE);
            relativeLayoutLoading.setVisibility(View.GONE);
            swipeRefreshLayout.setVisibility(View.GONE);
            swipeRefreshLayout.setRefreshing(false);
            textView.setText(String.valueOf("0"));
            Toast.makeText(getActivity(), "No Network Connection",
                    Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }


    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
    }

    public void getMerchantOrder() {
        if (datum != null) {
            datum.clear();
        }


        HashMap<String, String> hashMap = sessionManager.getUserDetails();
        String mSessionId = hashMap.get(SessionManager.KEY_SESSION_ID);
        String mMobile = hashMap.get(SessionManager.KEY_PHONE_NUMBER);


        ApiInterface apiInterfacec = ApiClient.getApiClient().create(ApiInterface.class);
        String mMerchantId = Preference.getDefaults(PreferenceConstants.MERCHANT_ID, getActivity());
        Call<OrderNew> call = apiInterfacec.getOrders(new RequestBodyOrderNow(mMobile, mMerchantId, "New", mSessionId));
        call.enqueue(new Callback<OrderNew>() {
            @Override
            public void onResponse(Call<OrderNew> call, retrofit2.Response<OrderNew> response) {
                if (response.isSuccessful()) {
                    if (response.body().getResponseCode() == 200) {
                        noOrders.setVisibility(View.GONE);
                        recyclerView.setVisibility(View.VISIBLE);
                        relativeLayoutLoading.setVisibility(View.GONE);

                        List<OrderSearchList> datum = response.body().getOrderSearchList();
                        Log.d("neworder", String.valueOf(response.body().getOrderSearchList()));
                        adapter = new NewOrderCardAdapter(datum, getActivity(), NewOrder.this);
                        recyclerView.setAdapter(adapter);
                        adapter.notifyDataSetChanged();
                        swipeRefreshLayout.setRefreshing(false);
                        int mSize = datum.size();
                        textView.setText(String.valueOf(mSize));

                    } else if (response.body().getResponseCode() == 400) {
                        noOrders.setVisibility(View.VISIBLE);
                        swipeRefreshLayout.setRefreshing(false);
                        relativeLayoutLoading.setVisibility(View.GONE);
                        noOrders.setVisibility(View.VISIBLE);
                        textView.setText(String.valueOf("0"));
                    } else {
                        noOrders.setVisibility(View.VISIBLE);
                        relativeLayoutLoading.setVisibility(View.GONE);
                        swipeRefreshLayout.setVisibility(View.GONE);
                        swipeRefreshLayout.setRefreshing(false);
                        textView.setText(String.valueOf("0"));
                    }
                }else {
                    noOrders.setVisibility(View.VISIBLE);
                    relativeLayoutLoading.setVisibility(View.GONE);
                    swipeRefreshLayout.setVisibility(View.GONE);
                    swipeRefreshLayout.setRefreshing(false);
                    textView.setText(String.valueOf("0"));
                }
            }


            @Override
            public void onFailure(Call<OrderNew> call, Throwable t) {
                relativeLayoutLoading.setVisibility(View.GONE);
                swipeRefreshLayout.setVisibility(View.GONE);
                swipeRefreshLayout.setRefreshing(false);
                Toast.makeText(getActivity(), "Something went wrong. Please Login Again ..",
                        Toast.LENGTH_LONG).show();

            }
        });


    }






    private class GetVersionCode extends AsyncTask<Void, String, String> {
        @Override
        protected String doInBackground(Void... voids) {
            String newVersion = null;
            try {
                newVersion = Jsoup.connect("https://play.google.com/store/apps/details?id=com.click.shop" + getActivity().getPackageName() + "&hl=it").timeout(30000)
                        .userAgent("Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6")
                        .referrer("http://www.google.com")
                        .get()
                        .select("div[itemprop=softwareVersion]")
                        .first()
                        .ownText();
                return newVersion;
            } catch (Exception e) {

                return newVersion;

            }
        }


        @Override

        protected void onPostExecute(String onlineVersion) {
            Log.d(TAG, "onlineVersion  : " + onlineVersion);
            super.onPostExecute(onlineVersion);
            if (onlineVersion != null && !onlineVersion.isEmpty()) {
                if (Float.valueOf(currentVersion) < Float.valueOf(onlineVersion)) {
                    Message("New Version Available in Play Store. Please Update");
                }
            }
            Log.d("update", "Current version " + currentVersion + "  playstore version " + onlineVersion);
        }
    }



    public void Message(String message) {
        LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
        View promptView = layoutInflater.inflate(R.layout.alert_message, null);
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setView(promptView);
        TextView textViewMessage = promptView.findViewById(R.id.alert_message_display);
        textViewMessage.setText(message);
        Button buttonblock = (Button) promptView.findViewById(R.id.alert_ok_buton);
        buttonblock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alert.dismiss();
            }
        });
        alert = alertDialogBuilder.create();
        alert.show();
    }





}
