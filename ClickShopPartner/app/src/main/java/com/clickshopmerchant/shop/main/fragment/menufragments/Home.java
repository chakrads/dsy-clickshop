package com.clickshopmerchant.shop.main.fragment.menufragments;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.clickshopmerchant.shop.R;
import com.clickshopmerchant.shop.main.fragment.hometabfragment.Archive;
import com.clickshopmerchant.shop.main.fragment.hometabfragment.FinishedOrder;
import com.clickshopmerchant.shop.main.fragment.hometabfragment.NewOrder;
import com.clickshopmerchant.shop.main.fragment.hometabfragment.ProgressOrder;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("ALL")
public class Home extends Fragment{
    private static final String TAG = Home.class.getSimpleName();
    public TabLayout tabLayout;
    public ViewPager viewPager;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getActivity().setTitle("Click Shop - Merchant");
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        viewPager = getActivity().findViewById(R.id.viewpager);
        setupViewPager(viewPager);
        tabLayout = getActivity().findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setTabTextColors(
                getResources().getColor(R.color.md_white_1000),
                getResources().getColor(R.color.md_white_1000)
        );




    }




    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());
        adapter.addFragment(new NewOrder(), "New");
        adapter.addFragment(new ProgressOrder(), "Progress");
        adapter.addFragment(new FinishedOrder(), "Finished");
        adapter.addFragment(new Archive(), "Archived");
        viewPager.setAdapter(adapter);
        viewPager.setCurrentItem(0);
        viewPager.setOffscreenPageLimit(4);


    }


    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }





}
