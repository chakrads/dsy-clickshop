
package com.clickshopmerchant.shop.main.model.selecteditemmodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ItemList {

    @SerializedName("itemId")
    @Expose
    private Integer itemId;
    @SerializedName("itemName")
    @Expose
    private String itemName;
    @SerializedName("itemCategory")
    @Expose
    private Object itemCategory;
    @SerializedName("itemWeight")
    @Expose
    private String itemWeight;
    @SerializedName("itemPrice")
    @Expose
    private String itemPrice;
    @SerializedName("itemImage")
    @Expose
    private String itemImage;

    @SerializedName("itemWeightUnit")
    @Expose
    private String itemWeightUnit;



    public Integer getItemId() {
        return itemId;
    }

    public void setItemId(Integer itemId) {
        this.itemId = itemId;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getItemWeightUnit() {
        return itemWeightUnit;
    }

    public void setItemWeightUnit(String itemWeightUnit) {
        this.itemWeightUnit = itemWeightUnit;
    }


    public Object getItemCategory() {
        return itemCategory;
    }

    public void setItemCategory(Object itemCategory) {
        this.itemCategory = itemCategory;
    }

    public String getItemWeight() {
        return itemWeight;
    }

    public void setItemWeight(String itemWeight) {
        this.itemWeight = itemWeight;
    }

    public String getItemPrice() {
        return itemPrice;
    }

    public void setItemPrice(String itemPrice) {
        this.itemPrice = itemPrice;
    }

    public String getItemImage() {
        return itemImage;
    }

    public void setItemImage(String itemImage) {
        this.itemImage = itemImage;
    }

}
