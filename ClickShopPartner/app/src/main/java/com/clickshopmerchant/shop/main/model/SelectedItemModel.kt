package com.clickshopmerchant.shop.main.model

class SelectedItemModel {
    var item_id : Int? = 0;
    var item_image_url: String? = null
    var item_name : String? = null
    var item_price : String? = null
    var item_weight : String? = null
    var isSelected: Boolean = false

}