package com.clickshopmerchant.shop.main.activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.widget.Toast;
import com.clickshopmerchant.shop.R;
import com.clickshopmerchant.shop.appconfig.ApiClient;
import com.clickshopmerchant.shop.appconfig.ApiInterface;
import com.clickshopmerchant.shop.appconfig.AppUtils;
import com.clickshopmerchant.shop.appconfig.CustomTypefaceSpan;
import com.clickshopmerchant.shop.appconfig.GPSTracker;
import com.clickshopmerchant.shop.appconfig.SessionManager;
import com.clickshopmerchant.shop.main.fragment.menufragments.ContactUs;
import com.clickshopmerchant.shop.main.fragment.menufragments.Home;
import com.clickshopmerchant.shop.main.fragment.menufragments.AboutUs;
import com.clickshopmerchant.shop.main.fragment.menufragments.PrivacyPolicy;
import com.clickshopmerchant.shop.main.fragment.menufragments.ResetPassword;
import com.clickshopmerchant.shop.main.fragment.menufragments.SelectedItems;
import com.clickshopmerchant.shop.main.fragment.menufragments.Profile;
import com.clickshopmerchant.shop.main.fragment.menufragments.WalletFragment;
import com.clickshopmerchant.shop.main.model.signout.RequestBodySignOut;
import com.clickshopmerchant.shop.main.model.signout.SignOut;
import java.util.HashMap;
import retrofit2.Call;
import retrofit2.Callback;


public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    Boolean doubleBackToExitPressedOnce = false;
    ProgressDialog progress;
    String mFragmentCheck;
    int mCheck;
    GPSTracker gpsTracker;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.fadein, R.anim.fadeout);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mCheck = 0;
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        displaySelectedScreen(R.id.nav_home);


        // For changing the font in Menu Items
        Menu m = navigationView.getMenu();
        for (int i = 0; i < m.size(); i++) {
            MenuItem mi = m.getItem(i);

            //for aapplying a font to subMenu ...
            SubMenu subMenu = mi.getSubMenu();
            if (subMenu != null && subMenu.size() > 0) {
                for (int j = 0; j < subMenu.size(); j++) {
                    MenuItem subMenuItem = subMenu.getItem(j);
                    applyFontToMenuItem(subMenuItem);
                }
            }

            //the method we have create in activity
            applyFontToMenuItem(mi);
        }

        gpsTracker = new GPSTracker(getApplicationContext());
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_logout) {
            if (AppUtils.internetConnectionAvailable(2000)) {
                progress = ProgressDialog.show(this, "Logging Out...", "Please wait...", false, false);
                sessionLogOut();
            } else {
                Toast.makeText(getApplicationContext(), "No Network Connection",
                        Toast.LENGTH_SHORT).show();
            }
            return true;
        }

        if (id == R.id.action_share) {
            Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
            sharingIntent.setType("text/plain");
            String shareBody = "Download Android Click Shop - Merchant App on Google Play Store - https://play.google.com/store/apps/details?id=com.click.shop";
            sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Click Shop Merchant App");
            sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
            startActivity(Intent.createChooser(sharingIntent, "Share via"));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void displaySelectedScreen(int itemId) {
        Fragment fragment = null;
        //initializing the fragment object which is selected
        switch (itemId) {
            case R.id.nav_home:
                clearBackStack();
                mFragmentCheck = "Home";
                fragment = new Home();
                mCheck = 0;
                break;
            case R.id.nav_store_item:
                mFragmentCheck = "SelectedItems";
                fragment = new SelectedItems();
                mCheck = 1; // 1 is to detect back button since there is fragment inside in fragment in HOme Fragment
                break;
            case R.id.nav_wallet:
                mFragmentCheck = "WalletFragment";
                fragment = new WalletFragment();
                mCheck = 1;
                break;
            case R.id.nav_store_profile:
                mFragmentCheck = "Profile";
                fragment = new Profile();
                mCheck = 1;
                break;
            case R.id.nav_about_us:
                mFragmentCheck = "AboutUs";
                fragment = new AboutUs();
                mCheck = 1;
                break;
            case R.id.nav_privacy_policy:
                mFragmentCheck = "PrivacyPolicy";
                fragment = new PrivacyPolicy();
                mCheck = 1;
                break;
            case R.id.reset_password:
                mFragmentCheck = "ResetPasswords";
                fragment = new ResetPassword();
                mCheck = 1;
                break;
            case R.id.nav_contact_us:
                mFragmentCheck = "ContactUs";
                fragment = new ContactUs();
                mCheck = 1;
                break;




            default:
                break;
        }

        //replacing the fragment
        if (fragment != null) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.content_frame, fragment, mFragmentCheck);
            ft.addToBackStack(mFragmentCheck);
            ft.commit();
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
    }


    private void applyFontToMenuItem(MenuItem mi) {
        Typeface font = Typeface.createFromAsset(getAssets(), "corbel-1361520258.ttf");
        SpannableString mNewTitle = new SpannableString(mi.getTitle());
        mNewTitle.setSpan(new CustomTypefaceSpan("", font), 0, mNewTitle.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        mi.setTitle(mNewTitle);
    }


    private void clearBackStack() {
        FragmentManager manager = getSupportFragmentManager();
        if (manager.getBackStackEntryCount() > 0) {
            FragmentManager.BackStackEntry first = manager.getBackStackEntryAt(0);
            manager.popBackStack(first.getId(), FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        displaySelectedScreen(item.getItemId());
        return true;
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            int fragments = getSupportFragmentManager().getBackStackEntryCount();
            if (fragments == 1) {
                if (doubleBackToExitPressedOnce) {
                    System.exit(0);
                    return;
                }
                this.doubleBackToExitPressedOnce = true;
                Toast.makeText(this, "Please click BACK again to exit",
                        Toast.LENGTH_SHORT).show();
            } else {
                if (getFragmentManager().getBackStackEntryCount() > 1) {
                    getFragmentManager().popBackStack();
                } else {
                    super.onBackPressed();
                }
            }
        }
    }









    public void sessionLogOut() {
        SessionManager sessionManager = new SessionManager(getApplicationContext());
        HashMap<String, String> hashMap = sessionManager.getUserDetails();
        String mPhone = hashMap.get(SessionManager.KEY_PHONE_NUMBER);


        ApiInterface apiInterfacec = ApiClient.getApiClient().create(ApiInterface.class);
        Call<SignOut> call = apiInterfacec.signOut(new RequestBodySignOut(mPhone));
        call.enqueue(new Callback<SignOut>() {
            @Override
            public void onResponse(Call<SignOut> call, retrofit2.Response<SignOut> response) {
                if (response.isSuccessful()) {
                    Toast.makeText(getApplicationContext(), "Successfully Logged Out",
                            Toast.LENGTH_SHORT).show();
                    progress.dismiss();
                    SessionManager.logoutUser();
                    finish();
                }else {
                    Toast.makeText(getApplicationContext(), "Successfully Logged Out\"",
                            Toast.LENGTH_SHORT).show();
                    SessionManager.logoutUser();
                    finish();

                }
            }

            @Override
            public void onFailure(Call<SignOut> call, Throwable t) {
                progress.dismiss();
                SessionManager.logoutUser();
                finish();
                Toast.makeText(getApplicationContext(), "Successfully Logged Out\"",
                        Toast.LENGTH_SHORT).show();

            }
        });
    }


}
