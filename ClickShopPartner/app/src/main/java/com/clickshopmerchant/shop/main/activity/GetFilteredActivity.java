package com.clickshopmerchant.shop.main.activity;
import android.content.Intent;
import android.os.Build;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.clickshopmerchant.shop.R;
import com.clickshopmerchant.shop.appconfig.AppUtils;
import com.clickshopmerchant.shop.appconfig.Constants;
import com.clickshopmerchant.shop.appconfig.EndPoint;
import com.clickshopmerchant.shop.appconfig.WebServiceConstants;
import com.clickshopmerchant.shop.fonts.CorbelTextView;
import com.clickshopmerchant.shop.main.adapter.GetFilteredItemCardAdapter;
import com.clickshopmerchant.shop.main.fragment.menufragments.CategoryListing;
import com.clickshopmerchant.shop.main.model.GetFilteredItems;
import com.clickshopmerchant.shop.network.NetworkUtils;
import com.clickshopmerchant.shop.storage.Preference;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class GetFilteredActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {
    private static final String TAG = CategoryListing.class.getSimpleName();
    EditText editTextSearch;
    public RelativeLayout relativeLayoutLoading;
    public SwipeRefreshLayout swipeRefreshLayout;
    public RecyclerView recyclerView;
    public List<GetFilteredItems> allItemlist;
    public GetFilteredItemCardAdapter adapter;
    public LinearLayoutManager linearLayoutManager;
    String newquery, mSubString;
    private String mItemId;
    private String mItemName;
    Toolbar toolbar;
    CorbelTextView corbelTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_filtered);

        // Showing Status bar
        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }
        toolbar = findViewById(R.id.get_filter_activity_toolbar);
        setSupportActionBar(toolbar);
        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_keyboard_backspace_black_24dp));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        corbelTextView = findViewById(R.id._title_diaplay);
        Views();
        getInent ();
        servicecall();
    }


    public void getInent (){
        Intent intent = getIntent();
        try {
            mItemId = intent.getStringExtra("ItemId");
            mItemName = intent.getStringExtra("ItemName");
        }catch (NullPointerException e){
            mItemName = "Products";
            e.printStackTrace();
        }
        corbelTextView.setText(mItemName);
    }


    public void Views() {
        editTextSearch = findViewById(R.id._search_existing);
        editTextSearch.setVisibility(View.GONE);
        editTextSearch.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES); // Keypad First Letter Caps
        relativeLayoutLoading = findViewById(R.id._loadingS);
        swipeRefreshLayout = findViewById(R.id.existing_items_refreshs);
        swipeRefreshLayout.setVisibility(View.GONE);
        swipeRefreshLayout.setOnRefreshListener(this);
        recyclerView = findViewById(R.id.existing_recyclerview_refreshs);
        linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        allItemlist = new ArrayList<>();
        editTextSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence query, int start, int before, int count) {
                if (count >= 2) {
                    mSubString = String.valueOf(query);
                    newquery = mSubString.substring(0, 1).toUpperCase() + mSubString.substring(1);
                } else {
                    mSubString = String.valueOf(query);
                    newquery = query.toString().toUpperCase();
                }

                final List<GetFilteredItems> filteredList = new ArrayList<>();

                if (allItemlist != null) {
                    for (int i = 0; i < allItemlist.size(); i++) {
                        try {
                            final String text = allItemlist.get(i).getItem_name();
                            if (text.contains(newquery)) {
                                filteredList.add(allItemlist.get(i));
                                updateRecyclerView(filteredList, false);
                            }
                        } catch (NullPointerException e) {
                            e.printStackTrace();
                        }
                    }
                }



            }
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

    @Override
    public void onRefresh() {
        relativeLayoutLoading.setVisibility(View.GONE);
        allItemlist.clear();
        recyclerView.setVisibility(View.GONE);
        servicecall();
    }

    public void updateRecyclerView(List<GetFilteredItems> lst, boolean swipeStatus) {
        recyclerView = findViewById(R.id.existing_recyclerview_refreshs);
        linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        adapter = new GetFilteredItemCardAdapter(lst, getApplicationContext());
        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        swipeRefreshLayout.setRefreshing(swipeStatus);
    }


    public void servicecall() {
        if (allItemlist != null){
            allItemlist.clear();
        }
        relativeLayoutLoading.setVisibility(View.GONE);
        if (AppUtils.internetConnectionAvailable(2000)) {
            getExisitingItems();
        } else {
            Toast.makeText(getApplicationContext(), "No Network Connection",
                    Toast.LENGTH_SHORT).show();
        }
    }

    public void getExisitingItems() {
        String mCatagroyId = Preference.getDefaults(Constants.CATAGORY_ID, getApplicationContext());
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("itemCategoryId", mCatagroyId);

        Log.d(TAG, "itemCategoryIdparams: " + params);

        JsonObjectRequest request_json = new JsonObjectRequest(EndPoint.GET_FILTERED_ITEM, new JSONObject(params),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        recyclerView.setVisibility(View.VISIBLE);
                        Log.d(TAG, "GetFilteredResponse: " + response);
                        try {
                            int mStatusCode = response.getInt(WebServiceConstants.EXISTING_RESPONSE_CODE);
                            if (mStatusCode == 200) {
                                relativeLayoutLoading.setVisibility(View.GONE);
                                swipeRefreshLayout.setVisibility(View.VISIBLE);
                                editTextSearch.setVisibility(View.VISIBLE);
                                JSONArray array = response.optJSONArray(WebServiceConstants.DATA);
                                parseData(array);
                            } else {
                                relativeLayoutLoading.setVisibility(View.GONE);
                                swipeRefreshLayout.setVisibility(View.GONE);
                                swipeRefreshLayout.setRefreshing(false);
                                editTextSearch.setVisibility(View.GONE);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkUtils.UnableToReachServer(getApplicationContext());
            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(request_json);
    }


    private void parseData(JSONArray array) {
        for (int i = 0; i < array.length(); i++) {
            GetFilteredItems getAllItems = new GetFilteredItems();
            JSONObject json;
            try {
                json = array.getJSONObject(i);
                if (json.getString(WebServiceConstants.EXISTING_ITEM_IMAGE) != null && !json.getString(WebServiceConstants.EXISTING_ITEM_IMAGE).isEmpty()
                        && !json.getString(WebServiceConstants.EXISTING_ITEM_IMAGE).equals(WebServiceConstants.NULL)) {

                    String mNewImage = modifyDropboxUrl(json.getString("itemImage"));
                    getAllItems.setItem_image_url(mNewImage);
                    Log.d(TAG, "Image Path: " + json.getString("itemImage"));

                }
                if (json.getString(WebServiceConstants.EXISTING_ITEM_ID) != null && !json.getString(WebServiceConstants.EXISTING_ITEM_ID).isEmpty()
                        && !json.getString(WebServiceConstants.EXISTING_ITEM_ID).equals(WebServiceConstants.NULL)) {
                    getAllItems.setItem_id((json.getInt(WebServiceConstants.EXISTING_ITEM_ID)));
                } else {
                    getAllItems.setItem_id((WebServiceConstants.EXISTING_ITEM_INTEGER_NONE));
                }
                if (json.getString(WebServiceConstants.EXISTING_ITEM_NAME) != null && !json.getString(WebServiceConstants.EXISTING_ITEM_NAME).isEmpty()
                        && !json.getString(WebServiceConstants.EXISTING_ITEM_NAME).equals(WebServiceConstants.NULL)) {
                    getAllItems.setItem_name((json.getString(WebServiceConstants.EXISTING_ITEM_NAME)));
                } else {
                    getAllItems.setItem_name((WebServiceConstants.EXISTING_ITEM_STRING_EMPTY));
                }
                if (json.getString(WebServiceConstants.EXISTING_ITEM_PRICE) != null && !json.getString(WebServiceConstants.EXISTING_ITEM_PRICE).isEmpty()
                        && !json.getString(WebServiceConstants.EXISTING_ITEM_PRICE).equals(WebServiceConstants.NULL)) {
                    getAllItems.setItem_price((json.getString(WebServiceConstants.EXISTING_ITEM_PRICE)));
                } else {
                    getAllItems.setItem_price((WebServiceConstants.EXISTING_ITEM_STRING_EMPTY));
                }

                if (json.getString("itemWeight") != null && !json.getString("itemWeight").isEmpty()
                        && !json.getString("itemWeight").equals("itemWeight")) {
                    getAllItems.setItem_weight((json.getString("itemWeight")));
                } else {
                    getAllItems.setItem_weight(("0"));
                }

                if (json.getString("itemWeightUnit") != null && !json.getString("itemWeightUnit").isEmpty()
                        && !json.getString("itemWeightUnit").equals("itemWeightUnit")) {
                    getAllItems.setItem_weight_unit((json.getString("itemWeightUnit")));
                } else {
                    getAllItems.setItem_weight_unit(("itemWeightUnit"));
                }

                if (json.getString("itemCategory") != null && !json.getString("itemCategory").isEmpty()
                        && !json.getString("itemCategory").equals("itemCategory")) {
                    getAllItems.setItemCategory((json.getString("itemCategory")));
                } else {
                    getAllItems.setItemCategory(("itemCategory"));
                }

                if (json.getString("itemSubcategory") != null && !json.getString("itemSubcategory").isEmpty()
                        && !json.getString("itemSubcategory").equals("itemSubcategory")) {
                    getAllItems.setItemSubcategory((json.getString("itemSubcategory")));
                } else {
                    getAllItems.setItemSubcategory(("itemSubcategory"));
                }

                if (json.getString("family") != null && !json.getString("family").isEmpty()
                        && !json.getString("family").equals("family")) {
                    getAllItems.setFamily((json.getString("family")));
                } else {
                    getAllItems.setFamily(("family"));
                }

                if (json.getString("barcode") != null && !json.getString("barcode").isEmpty()
                        && !json.getString("barcode").equals("barcode")) {
                    getAllItems.setBarcode((json.getString("barcode")));
                } else {
                    getAllItems.setBarcode(("barcode"));
                }


                if (json.getString("priceUnit") != null && !json.getString("priceUnit").isEmpty()
                        && !json.getString("priceUnit").equals("priceUnit")) {
                    getAllItems.setPriceUnit((json.getString("priceUnit")));
                } else {
                    getAllItems.setPriceUnit(("priceUnit"));
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
            allItemlist.add(getAllItems);
            // Collections.sort(recordsList);
        }
        adapter = new GetFilteredItemCardAdapter(allItemlist,this);
        recyclerView.setAdapter(adapter);
        swipeRefreshLayout.setRefreshing(false);
        adapter.notifyDataSetChanged();
    }

    public static String modifyDropboxUrl(String originalUrl)
    {
        String newUrl = originalUrl.replace("www.dropbox." ,"dl.dropboxusercontent.");

        //just for sure for case if www is missing in url string
        newUrl = newUrl.replace("dropbox.", "dl.dropboxusercontent.");

        return newUrl;
    }
}
