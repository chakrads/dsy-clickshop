package com.clickshopmerchant.shop.main.adapter;
import android.app.ProgressDialog;
import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.Volley;
import com.clickshopmerchant.shop.R;
import com.clickshopmerchant.shop.appconfig.AppUtils;
import com.clickshopmerchant.shop.appconfig.Constants;
import com.clickshopmerchant.shop.appconfig.EndPoint;
import com.clickshopmerchant.shop.appconfig.PreferenceConstants;
import com.clickshopmerchant.shop.appconfig.SessionManager;
import com.clickshopmerchant.shop.appconfig.WebServiceConstants;
import com.clickshopmerchant.shop.main.model.GetFilteredItems;
import com.clickshopmerchant.shop.storage.Preference;
import com.clickshopmerchant.shop.volley.CustomVolleyRequest;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.HashMap;
import java.util.List;

public class GetFilteredItemCardAdapter extends
        RecyclerView.Adapter<GetFilteredItemCardAdapter.ViewHolder> {

    private static final String TAG = GetFilteredItemCardAdapter.class.getSimpleName();
    Context context;
    List<GetFilteredItems> listInAss;
    public ImageLoader imageLoader;
    private NetworkImageView networkImageView;
    private AlertDialog alert;
    private ProgressDialog progress;

    public GetFilteredItemCardAdapter(List<GetFilteredItems> listInAss, Context context) {
        super();
        this.listInAss = listInAss;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.exisiting_item_card_adapter, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public int getItemCount() {
        return listInAss.size();
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final GetFilteredItems getAllItems = listInAss.get(position);
        holder.textViewItemName.setText(getAllItems.getItem_name());
        holder.textViewItemPrice.setText(new StringBuilder("Rs. ")
                .append(getAllItems.getItem_price()));
        final String image = getAllItems.getItem_image_url();
        imageLoader = CustomVolleyRequest.getInstance(context).getImageLoader();
        imageLoader.get(image, ImageLoader.getImageListener(networkImageView, R.drawable.noimage,
                android.R.drawable.ic_dialog_alert));
        networkImageView.setImageUrl(image, imageLoader);
        holder.buttonAddItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String mItemId = String.valueOf(getAllItems.getItem_id());
                String mItemName = getAllItems.getItem_name();
                String mItemPrice = getAllItems.getItem_price();
                String mItemWeight = getAllItems.getItem_weight();
                String mItemWeightUnit = getAllItems.getItem_weight_unit();
                addPriceAlert(mItemName, mItemId, mItemPrice, mItemWeight, mItemWeightUnit);
            }
        });
    }


    class ViewHolder extends RecyclerView.ViewHolder {
        TextView textViewItemName;
        TextView textViewItemPrice;
        Button buttonAddItem;

        public ViewHolder(View itemView) {
            super(itemView);
            networkImageView = itemView.findViewById(R.id.existing_item_image);
            textViewItemName = itemView.findViewById(R.id.existing_item_name);
            textViewItemPrice = itemView.findViewById(R.id.existing_item_price);
            buttonAddItem = itemView.findViewById(R.id.btn_add_price);
            this.setIsRecyclable(false);
        }
    }


    private void addPriceAlert(final String mName, final String mItemId, final String mItemPrice, final String mItemWeight, final String mItemWeightUnit) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        final View promptView = layoutInflater.inflate(R.layout.add_price_alert, null);
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setView(promptView);
        TextView textViewDisplay = promptView.findViewById(R.id._selected_display);
        textViewDisplay.setText(new StringBuilder("").append("").append(mName));
        final EditText editTextPrice = promptView.findViewById(R.id._get_price);
        editTextPrice.setText(mItemPrice);
        Button buttonSubmit = promptView.findViewById(R.id.btn_add_price);
        buttonSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String mPri = editTextPrice.getText().toString().trim();
           //     AppGlobal.hideKeyboard(context);
                if (editTextPrice.getText().length() == 0) {
                    editTextPrice.setError("Enter Price");

                }else if (mPri.equalsIgnoreCase("0")){
                    Toast.makeText(context, "Price Cannot be 0",
                            Toast.LENGTH_SHORT).show();
                } else {
                    if (AppUtils.internetConnectionAvailable(2000)) {
                        String mPrice = editTextPrice.getText().toString().trim();
                        submitSelectedItems(mItemId, mPrice, mName, mItemWeight, mItemWeightUnit);
                        alert.dismiss();
                    } else {
                        Toast.makeText(context, "No Network Connection",
                                Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        alert = alertDialogBuilder.create();
        alert.show();
    }


    public void submitSelectedItems(String itemid, String itemPrice, String itemName, String mWeight, String mWeightUnit ) {
        String mCatagoryId =  Preference.getDefaults(Constants.CATAGORY_ID, context);
        SessionManager sessionManager = new SessionManager(context);
        HashMap<String, String> hashMap = sessionManager.getUserDetails();
        String mSessionId = hashMap.get(SessionManager.KEY_SESSION_ID);
        String mMobile = hashMap.get(SessionManager.KEY_PHONE_NUMBER);



        progress = ProgressDialog.show(context, "", "Please wait...", false, false);
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("merchantId", Preference.getDefaults(PreferenceConstants.MERCHANT_ID, context));
        params.put("itemId", itemid);
        params.put("itemName", itemName);
        params.put("itemPrice", itemPrice);
        params.put("itemCategoryId", mCatagoryId);
        params.put("type", "Non-Brand");
        params.put("sessionId", mSessionId);
        params.put("mobileNumber", mMobile);

        String mCatagory =  Preference.getDefaults("catagoryname", context);
        params.put("itemCategory", mCatagory);


        Log.d(TAG, "params: " + params);
        //StringRequest request_json=new StringRequest(Request.Method.POST)

        JsonObjectRequest request_json = new JsonObjectRequest(Request.Method.POST, EndPoint.ADD_SELECTED_ITEM, new JSONObject(params),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        progress.dismiss();
                        try {
                            Integer mStatusCode = response.getInt(WebServiceConstants.EXISTING_RESPONSE_CODE);
                            if (mStatusCode == 400) {
                                Toast.makeText(context, "Item Already Added",
                                        Toast.LENGTH_SHORT).show();
                            } else if (mStatusCode == 200) {
                                Toast.makeText(context, "Added Successfully",
                                        Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Log.d(TAG, "Add Item Response: " + response);

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("JsonErrors",error.toString());
                try {
                    progress.dismiss();
                    Toast.makeText(context, "Unable to Reach Remote Server",
                            Toast.LENGTH_SHORT).show();
                }catch (Exception e){
                    Log.d("JsonErrors",e.toString());
                    e.printStackTrace();}
            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(context);

        requestQueue.add(request_json);
    }

    public void Message(String message) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View promptView = layoutInflater.inflate(R.layout.alert_message, null);
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setView(promptView);
        TextView textViewMessage = promptView.findViewById(R.id.alert_message_display);
        textViewMessage.setText(message);
        Button buttonblock = (Button) promptView.findViewById(R.id.alert_ok_buton);
        buttonblock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alert.dismiss();
            }
        });
        alert = alertDialogBuilder.create();
        alert.show();
    }


}



