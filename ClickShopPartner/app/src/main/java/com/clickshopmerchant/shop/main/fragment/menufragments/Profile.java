package com.clickshopmerchant.shop.main.fragment.menufragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.database.CursorIndexOutOfBoundsException;
import android.database.SQLException;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.clickshopmerchant.shop.R;
import com.clickshopmerchant.shop.appconfig.AppUtils;
import com.clickshopmerchant.shop.appconfig.EndPoint;
import com.clickshopmerchant.shop.appconfig.GPSTracker;
import com.clickshopmerchant.shop.appconfig.PreferenceConstants;
import com.clickshopmerchant.shop.appconfig.SessionManager;
import com.clickshopmerchant.shop.appconfig.WebServiceConstants;
import com.clickshopmerchant.shop.fonts.CorbelTextView;
import com.clickshopmerchant.shop.main.activity.MainActivity;
import com.clickshopmerchant.shop.network.NetworkUtils;
import com.clickshopmerchant.shop.places.PlacesActivity;
import com.clickshopmerchant.shop.storage.Preference;
import com.clickshopmerchant.shop.volley.VolleyMultipartRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Method;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;


public class Profile extends Fragment implements View.OnClickListener {
    private static final String TAG = Profile.class.getSimpleName();





    String timeformat;
    static Dialog d ;

    final Calendar c = Calendar.getInstance();

    final int hour = c.get(Calendar.HOUR_OF_DAY);

    final int minute = c.get(Calendar.MINUTE);

    int year = Calendar.getInstance().get(Calendar.YEAR);




    ProgressDialog progress, downloadprogress;
    AlertDialog alert, alertCamara;
    EditText editTextDeliveryDuration, editTextWorkingFrom, editTextWorkingTo,
            editTextDeliveryRange, editTextLocation, editTextStoreName, editTextDeliveryCharge,
            editTextDeliveryUpto,edit_store_location;
    Button buttonNext;
    LinearLayout google_location_layout;
    CorbelTextView _change_address;
    ImageView imageViewUploadButton;
    ImageView networkImageViewStorePhoto;
    Geocoder geocoder;
    File file;
    Uri outPutfileUri;
    String mDeliveryDuration, mWorkingHourFromTime, mWorkingHourToTime, mDeliveryRange, mStoreId, mStoreName;
    String postlatitude;
    String postlongitude;
    String postaddress;
    String postcity;
    String poststate;
    String postcountry;
    String postpinCode;
    String mDeliveryCharge, mDeliveryChargeUpto;
    private int PICK_IMAGE_REQUEST = 1;
    private int CAMERA_REQUEST = 2;
    String uri;
    Bitmap bitmap;
    Cursor cursor;
    String path;
    String filepathshort;
    Bitmap decodeimage;
    GPSTracker gpsTracker;
    EditText editTextGoogleLocation;
    Button buttonFetchLocation;
    int mCheckCamaraOrLocation;

    public Profile() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getActivity().setTitle("Profile");
        return inflater.inflate(R.layout.fragment_update_store_address, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        google_location_layout = getActivity().findViewById(R.id.google_location_layout);
        _change_address = getActivity().findViewById(R.id._change_address);

        editTextStoreName = getActivity().findViewById(R.id._store_name_enter);
        editTextDeliveryDuration = getActivity().findViewById(R.id.delivery_duration);
        editTextWorkingFrom = getActivity().findViewById(R.id.working_from_input);
        editTextWorkingTo = getActivity().findViewById(R.id.working_to_input);
        editTextDeliveryRange = getActivity().findViewById(R.id.delivery_range);
        editTextLocation = getActivity().findViewById(R.id.your_location);
        editTextDeliveryCharge = getActivity().findViewById(R.id._edit_delivery_charge);
        editTextDeliveryUpto = getActivity().findViewById(R.id._edit_free_delivery_above);

        edit_store_location = getActivity().findViewById(R.id.store_location);

        buttonNext = getActivity().findViewById(R.id.btn_update);
        networkImageViewStorePhoto = getActivity().findViewById(R.id._store_image);
        imageViewUploadButton = getActivity().findViewById(R.id._image_upload_button);
        editTextGoogleLocation = getActivity().findViewById(R.id._edit_text_gogole_location);
        buttonFetchLocation = getActivity().findViewById(R.id._button_get_location);
        buttonFetchLocation.setOnClickListener(this);
        gpsTracker = new GPSTracker(getActivity());
        editTextDeliveryDuration.setOnClickListener(this);
        editTextWorkingFrom.setOnClickListener(this);
        editTextWorkingTo.setOnClickListener(this);
        editTextDeliveryRange.setOnClickListener(this);
        buttonNext.setOnClickListener(this);
        imageViewUploadButton.setOnClickListener(this);

        _change_address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                _change_address.setVisibility(View.GONE);
                edit_store_location.setVisibility(View.GONE);
                google_location_layout.setVisibility(View.VISIBLE);
            }
        });
        if (AppUtils.internetConnectionAvailable(2000)) {
            googlelocation();
            getStoreData();
            getProfileImage();
        } else {
            Toast.makeText(getActivity(), "No Network Connection",
                    Toast.LENGTH_SHORT).show();
        }


        if (Build.VERSION.SDK_INT >= 24) {
            try {
                Method m = StrictMode.class.getMethod("disableDeathOnFileUriExposure");
                m.invoke(null);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        super.onActivityCreated(savedInstanceState);
    }


    public void googlelocation() {
        geocoder = new Geocoder(getActivity(), Locale.getDefault());
    }

    public void getStoreData() {

        HashMap<String, String> params = new HashMap<String, String>();
        SessionManager sessionManager = new SessionManager(getActivity());
        HashMap<String, String> hashMap = sessionManager.getUserDetails();
        String mSessionId = hashMap.get(SessionManager.KEY_SESSION_ID);
        String mMobile = hashMap.get(SessionManager.KEY_PHONE_NUMBER);

        params.put("sessionId", mSessionId);
        params.put("merchantId", Preference.getDefaults(PreferenceConstants.MERCHANT_ID, getActivity()));
        params.put("mobileNumber", mMobile);

        Log.d(TAG, "params: " + params);
        JsonObjectRequest request_json = new JsonObjectRequest(EndPoint.FETCH_STORE_SET_UP_DATA, new JSONObject(params),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, "SelectedItem " + response);
                        try {
                            Integer mStatusCode = response.getInt(WebServiceConstants.EXISTING_RESPONSE_CODE);
                            if (mStatusCode == 400) {

                                // Any message not necessary. If there is no data response will be 400.
                            } else if (mStatusCode == 200) {
                                mStoreId = response.getString(WebServiceConstants.STORE_PROFILE_STORE_ID);
                                mStoreName = response.getString(WebServiceConstants.STORE_PROFILE_STORE_NAME);
                                mDeliveryDuration = response.getString(WebServiceConstants.STORE_PROFILE_DELIVERY_DURATION);
                                mWorkingHourFromTime = response.getString(WebServiceConstants.STORE_PROFILE_WORKING_HOURS_FROM);
                                mWorkingHourToTime = response.getString(WebServiceConstants.STORE_PROFILE_WORKING_HOURS_TO);
                                mDeliveryRange = response.getString(WebServiceConstants.STORE_PROFILE_DELIVERY_RANGE);
                                postlatitude = response.getString(WebServiceConstants.STORE_PROFILE_LATITTUDE);
                                postlongitude = response.getString(WebServiceConstants.STORE_PROFILE_LONGITUDE);
                                postaddress = response.getString(WebServiceConstants.STORE_PROFILE_ADDRESS);
                                postcity = response.getString(WebServiceConstants.STORE_PROFILE_CITY);
                                poststate = response.getString(WebServiceConstants.STORE_PROFILE_STATE);
                                postcountry = response.getString(WebServiceConstants.STORE_PROFILE_COUNTRY);
                                postpinCode = response.getString(WebServiceConstants.STORE_PROFILE_PIN_CODE);
                                mDeliveryCharge = response.getString(WebServiceConstants.STORE_DELIVERY_CHARGE);
                                mDeliveryChargeUpto = response.getString(WebServiceConstants.STORE_DELIVERY_CHARGE_UPTO);


                                editTextStoreName.setText(mStoreName);
                                editTextDeliveryDuration.setText(mDeliveryDuration);
                                editTextWorkingFrom.setText(mWorkingHourFromTime);
                                editTextWorkingTo.setText(mWorkingHourToTime);
                                editTextDeliveryRange.setText(mDeliveryRange);
                                editTextLocation.setText(postaddress);
                                editTextDeliveryCharge.setText(mDeliveryCharge);
                                editTextDeliveryUpto.setText(mDeliveryChargeUpto);


                                edit_store_location.setText(getAddress(Double.parseDouble(postlatitude),Double.parseDouble(postlongitude)));
                                editTextGoogleLocation.setText(getAddress(Double.parseDouble(postlatitude),Double.parseDouble(postlongitude)));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkUtils.UnableToReachServer(getActivity());
            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(request_json);
    }




    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_update:
                String mWorkFrom = editTextWorkingFrom.getText().toString().trim();
                String mWrokTo = editTextWorkingTo.getText().toString().trim();
                String mDr = editTextDeliveryDuration.getText().toString().trim();
                String mDlRg = editTextDeliveryRange.getText().toString().trim();
                mWorkingHourFromTime = editTextWorkingFrom.getText().toString().trim();
                mWorkingHourToTime = editTextWorkingTo.getText().toString().trim();
                mDeliveryRange = editTextDeliveryRange.getText().toString().trim();
                mDeliveryDuration = editTextDeliveryDuration.getText().toString().trim();
                mStoreName = editTextStoreName.getText().toString().trim();

                if (editTextStoreName.getText().length() == 0) {
                    Toast.makeText(getActivity(), "Enter Store Name",
                            Toast.LENGTH_SHORT).show();
                } else if (editTextDeliveryDuration.getText().length() == 0) {
                    Toast.makeText(getActivity(), "Enter Delivery Duration",
                            Toast.LENGTH_SHORT).show();
                } else if (mDr.equalsIgnoreCase("00:00") || (mDr.equalsIgnoreCase("0:0"))) {
                    Toast.makeText(getActivity(), "Enter Correct Time Required for Delivery",
                            Toast.LENGTH_SHORT).show();
                } else if (editTextWorkingFrom.getText().length() == 0) {
                    Toast.makeText(getActivity(), "Enter Working From\"",
                            Toast.LENGTH_SHORT).show();
                } else if (mWorkFrom.equalsIgnoreCase("00:00") || (mWorkFrom.equalsIgnoreCase("0:0"))) {
                    Toast.makeText(getActivity(), "Enter Correct From Time",
                            Toast.LENGTH_SHORT).show();
                } else if (editTextWorkingTo.getText().length() == 0) {
                    Toast.makeText(getActivity(), "Enter Working To",
                            Toast.LENGTH_SHORT).show();
                } else if (mWrokTo.equalsIgnoreCase("00:00") || (mWrokTo.equalsIgnoreCase("0:0"))) {
                    Toast.makeText(getActivity(), "Enter Correct To Time",
                            Toast.LENGTH_SHORT).show();
                } else if (editTextDeliveryRange.getText().length() == 0) {
                    Toast.makeText(getActivity(), "Enter Delivery Range",
                            Toast.LENGTH_SHORT).show();
                } else if (mDlRg.equalsIgnoreCase("0")) {
                    Toast.makeText(getActivity(), "Enter Correct Delivery Distance",
                            Toast.LENGTH_SHORT).show();
                } else if (editTextLocation.getText().length() == 0) {
                    Toast.makeText(getActivity(), "Enter Address",
                            Toast.LENGTH_SHORT).show();
                } else if (editTextGoogleLocation.getText().length() == 0) {
                    Toast.makeText(getActivity(), "Fetch Store Location",
                            Toast.LENGTH_SHORT).show();
                } else if (editTextDeliveryCharge.getText().length() == 0) {
                    Toast.makeText(getActivity(), "Enter Delivery Charge",
                            Toast.LENGTH_SHORT).show();
                } else if (editTextDeliveryUpto.getText().length() == 0) {
                    Toast.makeText(getActivity(), "Enter Free Delivery Upto",
                            Toast.LENGTH_SHORT).show();
                } else {
                    if (AppUtils.internetConnectionAvailable(2000)) {
                        UpdateStoreSetUp();
                    } else {
                        Toast.makeText(getActivity(), "No Network Connection",
                                Toast.LENGTH_SHORT).show();
                    }

                }
                break;
            case R.id.delivery_duration:
                //deliveryduration();
                showYearDialog(R.id.delivery_duration);
                break;
            case R.id.working_to_input:
               // WorkingHourToTime();
                showYearDialog(R.id.working_to_input);
                break;
            case R.id.working_from_input:
                //WorkingHourFromTime();
                showYearDialog(R.id.working_from_input);
                break;
            case R.id.delivery_range:
                mDeliveryRange = editTextDeliveryRange.getText().toString().trim();
                break;
            case R.id._image_upload_button:
                mCheckCamaraOrLocation = 1;
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.putExtra(MediaStore.EXTRA_OUTPUT, outPutfileUri);
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
                break;
            case R.id._button_get_location:
                mCheckCamaraOrLocation = 0;
                if (NetworkUtils.isInternetAvailable(true, getActivity())) {
                    Intent i = new Intent(getActivity(), PlacesActivity.class);
                    startActivityForResult(i, 50);
                }
                break;
        }
    }


    public void chooseimagepicker() {
        TextView takephoto;
        TextView choosegallery;
        LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
        View promptView = layoutInflater.inflate(R.layout.alertboxtochoosephoto, null);
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setView(promptView);
        takephoto = promptView.findViewById(R.id.TakePhoto);
        takephoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                file = new File(Environment.getExternalStorageDirectory(),
                        "MyPhoto.jpg");
                outPutfileUri = Uri.fromFile(file);
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, outPutfileUri);
                cameraIntent.putExtra(MediaStore.EXTRA_MEDIA_FOCUS, outPutfileUri);
                cameraIntent.putExtra(MediaStore.EXTRA_SCREEN_ORIENTATION, ActivityInfo.SCREEN_ORIENTATION_LOCKED);
                cameraIntent.putExtra(MediaStore.EXTRA_SCREEN_ORIENTATION, ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                startActivityForResult(cameraIntent, CAMERA_REQUEST);
                alertCamara.dismiss();
            }
        });


        choosegallery = promptView.findViewById(R.id.ChooseFromGallery);
        choosegallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertCamara.dismiss();
            }
        });
        alertCamara = alertDialogBuilder.create();
        alertCamara.show();
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (mCheckCamaraOrLocation == 0) {
            if (resultCode == 50) {
                Double mLatitude = Double.valueOf(data.getStringExtra("latitude"));
                Double mLongitude = Double.valueOf(data.getStringExtra("longitude"));
                postlatitude = String.valueOf(mLatitude);
                postlongitude = String.valueOf(mLongitude);
                edit_store_location.setText(getAddress(Double.parseDouble(postlatitude),Double.parseDouble(postlongitude)));
                _change_address.setVisibility(View.VISIBLE);
                edit_store_location.setVisibility(View.VISIBLE);
                google_location_layout.setVisibility(View.GONE);
                editTextGoogleLocation.setText("Location Received");
            }
        }
        if (mCheckCamaraOrLocation == 1) {
            if (resultCode == Activity.RESULT_OK) {
                if (requestCode == PICK_IMAGE_REQUEST) {
                    onSelectFromGalleryResult(data);
                }
//            if (requestCode == CAMERA_REQUEST) {
//                onCaptureImageResult(data);
//
//            }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Toast.makeText(getActivity(), "Photo Cancelled.",
                        Toast.LENGTH_SHORT).show();
            }
        }
    }


    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {
        try {
            if (data != null) {
                String ddd = getImagePath(data.getData());
                outPutfileUri = Uri.parse(ddd);
                String att = (outPutfileUri.toString()).substring(outPutfileUri.toString().lastIndexOf("/"));
                try {
                    Bitmap convbit = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), data.getData());
                    String imagebit = getStringImage(convbit);
                    bitmap = StringToBitMap(imagebit);
//                bitmap = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), data.getData());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        networkImageViewStorePhoto.setImageBitmap(bitmap);
        if (NetworkUtils.isInternetAvailable(true, getActivity())) {
            profileImageUpload();
        }
    }


    public void getProfileImage() {
        downloadprogress = ProgressDialog.show(getActivity(), "", "Please wait...", false, false);
        String mId = Preference.getDefaults(PreferenceConstants.MERCHANT_ID, getActivity());

        // Server
           String mMerchantId = "http://18.222.124.117:9091/clickshop/getProfileImage?merchantId=" + mId;


//        // Local
//        String mMerchantId = "http://18.222.124.117:8080/clickshop/getProfileImage?merchantId=" + mId;

        StringRequest stringRequest = new StringRequest(Request.Method.GET, mMerchantId,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d(TAG, "responseImg: " + response);
                        downloadprogress.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);

//                            Glide.with(getActivity())
//                                        .load(jsonObject.getString("profile_pic"))
//                                    .error(R.drawable.clickshoplogo)
//                                    .placeholder(R.drawable.clickshoplogo)
//                                        .into(networkImageViewStorePhoto);
                            int mStatusCode = jsonObject.getInt(WebServiceConstants.EXISTING_RESPONSE_CODE);
                            if (mStatusCode == 200) {

                                Log.d("ImageLoded",jsonObject.getString("profilePic"));
                                Glide.with(getActivity())
                                        .load(jsonObject.getString("profilePic"))
                                        .into(networkImageViewStorePhoto);

//                                decodeimage = decodeBase64(jsonObject.getString(WebServiceConstants.PROFILE_IMAGE));
//                                networkImageViewStorePhoto.setImageBitmap(decodeimage);
                            } else if (mStatusCode == 400) {
                                Toast.makeText(getActivity(), "Store Photo Not Available",
                                        Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        downloadprogress.dismiss();
                        NetworkUtils.UnableToReachServer(getActivity());
                    }
                });
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(stringRequest);
    }

    public static Bitmap decodeBase64(String input) {
        byte[] decodedBytes = Base64.decode(input, 0);
        return BitmapFactory.decodeByteArray(decodedBytes, 0, decodedBytes.length);
    }


    public void Message(String message) {
        LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
        View promptView = layoutInflater.inflate(R.layout.alert_message, null);
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setView(promptView);
        TextView textViewMessage = promptView.findViewById(R.id.alert_message_display);
        textViewMessage.setText(message);
        Button buttonblock = (Button) promptView.findViewById(R.id.alert_ok_buton);
        buttonblock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alert.dismiss();
            }
        });
        alert = alertDialogBuilder.create();
        alert.show();
    }


    private void profileImageUpload() {
        progress = ProgressDialog.show(getActivity(), "", "Please wait...", false, false);
        VolleyMultipartRequest multipartRequest = new VolleyMultipartRequest(Request.Method.POST, EndPoint.PROFILE_IMAGE_UPLOAD, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {

                String resultResponse = new String(response.data);
                Log.d(TAG, "resultResponse  " + resultResponse);


                try {
                    String jsonresp = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                    Log.d(TAG, "jsonresp  " + jsonresp);
                    try {
                        JSONObject jsonObject = new JSONObject(jsonresp);
                        String mMessage = jsonObject.getString("responseCode");
                        if (mMessage.equalsIgnoreCase("200")) {
                            Message("Store Photo Uploaded Successfully");
                        } else if (mMessage.equalsIgnoreCase("400")) {
                            Message("Store Photo Upload Failed. Please Retry..");
                        } else {
                            Message("Store Photo Upload Failed. Please Retry..");
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                progress.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "PostingImageUploaderror  " + error);
                progress.dismiss();

            }
        }) {


            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                SessionManager sessionManager = new SessionManager(getActivity());
                HashMap<String, String> hashMap = sessionManager.getUserDetails();
                String mSessionId = hashMap.get(SessionManager.KEY_SESSION_ID);

                params.put("merchantId", Preference.getDefaults(PreferenceConstants.MERCHANT_ID, getActivity()));
//                params.put("sessionId", mSessionId);

                Log.d(TAG, "PostingImageUpload  " + params);
                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                String file_path = (outPutfileUri.toString()).substring(outPutfileUri.toString().lastIndexOf("/"));
                filepathshort = removeFirstOne(file_path);
                String compressfile = compressImage(outPutfileUri.toString());
                Bitmap bitmaps = BitmapFactory.decodeFile(compressfile);
                Bitmap bitmaparray = Bitmap.createScaledBitmap(bitmaps, 250, 100, false);
                ByteArrayOutputStream bosarray = new ByteArrayOutputStream();
                bitmaparray.compress(Bitmap.CompressFormat.JPEG, 80, bosarray);
                byte[] imageBytesarray = bosarray.toByteArray();
                params.put("file", new DataPart(filepathshort, imageBytesarray, "image/jpg"));
                Log.d(TAG, "Data Params   " + params);
                return params;
            }
        };


        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(multipartRequest);
    }


    public String getAddress(Double latitude,Double longitude){


        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(getActivity(), Locale.getDefault());

        String address = "",city="";
        try {
            addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5


         address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
         city = addresses.get(0).getLocality();
        String state = addresses.get(0).getAdminArea();
        String country = addresses.get(0).getCountryName();
        String postalCode = addresses.get(0).getPostalCode();
        String knownName = addresses.get(0).getFeatureName(); // Only if available else return NULL
        } catch (IOException e) {
            e.printStackTrace();
        }

        return  address+" "+city;
    }



    public String removeFirstOne(String input) {
        return input.substring(1);
    }


    public String getImagePath(Uri uri) {
        try {
            cursor = getActivity().getContentResolver().query(uri, null, null, null, null);
            cursor.moveToFirst();
            String document_id = cursor.getString(0);
            document_id = document_id.substring(document_id.lastIndexOf(":") + 1);
            cursor.close();
            cursor = getActivity().getContentResolver().query(
                    android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                    null, MediaStore.Images.Media._ID + " = ? ", new String[]{document_id}, null);
            cursor.moveToFirst();
            path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
            cursor.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (CursorIndexOutOfBoundsException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        return path;
    }


    public Bitmap StringToBitMap(String encodedString) {
        try {
            byte[] encodeByte = Base64.decode(encodedString, Base64.DEFAULT);
            Bitmap bitmap = BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
            return bitmap;
        } catch (Exception e) {
            e.getMessage();
            return null;
        }
    }

    public String getStringImage(Bitmap bmp) {
        int newWidth = 800;
        int newHeight = 600;

        Bitmap resized = Bitmap.createScaledBitmap(bmp, newWidth, newHeight, true);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        resized.compress(Bitmap.CompressFormat.JPEG, 90, baos);
        byte[] imageBytes = baos.toByteArray();
        String input = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return input;
    }


    // Whatsapp like image compression
    public String compressImage(String imageUri) {
        String filePath = getRealPathFromURI(imageUri);
        Bitmap scaledBitmap = null;
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        Bitmap bmp = BitmapFactory.decodeFile(filePath, options);
        int actualHeight = options.outHeight;
        int actualWidth = options.outWidth;
//      max Height and width values of the compressed image is taken as 816x612
        float maxHeight = 816.0f;
        float maxWidth = 812.0f;


        // This size will take too much time to load
//        float maxHeight = 1200.0f;
//        float maxWidth = 1066.0f;
        float imgRatio = actualWidth / actualHeight;
        float maxRatio = maxWidth / maxHeight;

//      width and height values are set maintaining the aspect ratio of the image

        if (actualHeight > maxHeight || actualWidth > maxWidth) {
            if (imgRatio < maxRatio) {
                imgRatio = maxHeight / actualHeight;
                actualWidth = (int) (imgRatio * actualWidth);
                actualHeight = (int) maxHeight;
            } else if (imgRatio > maxRatio) {
                imgRatio = maxWidth / actualWidth;
                actualHeight = (int) (imgRatio * actualHeight);
                actualWidth = (int) maxWidth;
            } else {
                actualHeight = (int) maxHeight;
                actualWidth = (int) maxWidth;

            }
        }
//      setting inSampleSize value allows to load a scaled down version of the original image
        options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight);
//      inJustDecodeBounds set to false to load the actual bitmap
        options.inJustDecodeBounds = false;
//      this options allow android to claim the bitmap memory if it runs low on memory
        options.inPurgeable = true;
        options.inInputShareable = true;
        options.inTempStorage = new byte[16 * 1024];
        try {
//          load the bitmap from its path
            bmp = BitmapFactory.decodeFile(filePath, options);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();

        }
        try {
            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();
        }

        float ratioX = actualWidth / (float) options.outWidth;
        float ratioY = actualHeight / (float) options.outHeight;
        float middleX = actualWidth / 2.0f;
        float middleY = actualHeight / 2.0f;
        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);
        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2, middleY - bmp.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));



//      check the rotation of the image and display it properly
//        try {
//            ExifInterface ei = new ExifInterface(filePath);
//            int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
//                    ExifInterface.ORIENTATION_UNDEFINED);
//            bitmap = null;
//            switch (orientation) {
//                case ExifInterface.ORIENTATION_ROTATE_90:
//                    bitmap = rotateImage(bitmap, 90);
//                    break;
//                case ExifInterface.ORIENTATION_ROTATE_180:
//                    bitmap = rotateImage(bitmap, 180);
//                    break;
//                case ExifInterface.ORIENTATION_ROTATE_270:
//                    bitmap = rotateImage(bitmap, 270);
//                    break;
//                case ExifInterface.ORIENTATION_NORMAL:
//                default:
//                    bitmap = bitmap;
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }


        FileOutputStream out = null;
        String filename = getFilename();
        try {
            out = new FileOutputStream(filename);
//          write the compressed bitmap at the destination specified by filename.
            scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return filename;
    }


    private static Bitmap rotateImage(Bitmap img, int degree) {
        Matrix matrix = new Matrix();
        matrix.postRotate(degree);
        Bitmap rotatedImg = Bitmap.createBitmap(img, 0, 0, img.getWidth(), img.getHeight(), matrix, true);
        img.recycle();
        return rotatedImg;
    }


    public String getFilename() {
        File file = new File(Environment.getExternalStorageDirectory().getPath(), "MyFolder/Images");
        if (!file.exists()) {
            file.mkdirs();
        }
        String uriSting = (file.getAbsolutePath() + "/" + System.currentTimeMillis() + ".jpg");
        return uriSting;

    }

    private String getRealPathFromURI(String contentURI) {
        Uri contentUri = Uri.parse(contentURI);
        Cursor cursor = getActivity().getContentResolver().query(contentUri, null, null, null, null);
        if (cursor == null) {
            return contentUri.getPath();
        } else {
            cursor.moveToFirst();
            int index = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(index);
        }
    }

    public int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;
        if (height > reqHeight || width > reqWidth) {
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }
        final float totalPixels = width * height;
        final float totalReqPixelsCap = reqWidth * reqHeight * 2;
        while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
            inSampleSize++;
        }
        return inSampleSize;
    }


    public void UpdateStoreSetUp() {


        Log.d("DeliveryRange", mDeliveryRange);
        progress = ProgressDialog.show(getActivity(), "", "Please wait...", false, false);

        SessionManager sessionManager = new SessionManager(getActivity());
        HashMap<String, String> hashMap = sessionManager.getUserDetails();
        String mSessionId = hashMap.get(SessionManager.KEY_SESSION_ID);
        String mMobile = hashMap.get(SessionManager.KEY_PHONE_NUMBER);
        String mDeliveryAmount = editTextDeliveryCharge.getText().toString().trim();
        String mFreeDeliveryAbove = editTextDeliveryUpto.getText().toString().trim();



        HashMap<String, String> params = new HashMap<String, String>();
        params.put("merchantId", Preference.getDefaults(PreferenceConstants.MERCHANT_ID, getActivity()));
        params.put("storeId", mStoreId);
        params.put("storeName", mStoreName);
        params.put("deliveryDuration", mDeliveryDuration);
        params.put("workingHourFrom", mWorkingHourFromTime);
        params.put("workingHourTo", mWorkingHourToTime);
        params.put("deliveryRange", mDeliveryRange);
        params.put("latitude", postlatitude);
        params.put("longitude", postlongitude);
        params.put("address", postaddress);
        params.put("city", postcity);
        params.put("state", poststate);
        params.put("country", postcountry);
        params.put("pinCode", postpinCode);
        params.put("flag", Preference.getDefaults(PreferenceConstants.LOGIN_FLAG, getActivity()));
        params.put("sessionId", mSessionId);
        params.put("mobileNumber", mMobile);
        params.put("deliveryCharge", mDeliveryAmount);
        params.put("deliveryFreeAbove", mFreeDeliveryAbove);

        Log.d(TAG, "params: " + params);
        JsonObjectRequest request_json = new JsonObjectRequest(EndPoint.UPDATE_PROFILE_SETUP, new JSONObject(params),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        progress.dismiss();
                        try {
                            Integer mStatusCode = response.getInt(WebServiceConstants.EXISTING_RESPONSE_CODE);
                            if (mStatusCode == 400) {
                                SuccessFailureMessage("Update Failed. Please Retry..");
                            } else if (mStatusCode == 200) {
                                SuccessFailureMessage("Updated Successfully");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Log.d(TAG, "Delete Item Response: " + response);

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progress.dismiss();
                NetworkUtils.UnableToReachServer(getActivity());
            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(request_json);
    }


    public void SuccessFailureMessage(String message) {
        try {
            LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
            View promptView = layoutInflater.inflate(R.layout.alert_success_failure_message, null);
            final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
            alertDialogBuilder.setView(promptView);
            TextView textViewMessage = promptView.findViewById(R.id.alert_message_display);
            textViewMessage.setText(message);
            Button buttonblock = promptView.findViewById(R.id.alert_ok_buton);
            buttonblock.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    alert.dismiss();
                    Intent i =new Intent(getActivity(),MainActivity.class);
                    startActivity(i);

                }
            });
            alert = alertDialogBuilder.create();
            alert.show();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }


    public void deliveryduration() {
        Calendar mcurrentTimeb = Calendar.getInstance();
        int hourb = mcurrentTimeb.get(Calendar.HOUR_OF_DAY);
        int minuteb = mcurrentTimeb.get(Calendar.MINUTE);
        TimePickerDialog mTimePickerb;
        mTimePickerb = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                editTextDeliveryDuration.setText(String.format("%02d:%02d", selectedHour, selectedMinute));
                mDeliveryDuration = String.format("%02d:%02d", selectedHour, selectedMinute);
            }
        }, hourb, minuteb, true);//Yes 24 hour time
        mTimePickerb.show();
    }

    public void WorkingHourFromTime() {
        Calendar mcurrentTimeb = Calendar.getInstance();
        int hourb = mcurrentTimeb.get(Calendar.HOUR_OF_DAY);
        int minuteb = mcurrentTimeb.get(Calendar.MINUTE);
        TimePickerDialog mTimePickerb;
        mTimePickerb = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHourFromTime, int selectedMinuteFromTime) {
                editTextWorkingFrom.setText(String.format("%02d:%02d", selectedHourFromTime, selectedMinuteFromTime));
                mWorkingHourFromTime = String.format("%02d:%02d", selectedHourFromTime, selectedMinuteFromTime);
            }
        }, hourb, minuteb, true);//Yes 24 hour time
        mTimePickerb.show();
    }


    public void WorkingHourToTime() {
        Calendar mcurrentTimeb = Calendar.getInstance();
        int hourb = mcurrentTimeb.get(Calendar.HOUR_OF_DAY);
        int minuteb = mcurrentTimeb.get(Calendar.MINUTE);
        TimePickerDialog mTimePickerb;
        mTimePickerb = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHourToTime, int selectedMinuteToTime) {
                editTextWorkingTo.setText(String.valueOf(selectedHourToTime) + ":" + String.valueOf(selectedMinuteToTime));
                mWorkingHourToTime = String.format("%02d:%02d", selectedHourToTime, selectedMinuteToTime);
            }
        }, hourb, minuteb, true);//Yes 24 hour time
        mTimePickerb.show();
    }





    public void showYearDialog(int str)

    {


        final int string=str;


        final Dialog d = new Dialog(getActivity());

        d.setTitle("Year Picker");

        d.setContentView(R.layout.timedialog);

        Button set = (Button) d.findViewById(R.id.button1);

        Button cancel = (Button) d.findViewById(R.id.button2);

//        TextView year_text=(TextView)d.findViewById(R.id.title_text);
//
//        title_text.setText(hour+":"+minute);

        final NumberPicker nopicker = (NumberPicker) d.findViewById(R.id.numberPicker1);

        final NumberPicker nopicker1 = (NumberPicker) d.findViewById(R.id.numberPicker2);
        final NumberPicker nopicker2 = (NumberPicker) d.findViewById(R.id.numberPicker3);

        nopicker.setMaxValue(12);

        nopicker.setMinValue(1);

        nopicker.setWrapSelectorWheel(false);

        nopicker.setValue(hour);

        nopicker.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);





        nopicker1.setMaxValue(60);

        nopicker1.setMinValue(minute);

        nopicker1.setWrapSelectorWheel(false);

        nopicker1.setValue(minute);

        nopicker1.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);




        final String[] values = {"AM", "PM"};
        nopicker2.setMinValue(0);
        nopicker2.setMaxValue(values.length - 1);
        nopicker2.setDisplayedValues(values);
        nopicker2.setWrapSelectorWheel(true);



        if(string==R.id.delivery_duration){

            nopicker2.setVisibility(View.GONE);
        }
        else
            nopicker2.setVisibility(View.VISIBLE);


        nopicker2.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {



            }
        });

        nopicker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {

            @Override

            public void onValueChange(NumberPicker picker, int oldVal, int newVal){

                //Display the newly selected number from picker

                //  tv.setText("Selected Number : " + newVal);

                if(nopicker.getValue()==hour)

                {

                    nopicker1.setMinValue(minute);

                }else {

                    nopicker1.setMinValue(0);

                }

            }

        });



        set.setOnClickListener(new View.OnClickListener()

        {

            @Override            public void onClick(View v) {


                if(nopicker2.getValue()==0){
                    timeformat="AM";
                }
                else
                    timeformat="PM";


                if(string==R.id.working_from_input)
                    editTextWorkingFrom.setText(String.valueOf(nopicker.getValue()+":"+nopicker1.getValue())+ " "+timeformat);
                else if(string==R.id.working_to_input)
                    editTextWorkingTo.setText(String.valueOf(nopicker.getValue()+":"+nopicker1.getValue())+ " "+timeformat);
                else
                    editTextDeliveryDuration.setText(String.valueOf(nopicker.getValue()+":"+nopicker1.getValue()));
                d.dismiss();

            }

        });

        cancel.setOnClickListener(new View.OnClickListener()

        {

            @Override            public void onClick(View v) {

                d.dismiss();

            }

        });

        d.show();

    }



}
