package com.clickshopmerchant.shop.main.model.selecteditemmodel;

public class RequestBodySelectedItem {
    String merchantId, sessionId,mobileNumber ;

    public RequestBodySelectedItem(String mobileNumber, String merchantId, String sessionId) {
        this.mobileNumber = mobileNumber;
        this.merchantId = merchantId;
        this.sessionId = sessionId;
    }
}
