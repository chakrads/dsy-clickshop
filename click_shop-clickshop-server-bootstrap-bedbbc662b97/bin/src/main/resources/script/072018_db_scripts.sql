# Merchant queries ##############################

SELECT * FROM clickshop.t_merchant;
insert into clickshop.t_merchant(address,mobile_number,password) values('Motihari','6482628476','merashop');

#################################################

# Item queries ##############################

SELECT * FROM clickshop.t_item;

#################################################

# Insert Item Category: ##############################

INSERT INTO `qaclickshop`.`t_item_category` (`item_category_id`, `item_category_image`, `item_category_name`) VALUES
(1, 'https://s3.ap-south-1.amazonaws.com/clickshop-images/categories/bakery.jpeg', 'Bakery'),
(2, 'https://s3.ap-south-1.amazonaws.com/clickshop-images/categories/beverages.jpeg', 'Beverages'),
(3, 'https://s3.ap-south-1.amazonaws.com/clickshop-images/categories/cereal.jpeg', 'Cooking Medium'),
(4, 'https://s3.ap-south-1.amazonaws.com/clickshop-images/categories/confectionery.jpeg', 'Confectionary'),
(5, 'https://s3.ap-south-1.amazonaws.com/clickshop-images/categories/cosmetic.jpeg', 'Cosmetic'),
(6, 'https://s3.ap-south-1.amazonaws.com/clickshop-images/categories/dairyproducts.jpeg', 'Dairy Products'),
(7, 'https://s3.ap-south-1.amazonaws.com/clickshop-images/categories/dryfruits.jpeg', 'Dry Fruits'),
(8, 'https://s3.ap-south-1.amazonaws.com/clickshop-images/categories/personalcare.png', 'Personal Care'),
(9, 'https://s3.ap-south-1.amazonaws.com/clickshop-images/categories/snacks.jpeg', 'Snacks'),
(10, 'https://s3.ap-south-1.amazonaws.com/clickshop-images/categories/spices.jpeg', 'Masala');

#################################################