package com.clickshop.server.fcm;

import org.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.stereotype.Component;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

@Component
public class SendOrderNotifs {

    public String sendOrderStatusNotifsToUser(String firebaseToken, String orderStatus) {

        JSONObject body = new JSONObject();
        body.put("to", firebaseToken);
        body.put("priority", "high");

        JSONObject notification = new JSONObject();
        notification.put("title", "Current Order Status from Merchant!");
        notification.put("body", "Order status: " + orderStatus);
        notification.put("sound", "default");

        body.put("notification", notification);
        HttpEntity<String> request = new HttpEntity<>(body.toString());

        try {
            AndroidPushNotificationsService androidPushNotificationsService = new AndroidPushNotificationsService();
            CompletableFuture<String> pushNotification = androidPushNotificationsService.send(request);
            CompletableFuture.allOf(pushNotification).join();
            String firebaseResponse = pushNotification.get();
            return firebaseResponse;
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        return null;
    }
}
