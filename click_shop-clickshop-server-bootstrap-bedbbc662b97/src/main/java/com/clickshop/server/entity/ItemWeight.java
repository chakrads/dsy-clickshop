package com.clickshop.server.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "t_item_weight")
@Getter
@Setter
public class ItemWeight implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "item_weight_id")
    private Long itemWeightId;

    @Column(name = "item_weight_name")
    private String itemWeightName;

}
