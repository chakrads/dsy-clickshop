package com.clickshop.server.entity;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "t_contactUs")
@Getter
@Setter
public class ContactUs implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "cus_id")
    private Long contactUsId;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "merchant_id")
    private Merchant merchant;

    @NotBlank
    @Column(name = "name")
    private String contactUsName;

    @NotBlank
    @Column(name = "email")
    private String contactUsEmail;

    @NotBlank
    @Column(name = "mobile")
    private String contactUsMobileNo;

    @NotBlank
    @Column(name = "message")
    private String contactUsMessage;

    @Temporal(TemporalType.DATE)
    @Column(name = "contacted_time", updatable = false, insertable = false, columnDefinition = "TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP")
    private Date contactedTime;

}
