package com.clickshop.server.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "t_branded_item")
@Getter
@Setter
public class BrandedItem implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "branded_item_id")
    private Long brandedItemId;

    @Column(name = "barcode")
    private String barcode;

    @Column(name = "brand_name")
    private String brandName;

    @Column(name = "company_name")
    private String companyName;

    @Column(name = "family")
    private String family;

    @Column(name = "item_category")
    private String itemCategory;

    @Column(name = "item_image")
    private String itemImage;

    @Column(name = "item_name")
    private String itemName;

    @Column(name = "item_price")
    private String itemPrice;

    @Column(name = "item_subcategory")
    private String itemSubcategory;

    @Column(name = "item_weight")
    private String itemWeight;

    @Column(name = "unit")
    private String itemWeightUnit;

    @Column(name = "last_updated_timestamp")
    private String lastUpdatedDate;

    @Column(name = "price_unit")
    private String priceUnit;

    @Column(name = "status")
    private String status;

}
