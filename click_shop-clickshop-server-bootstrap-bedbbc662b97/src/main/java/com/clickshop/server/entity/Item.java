package com.clickshop.server.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Dell on 7/21/2018.
 */
@Entity
@Table(name = "t_item")
@Getter
@Setter
public class Item implements Serializable {

    private static final long serialVersionUID = 2307741389804763162L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "item_id")
    private Long itemId;

    @Column(name = "item_name")
    private String itemName;

    @Column(name = "brand_name")
    private String brandName;

    @Column(name = "company_name")
    private String companyName;

    @Column(name = "item_weight")
    private String itemWeight;

    @Column(name = "unit")
    private String itemWeightUnit;

    @Column(name = "item_category")
    private String itemCategory;

    @Column(name = "item_subcategory")
    private String itemSubcategory;

    @Column(name = "family")
    private String family;

    @Column(name = "item_price")
    private String itemPrice;

    @Column(name = "price_unit")
    private String priceUnit;

    @Column(name = "barcode")
    private String barcode;

    @Column(name = "item_image")
    private String itemImage;

    @Column(name = "last_updated_timestamp")
    private String lastUpdatedDate;

    @Column(name = "status")
    private String status;

}
