package com.clickshop.server.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "t_profile_image")
@Getter
@Setter
public class ProfileImage implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "imageId")
    private Long imageId;

    // @Lob
    @Column(name = "image_url")
    // private byte[] profilePic;
    private String profilePic;

    @Column(name = "merchant_Id")
    private Long merchantId;

}
