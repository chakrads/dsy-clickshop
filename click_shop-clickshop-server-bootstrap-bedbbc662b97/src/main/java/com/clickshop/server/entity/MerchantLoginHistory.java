package com.clickshop.server.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "t_merchant_login_history")
@Getter
@Setter
public class MerchantLoginHistory implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "merchant_login_history_id")
    private Long merchantLoginHistoryId;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "merchant_user_id")
    private MerchantUser merchantUser;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "merchant_device_id")
    private MerchantDevice merchantDevice;

    @Column(name = "latitude")
    private double latitude;

    @Column(name = "longitude")
    private double longitude;

    @Column(name = "session_id")
    private String sessionId;

    @Temporal(TemporalType.DATE)
    @Column(name = "login_date", updatable = false, insertable = false, columnDefinition = "TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP")
    private Date loginDate;

}
