package com.clickshop.server.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "t_merchant_wallet")
@Getter
@Setter
public class MerchantWallet implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "merchant_wallet_id")
    private Long merchantWalletId;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "merchant_id")
    private Merchant merchant;

    @Column(name = "total_point")
    private String totalPoint;

    @Column(name = "total_Order_Amount")
    private String totalOrderAmount;

    @Column(name = "balance_point")
    private String balancePoint;

    @Temporal(TemporalType.DATE)
    @Column(name = "updated_time", updatable = false, insertable = false, columnDefinition = "TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP")
    private Date updatedTime;

}
