package com.clickshop.server.entity;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "t_order")
@Getter
@Setter
public class Order implements Serializable {

    @Id
    @GenericGenerator(name = "sequence_order_id", strategy = "com.clickshop.server.utils.OrderUtility")
    @GeneratedValue(generator = "sequence_order_id")
    @Column(name = "order_id")
    private String orderId;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user;

    @Column(name = "shipping_address")
    private String shippingAddress;

    @Column(name = "mobile_number")
    private String mobileNumber;

    @Column(name = "payment_mode")
    private String paymentMode;

    @Column(name = "order_amount")
    private String orderAmount;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "merchant_id")
    private Merchant merchant;

    @Column(name = "order_status")
    private String orderStatus;

    @Column(name = "delivery_charge")
    private String deliveryCharge;

    @Temporal(TemporalType.DATE)
    @Column(name = "order_time", updatable = false, insertable = false, columnDefinition = "TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP")
    private Date orderTime;

}
