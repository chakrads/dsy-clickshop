package com.clickshop.server.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "t_item_category")
@Getter
@Setter
public class ItemCategory implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "item_category_id")
    private Long itemCategoryId;

    @Column(name = "item_category_name", nullable = false)
    private String itemCategoryName;

    @Column(name = "item_category_image", nullable = false)
    private String itemCategoryImage;

}
