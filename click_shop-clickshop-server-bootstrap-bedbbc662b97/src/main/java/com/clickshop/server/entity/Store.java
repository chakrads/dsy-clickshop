package com.clickshop.server.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Dell on 8/2/2018.
 */
@Entity
@Table(name = "t_store")
@Getter
@Setter
public class Store implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "store_id")
    private Long storeId;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "merchant_id")
    private Merchant merchant;

    @Column(name = "store_name")
    private String storeName;

    @Column(name = "store_rating")
    private Double storeRating;

    @Column(name = "delivery_duration")
    private String deliveryDuration;

    @Column(name = "working_hour_from")
    private String workingHourFrom;

    @Column(name = "working_hour_to")
    private String workingHourTo;

    @Column(name = "delivery_range")
    private String deliveryRange;

    @Column(name = "latitude")
    private double latitude;

    @Column(name = "longitude")
    private double longitude;

    @Column(name = "address")
    private String address;

    @Column(name = "city")
    private String city;

    @Column(name = "state")
    private String state;

    @Column(name = "country")
    private String country;

    @Column(name = "pin_code")
    private String pinCode;

    @Column(name = "flag")
    private String flag;

    @Column(name = "delivery_charge")
    private String deliveryCharge;

    @Column(name = "delivery_free_above")
    private String deliveryFreeAbove;
}
