package com.clickshop.server.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "t_merchant_device")
@Getter
@Setter
public class MerchantDevice implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "merchant_device_id")
    private Long merchantDeviceId;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "merchant_user_id")
    private MerchantUser merchantUser;

    @Column(name = "device_unique_id")
    private String deviceUniqueId;

    @Column(name = "device_type")
    private String deviceType;

    @Column(name = "is_active")
    private Integer isActive;

    @Column(name = "is_blocked")
    private Integer isBlocked;

}
