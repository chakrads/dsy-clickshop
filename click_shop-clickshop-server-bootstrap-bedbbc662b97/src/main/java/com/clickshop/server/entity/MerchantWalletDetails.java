package com.clickshop.server.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "t_merchant_wallet_details")
@Getter
@Setter
public class MerchantWalletDetails implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "merchant_wallet_details_id")
    private Long merchantWalletDetailsId;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "merchant_wallet_id")
    private MerchantWallet merchantWallet;

    @Column(name = "amount")
    private String amount;

    @Column(name = "mode_of_payment")
    private String modeOfPayment;

    @Column(name = "approved")
    private String approved;

    @Column(name = "approved_by")
    private String approvedBy;

    @Temporal(TemporalType.DATE)
    @Column(name = "payment_time", updatable = false, insertable = false, columnDefinition = "TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP")
    private Date paymentTime;

}
