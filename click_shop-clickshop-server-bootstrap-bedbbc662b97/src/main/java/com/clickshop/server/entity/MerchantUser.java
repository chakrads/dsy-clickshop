package com.clickshop.server.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "t_merchant_user")
@Getter
@Setter
public class MerchantUser implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "merchant_user_id")
    private Long merchantUserId;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "merchant_id")
    private Merchant merchant;

    @Column(name = "mobile_number")
    private String mobileNumber;

    @Column(name = "mobile_verified")
    private String mobileVerified;

    @Column(name = "password")
    private String password;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "active")
    private String active;

    @Column(name = "firebase_token")
    private String firebaseToken;

    @Temporal(TemporalType.DATE)
    @Column(name = "registered_date", updatable = false, insertable = false, columnDefinition = "TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP")
    private Date registeredDate;

}
