package com.clickshop.server.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class OrderSearchRequestDto implements Serializable {
    private String mobileNumber;
    private Long merchantId;
    private String orderStatus;
    private String sessionId;
}
