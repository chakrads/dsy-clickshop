package com.clickshop.server.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class OrderStatusSaveRequestDto implements Serializable {

    private String mobileNumber;
    private Long merchantId;
    private String orderId;
    private String orderStatus;
    private String sessionId;
}
