package com.clickshop.server.dto.response;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class StoreItemResponseDto implements Serializable {

    private Long itemId;
    private String itemName;
    private String itemCategory;
    private String itemWeight;
    private String itemWeightUnit;
    private String itemPrice;
    private String itemImage;
    private String itemMRP;
    private String type;

}
