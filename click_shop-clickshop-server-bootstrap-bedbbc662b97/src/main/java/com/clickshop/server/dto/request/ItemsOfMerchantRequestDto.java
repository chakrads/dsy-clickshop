package com.clickshop.server.dto.request;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * Created by Dell on 7/22/2018.
 */
@Getter
@Setter
public class ItemsOfMerchantRequestDto implements Serializable {

    private String mobileNumber;
    private Long merchantId;
    private Long itemId;
    private String itemName;
    private String itemPrice;
    private Long itemCategoryId;
    private String type;
    private String sessionId;

}
