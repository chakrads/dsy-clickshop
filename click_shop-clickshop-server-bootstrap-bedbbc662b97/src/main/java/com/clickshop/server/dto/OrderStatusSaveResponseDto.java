package com.clickshop.server.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class OrderStatusSaveResponseDto implements Serializable {

    private Long responseCode;
    private String responseMessage;
    private String orderId;
    private Long userId;
    private String shippingAddress;
    private String paymentMode;
    private String orderAmount;
    private Long merchantId;
    private String orderStatus;
    private String deliveryCharge;

}
