package com.clickshop.server.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class MerchantRequestDto implements Serializable {

    private String mobileNumber;
    private String password;
    private String firstName;
    private String lastName;
    private String address;
    private String gstNumber;
    private String storeNumber;
    private String email;
    private String flag;
    private String firebaseToken;
    private String deviceUniqueId;
    private String deviceType;
    private String latitude;
    private String longitude;

}
