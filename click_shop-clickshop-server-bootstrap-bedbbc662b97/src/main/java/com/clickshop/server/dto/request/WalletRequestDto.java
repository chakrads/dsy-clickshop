package com.clickshop.server.dto.request;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class WalletRequestDto implements Serializable {
    private String mobileNumber;
    private Long merchantId;
    private String amount;
    private String sessionId;
}
