package com.clickshop.server.dto.response;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class WalletResponseDto implements Serializable {

    private Long responseCode;
    private String responseMessage;
    private Long merchantId;
    private String totalPoint;
    private String totalOrderAmount;
    private String balancePoint;

}
