package com.clickshop.server.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class OrderSearchListResponseDto implements Serializable {

    private String orderId;
    private Long userId;
    private String userName;
    private String shippingAddress;
    private String mobileNumber;
    private String paymentMode;
    private String orderAmount;
    private Long merchantId;
    private String orderStatus;
    private String deliveryCharge;

}
