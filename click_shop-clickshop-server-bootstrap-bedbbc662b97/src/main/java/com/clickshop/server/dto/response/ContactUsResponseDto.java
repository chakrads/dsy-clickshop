package com.clickshop.server.dto.response;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class ContactUsResponseDto implements Serializable {

    private Long responseCode;
    private String responseMessage;
    private Long contactUsId;
    private Long merchantId;
    private String contactUsName;
    private String contactUsEmail;
    private String contactUsMobileNo;
    private String contactUsMessage;
}
