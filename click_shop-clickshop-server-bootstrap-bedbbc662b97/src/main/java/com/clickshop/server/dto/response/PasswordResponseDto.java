package com.clickshop.server.dto.response;


import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class PasswordResponseDto implements Serializable {

    private String responseMessage;
    private Long responseCode;
}
