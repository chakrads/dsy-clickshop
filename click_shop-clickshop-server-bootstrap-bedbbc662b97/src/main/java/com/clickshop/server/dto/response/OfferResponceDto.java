package com.clickshop.server.dto.response;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * Created by Dell on 7/22/2018.
 */
@Getter
@Setter
public class OfferResponceDto implements Serializable {

    private Long responseCode;
    private String responseMessage;
    private Long offerId;
    private Long merchantId;
    private Long itemId;
    private String actualPrice;
    private String discountPercentage;
    private String discountAmount;
    private String priceAfterDiscount;

}
