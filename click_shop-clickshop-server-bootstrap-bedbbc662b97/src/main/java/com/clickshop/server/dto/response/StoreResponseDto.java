package com.clickshop.server.dto.response;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * Created by Dell on 8/2/2018.
 */
@Getter
@Setter
public class StoreResponseDto implements Serializable {

    private Long responseCode;
    private String responseMessage;
    private Long storeId;
    private Long merchantId;
    private String storeName;
    private Double storeRating;
    private String deliveryDuration;
    private String workingHourFrom;
    private String workingHourTo;
    private String deliveryRange;
    private double latitude;
    private double longitude;
    private String address;
    private String city;
    private String state;
    private String country;
    private String pinCode;
    private String flag;
    private String deliveryCharge;
    private String deliveryFreeAbove;
}
