package com.clickshop.server.dto.response;

import com.clickshop.server.entity.Item;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Dell on 7/30/2018.
 */
@Getter
@Setter
public class MerchantItemGetResponseDto implements Serializable {

    private Long responseCode;
    private String responseMessage;
    private List<StoreItemResponseDto> itemList;
}
