package com.clickshop.server.dto.response;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class BrandedItemsOfMerchantResponseDto implements Serializable {

    private Long responseCode;
    private String responseMessage;
    private Long itemMerchantId;
    private Long merchantId;
    private Long brandedItemId;
    private String storeNumber;
    private String itemName;
    private String itemPrice;
    private String itemWeight;
    private String itemCategory;
    private String createdOn;
    private String createdBy;
    private String lastUpdatedBy;
    private String lastUpdatedOn;

}
