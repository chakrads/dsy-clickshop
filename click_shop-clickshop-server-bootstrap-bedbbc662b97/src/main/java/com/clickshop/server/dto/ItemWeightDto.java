package com.clickshop.server.dto;

import com.clickshop.server.entity.ItemWeight;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
public class ItemWeightDto implements Serializable {

    private Long responseCode;
    private String responseMessage;
    private List<ItemWeight> data;

}
