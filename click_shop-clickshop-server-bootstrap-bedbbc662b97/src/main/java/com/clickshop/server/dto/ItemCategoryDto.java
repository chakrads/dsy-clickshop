package com.clickshop.server.dto;

import com.clickshop.server.entity.ItemCategory;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
public class ItemCategoryDto implements Serializable {

    private Long responseCode;
    private String responseMessage;
    private List<ItemCategory> data;

}
