package com.clickshop.server.dto.response;

import com.clickshop.server.entity.Item;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
public class ItemCategoryListResponseDto implements Serializable {
    private String itemCategory;
    private List<Item> itemList;
}
