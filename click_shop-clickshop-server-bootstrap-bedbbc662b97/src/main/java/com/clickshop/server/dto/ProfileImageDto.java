package com.clickshop.server.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class ProfileImageDto implements Serializable {

    private Long responseCode;
    private String responseMessage;
    private Long imageId;
    private String profilePic;
    private Long merchantId;

}
