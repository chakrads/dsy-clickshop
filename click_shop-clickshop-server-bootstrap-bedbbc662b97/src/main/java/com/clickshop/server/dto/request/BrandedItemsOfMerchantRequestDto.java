package com.clickshop.server.dto.request;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class BrandedItemsOfMerchantRequestDto implements Serializable {

    private Long merchantId;
    private Long brandedItemId;
    private String itemName;
    private String itemPrice;
    private String itemWeight;
    private String itemCategory;

}
