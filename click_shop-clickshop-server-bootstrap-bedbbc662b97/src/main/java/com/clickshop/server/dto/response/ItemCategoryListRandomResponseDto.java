package com.clickshop.server.dto.response;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
public class ItemCategoryListRandomResponseDto implements Serializable {

    private Long responseCode;
    private String responseMessage;
    private List<ItemCategoryListResponseDto> itemCategoryListRandom;
}
