package com.clickshop.server.dto.response;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * Created by Dell on 7/28/2018.
 */

@Getter
@Setter
public class OfferDeleteResponceDto implements Serializable {
    private Long responseCode;
    private String responseMessage;
}
