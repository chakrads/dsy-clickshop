package com.clickshop.server.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class OrderDetailsListResponseDto implements Serializable {
    private String orderId;
    private Long itemId;
    private String itemName;
    private String itemImage;
    private String itemPrice;
    private String itemQuantity;
    private String totalPrice;
    private String itemWeight;
}
