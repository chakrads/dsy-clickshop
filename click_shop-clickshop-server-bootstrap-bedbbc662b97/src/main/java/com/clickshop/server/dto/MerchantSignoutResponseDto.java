package com.clickshop.server.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class MerchantSignoutResponseDto implements Serializable {

    private String responseMessage;
    private Long responseCode;

}
