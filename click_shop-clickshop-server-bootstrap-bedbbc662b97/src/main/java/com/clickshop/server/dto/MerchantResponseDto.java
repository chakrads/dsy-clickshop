package com.clickshop.server.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * Created by Dell on 7/18/2018.
 */
@Getter
@Setter
public class MerchantResponseDto implements Serializable{

    private String responseMessage;
    private Long responseCode;
    private Long merchantId;
    private String mobileNumber;
    private String address;
    private String gstNumber;
    private String storeNumber;
    private String email;
    private String firstName;
    private String lastName;
    private String flag;
    private String sessionId;

}