package com.clickshop.server.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * Created by Dell on 7/22/2018.
 */
@Getter
@Setter
public class ItemsOfMerchantDto implements Serializable {

    private Long itemMerchantId;
    private Long merchantId;
    private Long itemId;
    private String createdOn;
    private String createdBy;
    private String lastUpdatedBy;
    private String lastUpdatedOn;
}
