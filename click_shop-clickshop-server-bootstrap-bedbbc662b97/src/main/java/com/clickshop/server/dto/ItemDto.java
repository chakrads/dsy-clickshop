package com.clickshop.server.dto;

import com.clickshop.server.entity.Item;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Dell on 7/28/2018.
 */
@Getter
@Setter
public class ItemDto  implements Serializable{

    private Long responseCode;
    private String responseMessage;
    private List<Item> data;

}
