package com.clickshop.server.dto.request;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * Created by Dell on 9/29/2018.
 */
@Getter
@Setter

public class SearchOrderDetailsRequestDto implements Serializable {
    private String mobileNumber;
    private Long merchantId;
    private String orderId;
    private String sessionId;
}
