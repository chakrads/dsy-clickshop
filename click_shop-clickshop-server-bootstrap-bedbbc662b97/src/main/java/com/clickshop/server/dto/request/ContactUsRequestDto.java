package com.clickshop.server.dto.request;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class ContactUsRequestDto  implements Serializable {

    private String mobileNumber;
    private Long contactUsId;
    private Long merchantId;
    private String contactUsName;
    private String contactUsEmail;
    private String contactUsMobileNo;
    private String contactUsMessage;
    private String sessionId;

}
