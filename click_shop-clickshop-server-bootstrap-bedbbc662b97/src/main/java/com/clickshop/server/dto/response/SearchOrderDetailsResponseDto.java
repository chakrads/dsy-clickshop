package com.clickshop.server.dto.response;

import com.clickshop.server.dto.OrderDetailsListResponseDto;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Dell on 9/29/2018.
 */
@Getter
@Setter

public class SearchOrderDetailsResponseDto implements Serializable {
    private Long responseCode;
    private String responseMessage;
    private List<OrderDetailsListResponseDto> orderDetailsList;
}
