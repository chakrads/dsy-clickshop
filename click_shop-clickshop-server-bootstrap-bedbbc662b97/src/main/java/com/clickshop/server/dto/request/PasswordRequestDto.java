package com.clickshop.server.dto.request;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class PasswordRequestDto implements Serializable {

    private String mobileNumber;
    private Long merchantId;
    private String oldPassword;
    private String newPassword;
    private String confirmPassword;
    private String sessionId;
}
