package com.clickshop.server.dto.response;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * Created by Dell on 7/22/2018.
 */
@Getter
@Setter
public class ItemsOfMerchantResponseDto implements Serializable {

    private Long responseCode;
    private String responseMessage;
    private Long itemMerchantId;
    private Long merchantId;
    private Long itemId;
    private String storeNumber;
    private String itemName;
    private String itemPrice;
    private String itemWeight;
    private String itemCategory;
    private String createdOn;
    private String createdBy;
    private String lastUpdatedBy;
    // private String lastUpdatedOn;
    private String type;
}
