package com.clickshop.server.dto;

import com.clickshop.server.entity.BrandedItem;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
public class BrandedItemDto implements Serializable {

    private Long responseCode;
    private String responseMessage;
    private List<BrandedItem> brandedItems;

}
