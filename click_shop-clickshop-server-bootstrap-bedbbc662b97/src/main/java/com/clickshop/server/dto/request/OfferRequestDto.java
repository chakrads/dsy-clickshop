package com.clickshop.server.dto.request;


import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * Created by Dell on 7/22/2018.
 */
@Getter
@Setter
public class OfferRequestDto implements Serializable {
    private String mobileNumber;
    private Long merchantId;
    private Long itemId;
    private String actualPrice;
    private String discountPercentage;
    private String discountAmount;
    private String priceAfterDiscount;
    private String sessionId;
}
