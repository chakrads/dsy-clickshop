package com.clickshop.server.constants;

/**
 * Created by Dell on 7/19/2018.
 */
public class MerchantConstants {

    public static String LOGIN_REGISTRATION_SUCCESS_MESSAGE = "Login registration Successful";
    public static String LOGIN_SUCCESS_MESSAGE = "Login Successful";
    public static String LOGIN_REGISTRATION_ERROR_MESSAGE = "User already exists";
    public static String LOGIN_FAILED_MESSAGE = "Merchant doesn't exist, Please register first before login";
    public static String LOGIN_PASSWORD_ERROR_MESSAGE = "Login credentials doesn't match";
    public static Long SUCCESS_RESPONSE_CODE = 200L;
    public static Long FAILED_RESPONSE_CODE = 400L;
    public static Long INVALID_SESSION_CODE = 101L;
    public static String INVALID_SESSION_MESSAGE = "Invalid Session!";
    public static String CLOUDINARY_URL = "cloudinary://299875363772933:ebw_zv2ppsabRt-IAS6mlP5UUe0@clickshopimage";

}
