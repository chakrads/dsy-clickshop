package com.clickshop.server.dao;

import com.clickshop.server.entity.ItemCategory;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface ItemCategoryDao {

    public List<ItemCategory> getItemsCategory();
    public List<ItemCategory> getItemsCategoryRandom();
    public ItemCategory getItemCategory(Long itemCategoryId);
}
