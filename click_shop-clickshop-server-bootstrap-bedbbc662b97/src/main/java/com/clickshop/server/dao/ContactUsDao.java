package com.clickshop.server.dao;

import com.clickshop.server.entity.ContactUs;
import org.springframework.stereotype.Component;

@Component
public interface ContactUsDao {

    ContactUs createContactUs(ContactUs contactUs);
}
