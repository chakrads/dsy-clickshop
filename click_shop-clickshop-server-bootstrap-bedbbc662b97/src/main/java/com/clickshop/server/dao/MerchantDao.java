package com.clickshop.server.dao;

import com.clickshop.server.dto.MerchantRequestDto;
import com.clickshop.server.entity.Merchant;
import com.clickshop.server.entity.MerchantDevice;
import com.clickshop.server.entity.MerchantLoginHistory;
import com.clickshop.server.entity.MerchantUser;
import org.springframework.stereotype.Component;

/**
 * Created by Dell on 7/18/2018.
 */
@Component
public interface MerchantDao {

    public Merchant findMerchantByMobileNumber(String mobileNumber);

    public MerchantUser findMerchantUserByMobileNumber(String mobileNumber, Long merchantId);

    public String getSessionIdByMerchantUserId(Long merchantUserId);

    public Merchant saveMerchant(Merchant merchant);

    public MerchantUser saveMerchantUser(MerchantUser merchantUser);

    public MerchantDevice saveMerchantDevice(MerchantDevice merchantDevice);

    public MerchantLoginHistory saveMerchantLoginHistory(MerchantLoginHistory merchantLoginHistory);

    public Merchant getMerchantById(Merchant merchant);

    public String getMerchantFlag(Long merchantId);

    public int updateMerchantFlag(String flag, Long merchantId);

    public int updateFirebaseToken(String firebaseToken, Long merchantId);

    public int updateSessionId(Long merchantUserId, String sessionId);

}
