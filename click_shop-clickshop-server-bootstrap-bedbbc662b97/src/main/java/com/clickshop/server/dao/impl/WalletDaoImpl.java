package com.clickshop.server.dao.impl;

import com.clickshop.server.dao.WalletDao;
import com.clickshop.server.dto.request.WalletRequestDto;
import com.clickshop.server.entity.MerchantWallet;
import com.clickshop.server.entity.MerchantWalletDetails;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Component
public class WalletDaoImpl implements WalletDao {

    private static final Logger LOGGER = LoggerFactory.getLogger(MerchantDaoImpl.class);

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private SessionFactory sessionFactory;

    private Session getSession() {
        return sessionFactory.getCurrentSession();
    }

    @Override
    public MerchantWallet loadMerchantWalletAmount(Long merchantId) {
        MerchantWallet merchantWallet;
        try {
            String hqlQueryString = "from MerchantWallet where merchant_Id=:merchantId";
            merchantWallet = (MerchantWallet) this.entityManager.createQuery(hqlQueryString)
                    .setParameter("merchantId", merchantId).getSingleResult();
        } catch (Exception exception) {
            LOGGER.info(exception.getMessage());
            return null;
        }
        return merchantWallet;
    }

    @Transactional
    @Override
    public MerchantWallet insertMerchantWalletAmount(MerchantWallet merchantWallet) {
        Session session = getSession();
        session.save(merchantWallet);
        return merchantWallet;
    }

    @Transactional
    @Override
    public void insertMerchantWalletDetails(MerchantWalletDetails merchantWalletDetails) {
        Session session = getSession();
        session.save(merchantWalletDetails);
    }

    @Transactional
    @Override
    public int updateMerchantWalletAmount(MerchantWallet merchantWallet, WalletRequestDto walletRequestDto, String totalAmountPoint, String balanceAmountPoint) {
        Session session = getSession();
        Query query = session.createQuery("update MerchantWallet set total_point=:totalPoint,"+" balance_point=:balancePoint"+ " where merchant_id=:merchantId");
        query.setParameter("totalPoint", totalAmountPoint);
        query.setParameter("balancePoint", balanceAmountPoint);
        query.setParameter("merchantId", walletRequestDto.getMerchantId());
        int result = query.executeUpdate();
        session.refresh(merchantWallet);
        session.flush();
        return result;
    }

    /*@Override
    public MerchantWallet loadMerchantWalletAmountAfterUpdate(Long merchantWalletId) {
        MerchantWallet merchantWallet;
        try {
            String hqlQueryString = "from MerchantWallet where merchant_wallet_Id=:merchantWalletId";
            merchantWallet = (MerchantWallet) this.entityManager.createQuery(hqlQueryString)
                    .setParameter("merchantWalletId", merchantWalletId).getSingleResult();
        } catch (Exception exception) {
            LOGGER.info(exception.getMessage());
            return null;
        }
        return merchantWallet;
    }*/
}
