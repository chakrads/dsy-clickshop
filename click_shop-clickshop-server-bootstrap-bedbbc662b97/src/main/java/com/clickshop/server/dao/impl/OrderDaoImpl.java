package com.clickshop.server.dao.impl;

import com.clickshop.server.dao.OrderDao;
import com.clickshop.server.dto.OrderSearchRequestDto;
import com.clickshop.server.dto.OrderStatusSaveRequestDto;
import com.clickshop.server.entity.Order;
import com.clickshop.server.entity.OrderDetails;
import com.clickshop.server.entity.User;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Component
public class OrderDaoImpl implements OrderDao {

    private static final Logger LOGGER = LoggerFactory.getLogger(OrderDaoImpl.class);

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private SessionFactory sessionFactory;

    private Session getSession() {
        return sessionFactory.getCurrentSession();
    }

    @Override
    public List<Order> findMerchantOrderByStatus(OrderSearchRequestDto orderSearchRequestDto) {
        List<Order> orderList;
        try {
            String hqlQueryString = "from Order where merchant_id=:merchantId and order_status=:orderStatus";
            orderList = (List<Order>) this.entityManager.createQuery(hqlQueryString)
                    .setParameter("merchantId", orderSearchRequestDto.getMerchantId())
                    .setParameter("orderStatus", orderSearchRequestDto.getOrderStatus())
                    .getResultList();
        } catch (Exception e) {
            LOGGER.info(e.getMessage());
            return null;
        }
        return orderList;
    }

    @Transactional
    @Override
    public Order saveMerchantOrderStatusById(OrderStatusSaveRequestDto orderStatusSaveRequestDto) {
        Order order = null;
        try {
            String hqlQueryString = "update Order set order_status=:orderStatus where order_id=:orderId";
            Session session = getSession();
            Query query = session.createQuery(hqlQueryString)
                    .setParameter("orderStatus", orderStatusSaveRequestDto.getOrderStatus())
                    .setParameter("orderId", orderStatusSaveRequestDto.getOrderId());
            int result = query.executeUpdate();
            session.flush();
            if (result == 1) {
                order = (Order) this.entityManager.createQuery("from Order where order_id=:orderId")
                        .setParameter("orderId", orderStatusSaveRequestDto.getOrderId())
                        .getSingleResult();
            }
        } catch (Exception e) {
            LOGGER.info(e.getMessage());
            return null;
        }
        return order;
    }

    @Override
    public Order findOrder(String orderId){
        Order order;
        try {
            order = (Order) this.entityManager.createQuery("from Order where order_id=:orderId")
                    .setParameter("orderId", orderId)
                    .getSingleResult();
        } catch (Exception e) {
            LOGGER.info(e.getMessage());
            return null;
        }
        return order;

    }

    @Override
    public List<OrderDetails> getOrderDetails(String orderId){
        List<OrderDetails> orderDetailsList;
        try {
            orderDetailsList = (List<OrderDetails>) this.entityManager.createQuery("from OrderDetails where order_id=:orderId")
                    .setParameter("orderId", orderId)
                    .getResultList();
        } catch (Exception e) {
            LOGGER.info(e.getMessage());
            return null;
        }
        return orderDetailsList;

    }

    @Override
    public List<Order> searchMerchantOrderArchive(Long merchantId) {
        List<Order> orderList;
        try {
            String hqlQueryString = "from Order where merchant_id=:merchantId and (order_status=:orderStatusArchived or order_status=:orderStatusRejected)";
            orderList = (List<Order>) this.entityManager.createQuery(hqlQueryString)
                    .setParameter("merchantId", merchantId)
                    .setParameter("orderStatusArchived", "Archived")
                    .setParameter("orderStatusRejected", "Rejected")
                    .getResultList();
        } catch (Exception e) {
            LOGGER.info(e.getMessage());
            return null;
        }
        return orderList;
    }

    @Override
    public String getUserFcmToken(Long userId) {
        String firebaseToken;
        try {
            User user = (User) this.entityManager.createQuery("from User where user_id=:userId")
                    .setParameter("userId", userId)
                    .getSingleResult();
            firebaseToken = user.getFirebaseToken();
        } catch (Exception e) {
            LOGGER.info(e.getMessage());
            return null;
        }
        return firebaseToken;
    }

    @Override
    public User getUserById(Long userId) {
        User userData;
        try {
            String hqlQueryString = "from User where user_id=:userId";
            userData = (User) this.entityManager.createQuery(hqlQueryString)
                    .setParameter("userId", userId).getSingleResult();
        } catch (Exception exception) {
            LOGGER.info(exception.getMessage());
            return null;
        }
        return userData;
    }
}
