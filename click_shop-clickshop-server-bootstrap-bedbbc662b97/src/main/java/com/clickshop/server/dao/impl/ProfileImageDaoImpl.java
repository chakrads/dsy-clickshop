package com.clickshop.server.dao.impl;

import com.clickshop.server.dao.ProfileImageDao;
import com.clickshop.server.entity.ProfileImage;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Component
public class ProfileImageDaoImpl implements ProfileImageDao {

    private static final Logger LOGGER = LoggerFactory.getLogger(MerchantDaoImpl.class);

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private SessionFactory sessionFactory;

    private Session getSession() {
        return sessionFactory.getCurrentSession();
    }


    @Transactional
    @Override
    public ProfileImage saveProfileImage(ProfileImage profileImage) {
        Session session = getSession();
        session.save(profileImage);
        return profileImage;
    }

    @Transactional
    @Override
    public ProfileImage updateProfileImage(ProfileImage profileImage, Long imageId) {
        Session session = getSession();
        Query query = session.createQuery("update ProfileImage set image_url = :imageUrl" +
                " where merchant_id = :merchantId");
        query.setParameter("imageUrl", profileImage.getProfilePic());
        query.setParameter("merchantId", profileImage.getMerchantId());
        int result = query.executeUpdate();
        session.flush();
        session.clear();
        ProfileImage profileImageData = new ProfileImage();
        if (result == 1) {
            profileImageData = session.get(ProfileImage.class, new Long(imageId));
        }
        return profileImageData;
    }

    @Override
    public ProfileImage getProfileImageById(Long merchantId) {
        ProfileImage profileImageGetData;
        try {
            String hqlQueryString = "from ProfileImage where merchant_Id=:merchantId";
            profileImageGetData = (ProfileImage) this.entityManager.createQuery(hqlQueryString)
                    .setParameter("merchantId", merchantId).getSingleResult();
        } catch (Exception e) {
            LOGGER.info(e.getMessage());
            return null;
        }
        return profileImageGetData;
    }
}
