package com.clickshop.server.dao;

import com.clickshop.server.dto.request.PasswordRequestDto;
import com.clickshop.server.entity.Merchant;
import org.springframework.stereotype.Component;

@Component
public interface MerchantPasswordDao {

    public Merchant findMerchantByMobileNo(String mobileNo);

    public int createNewPassword(PasswordRequestDto passwordRequestDto);
}
