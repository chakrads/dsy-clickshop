package com.clickshop.server.dao;

import com.clickshop.server.dto.request.WalletRequestDto;
import com.clickshop.server.entity.MerchantWallet;
import com.clickshop.server.entity.MerchantWalletDetails;
import org.springframework.stereotype.Component;

@Component
public interface WalletDao {

    public MerchantWallet loadMerchantWalletAmount(Long merchantId);

    public MerchantWallet insertMerchantWalletAmount(MerchantWallet merchantWallet);

    public void insertMerchantWalletDetails(MerchantWalletDetails merchantWalletDetails);

    public int updateMerchantWalletAmount(MerchantWallet merchantWallet, WalletRequestDto walletRequestDto, String totalPoint, String balanceAmountPoint);

    /*public MerchantWallet loadMerchantWalletAmountAfterUpdate(Long merchantWalletId);*/

}
