package com.clickshop.server.dao.impl;

import com.clickshop.server.dao.OfferDao;
import com.clickshop.server.dto.request.OfferRequestDto;
import com.clickshop.server.entity.Item;
import com.clickshop.server.entity.Merchant;
import com.clickshop.server.entity.Offer;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Created by Dell on 7/18/2018.
 */

@Component
public class OfferDaoImpl implements OfferDao {
    private static final Logger LOGGER = LoggerFactory.getLogger(OfferDaoImpl.class);

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private SessionFactory sessionFactory;

    private Session getSession() {
        return sessionFactory.getCurrentSession();
    }

    @Transactional
    @Override
    public Offer saveOffer(Offer offer){
        Session session = getSession();
        session.save(offer);
        return offer;

    }

    @Transactional
    @Override
    public  int deleteOffer(OfferRequestDto offerRequestDto){
        int result = 0;
        try {
            Merchant merchant = new Merchant();
            merchant.setMerchantId(offerRequestDto.getMerchantId());
            Item item = new Item();
            item.setItemId(offerRequestDto.getItemId());
            String hqlQueryString = "delete from Offer where merchant_Id=:merchantId and item_Id=:itemId";
            result = this.entityManager.createQuery(hqlQueryString)
                    .setParameter("merchantId", merchant)
                    .setParameter("itemId", item)
                    .executeUpdate();
        } catch (Exception exception) {
            LOGGER.info(exception.getMessage());
        }
        return result;
    }

    @Override
    public Offer getItemsOffer(OfferRequestDto offerRequestDto) {

        Offer itemData = null;
        try {
            itemData = (Offer) this.entityManager.createQuery("from Offer where merchant_id=:merchantId" + " and item_Id = :itemId")
                    .setParameter("merchantId",offerRequestDto.getMerchantId())
                    .setParameter("itemId", offerRequestDto.getItemId())
                    .getSingleResult();
        } catch (Exception exception) {
            LOGGER.info(exception.getMessage());
        }
        return itemData;
    }

    @Transactional
    @Override
    public int updateOffer(Offer offer) {
        Merchant merchant = new Merchant();
        merchant.setMerchantId(offer.getMerchant().getMerchantId());
        Session session = getSession();
        Query query = session.createQuery("update Offer set discount_percentage = :discount," + "price_after_discount = :priceAfterDiscount" +
                " where item_Id = :itemId" + " and merchant_id = :merchantId");
        query.setParameter("discount", offer.getDiscountPercentage());
        query.setParameter("priceAfterDiscount", offer.getPriceAfterDiscount());
        query.setParameter("itemId", offer.getItemId());
        query.setParameter("merchantId", merchant);
        int result = query.executeUpdate();
        session.flush();
        return result;

    }
}
