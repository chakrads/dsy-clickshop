package com.clickshop.server.dao;

import com.clickshop.server.dto.OrderSearchRequestDto;
import com.clickshop.server.dto.OrderStatusSaveRequestDto;
import com.clickshop.server.entity.Order;
import com.clickshop.server.entity.OrderDetails;
import com.clickshop.server.entity.User;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface OrderDao {

    public List<Order> findMerchantOrderByStatus(OrderSearchRequestDto orderSearchRequestDto);

    public Order saveMerchantOrderStatusById(OrderStatusSaveRequestDto orderStatusSaveRequestDto);

    public Order findOrder(String orderId);

    public List<OrderDetails> getOrderDetails(String orderId);

    public List<Order> searchMerchantOrderArchive(Long merchantId);

    public String getUserFcmToken(Long userId);

    public User getUserById(Long userId);

}
