package com.clickshop.server.dao.impl;

import com.clickshop.server.dao.ContactUsDao;
import com.clickshop.server.entity.ContactUs;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Component
public class ContactUsDaoImpl implements ContactUsDao {

    private static final Logger LOGGER = LoggerFactory.getLogger(ContactUsDaoImpl.class);

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private SessionFactory sessionFactory;

    private Session getSession() {
        return sessionFactory.getCurrentSession();
    }

    @Transactional
    @Override
    public ContactUs createContactUs(ContactUs contactUs) {
        Session session = getSession();
        session.save(contactUs);
        return contactUs;
    }

}
