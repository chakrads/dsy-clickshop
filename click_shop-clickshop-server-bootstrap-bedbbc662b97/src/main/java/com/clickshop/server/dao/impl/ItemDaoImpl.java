package com.clickshop.server.dao.impl;

import com.clickshop.server.dao.ItemDao;
import com.clickshop.server.entity.Item;
import com.clickshop.server.entity.ItemCategory;
import com.clickshop.server.entity.ItemWeight;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Date;
import java.util.List;

/**
 * Created by Dell on 7/24/2018.
 */
@Component
public class ItemDaoImpl implements ItemDao{

    private static final Logger LOGGER = LoggerFactory.getLogger(MerchantDaoImpl.class);

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private SessionFactory sessionFactory;

    private Session getSession() {
        return sessionFactory.getCurrentSession();
    }

   /* @Transactional
    @Override
    public Item saveItem(Item item) {
        Session session = getSession();
        return (Item) session.save(item);
    }*/

    @Override
    public List<Item> getAllItems() {
        List<Item> itemList = null;

        try {
            itemList = (List<Item>) this.entityManager.createQuery("from Item").getResultList();
        } catch (Exception exception) {
            LOGGER.info(exception.getMessage());
        }
        return itemList;
    }

    @Override
    public Item getItemById(Long itemId) {
        Item itemData = null;

        try {
            itemData = (Item) this.entityManager.createQuery("from Item where item_id=:itemId")
                    .setParameter("itemId",itemId).getSingleResult();
        } catch (Exception exception) {
            LOGGER.info(exception.getMessage());
        }
        return itemData;
    }

    @Override
    public List<Item> getItemsByCategory(ItemCategory itemCategory) {

        List<Item> itemList= null;
        try {
            itemList = (List<Item>) this.entityManager.createQuery("from Item where item_category=:itemCategory")
                    .setParameter("itemCategory",itemCategory.getItemCategoryName()).getResultList();
        } catch (Exception exception) {
            LOGGER.info(exception.getMessage());
        }
        return itemList;
    }

    @Override
    public List<ItemWeight> getItemsWeight() {
        List<ItemWeight> itemWeights = null;

        try {
            itemWeights = (List<ItemWeight>) this.entityManager.createQuery("from ItemWeight").getResultList();
        } catch (Exception exception) {
            LOGGER.info(exception.getMessage());
        }
        return itemWeights;
    }

    @Transactional
    @Override
    public int saveItems(List<Item> items) {
        //int countFromDB = 0;
        Session session = getSession();
        int count = 0;
        Date start = new Date();
        LOGGER.info("Start time to process file in millisecond: "+start.getTime());
        for (Item uploadItem : items) {
            session.save(uploadItem);
            count++;
            if (count % 100 == 0) {
                session.flush();
                session.clear();
            }
        }
        Date end = new Date();
        LOGGER.info("End time to process file in millisecond: "+start.getTime());
        System.out.println("Time taken : " + (end.getTime() - start.getTime()));
        LOGGER.info("Total time taken to process file in millisecond: "+(end.getTime() - start.getTime()));
        //countFromDB = this.entityManager.createQuery("from Item").getResultList().size();
        return count;
    }

    @Override
    public List<Item> getItemListRandom(String categoryName) {
        List<Item> itemList;
        try {
            itemList = (List<Item>) this.entityManager.createQuery("from Item where item_category=:categoryName order by rand()")
                    .setParameter("categoryName", categoryName)
                    .setMaxResults(6)
                    .getResultList();
        } catch (Exception exception) {
            LOGGER.info(exception.getMessage());
            return null;
        }
        return itemList;
    }
}
