package com.clickshop.server.dao.impl;

import com.clickshop.server.dao.StoreDao;
import com.clickshop.server.entity.Merchant;
import com.clickshop.server.entity.Store;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Created by Dell on 8/2/2018.
 */
@Component
public class StoreDaoImpl implements StoreDao {

    private static final Logger LOGGER = LoggerFactory.getLogger(MerchantDaoImpl.class);

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private SessionFactory sessionFactory;

    private Session getSession() {
        return sessionFactory.getCurrentSession();
    }

    @Transactional
    @Override
    public Store saveStore(Store store) {
        Session session = getSession();
        session.save(store);
        return store;
    }

    @Transactional
    @Override
    public Store updateStore(Store store) {
        Session session = getSession();
        session.update(store);
        return store;
    }

    @Override
    public Store getStoreSetup(Long merchantId) {
        Store storeData;
        try {
            String hqlQueryString = "from Store where merchant_Id=:merchantId";
            storeData = (Store) this.entityManager.createQuery(hqlQueryString)
                    .setParameter("merchantId", merchantId).getSingleResult();
        } catch (Exception exception) {
            LOGGER.info(exception.getMessage());
            return null;
        }
        return storeData;
    }
}
