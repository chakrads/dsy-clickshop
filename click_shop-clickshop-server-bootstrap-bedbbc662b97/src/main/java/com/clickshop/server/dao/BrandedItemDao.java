package com.clickshop.server.dao;

import com.clickshop.server.entity.BrandedItem;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface BrandedItemDao {

    public List<BrandedItem> getAllBrandedItems();

    public BrandedItem getBrandedItemById(Long brandedItemId);

}
