package com.clickshop.server.dao.impl;

import com.clickshop.server.dao.ItemCategoryDao;
import com.clickshop.server.entity.ItemCategory;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Component
public class ItemCategoryDaoImpl implements ItemCategoryDao {

    private static final Logger LOGGER = LoggerFactory.getLogger(MerchantDaoImpl.class);

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private SessionFactory sessionFactory;

    private Session getSession() {
        return sessionFactory.getCurrentSession();
    }

    @Override
    public List<ItemCategory> getItemsCategory() {
        List<ItemCategory> itemCategoryList = null;
        try {
            itemCategoryList = (List<ItemCategory>) this.entityManager.createQuery("from ItemCategory").getResultList();
        } catch (Exception exception) {
            LOGGER.info(exception.getMessage());
        }
        return itemCategoryList;
    }

    @Override
    public List<ItemCategory> getItemsCategoryRandom() {
        List<ItemCategory> itemList;
        try {
            itemList = (List<ItemCategory>) this.entityManager.createQuery("from ItemCategory order by rand()")
                    .setMaxResults(6)
                    .getResultList();
        } catch (Exception exception) {
            LOGGER.info(exception.getMessage());
            return null;
        }
        return itemList;
    }

    @Override
    public ItemCategory getItemCategory(Long itemCategoryId) {
        ItemCategory itemCategory;
        try {
            itemCategory = (ItemCategory) this.entityManager.createQuery("from ItemCategory where item_category_id=:itemCategoryId")
                    .setParameter("itemCategoryId", itemCategoryId)
                    .getSingleResult();
        } catch (Exception exception) {
            LOGGER.info(exception.getMessage());
            return null;
        }
        return itemCategory;
    }
}
