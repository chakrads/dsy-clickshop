package com.clickshop.server.dao.impl;

import com.clickshop.server.constants.SecurityConstants;
import com.clickshop.server.dao.MerchantDao;
import com.clickshop.server.dao.MerchantPasswordDao;
import com.clickshop.server.dto.request.PasswordRequestDto;
import com.clickshop.server.entity.Merchant;
import com.clickshop.server.utils.PasswordUtility;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Component
public class MerchantPasswordDaoImpl implements MerchantPasswordDao {
    private static final Logger LOGGER = LoggerFactory.getLogger(MerchantDaoImpl.class);


    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private SessionFactory sessionFactory;

    private Session getSession() {
        return sessionFactory.getCurrentSession();
    }

    @Transactional
    @Override
    public int createNewPassword(PasswordRequestDto passwordRequestDto) {
        Merchant merchant = findMerchantByMobileNo(passwordRequestDto.getMobileNumber());
        int result;
        try {
            Session session = getSession();
            String secureMerchantPassword = PasswordUtility.generateSecurePassword(passwordRequestDto.getNewPassword(), SecurityConstants.PASSWORD_ENCRYPTION_SALT);
            passwordRequestDto.setNewPassword(secureMerchantPassword);
            String newMerchantPassword = passwordRequestDto.getNewPassword();
            Query query = session.createQuery("update MerchantUser set password=:newPassword" +
                    " where mobile_number=:mobileNumber and merchant_id=:merchantId");
            query.setParameter("newPassword", newMerchantPassword);
            query.setParameter("mobileNumber", passwordRequestDto.getMobileNumber());
            query.setParameter("merchantId", merchant.getMerchantId());
            result = query.executeUpdate();
            session.flush();
        } catch (Exception e) {
            LOGGER.info(e.getMessage());
            return 0;
        }
        return result;
    }

    @Override
    public Merchant findMerchantByMobileNo(String mobileNo) {
        Merchant merchantData = null;
        try {
            merchantData = (Merchant) this.entityManager
                    .createQuery("from Merchant where mobile_number=:mobileNumber")
                    .setParameter("mobileNumber", mobileNo)
                    .getSingleResult();
        } catch (Exception exception) {
            LOGGER.info(exception.getMessage());
        }
        return merchantData;
    }
}
