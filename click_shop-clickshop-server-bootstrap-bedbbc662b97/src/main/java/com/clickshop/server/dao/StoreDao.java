package com.clickshop.server.dao;

import com.clickshop.server.entity.Store;
import org.springframework.stereotype.Component;

/**
 * Created by Dell on 8/2/2018.
 */
@Component
public interface StoreDao {

    public Store saveStore(Store store);

    public Store updateStore(Store store);

    public Store getStoreSetup(Long merchantId);
}
