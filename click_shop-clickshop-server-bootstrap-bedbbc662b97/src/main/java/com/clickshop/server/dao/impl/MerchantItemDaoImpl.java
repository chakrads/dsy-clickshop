package com.clickshop.server.dao.impl;

import com.clickshop.server.dao.MerchantItemDao;
import com.clickshop.server.dto.request.ItemsOfMerchantRequestDto;
import com.clickshop.server.entity.Item;
import com.clickshop.server.entity.ItemsOfMerchant;
import com.clickshop.server.entity.Merchant;
import com.clickshop.server.utils.Utility;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * Created by Dell on 7/22/2018.
 */
@Component
public class MerchantItemDaoImpl implements MerchantItemDao {

    private static final Logger LOGGER = LoggerFactory.getLogger(MerchantDaoImpl.class);

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private SessionFactory sessionFactory;

    private Session getSession() {
        return sessionFactory.getCurrentSession();
    }

    @Transactional
    @Override
    public ItemsOfMerchant saveItem(ItemsOfMerchant itemsOfMerchant) {
        Session session = getSession();
        session.save(itemsOfMerchant);
        return itemsOfMerchant;
    }

    @Transactional
    @Override
    public int deleteMerchantItem(ItemsOfMerchantRequestDto itemsOfMerchantRequestDto) {
        int result = 0;
        try {
            Merchant merchant = new Merchant();
            merchant.setMerchantId(itemsOfMerchantRequestDto.getMerchantId());
            /*Item item = new Item();
            item.setItemId(itemsOfMerchantRequestDto.getItemId());*/
            String hqlQueryString = "delete from ItemsOfMerchant where merchant_Id=:merchantId and item_Id=:itemId and type=:type";
            result = this.entityManager.createQuery(hqlQueryString)
                    .setParameter("merchantId", merchant)
                    .setParameter("itemId", itemsOfMerchantRequestDto.getItemId())
                    .setParameter("type", itemsOfMerchantRequestDto.getType())
                    .executeUpdate();
        } catch (Exception exception) {
            LOGGER.info(exception.getMessage());
        }
        return result;
    }

    @Transactional
    @Override
    public ItemsOfMerchant getMerchantItemData(ItemsOfMerchantRequestDto itemsOfMerchantRequestDto) {
        ItemsOfMerchant itemsOfMerchant;
        try {
            Merchant merchant = new Merchant();
            merchant.setMerchantId(itemsOfMerchantRequestDto.getMerchantId());
            /*Item item = new Item();
            item.setItemId(itemsOfMerchantRequestDto.getItemId());*/
            String hqlQueryString = "from ItemsOfMerchant where merchant_Id=:merchantId and item_Id=:itemId and type=:type";
            itemsOfMerchant = (ItemsOfMerchant) this.entityManager.createQuery(hqlQueryString)
                    .setParameter("merchantId", merchant)
                    .setParameter("itemId", itemsOfMerchantRequestDto.getItemId())
                    .setParameter("type", itemsOfMerchantRequestDto.getType())
                    .getSingleResult();
        } catch (Exception exception) {
            LOGGER.info(exception.getMessage());
            return null;
        }
        return itemsOfMerchant;
    }

    @Transactional
    @Override
    public int updateMerchantItemData(ItemsOfMerchant itemsOfMerchantData, String itemPrice/*, String updatedItemWeight*/) {

        //String lastUpdatedOn = Utility.getCurrentDateTime();
        Merchant merchant = new Merchant();
        merchant.setMerchantId(itemsOfMerchantData.getMerchant().getMerchantId());
        /*Item item = new Item();
        item.setItemId(itemsOfMerchantData.getItemId());*/
        Session session = getSession();
        Query query = session.createQuery("update ItemsOfMerchant set item_price = :itemPrice" +
                " where item_id = :itemId" + " and merchant_id = :merchantId" + " and type = :type");
        query.setParameter("itemPrice", itemPrice);
        //query.setParameter("lastUpdatedOn", lastUpdatedOn);
        //query.setParameter("itemWeight", updatedItemWeight);
        query.setParameter("itemId", itemsOfMerchantData.getItemId());
        query.setParameter("merchantId", merchant);
        query.setParameter("type", itemsOfMerchantData.getType());
        int result = query.executeUpdate();
        session.refresh(itemsOfMerchantData);
        session.flush();
        return result;
    }

    @Transactional
    @Override
    public List<ItemsOfMerchant> getStoreItemByMerchantId(ItemsOfMerchantRequestDto itemsOfMerchantRequestDto) {
        List<ItemsOfMerchant> itemsOfMerchantList = null;
        try {
            Merchant merchant = new Merchant();
            merchant.setMerchantId(itemsOfMerchantRequestDto.getMerchantId());
            Item item = new Item();
            item.setItemId(itemsOfMerchantRequestDto.getItemId());
            String hqlQueryString = "from ItemsOfMerchant where merchant_Id=:merchantId";
            itemsOfMerchantList = (List<ItemsOfMerchant>) this.entityManager.createQuery(hqlQueryString)
                    .setParameter("merchantId", merchant)
                    .getResultList();
        } catch (Exception exception) {
            LOGGER.info(exception.getMessage());
        }
        return itemsOfMerchantList;
    }

    @Override
    public ItemsOfMerchant getMerchantItemById(Long itemMerchantId) {
        ItemsOfMerchant itemsOfMerchant;
        try {
            String hqlQueryString = "from ItemsOfMerchant where item_merchant_Id=:itemMerchantId";
            itemsOfMerchant = (ItemsOfMerchant) this.entityManager.createQuery(hqlQueryString)
                    .setParameter("itemMerchantId", itemMerchantId)
                    .getSingleResult();
        } catch (Exception exception) {
            LOGGER.info(exception.getMessage());
            return null;
        }
        return itemsOfMerchant;
    }

}
