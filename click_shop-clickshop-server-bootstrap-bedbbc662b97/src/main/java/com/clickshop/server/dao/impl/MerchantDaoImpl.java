package com.clickshop.server.dao.impl;

import com.clickshop.server.dao.MerchantDao;
import com.clickshop.server.entity.Merchant;
import com.clickshop.server.entity.MerchantDevice;
import com.clickshop.server.entity.MerchantLoginHistory;
import com.clickshop.server.entity.MerchantUser;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Created by Dell on 7/18/2018.
 */
@Component
public class MerchantDaoImpl implements MerchantDao {

    private static final Logger LOGGER = LoggerFactory.getLogger(MerchantDaoImpl.class);
    public static final String MOBILE_NUMBER = "mobileNumber";

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private SessionFactory sessionFactory;

    private Session getSession() {
        return sessionFactory.getCurrentSession();
    }

    @Override
    public Merchant findMerchantByMobileNumber(String mobileNumber) {

        Merchant merchantData;
        try {
            //String mobileNumber = merchant.getMobileNumber();
            String hqlQueryString = "from Merchant where mobile_number=:mobileNumber";
            merchantData = (Merchant) this.entityManager.createQuery(hqlQueryString)
                    .setParameter(MOBILE_NUMBER, mobileNumber).getSingleResult();
        } catch (Exception exception) {
            LOGGER.info(exception.getMessage());
            return null;
        }
        return merchantData;
    }

    @Override
    public MerchantUser findMerchantUserByMobileNumber(String mobileNumber, Long merchantId) {
        MerchantUser merchantUserData;
        try {
            String hqlQueryString = "from MerchantUser where mobile_number=:mobileNumber and merchant_id=:merchantId";
            merchantUserData = (MerchantUser) this.entityManager.createQuery(hqlQueryString)
                    .setParameter(MOBILE_NUMBER, mobileNumber)
                    .setParameter("merchantId", merchantId).getSingleResult();
        } catch (Exception exception) {
            LOGGER.info(exception.getMessage());
            return null;
        }
        return merchantUserData;
    }

    @Override
    public String getSessionIdByMerchantUserId(Long merchantUserId) {
        String sessionId;
        try {
            String hqlQueryString = "from MerchantLoginHistory where merchant_user_id=:merchantUserId";
            MerchantLoginHistory merchantLoginHistory = (MerchantLoginHistory) this.entityManager.createQuery(hqlQueryString)
                    .setParameter("merchantUserId", merchantUserId).getSingleResult();
            sessionId = merchantLoginHistory.getSessionId();
        } catch (Exception exception) {
            LOGGER.info(exception.getMessage());
            return null;
        }
        return sessionId;
    }

    @Transactional
    @Override
    public Merchant saveMerchant(Merchant merchant) {
        Session session = getSession();
        session.save(merchant);
        return merchant;
    }

    @Transactional
    @Override
    public MerchantUser saveMerchantUser(MerchantUser merchantUser) {
        Session session = getSession();
        session.save(merchantUser);
        return merchantUser;
    }

    @Transactional
    @Override
    public MerchantDevice saveMerchantDevice(MerchantDevice merchantDevice) {
        Session session = getSession();
        session.save(merchantDevice);
        return merchantDevice;
    }

    @Transactional
    @Override
    public MerchantLoginHistory saveMerchantLoginHistory(MerchantLoginHistory merchantLoginHistory) {
        Session session = getSession();
        session.save(merchantLoginHistory);
        return merchantLoginHistory;
    }

    @Override
    public Merchant getMerchantById(Merchant merchant) {
        Merchant merchantData = null;
        try {
            String hqlQueryString = "from Merchant where merchant_Id=:merchantId";
            merchantData = (Merchant) this.entityManager.createQuery(hqlQueryString)
                    .setParameter("merchantId", merchant.getMerchantId()).getSingleResult();
        } catch (Exception exception) {
            LOGGER.info(exception.getMessage());
        }
        return merchantData;
    }

    @Override
    public String getMerchantFlag(Long merchantId) {
        Merchant merchantData = null;
        try {
            String hqlQueryString = "from Merchant where merchant_Id=:merchantId";
            merchantData = (Merchant) this.entityManager.createQuery(hqlQueryString)
                    .setParameter("merchantId", merchantId).getSingleResult();
        } catch (Exception exception) {
            LOGGER.info(exception.getMessage());
        }
        return merchantData.getFlag();
    }

    @Transactional
    @Override
    public int updateMerchantFlag(String flag, Long merchantId) {

        Session session = getSession();
        Query query = session.createQuery("update Merchant set flag = :flag" +
                " where merchant_id = :merchantId");
        query.setParameter("flag", flag);
        query.setParameter("merchantId", merchantId);
        int result = query.executeUpdate();
        session.flush();
        return result;
    }

    @Transactional
    @Override
    public int updateFirebaseToken(String firebaseToken, Long merchantId) {
        Session session = getSession();
        Query query = session.createQuery("update Merchant set firebase_token = :firebaseToken" +
                " where merchant_id = :merchantId");
        query.setParameter("firebaseToken", firebaseToken);
        query.setParameter("merchantId", merchantId);
        int result = query.executeUpdate();
        session.flush();
        return result;
    }

    @Transactional
    @Override
    public int updateSessionId(Long merchantUserId, String sessionId) {
        Session session = getSession();
        Query query = session.createQuery("update MerchantLoginHistory set session_id = :sessionId" +
                " where merchant_user_id = :merchantUserId");
        query.setParameter("sessionId", sessionId);
        query.setParameter("merchantUserId", merchantUserId);
        int result = query.executeUpdate();
        if (1 == result) {
            MerchantLoginHistory merchantLoginHistory = getMerchantLoginHistoryById(merchantUserId);
            session.refresh(merchantLoginHistory);
        }
        session.flush();
        return result;
    }

    private MerchantLoginHistory getMerchantLoginHistoryById(Long merchantUserId) {
        MerchantLoginHistory merchantLoginHistory;
        try {
            String hqlQueryString = "from MerchantLoginHistory where merchant_user_id=:merchantUserId";
            merchantLoginHistory = (MerchantLoginHistory) this.entityManager.createQuery(hqlQueryString)
                    .setParameter("merchantUserId", merchantUserId).getSingleResult();
        } catch (Exception exception) {
            LOGGER.info(exception.getMessage());
            return null;
        }
        return merchantLoginHistory;
    }

}
