package com.clickshop.server.dao;

import com.clickshop.server.entity.Item;
import com.clickshop.server.entity.ItemCategory;
import com.clickshop.server.entity.ItemWeight;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by Dell on 7/24/2018.
 */
@Component
public interface ItemDao {

    // public Item saveItem(Item item);

    public List<Item> getAllItems();

    public Item getItemById(Long itemId);

    public List<Item> getItemsByCategory(ItemCategory itemCategory);

    public List<ItemWeight> getItemsWeight();

    public int saveItems(List<Item> items);

    public List<Item> getItemListRandom(String categoryName);

}
