package com.clickshop.server.dao;

import com.clickshop.server.entity.ProfileImage;
import org.springframework.stereotype.Component;

import java.sql.Blob;

@Component
public interface ProfileImageDao {

    public ProfileImage saveProfileImage(ProfileImage profileImage);

    public ProfileImage getProfileImageById(Long merchantId);

    public ProfileImage updateProfileImage(ProfileImage profileImage, Long imageId);

}
