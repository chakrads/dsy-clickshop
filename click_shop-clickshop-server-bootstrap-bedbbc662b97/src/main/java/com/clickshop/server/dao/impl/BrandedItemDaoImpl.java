package com.clickshop.server.dao.impl;

import com.clickshop.server.dao.BrandedItemDao;
import com.clickshop.server.entity.BrandedItem;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Component
public class BrandedItemDaoImpl implements BrandedItemDao {

    private static final Logger LOGGER = LoggerFactory.getLogger(MerchantDaoImpl.class);

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private SessionFactory sessionFactory;

    private Session getSession() {
        return sessionFactory.getCurrentSession();
    }

    @Override
    public List<BrandedItem> getAllBrandedItems() {
        List<BrandedItem> brandedItemList = null;

        try {
            brandedItemList = (List<BrandedItem>) this.entityManager.createQuery("from BrandedItem").getResultList();
        } catch (Exception exception) {
            LOGGER.info(exception.getMessage());
            return null;
        }
        return brandedItemList;
    }

    @Override
    public BrandedItem getBrandedItemById(Long brandedItemId) {
        BrandedItem itemData;

        try {
            itemData = (BrandedItem) this.entityManager.createQuery("from BrandedItem where branded_item_id=:brandedItemId")
                    .setParameter("brandedItemId",brandedItemId).getSingleResult();
        } catch (Exception exception) {
            LOGGER.info(exception.getMessage());
            return null;
        }
        return itemData;
    }
}
