package com.clickshop.server.dao;

import com.clickshop.server.dto.request.ItemsOfMerchantRequestDto;
import com.clickshop.server.entity.ItemsOfMerchant;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by Dell on 7/22/2018.
 */
@Component
public interface MerchantItemDao {

    public ItemsOfMerchant saveItem(ItemsOfMerchant itemsOfMerchant);

    public int deleteMerchantItem(ItemsOfMerchantRequestDto itemsOfMerchantRequestDto);

    public ItemsOfMerchant getMerchantItemData(ItemsOfMerchantRequestDto itemsOfMerchantRequestDto);

    public int updateMerchantItemData(ItemsOfMerchant itemsOfMerchantRequestDto, String itemPrice/*, String itemWeight*/);

    public List<ItemsOfMerchant> getStoreItemByMerchantId(ItemsOfMerchantRequestDto itemsOfMerchantRequestDto);

    public ItemsOfMerchant getMerchantItemById(Long itemMerchantId);
}
