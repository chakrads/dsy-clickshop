package com.clickshop.server.dao;

import com.clickshop.server.dto.request.OfferRequestDto;
import com.clickshop.server.entity.Offer;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by Dell on 7/18/2018.
 */
@Component
public interface OfferDao {
    public Offer saveOffer(Offer offer);
    public  int deleteOffer(OfferRequestDto offerRequestDto);
    public Offer getItemsOffer(OfferRequestDto offerRequestDto);
    public int updateOffer(Offer offer);
}
