package com.clickshop.server.utils;

import com.clickshop.server.constants.CommonConstants;
import com.clickshop.server.constants.ExcelFileConstant;
import com.clickshop.server.entity.Item;
import org.apache.poi.ss.usermodel.*;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;


public class FileUtility {
	public static List<Item> readExcel(InputStream inputStream) throws Exception {
		List<Item> items=new ArrayList<>();
		Workbook workbook=null;
		try {
			 workbook=WorkbookFactory.create(inputStream);
			for(Sheet sheet: workbook) {
	            int numberOfRows=sheet.getLastRowNum() - sheet.getFirstRowNum();
	            for ( int i=1 ; i<= numberOfRows ; i++) {
	            	Row row = sheet.getRow(i);
	            	 Item item = buildUploadItem(row);
	            	 items.add(item);
				}
	        }
			
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		finally {
			if(workbook !=null) {
			workbook.close();
			}
		}
		return items;
	}
	
	private static Item buildUploadItem(Row row) {
		Item item=new Item();

		if (extractCellValue(row.getCell(ExcelFileConstant.POSITION_ITEM_NAME)) instanceof Double) {
			item.setItemName("" + (Double) extractCellValue(row.getCell(ExcelFileConstant.POSITION_ITEM_NAME)));
		} else if (extractCellValue(row.getCell(ExcelFileConstant.POSITION_ITEM_NAME)) instanceof String) {
			item.setBrandName((String)extractCellValue(row.getCell(ExcelFileConstant.POSITION_ITEM_BRAND_NAME)));
		}
		item.setCompanyName((String)extractCellValue(row.getCell(ExcelFileConstant.POSITION_ITEM_COMPANY_NAME)));
		if(extractCellValue(row.getCell(ExcelFileConstant.POSITION_ITEM_WEIGHT)) instanceof Double) {
		    item.setItemWeight(""+(Double)extractCellValue(row.getCell(ExcelFileConstant.POSITION_ITEM_WEIGHT)));
		}
		else if(extractCellValue(row.getCell(ExcelFileConstant.POSITION_ITEM_WEIGHT)) instanceof String) {
			item.setItemWeight((String)extractCellValue(row.getCell(ExcelFileConstant.POSITION_ITEM_WEIGHT)));
		}
		item.setItemWeightUnit((String)extractCellValue(row.getCell(ExcelFileConstant.POSITION_ITEM_UNIT)));
		item.setItemSubcategory((String)extractCellValue(row.getCell(ExcelFileConstant.POSITION_ITEM_SUBCATEGORY)));
		item.setItemCategory((String)extractCellValue(row.getCell(ExcelFileConstant.POSITION_ITEM_CATEGORY)));
		if (extractCellValue(row.getCell(ExcelFileConstant.POSITION_ITEM_FAMILY)) instanceof Double) {
			item.setFamily("" + (Double) extractCellValue(row.getCell(ExcelFileConstant.POSITION_ITEM_FAMILY)));
		} else if (extractCellValue(row.getCell(ExcelFileConstant.POSITION_ITEM_FAMILY)) instanceof String) {
			item.setFamily((String) extractCellValue(row.getCell(ExcelFileConstant.POSITION_ITEM_FAMILY)));
		}
		if(extractCellValue(row.getCell(ExcelFileConstant.POSITION_ITEM_MRP)) instanceof Double) {
		    item.setItemPrice(""+(Double)extractCellValue(row.getCell(ExcelFileConstant.POSITION_ITEM_MRP)));
		}
		else if(extractCellValue(row.getCell(ExcelFileConstant.POSITION_ITEM_MRP)) instanceof String) {
			item.setItemPrice((String)extractCellValue(row.getCell(ExcelFileConstant.POSITION_ITEM_MRP)));
		}
		item.setPriceUnit(CommonConstants.CURRENCY);
		if(extractCellValue(row.getCell(ExcelFileConstant.POSITION_ITEM_BARCODE)) instanceof Double) {
		    item.setBarcode(""+(Double)extractCellValue(row.getCell(ExcelFileConstant.POSITION_ITEM_BARCODE)));
		}
		else if(extractCellValue(row.getCell(ExcelFileConstant.POSITION_ITEM_BARCODE)) instanceof String) {
			item.setBarcode((String)extractCellValue(row.getCell(ExcelFileConstant.POSITION_ITEM_BARCODE)));
		}
		item.setItemImage((String)extractCellValue(row.getCell(ExcelFileConstant.POSITION_ITEM_IMAGE)));
		String lastUpdatedDate = Utility.getCurrentDateTime();
		item.setLastUpdatedDate(lastUpdatedDate);
		return item;
	}

	private static Object extractCellValue(Cell cell) {
		Object cellValue=null;
		switch (cell.getCellTypeEnum()) {
        case BOOLEAN:
        	cellValue=cell.getBooleanCellValue();
            break;
        case STRING:
        	cellValue=cell.getRichStringCellValue().getString();
            break;
        case NUMERIC:
        	cellValue=cell.getNumericCellValue();
            break;
        case BLANK:
            break;
		default:
			break;
    }
		return cellValue;
	}
}
