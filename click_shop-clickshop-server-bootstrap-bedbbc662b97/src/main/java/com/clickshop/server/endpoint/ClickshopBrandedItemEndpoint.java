package com.clickshop.server.endpoint;

import com.clickshop.server.constants.MerchantConstants;
import com.clickshop.server.dto.BrandedItemDto;
import com.clickshop.server.entity.BrandedItem;
import com.clickshop.server.service.BrandedItemService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RequestMapping("/clickshop")
@RestController
public class ClickshopBrandedItemEndpoint {

    public static final Logger LOGGER = LoggerFactory.getLogger(ClickshopItemsEndPoint.class);

    @Autowired
    private BrandedItemService brandedItemService;

    @RequestMapping(value = "/getAllBrandedItems", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<BrandedItemDto> getAllBrandedItems() {

        ResponseEntity<BrandedItemDto> responseStatus = null;
        try {
            BrandedItemDto brandedItemDto = new BrandedItemDto();
            List<BrandedItem> brandedItems = brandedItemService.getAllBrandedItems();

            if (brandedItems.size() >= 1) {
                brandedItemDto.setResponseCode(MerchantConstants.SUCCESS_RESPONSE_CODE);
                brandedItemDto.setResponseMessage("All Branded Items fetch Success");
                brandedItemDto.setBrandedItems(brandedItems);
                responseStatus = ResponseEntity.status(HttpStatus.OK).body(brandedItemDto);
            } else {
                brandedItemDto.setResponseCode(MerchantConstants.FAILED_RESPONSE_CODE);
                brandedItemDto.setResponseMessage("All Branded Items get Failed");
                responseStatus = ResponseEntity.status(HttpStatus.OK).body(brandedItemDto);
            }

        } catch (Exception ex) {
            LOGGER.error(ex.toString());
            responseStatus = ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        } finally {
            return responseStatus;
        }
    }
}
