package com.clickshop.server.endpoint;

import com.clickshop.server.constants.MerchantConstants;
import com.clickshop.server.dao.MerchantDao;
import com.clickshop.server.dto.request.StoreRequestDto;
import com.clickshop.server.dto.response.StoreResponseDto;
import com.clickshop.server.entity.MerchantUser;
import com.clickshop.server.service.StoreService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Created by Dell on 8/2/2018.
 */
@RequestMapping("/store")
@RestController
public class ClickshopStoresEndPoint {

    public static final Logger LOGGER = LoggerFactory.getLogger(ClickshopStoresEndPoint.class);

    @Autowired
    private StoreService storeService;

    @Autowired
    private MerchantDao merchantDao;

    @RequestMapping(value = "/saveStoreSetup", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<StoreResponseDto> saveStoreSetup(@RequestBody StoreRequestDto storeRequestDto) {

        StoreResponseDto storeResponseDto = new StoreResponseDto();
        ResponseEntity<StoreResponseDto> responseStatus = null;
        String sessionId = getSessionId(storeRequestDto.getMobileNumber(), storeRequestDto.getMerchantId());
        if(!(storeRequestDto.getSessionId()).equals(sessionId)){
            storeResponseDto.setResponseCode(MerchantConstants.INVALID_SESSION_CODE);
            storeResponseDto.setResponseMessage(MerchantConstants.INVALID_SESSION_MESSAGE);
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(storeResponseDto);
        }
        try {
            storeResponseDto = storeService.saveStore(storeRequestDto);
            responseStatus = ResponseEntity.status(HttpStatus.OK).body(storeResponseDto);
        } catch (Exception ex) {
            LOGGER.error(ex.toString());
            responseStatus = ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        } finally {
            return responseStatus;
        }
    }

    @RequestMapping(value = "/getStoreSetup", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<StoreResponseDto> getStoreSetup(@RequestBody StoreRequestDto storeRequestDto) {

        StoreResponseDto storeResponseDto = new StoreResponseDto();
        ResponseEntity<StoreResponseDto> responseStatus = null;
        String sessionId = getSessionId(storeRequestDto.getMobileNumber(), storeRequestDto.getMerchantId());
        if(!(storeRequestDto.getSessionId()).equals(sessionId)){
            storeResponseDto.setResponseCode(MerchantConstants.INVALID_SESSION_CODE);
            storeResponseDto.setResponseMessage(MerchantConstants.INVALID_SESSION_MESSAGE);
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(storeResponseDto);
        }
        try {
            storeResponseDto = storeService.getStoreSetup(storeRequestDto);
            responseStatus = ResponseEntity.status(HttpStatus.OK).body(storeResponseDto);
        } catch (Exception ex) {
            LOGGER.error(ex.toString());
            responseStatus = ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        } finally {
            return responseStatus;
        }
    }

    @RequestMapping(value = "/updateStoreSetup", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<StoreResponseDto> updateStoreSetup(@RequestBody StoreRequestDto storeRequestDto) {

        StoreResponseDto storeResponseDto =new StoreResponseDto();
        ResponseEntity<StoreResponseDto> responseStatus = null;
        String sessionId = getSessionId(storeRequestDto.getMobileNumber(), storeRequestDto.getMerchantId());
        if(!(storeRequestDto.getSessionId()).equals(sessionId)){
            storeResponseDto.setResponseCode(MerchantConstants.INVALID_SESSION_CODE);
            storeResponseDto.setResponseMessage(MerchantConstants.INVALID_SESSION_MESSAGE);
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(storeResponseDto);
        }
        try {
            storeResponseDto = storeService.updateStore(storeRequestDto);
            responseStatus = ResponseEntity.status(HttpStatus.OK).body(storeResponseDto);
        } catch (Exception ex) {
            LOGGER.error(ex.toString());
            responseStatus = ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        } finally {
            return responseStatus;
        }
    }

    public String getSessionId(String mobileNumber, Long merchantId) {
        MerchantUser merchantUser = merchantDao.findMerchantUserByMobileNumber(mobileNumber, merchantId);
        String sessionId = merchantDao.getSessionIdByMerchantUserId(merchantUser.getMerchantUserId());
        return sessionId;
    }
}
