package com.clickshop.server.endpoint;

import com.clickshop.server.constants.MerchantConstants;
import com.clickshop.server.dao.MerchantDao;
import com.clickshop.server.dto.*;
import com.clickshop.server.dto.request.SearchOrderDetailsRequestDto;
import com.clickshop.server.dto.response.SearchOrderDetailsResponseDto;
import com.clickshop.server.entity.MerchantUser;
import com.clickshop.server.service.OrderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/merchant")
@RestController
public class ClickshopMerchantOrderEndpoint {

    public static final Logger LOGGER = LoggerFactory.getLogger(ClickshopMerchantOrderEndpoint.class);

    @Autowired
    private OrderService orderService;

    @Autowired
    private MerchantDao merchantDao;

    @RequestMapping(value = "/searchMerchantOrder", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<OrderSearchResponseDto> searchMerchantOrder(@RequestBody OrderSearchRequestDto orderSearchRequestDto) {
        OrderSearchResponseDto orderSearchResponseDto = new OrderSearchResponseDto();
        ResponseEntity<OrderSearchResponseDto> responseStatus = null;
        String sessionId = getSessionId(orderSearchRequestDto.getMobileNumber(), orderSearchRequestDto.getMerchantId());
        if(!(orderSearchRequestDto.getSessionId()).equals(sessionId)){
            orderSearchResponseDto.setResponseCode(MerchantConstants.INVALID_SESSION_CODE);
            orderSearchResponseDto.setResponseMessage(MerchantConstants.INVALID_SESSION_MESSAGE);
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(orderSearchResponseDto);
        }
        try {
            orderSearchResponseDto = orderService.findOrder(orderSearchRequestDto);
            responseStatus = ResponseEntity.status(HttpStatus.OK).body(orderSearchResponseDto);
        } catch (Exception ex) {
            LOGGER.error(ex.toString());
            responseStatus = ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        } finally {
            return responseStatus;
        }
    }

    @RequestMapping(value = "/saveMerchantOrderStatus", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<OrderStatusSaveResponseDto> saveMerchantOrderStatus(@RequestBody OrderStatusSaveRequestDto orderStatusSaveRequestDto) {
        OrderStatusSaveResponseDto orderStatusSaveResponseDto = new OrderStatusSaveResponseDto();
        ResponseEntity<OrderStatusSaveResponseDto> responseStatus = null;
        String sessionId = getSessionId(orderStatusSaveRequestDto.getMobileNumber(), orderStatusSaveRequestDto.getMerchantId());
        if(!(orderStatusSaveRequestDto.getSessionId()).equals(sessionId)){
            orderStatusSaveResponseDto.setResponseCode(MerchantConstants.INVALID_SESSION_CODE);
            orderStatusSaveResponseDto.setResponseMessage(MerchantConstants.INVALID_SESSION_MESSAGE);
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(orderStatusSaveResponseDto);
        }
        try {
            orderStatusSaveResponseDto = orderService.saveMerchantOrderStatus(orderStatusSaveRequestDto);
            responseStatus = ResponseEntity.status(HttpStatus.OK).body(orderStatusSaveResponseDto);
        } catch (Exception ex) {
            LOGGER.error(ex.toString());
            responseStatus = ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        } finally {
            return responseStatus;
        }
    }

    @RequestMapping(value = "/searchMerchantOrderDetails", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<SearchOrderDetailsResponseDto> searchMerchantOrderDetails(@RequestBody SearchOrderDetailsRequestDto searchOrderDetailsRequestDto) {
        SearchOrderDetailsResponseDto searchOrderDetailsResponseDto = new SearchOrderDetailsResponseDto();
        ResponseEntity<SearchOrderDetailsResponseDto> responseStatus = null;
        String sessionId = getSessionId(searchOrderDetailsRequestDto.getMobileNumber(), searchOrderDetailsRequestDto.getMerchantId());
        if(!(searchOrderDetailsRequestDto.getSessionId()).equals(sessionId)){
            searchOrderDetailsResponseDto.setResponseCode(MerchantConstants.INVALID_SESSION_CODE);
            searchOrderDetailsResponseDto.setResponseMessage(MerchantConstants.INVALID_SESSION_MESSAGE);
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(searchOrderDetailsResponseDto);
        }
        try {
            searchOrderDetailsResponseDto = orderService.findOrderDetails(searchOrderDetailsRequestDto);
            responseStatus = ResponseEntity.status(HttpStatus.OK).body(searchOrderDetailsResponseDto);
        } catch (Exception ex) {
            LOGGER.error(ex.toString());
            responseStatus = ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        } finally {
            return responseStatus;
        }
    }

    @RequestMapping(value = "/searchMerchantOrderArchive", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<OrderSearchResponseDto> searchMerchantOrderArchive(@RequestBody OrderArchiveRequestDto orderArchiveRequestDto) {
        OrderSearchResponseDto orderSearchResponseDto = new OrderSearchResponseDto();
        ResponseEntity<OrderSearchResponseDto> responseStatus = null;
        String sessionId = getSessionId(orderArchiveRequestDto.getMobileNumber(), orderArchiveRequestDto.getMerchantId());
        if(!(orderArchiveRequestDto.getSessionId()).equals(sessionId)){
            orderSearchResponseDto.setResponseCode(MerchantConstants.INVALID_SESSION_CODE);
            orderSearchResponseDto.setResponseMessage(MerchantConstants.INVALID_SESSION_MESSAGE);
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(orderSearchResponseDto);
        }
        try {
            orderSearchResponseDto = orderService.searchMerchantOrderArchive(orderArchiveRequestDto);
            responseStatus = ResponseEntity.status(HttpStatus.OK).body(orderSearchResponseDto);
        } catch (Exception ex) {
            LOGGER.error(ex.toString());
            responseStatus = ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        } finally {
            return responseStatus;
        }
    }

    public String getSessionId(String mobileNumber, Long merchantId) {
        MerchantUser merchantUser = merchantDao.findMerchantUserByMobileNumber(mobileNumber, merchantId);
        String sessionId = merchantDao.getSessionIdByMerchantUserId(merchantUser.getMerchantUserId());
        return sessionId;
    }
}
