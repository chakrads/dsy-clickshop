package com.clickshop.server.endpoint;

import com.clickshop.server.dto.request.BrandedItemsOfMerchantRequestDto;
import com.clickshop.server.dto.response.BrandedItemsOfMerchantResponseDto;
import com.clickshop.server.service.MerchantBrandedItemService;
import com.clickshop.server.service.MerchantService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/merchant")
@RestController
public class ClickshopMerchantBrandedItemEndPoint {

    public static final Logger LOGGER = LoggerFactory.getLogger(ClickshopMerchantBrandedItemEndPoint.class);

    @Autowired
    private MerchantBrandedItemService merchantBrandedItemService;

    @Autowired
    private MerchantService merchantService;

    @RequestMapping(value = "/saveBrandedItem", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<BrandedItemsOfMerchantResponseDto> saveItem(@RequestBody BrandedItemsOfMerchantRequestDto itemsOfMerchantRequestDto) {

        ResponseEntity<BrandedItemsOfMerchantResponseDto> responseStatus = null;
        try {
            BrandedItemsOfMerchantResponseDto itemsOfMerchantResponseDto = merchantBrandedItemService.saveBrandedItem(itemsOfMerchantRequestDto);
            responseStatus = ResponseEntity.status(HttpStatus.OK).body(itemsOfMerchantResponseDto);
        } catch (Exception ex) {
            LOGGER.error(ex.toString());
            responseStatus = ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        } finally {
            return responseStatus;
        }
    }
}
