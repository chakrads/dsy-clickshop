package com.clickshop.server.endpoint;

import com.clickshop.server.dto.ProfileImageDto;
import com.clickshop.server.service.ProfileImageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RequestMapping("/clickshop")
@RestController
public class ClickshopProfileImageEndPoint {

    public static final Logger LOGGER = LoggerFactory.getLogger(ClickshopStoresEndPoint.class);

    @Autowired
    private ProfileImageService profileImageService;

    @PostMapping("/saveProfileImage")
    @ResponseBody
    public ResponseEntity<ProfileImageDto> saveProfileImage(@RequestParam("file") MultipartFile imageFile,
                                                            @RequestParam("merchantId") Long merchantId) {
        ProfileImageDto profileImageDto;
        ResponseEntity<ProfileImageDto> responseStatus = null;
        try {
            profileImageDto = profileImageService.saveProfileImage(imageFile, merchantId);
            responseStatus = ResponseEntity.status(HttpStatus.OK).body(profileImageDto);
        } catch (Exception ex) {
            LOGGER.error(ex.toString());
            responseStatus = ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        } finally {
            return responseStatus;
        }
    }

    @RequestMapping(value = "/getProfileImage", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<ProfileImageDto> getProfileImage(@RequestParam("merchantId") Long merchantId) {
        ProfileImageDto profileImageDto;
        ResponseEntity<ProfileImageDto> responseStatus = null;
        try {
            profileImageDto = profileImageService.getProfileImage(merchantId);
            responseStatus = ResponseEntity.status(HttpStatus.OK).body(profileImageDto);
        } catch (Exception ex) {
            LOGGER.error(ex.toString());
            responseStatus = ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        } finally {
            return responseStatus;
        }
    }
}
