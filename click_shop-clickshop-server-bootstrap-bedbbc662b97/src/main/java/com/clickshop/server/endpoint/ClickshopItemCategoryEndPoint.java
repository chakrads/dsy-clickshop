package com.clickshop.server.endpoint;

import com.clickshop.server.constants.MerchantConstants;
import com.clickshop.server.dao.ItemCategoryDao;
import com.clickshop.server.dto.ItemCategoryDto;
import com.clickshop.server.dto.ItemDto;
import com.clickshop.server.dto.response.ItemCategoryListRandomResponseDto;
import com.clickshop.server.dto.response.ItemCategoryListResponseDto;
import com.clickshop.server.entity.Item;
import com.clickshop.server.entity.ItemCategory;
import com.clickshop.server.service.ItemCategoryService;
import com.clickshop.server.service.ItemService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RequestMapping("/clickshop")
@RestController
public class ClickshopItemCategoryEndPoint {

    public static final Logger LOGGER = LoggerFactory.getLogger(ClickshopItemsEndPoint.class);

    @Autowired
    private ItemCategoryService itemCategoryService;

    @Autowired
    private ItemCategoryDao itemCategoryDao;

    @Autowired
    private ItemService itemService;

    @RequestMapping(value = "/getItemsCategory", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<ItemCategoryDto> getItemsCategory() {

        ResponseEntity<ItemCategoryDto> responseStatus = null;
        try {
            List<ItemCategory> listItemData = itemCategoryService.getItemsCategory();
            ItemCategoryDto itemCategoryDto = new ItemCategoryDto();

            if (listItemData.size() >= 1) {
                itemCategoryDto.setResponseCode(MerchantConstants.SUCCESS_RESPONSE_CODE);
                itemCategoryDto.setResponseMessage("Items category get Success");
                itemCategoryDto.setData(listItemData);
                responseStatus = ResponseEntity.status(HttpStatus.OK).body(itemCategoryDto);
            } else {
                itemCategoryDto.setResponseCode(MerchantConstants.FAILED_RESPONSE_CODE);
                itemCategoryDto.setResponseMessage("Items category get Failed");
                responseStatus = ResponseEntity.status(HttpStatus.OK).body(itemCategoryDto);
            }

        } catch (Exception ex) {
            LOGGER.error(ex.toString());
            responseStatus = ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        } finally {
            return responseStatus;
        }
    }

    @RequestMapping(value = "/getItemsByCategory", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<ItemDto> getItemsByCategory(@RequestBody ItemCategory itemCategory) {

        ResponseEntity<ItemDto> responseStatus = null;
        try {
            ItemCategory itemCategoryData = itemCategoryDao.getItemCategory(itemCategory.getItemCategoryId());
            List<Item> itemList = itemService.getItemsByCategory(itemCategoryData);
            ItemDto itemDto = new ItemDto();
            if (itemList.size() >= 1) {
                itemDto.setResponseCode(MerchantConstants.SUCCESS_RESPONSE_CODE);
                itemDto.setResponseMessage("Get Item by Category Success");
                itemDto.setData(itemList);
                responseStatus = ResponseEntity.status(HttpStatus.OK).body(itemDto);
            } else {
                itemDto.setResponseCode(MerchantConstants.FAILED_RESPONSE_CODE);
                itemDto.setResponseMessage("Get Item by Category Failed");
                responseStatus = ResponseEntity.status(HttpStatus.OK).body(itemDto);
            }

        } catch (Exception ex) {
            LOGGER.error(ex.toString());
            responseStatus = ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        } finally {
            return responseStatus;
        }
    }

    @RequestMapping(value = "/getItemsRandomly", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<ItemCategoryListRandomResponseDto> getItemsListRandomly() {

        ResponseEntity<ItemCategoryListRandomResponseDto> responseStatus = null;
        try {
            ItemCategoryListRandomResponseDto itemCategoryListRandomResponseDto = new ItemCategoryListRandomResponseDto();
            List<ItemCategory> listItemData = itemCategoryService.getItemsCategory();
            List<ItemCategoryListResponseDto> itemCategoryList = new ArrayList<>();
            listItemData.forEach(itemCategory -> {
                ItemCategoryListResponseDto itemCategoryListResponseDto = new ItemCategoryListResponseDto();
                itemCategoryListResponseDto.setItemCategory(itemCategory.getItemCategoryName());
                List<Item> listItem = itemService.getItemListRandom(itemCategory.getItemCategoryName());
                itemCategoryListResponseDto.setItemList(listItem);
                itemCategoryList.add(itemCategoryListResponseDto);
            });

            if (0 != itemCategoryList.size()) {
                itemCategoryListRandomResponseDto.setResponseCode(MerchantConstants.SUCCESS_RESPONSE_CODE);
                itemCategoryListRandomResponseDto.setResponseMessage("Items list random get Success");
                itemCategoryListRandomResponseDto.setItemCategoryListRandom(itemCategoryList);
                responseStatus = ResponseEntity.status(HttpStatus.OK).body(itemCategoryListRandomResponseDto);
            } else {
                itemCategoryListRandomResponseDto.setResponseCode(MerchantConstants.FAILED_RESPONSE_CODE);
                itemCategoryListRandomResponseDto.setResponseMessage("Items list random get Failed");
                responseStatus = ResponseEntity.status(HttpStatus.OK).body(itemCategoryListRandomResponseDto);
            }

        } catch (Exception ex) {
            LOGGER.error(ex.toString());
            responseStatus = ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        } finally {
            return responseStatus;
        }
    }

    @RequestMapping(value = "/findItemCategoryRandomly", method = RequestMethod.GET)
    public ResponseEntity<ItemCategoryDto> findItemCategoryRandomly() {
        ResponseEntity<ItemCategoryDto> responseStatus;
        try {
            ItemCategoryDto itemCategoryDto = itemCategoryService.findItemCategoryRandom();
            responseStatus = ResponseEntity.status(HttpStatus.OK).body(itemCategoryDto);
        } catch (Exception e) {
            LOGGER.info(e.getMessage());
            responseStatus = ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }
        return responseStatus;
    }
}
