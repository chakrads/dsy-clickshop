package com.clickshop.server.endpoint;

import com.clickshop.server.constants.MerchantConstants;
import com.clickshop.server.dao.MerchantDao;
import com.clickshop.server.dto.request.ContactUsRequestDto;
import com.clickshop.server.dto.response.ContactUsResponseDto;
import com.clickshop.server.entity.MerchantUser;
import com.clickshop.server.service.ContactUsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/merchant")
@RestController
public class ClickshopMerchantContactUsEndPoint {

    public static final Logger LOGGER = LoggerFactory.getLogger(ClickshopMerchantContactUsEndPoint.class);

    @Autowired
    private ContactUsService contactUsService;

    @Autowired
    private MerchantDao merchantDao;

    @RequestMapping(value = "/saveContactUs", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<ContactUsResponseDto> saveContactUs(@RequestBody ContactUsRequestDto contactUsRequestDto) {

        ContactUsResponseDto contactUsResponseDto = new ContactUsResponseDto();
        ResponseEntity<ContactUsResponseDto> responseStatus = null;
        String sessionId = getSessionId(contactUsRequestDto.getMobileNumber(), contactUsRequestDto.getMerchantId());
        if(!(contactUsRequestDto.getSessionId()).equals(sessionId)){
            contactUsResponseDto.setResponseCode(MerchantConstants.INVALID_SESSION_CODE);
            contactUsResponseDto.setResponseMessage(MerchantConstants.INVALID_SESSION_MESSAGE);
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(contactUsResponseDto);
        }
        try {
            contactUsResponseDto = contactUsService.saveContactUs(contactUsRequestDto);
            responseStatus = ResponseEntity.status(HttpStatus.OK).body(contactUsResponseDto);
        } catch (Exception ex) {
            LOGGER.error(ex.toString());
            responseStatus = ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        } finally {
            return responseStatus;
        }
    }

    public String getSessionId(String mobileNumber, Long merchantId) {
        MerchantUser merchantUser = merchantDao.findMerchantUserByMobileNumber(mobileNumber, merchantId);
        String sessionId = merchantDao.getSessionIdByMerchantUserId(merchantUser.getMerchantUserId());
        return sessionId;
    }

}
