package com.clickshop.server.endpoint;

import com.clickshop.server.constants.MerchantConstants;
import com.clickshop.server.dao.MerchantDao;
import com.clickshop.server.dto.request.WalletRequestDto;
import com.clickshop.server.dto.response.WalletResponseDto;
import com.clickshop.server.entity.MerchantUser;
import com.clickshop.server.service.WalletService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/merchant")
@RestController
public class ClickshopMerchantWalletEndpoint {

    public static final Logger LOGGER = LoggerFactory.getLogger(ClickshopMerchantOrderEndpoint.class);

    @Autowired
    private WalletService walletService;

    @Autowired
    private MerchantDao merchantDao;

    @RequestMapping(value = "/getMerchantWalletAmount", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<WalletResponseDto> getMerchantWalletAmount(@RequestBody WalletRequestDto walletRequestDto) {
        WalletResponseDto walletResponseDto = new WalletResponseDto();
        ResponseEntity<WalletResponseDto> responseStatus = null;
        String sessionId = getSessionId(walletRequestDto.getMobileNumber(), walletRequestDto.getMerchantId());
        if(!(walletRequestDto.getSessionId()).equals(sessionId)){
            walletResponseDto.setResponseCode(MerchantConstants.INVALID_SESSION_CODE);
            walletResponseDto.setResponseMessage(MerchantConstants.INVALID_SESSION_MESSAGE);
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(walletResponseDto);
        }
        try {
            walletResponseDto = walletService.getMerchantWalletAmount(walletRequestDto);
            responseStatus = ResponseEntity.status(HttpStatus.OK).body(walletResponseDto);
        } catch (Exception ex) {
            LOGGER.error(ex.toString());
            responseStatus = ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        } finally {
            return responseStatus;
        }
    }

    @RequestMapping(value = "/updateMerchantWalletAmount", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<WalletResponseDto> updateMerchantWalletAmount(@RequestBody WalletRequestDto walletRequestDto) {
        WalletResponseDto walletResponseDto = new WalletResponseDto();
        ResponseEntity<WalletResponseDto> responseStatus = null;
        String sessionId = getSessionId(walletRequestDto.getMobileNumber(), walletRequestDto.getMerchantId());
        if(!(walletRequestDto.getSessionId()).equals(sessionId)){
            walletResponseDto.setResponseCode(MerchantConstants.INVALID_SESSION_CODE);
            walletResponseDto.setResponseMessage(MerchantConstants.INVALID_SESSION_MESSAGE);
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(walletResponseDto);
        }
        try {
            walletResponseDto = walletService.updateMerchantWalletAmount(walletRequestDto);
            responseStatus = ResponseEntity.status(HttpStatus.OK).body(walletResponseDto);
        } catch (Exception ex) {
            LOGGER.error(ex.toString());
            responseStatus = ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        } finally {
            return responseStatus;
        }
    }

    public String getSessionId(String mobileNumber, Long merchantId) {
        MerchantUser merchantUser = merchantDao.findMerchantUserByMobileNumber(mobileNumber, merchantId);
        String sessionId = merchantDao.getSessionIdByMerchantUserId(merchantUser.getMerchantUserId());
        return sessionId;
    }
}
