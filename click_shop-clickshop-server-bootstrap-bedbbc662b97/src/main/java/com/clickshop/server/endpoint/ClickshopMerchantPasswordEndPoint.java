package com.clickshop.server.endpoint;


import com.clickshop.server.constants.MerchantConstants;
import com.clickshop.server.dao.MerchantDao;
import com.clickshop.server.dto.request.PasswordRequestDto;
import com.clickshop.server.dto.response.PasswordResponseDto;
import com.clickshop.server.entity.MerchantUser;
import com.clickshop.server.service.MerchantPasswordService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RequestMapping("/merchant")
@RestController
public class ClickshopMerchantPasswordEndPoint {

    public static final Logger LOGGER = LoggerFactory.getLogger(ClickshopMerchantPasswordEndPoint.class);

    @Autowired
    private MerchantPasswordService merchantPasswordService;

    @Autowired
    private MerchantDao merchantDao;

    @RequestMapping(value = "/forgetPassword", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<PasswordResponseDto> forgetPassword(@RequestBody PasswordRequestDto passwordRequestDto) {
        PasswordResponseDto passwordResponseDto = new PasswordResponseDto();
        ResponseEntity<PasswordResponseDto> responseStatus = null;
        /*String sessionId = getSessionId(passwordRequestDto.getMobileNumber(), passwordRequestDto.getMerchantId());
        if(!(passwordRequestDto.getSessionId()).equals(sessionId)){
            passwordResponseDto.setResponseCode(MerchantConstants.INVALID_SESSION_CODE);
            passwordResponseDto.setResponseMessage(MerchantConstants.INVALID_SESSION_MESSAGE);
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(passwordResponseDto);
        }*/
        try {
            passwordResponseDto = merchantPasswordService.setNewPassword(passwordRequestDto);
            responseStatus = ResponseEntity.status(HttpStatus.OK).body(passwordResponseDto);
        } catch (Exception ex) {
            LOGGER.error(ex.toString());
            responseStatus = ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        } finally {
            return responseStatus;
        }
    }

    @RequestMapping(value = "/resetPassword", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<PasswordResponseDto> resetPassword(@RequestBody PasswordRequestDto passwordRequestDto) {
        PasswordResponseDto passwordResponseDto = new PasswordResponseDto();
        ResponseEntity<PasswordResponseDto> responseStatus = null;
        String sessionId = getSessionId(passwordRequestDto.getMobileNumber(), passwordRequestDto.getMerchantId());
        if(!(passwordRequestDto.getSessionId()).equals(sessionId)){
            passwordResponseDto.setResponseCode(MerchantConstants.INVALID_SESSION_CODE);
            passwordResponseDto.setResponseMessage(MerchantConstants.INVALID_SESSION_MESSAGE);
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(passwordResponseDto);
        }
        try {
            passwordResponseDto = merchantPasswordService.resetPassword(passwordRequestDto);
            responseStatus = ResponseEntity.status(HttpStatus.OK).body(passwordResponseDto);
        } catch (Exception ex) {
            LOGGER.error(ex.toString());
            responseStatus = ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        } finally {
            return responseStatus;
        }
    }

    public String getSessionId(String mobileNumber, Long merchantId) {
        MerchantUser merchantUser = merchantDao.findMerchantUserByMobileNumber(mobileNumber, merchantId);
        String sessionId = merchantDao.getSessionIdByMerchantUserId(merchantUser.getMerchantUserId());
        return sessionId;
    }
}
