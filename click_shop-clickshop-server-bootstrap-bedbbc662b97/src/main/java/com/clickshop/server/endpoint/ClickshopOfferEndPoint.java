package com.clickshop.server.endpoint;

import com.clickshop.server.constants.MerchantConstants;
import com.clickshop.server.dao.MerchantDao;
import com.clickshop.server.dto.request.OfferRequestDto;
import com.clickshop.server.dto.response.OfferDeleteResponceDto;
import com.clickshop.server.dto.response.OfferResponceDto;
import com.clickshop.server.entity.MerchantUser;
import com.clickshop.server.service.OfferService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Created by Dell on 7/17/2018.
 */


@RequestMapping("/clickshop")
@RestController
public class ClickshopOfferEndPoint {

    public static final Logger LOGGER = LoggerFactory.getLogger(ClickshopOfferEndPoint.class);

    @Autowired
    private OfferService offerService;


    @Autowired
    private MerchantDao merchantDao;

    @RequestMapping(value = "/saveOffer", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<OfferResponceDto> saveOffer(@RequestBody OfferRequestDto offerRequestDto) {
        OfferResponceDto offerResponceDto = new OfferResponceDto();
        ResponseEntity<OfferResponceDto> responseStatus = null;
        String sessionId = getSessionId(offerRequestDto.getMobileNumber(), offerRequestDto.getMerchantId());
        if(!(offerRequestDto.getSessionId()).equals(sessionId)){
            offerResponceDto.setResponseCode(MerchantConstants.INVALID_SESSION_CODE);
            offerResponceDto.setResponseMessage(MerchantConstants.INVALID_SESSION_MESSAGE);
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(offerResponceDto);
        }
        try {
            offerResponceDto = offerService.saveOffer(offerRequestDto);
            responseStatus = ResponseEntity.status(HttpStatus.OK).body(offerResponceDto);
        } catch (Exception ex) {
            LOGGER.error(ex.toString());
            responseStatus = ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        } finally {
            return responseStatus;
        }

    }

    @RequestMapping(value = "/deleteOffer", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<OfferDeleteResponceDto> deleteOffer(@RequestBody OfferRequestDto offerRequestDto) {
        OfferDeleteResponceDto offerDeleteResponceDto = new OfferDeleteResponceDto();
        ResponseEntity<OfferDeleteResponceDto> responseStatus = null;
        String sessionId = getSessionId(offerRequestDto.getMobileNumber(), offerRequestDto.getMerchantId());
        if(!(offerRequestDto.getSessionId()).equals(sessionId)){
            offerDeleteResponceDto.setResponseCode(MerchantConstants.INVALID_SESSION_CODE);
            offerDeleteResponceDto.setResponseMessage(MerchantConstants.INVALID_SESSION_MESSAGE);
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(offerDeleteResponceDto);
        }
        try {
            offerDeleteResponceDto = offerService.deleteOffer(offerRequestDto);
            responseStatus = ResponseEntity.status(HttpStatus.OK).body(offerDeleteResponceDto);
        } catch (Exception ex) {
            LOGGER.error(ex.toString());
            responseStatus = ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        } finally {
            return responseStatus;
        }
    }

    @RequestMapping(value = "/getOffer", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<OfferResponceDto> getOffer(@RequestBody OfferRequestDto offerRequestDto) {
        OfferResponceDto offerResponceDto = new OfferResponceDto();
        ResponseEntity<OfferResponceDto> responseStatus = null;
        String sessionId = getSessionId(offerRequestDto.getMobileNumber(), offerRequestDto.getMerchantId());
        if(!(offerRequestDto.getSessionId()).equals(sessionId)){
            offerResponceDto.setResponseCode(MerchantConstants.INVALID_SESSION_CODE);
            offerResponceDto.setResponseMessage(MerchantConstants.INVALID_SESSION_MESSAGE);
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(offerResponceDto);
        }
        try {
            offerResponceDto = offerService.getOffer(offerRequestDto);
            responseStatus = ResponseEntity.status(HttpStatus.OK).body(offerResponceDto);
        } catch (Exception ex) {
            LOGGER.error(ex.toString());
            responseStatus = ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        } finally {
            return responseStatus;
        }
    }

    @RequestMapping(value = "/updateOffer", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<OfferResponceDto> updateOffer(@RequestBody OfferRequestDto offerRequestDto) {
        OfferResponceDto offerResponceDto = new OfferResponceDto();
        ResponseEntity<OfferResponceDto> responseStatus = null;
        String sessionId = getSessionId(offerRequestDto.getMobileNumber(), offerRequestDto.getMerchantId());
        if(!(offerRequestDto.getSessionId()).equals(sessionId)){
            offerResponceDto.setResponseCode(MerchantConstants.INVALID_SESSION_CODE);
            offerResponceDto.setResponseMessage(MerchantConstants.INVALID_SESSION_MESSAGE);
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(offerResponceDto);
        }
        try {
            offerResponceDto = offerService.updateOffer(offerRequestDto);
            responseStatus = ResponseEntity.status(HttpStatus.OK).body(offerResponceDto);
        } catch (Exception ex) {
            LOGGER.error(ex.toString());
            responseStatus = ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        } finally {
            return responseStatus;
        }
    }

    public String getSessionId(String mobileNumber, Long merchantId) {
        MerchantUser merchantUser = merchantDao.findMerchantUserByMobileNumber(mobileNumber, merchantId);
        String sessionId = merchantDao.getSessionIdByMerchantUserId(merchantUser.getMerchantUserId());
        return sessionId;
    }
}