package com.clickshop.server.endpoint;

import com.clickshop.server.constants.MerchantConstants;
import com.clickshop.server.dto.ItemDto;
import com.clickshop.server.dto.ItemWeightDto;
import com.clickshop.server.entity.Item;
import com.clickshop.server.entity.ItemWeight;
import com.clickshop.server.service.ItemService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by Dell on 7/24/2018.
 */
@RequestMapping("/clickshop")
@RestController
public class ClickshopItemsEndPoint {

    public static final Logger LOGGER = LoggerFactory.getLogger(ClickshopItemsEndPoint.class);

    @Autowired
    private ItemService itemService;

    /*@RequestMapping(value = "/saveItem", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<Item> saveItem(@RequestBody Item item) {

        ResponseEntity<Item> responseStatus = null;
        try {
            Item itemData = itemService.saveItem(item);
            responseStatus = ResponseEntity.status(HttpStatus.OK).body(itemData);
        } catch (Exception ex) {
            LOGGER.error(ex.toString());
            responseStatus = ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        } finally {
            return responseStatus;
        }
    }*/

    @RequestMapping(value = "/getAllItems", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<ItemDto> getAllItems() {

        ResponseEntity<ItemDto> responseStatus = null;
        try {
            ItemDto itemDto = new ItemDto();
            List<Item> listItemData = itemService.getAllItems();

            if(listItemData.size() >= 1){
                itemDto.setResponseCode(MerchantConstants.SUCCESS_RESPONSE_CODE);
                itemDto.setResponseMessage("All Items fetch Success");
                itemDto.setData(listItemData);
                responseStatus = ResponseEntity.status(HttpStatus.OK).body(itemDto);
            } else{
                itemDto.setResponseCode(MerchantConstants.FAILED_RESPONSE_CODE);
                itemDto.setResponseMessage("All Items fetch Failed");
                responseStatus = ResponseEntity.status(HttpStatus.OK).body(itemDto);
            }

        } catch (Exception ex) {
            LOGGER.error(ex.toString());
            responseStatus = ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        } finally {
            return responseStatus;
        }
    }

    @RequestMapping(value = "/getItemsWeight", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<ItemWeightDto> getItemsWeight() {

        ResponseEntity<ItemWeightDto> responseStatus = null;
        try {
            ItemWeightDto itemWeightDto = new ItemWeightDto();
            List<ItemWeight> itemWeightList = itemService.getItemsWeight();

            if(itemWeightList.size() >= 1){
                itemWeightDto.setResponseCode(MerchantConstants.SUCCESS_RESPONSE_CODE);
                itemWeightDto.setResponseMessage("All Items weight get Success");
                itemWeightDto.setData(itemWeightList);
                responseStatus = ResponseEntity.status(HttpStatus.OK).body(itemWeightDto);
            } else{
                itemWeightDto.setResponseCode(MerchantConstants.FAILED_RESPONSE_CODE);
                itemWeightDto.setResponseMessage("All Items get fetch Failed");
                responseStatus = ResponseEntity.status(HttpStatus.OK).body(itemWeightDto);
            }

        } catch (Exception ex) {
            LOGGER.error(ex.toString());
            responseStatus = ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        } finally {
            return responseStatus;
        }
    }

}
