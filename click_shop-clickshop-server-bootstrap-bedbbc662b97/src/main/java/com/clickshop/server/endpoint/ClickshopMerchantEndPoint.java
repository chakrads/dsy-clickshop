
package com.clickshop.server.endpoint;


import com.clickshop.server.dto.MerchantResponseDto;
import com.clickshop.server.dto.MerchantRequestDto;
import com.clickshop.server.dto.MerchantSignoutResponseDto;
import com.clickshop.server.service.MerchantService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


/**
 * Created by Dell on 7/17/2018.
 */


@RequestMapping("/merchant")
@RestController
public class ClickshopMerchantEndPoint {

    public static final Logger LOGGER = LoggerFactory.getLogger(ClickshopMerchantEndPoint.class);

    @Autowired
    private MerchantService merchantService;

    @RequestMapping(value = "/signin", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<MerchantResponseDto> signinMerchant(@RequestBody MerchantRequestDto merchantRequestDto) {
        ResponseEntity<MerchantResponseDto> responseStatus = null;
        try {
            MerchantResponseDto merchantResponseDto = merchantService.signinMerchant(merchantRequestDto);
            responseStatus = ResponseEntity.status(HttpStatus.OK).body(merchantResponseDto);
        } catch (Exception ex) {
            LOGGER.error(ex.toString());
            responseStatus = ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        } finally {
            return responseStatus;
        }
    }

    @RequestMapping(value = "/signup", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<MerchantResponseDto> signupMerchant(@RequestBody MerchantRequestDto merchantRequestDto) {

        ResponseEntity<MerchantResponseDto> responseStatus = null;
        try {
            MerchantResponseDto merchantResponseDto = merchantService.signupMerchant(merchantRequestDto);
            if (merchantResponseDto.getResponseCode() == 200) {
                responseStatus = ResponseEntity.status(HttpStatus.OK).body(merchantResponseDto);
            } else {
                responseStatus = ResponseEntity.status(HttpStatus.OK).body(merchantResponseDto);
            }
        } catch (Exception ex) {
            LOGGER.error(ex.toString());
            responseStatus = ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        } finally {
            return responseStatus;
        }
    }

    @RequestMapping(value = "/signout", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<MerchantSignoutResponseDto> signoutMerchant(@RequestBody MerchantRequestDto merchantRequestDto) {
        ResponseEntity<MerchantSignoutResponseDto> responseStatus = null;
        try {
            MerchantSignoutResponseDto merchantSignoutResponseDto = merchantService.signoutMerchant(merchantRequestDto);
            responseStatus = ResponseEntity.status(HttpStatus.OK).body(merchantSignoutResponseDto);
        } catch (Exception ex) {
            LOGGER.error(ex.toString());
            responseStatus = ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        } finally {
            return responseStatus;
        }
    }

    @RequestMapping(value = "/getFlag", method = RequestMethod.GET)
    @ResponseBody
    public String getFlag(@RequestParam(value = "merchantId", defaultValue = "0") Long merchantId) {
        return merchantService.getMerchantFlag(merchantId);
    }

    @RequestMapping(value = "/testMerchantEndpointUrl", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<String> testEndpointUrl() {

        ResponseEntity<String> responseStatus = null;
        try {
            responseStatus = ResponseEntity.status(HttpStatus.OK).body("Merchant Endpoint is working!");
        } catch (Exception ex) {
            LOGGER.error(ex.toString());
            responseStatus = ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        } finally {
            return responseStatus;
        }
    }

}
