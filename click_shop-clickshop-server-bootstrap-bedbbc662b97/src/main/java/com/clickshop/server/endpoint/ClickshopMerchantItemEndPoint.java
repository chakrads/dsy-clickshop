package com.clickshop.server.endpoint;

import com.clickshop.server.constants.MerchantConstants;
import com.clickshop.server.dao.MerchantDao;
import com.clickshop.server.dto.MerchantRequestDto;
import com.clickshop.server.dto.MerchantResponseDto;
import com.clickshop.server.dto.request.ItemsOfMerchantRequestDto;
import com.clickshop.server.dto.response.ItemsOfMerchantResponseDto;
import com.clickshop.server.dto.response.MerchantItemDeleteResponseDto;
import com.clickshop.server.dto.response.MerchantItemGetResponseDto;
import com.clickshop.server.entity.MerchantUser;
import com.clickshop.server.service.MerchantItemService;
import com.clickshop.server.service.MerchantService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

/**
 * Created by Dell on 7/22/2018.
 */
@RequestMapping("/merchant")
@RestController
public class ClickshopMerchantItemEndPoint {

    public static final Logger LOGGER = LoggerFactory.getLogger(ClickshopMerchantItemEndPoint.class);

    @Autowired
    private MerchantItemService merchantItemService;

    @Autowired
    private MerchantService merchantService;

    @Autowired
    private MerchantDao merchantDao;

    @RequestMapping(value = "/saveItem", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<ItemsOfMerchantResponseDto> saveItem(@RequestBody ItemsOfMerchantRequestDto itemsOfMerchantRequestDto) {
        ItemsOfMerchantResponseDto itemsOfMerchantResponseDto = new ItemsOfMerchantResponseDto();
        ResponseEntity<ItemsOfMerchantResponseDto> responseStatus = null;
        String sessionId = getSessionId(itemsOfMerchantRequestDto.getMobileNumber(), itemsOfMerchantRequestDto.getMerchantId());
        if(!(itemsOfMerchantRequestDto.getSessionId()).equals(sessionId)){
            itemsOfMerchantResponseDto.setResponseCode(MerchantConstants.INVALID_SESSION_CODE);
            itemsOfMerchantResponseDto.setResponseMessage(MerchantConstants.INVALID_SESSION_MESSAGE);
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(itemsOfMerchantResponseDto);
        }
        try {
            itemsOfMerchantResponseDto = merchantItemService.saveItem(itemsOfMerchantRequestDto);
            responseStatus = ResponseEntity.status(HttpStatus.OK).body(itemsOfMerchantResponseDto);
        } catch (Exception ex) {
            LOGGER.error(ex.toString());
            responseStatus = ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        } finally {
            return responseStatus;
        }
    }

    @RequestMapping(value = "/deleteItem", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<MerchantItemDeleteResponseDto> deleteItem(@RequestBody ItemsOfMerchantRequestDto itemsOfMerchantRequestDto) {

        MerchantItemDeleteResponseDto merchantItemDeleteResponse = new MerchantItemDeleteResponseDto();
        ResponseEntity<MerchantItemDeleteResponseDto> responseStatus = null;
        String sessionId = getSessionId(itemsOfMerchantRequestDto.getMobileNumber(), itemsOfMerchantRequestDto.getMerchantId());
        if(!(itemsOfMerchantRequestDto.getSessionId()).equals(sessionId)){
            merchantItemDeleteResponse.setResponseCode(MerchantConstants.INVALID_SESSION_CODE);
            merchantItemDeleteResponse.setResponseMessage(MerchantConstants.INVALID_SESSION_MESSAGE);
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(merchantItemDeleteResponse);
        }
        try {
            merchantItemDeleteResponse = merchantItemService.deleteItem(itemsOfMerchantRequestDto);
            responseStatus = ResponseEntity.status(HttpStatus.OK).body(merchantItemDeleteResponse);
        } catch (Exception ex) {
            LOGGER.error(ex.toString());
            responseStatus = ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        } finally {
            return responseStatus;
        }
    }

    @RequestMapping(value = "/updateItem", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<ItemsOfMerchantResponseDto> updateItem(@RequestBody ItemsOfMerchantRequestDto itemsOfMerchantRequestDto) {
        ItemsOfMerchantResponseDto itemsOfMerchantResponseDto = new ItemsOfMerchantResponseDto();
        ResponseEntity<ItemsOfMerchantResponseDto> responseStatus = null;
        String sessionId = getSessionId(itemsOfMerchantRequestDto.getMobileNumber(), itemsOfMerchantRequestDto.getMerchantId());
        if(!(itemsOfMerchantRequestDto.getSessionId()).equals(sessionId)){
            itemsOfMerchantResponseDto.setResponseCode(MerchantConstants.INVALID_SESSION_CODE);
            itemsOfMerchantResponseDto.setResponseMessage(MerchantConstants.INVALID_SESSION_MESSAGE);
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(itemsOfMerchantResponseDto);
        }
        try {
            itemsOfMerchantResponseDto = merchantItemService.updateItem(itemsOfMerchantRequestDto);
            responseStatus = ResponseEntity.status(HttpStatus.OK).body(itemsOfMerchantResponseDto);
        } catch (Exception ex) {
            LOGGER.error(ex.toString());
            responseStatus = ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        } finally {
            return responseStatus;
        }
    }

    @RequestMapping(value = "/getStoreItem", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<MerchantItemGetResponseDto> getStoreItem(@RequestBody ItemsOfMerchantRequestDto itemsOfMerchantRequestDto) {
        MerchantItemGetResponseDto merchantItemGetResponseDto = new MerchantItemGetResponseDto();
        ResponseEntity<MerchantItemGetResponseDto> responseStatus = null;
        String sessionId = getSessionId(itemsOfMerchantRequestDto.getMobileNumber(), itemsOfMerchantRequestDto.getMerchantId());
        if(!(itemsOfMerchantRequestDto.getSessionId()).equals(sessionId)){
            merchantItemGetResponseDto.setResponseCode(MerchantConstants.INVALID_SESSION_CODE);
            merchantItemGetResponseDto.setResponseMessage(MerchantConstants.INVALID_SESSION_MESSAGE);
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(merchantItemGetResponseDto);
        }
        try {
            merchantItemGetResponseDto = merchantItemService.getStoreItem(itemsOfMerchantRequestDto);
            responseStatus = ResponseEntity.status(HttpStatus.OK).body(merchantItemGetResponseDto);
        } catch (Exception ex) {
            LOGGER.error(ex.toString());
            responseStatus = ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        } finally {
            return responseStatus;
        }
    }

    @CrossOrigin(origins = "http://52.66.163.81:8080")
    @PostMapping("/processStoreDataFile")
    public ResponseEntity<String> processStoreDataFile(@RequestParam("file") MultipartFile multipartFile , @RequestParam("userName") String userName , @RequestParam("password") String password) {

        ResponseEntity<String> responseStatus;
        try {
            /*MerchantUser merchant =new MerchantUser();
            merchant.setMobileNumber(userName.trim());
            merchant.setPassword(password);*/
            MerchantRequestDto merchantRequestDto = new MerchantRequestDto();
            merchantRequestDto.setMobileNumber(userName.trim());
            merchantRequestDto.setPassword(password);
            MerchantResponseDto signinMerchant = merchantService.signinMerchant(merchantRequestDto);
            if(signinMerchant.getResponseCode().equals(MerchantConstants.FAILED_RESPONSE_CODE)) {
                return ResponseEntity.status(HttpStatus.OK).body(signinMerchant.getResponseMessage());
            }

            boolean isProcessed = merchantItemService.processStroreitem(multipartFile.getInputStream());
            if(isProcessed) {
                responseStatus=ResponseEntity.status(HttpStatus.OK).body("File process success, Please check the data after sometime!");
            }else {
                responseStatus=ResponseEntity.status(HttpStatus.OK).body("File process failed");
            }
        } catch (Exception e) {
            LOGGER.error(e.toString());
            responseStatus = ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }
        return responseStatus;
    }

    public String getSessionId(String mobileNumber, Long merchantId) {
        MerchantUser merchantUser = merchantDao.findMerchantUserByMobileNumber(mobileNumber, merchantId);
        String sessionId = merchantDao.getSessionIdByMerchantUserId(merchantUser.getMerchantUserId());
        return sessionId;
    }

}
