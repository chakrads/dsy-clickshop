package com.clickshop.server.service;

import com.clickshop.server.dto.request.StoreRequestDto;
import com.clickshop.server.dto.response.StoreResponseDto;
import org.springframework.stereotype.Component;

/**
 * Created by Dell on 8/2/2018.
 */
@Component
public interface StoreService {

    public StoreResponseDto saveStore(StoreRequestDto storeRequestDto);

    public StoreResponseDto updateStore(StoreRequestDto storeRequestDto);

    public StoreResponseDto getStoreSetup(StoreRequestDto storeRequestDto);
}
