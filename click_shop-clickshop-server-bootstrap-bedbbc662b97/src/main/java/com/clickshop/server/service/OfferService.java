package com.clickshop.server.service;

import com.clickshop.server.dto.request.OfferRequestDto;
import com.clickshop.server.dto.response.OfferDeleteResponceDto;
import com.clickshop.server.dto.response.OfferResponceDto;
import org.springframework.stereotype.Component;

/**
 * Created by Dell on 7/18/2018.
 */
@Component
public interface OfferService {

    public OfferResponceDto saveOffer(OfferRequestDto offerRequestDto);
    public OfferDeleteResponceDto deleteOffer(OfferRequestDto offerRequestDto);
    public OfferResponceDto getOffer(OfferRequestDto offerRequestDto);
    public OfferResponceDto updateOffer(OfferRequestDto offerRequestDto);


}
