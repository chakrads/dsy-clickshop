package com.clickshop.server.service.impl;

import com.clickshop.server.dao.BrandedItemDao;
import com.clickshop.server.entity.BrandedItem;
import com.clickshop.server.service.BrandedItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class BrandedItemServiceImpl implements BrandedItemService {

    @Autowired
    private BrandedItemDao brandedItemDao;

    @Override
    public List<BrandedItem> getAllBrandedItems() {
        List<BrandedItem> listOfAllItems = brandedItemDao.getAllBrandedItems();
        return listOfAllItems;
    }
}
