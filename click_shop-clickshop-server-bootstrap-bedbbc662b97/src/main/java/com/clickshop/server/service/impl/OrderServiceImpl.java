package com.clickshop.server.service.impl;

import com.clickshop.server.constants.MerchantConstants;
import com.clickshop.server.dao.ItemDao;
import com.clickshop.server.dao.MerchantItemDao;
import com.clickshop.server.dao.OrderDao;
import com.clickshop.server.dto.*;
import com.clickshop.server.dto.request.SearchOrderDetailsRequestDto;
import com.clickshop.server.dto.response.SearchOrderDetailsResponseDto;
import com.clickshop.server.entity.*;
import com.clickshop.server.fcm.SendOrderNotifs;
import com.clickshop.server.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class OrderServiceImpl implements OrderService {

    @Autowired
    private OrderDao orderDao;

    @Autowired
    private ItemDao itemDao;

    @Autowired
    private MerchantItemDao merchantItemDao;

    @Override
    public OrderSearchResponseDto findOrder(OrderSearchRequestDto orderSearchRequestDto) {
        OrderSearchResponseDto orderSearchResponseDto = new OrderSearchResponseDto();
        List<OrderSearchListResponseDto> orderSearchList = new ArrayList<>();
        List<Order> orderList = orderDao.findMerchantOrderByStatus(orderSearchRequestDto);
        if (orderList.size() != 0) {
            orderList.forEach(order -> {
                OrderSearchListResponseDto orderSearchListResponseDto = new OrderSearchListResponseDto();
                orderSearchListResponseDto.setMerchantId(order.getMerchant().getMerchantId());
                orderSearchListResponseDto.setOrderId(order.getOrderId());
                orderSearchListResponseDto.setOrderStatus(order.getOrderStatus());
                orderSearchListResponseDto.setOrderAmount(order.getOrderAmount());
                orderSearchListResponseDto.setPaymentMode(order.getPaymentMode());
                orderSearchListResponseDto.setShippingAddress(order.getShippingAddress());
                orderSearchListResponseDto.setMobileNumber(order.getMobileNumber());
                orderSearchListResponseDto.setUserId(order.getUser().getUserId());
                User user = orderDao.getUserById(order.getUser().getUserId());
                orderSearchListResponseDto.setUserName(user.getFirstName()+" "+user.getLastName());
                orderSearchListResponseDto.setDeliveryCharge(order.getDeliveryCharge());
                orderSearchList.add(orderSearchListResponseDto);
            });
            orderSearchResponseDto.setResponseCode(MerchantConstants.SUCCESS_RESPONSE_CODE);
            orderSearchResponseDto.setResponseMessage("Merchant order get success");
            orderSearchResponseDto.setOrderSearchList(orderSearchList);
        } else {
            orderSearchResponseDto.setResponseCode(MerchantConstants.FAILED_RESPONSE_CODE);
            orderSearchResponseDto.setResponseMessage("Merchant order list for orderStatus:"+orderSearchRequestDto.getOrderStatus()+", is empty");
        }
        return orderSearchResponseDto;
    }

    @Override
    public OrderStatusSaveResponseDto saveMerchantOrderStatus(OrderStatusSaveRequestDto orderStatusSaveRequestDto) {
        OrderStatusSaveResponseDto orderStatusSaveResponseDto = new OrderStatusSaveResponseDto();
        Order order = orderDao.saveMerchantOrderStatusById(orderStatusSaveRequestDto);
        if (null != order) {
            orderStatusSaveResponseDto.setResponseCode(MerchantConstants.SUCCESS_RESPONSE_CODE);
            orderStatusSaveResponseDto.setResponseMessage("Merchant order status updated successfully");
            orderStatusSaveResponseDto.setUserId(order.getUser().getUserId());
            orderStatusSaveResponseDto.setOrderId(order.getOrderId());
            orderStatusSaveResponseDto.setOrderStatus(orderStatusSaveRequestDto.getOrderStatus());
            orderStatusSaveResponseDto.setShippingAddress(order.getShippingAddress());
            orderStatusSaveResponseDto.setOrderAmount(order.getOrderAmount());
            orderStatusSaveResponseDto.setPaymentMode(order.getPaymentMode());
            orderStatusSaveResponseDto.setMerchantId(order.getMerchant().getMerchantId());
            orderStatusSaveResponseDto.setDeliveryCharge(order.getDeliveryCharge());
            //Send Order status change Notifs to User start
            Long userId = order.getUser().getUserId();
            String firebaseToken = orderDao.getUserFcmToken(userId);
            if (null != firebaseToken) {
                SendOrderNotifs orderNotifs = new SendOrderNotifs();
                orderNotifs.sendOrderStatusNotifsToUser(firebaseToken, order.getOrderStatus());
            }
            //Send Order status change Notifs to User start
        } else {
            orderStatusSaveResponseDto.setResponseCode(MerchantConstants.FAILED_RESPONSE_CODE);
            orderStatusSaveResponseDto.setResponseMessage("Merchant order status update failed");
            orderStatusSaveResponseDto.setOrderId(orderStatusSaveRequestDto.getOrderId());
        }
        return orderStatusSaveResponseDto;
    }

    @Override
    public SearchOrderDetailsResponseDto findOrderDetails(SearchOrderDetailsRequestDto searchOrderDetailsRequestDto){
        SearchOrderDetailsResponseDto searchOrderDetailsResponseDto = new SearchOrderDetailsResponseDto();
        List<OrderDetailsListResponseDto>  orderDetailsListResponseDto = new ArrayList<>();
        /*Order order = orderDao.findOrder(searchOrderDetailsRequestDto.getOrderId());
        if(null != order){*/
            List<OrderDetails> orderDetailsList = orderDao.getOrderDetails(searchOrderDetailsRequestDto.getOrderId());
            if(null != orderDetailsList){
                orderDetailsList.forEach(orderDetails->{
                    OrderDetailsListResponseDto orderListResponseDto = new OrderDetailsListResponseDto();
                    ItemsOfMerchant itemsOfMerchant = merchantItemDao.getMerchantItemById(orderDetails.getItemMerchantId());
                    Item item = itemDao.getItemById(itemsOfMerchant.getItemId());
                    orderListResponseDto.setItemId(item.getItemId());
                    orderListResponseDto.setOrderId(orderDetails.getOrder().getOrderId());
                    orderListResponseDto.setItemPrice(orderDetails.getItemPrice());
                    orderListResponseDto.setItemQuantity(orderDetails.getItemQuantityRequested());
                    orderListResponseDto.setItemWeight(item.getItemWeight());
                    orderListResponseDto.setTotalPrice(orderDetails.getTotalPrice());
                    orderListResponseDto.setItemName(item.getItemName());
                    orderListResponseDto.setItemImage(item.getItemImage());
                    orderDetailsListResponseDto.add(orderListResponseDto);
                });
//            }
            searchOrderDetailsResponseDto.setResponseCode(MerchantConstants.SUCCESS_RESPONSE_CODE);
            searchOrderDetailsResponseDto.setResponseMessage("Merchant Order details get success");
            searchOrderDetailsResponseDto.setOrderDetailsList(orderDetailsListResponseDto);
        }else{
            searchOrderDetailsResponseDto.setResponseCode(MerchantConstants.FAILED_RESPONSE_CODE);
            searchOrderDetailsResponseDto.setResponseMessage("Merchant Order details get failed");
        }
        return searchOrderDetailsResponseDto;
    }

    @Override
    public OrderSearchResponseDto searchMerchantOrderArchive(OrderArchiveRequestDto orderArchiveRequestDto) {
        OrderSearchResponseDto orderSearchResponseDto = new OrderSearchResponseDto();
        List<OrderSearchListResponseDto> orderSearchList = new ArrayList<>();
        List<Order> orderList = orderDao.searchMerchantOrderArchive(orderArchiveRequestDto.getMerchantId());
        if (orderList.size() != 0) {
            orderList.forEach(order -> {
                OrderSearchListResponseDto orderSearchListResponseDto = new OrderSearchListResponseDto();
                orderSearchListResponseDto.setMerchantId(order.getMerchant().getMerchantId());
                orderSearchListResponseDto.setOrderId(order.getOrderId());
                orderSearchListResponseDto.setOrderStatus(order.getOrderStatus());
                orderSearchListResponseDto.setOrderAmount(order.getOrderAmount());
                orderSearchListResponseDto.setPaymentMode(order.getPaymentMode());
                orderSearchListResponseDto.setShippingAddress(order.getShippingAddress());
                orderSearchListResponseDto.setMobileNumber(order.getMobileNumber());
                orderSearchListResponseDto.setUserId(order.getUser().getUserId());
                orderSearchListResponseDto.setDeliveryCharge(order.getDeliveryCharge());
                orderSearchList.add(orderSearchListResponseDto);
            });
            orderSearchResponseDto.setResponseCode(MerchantConstants.SUCCESS_RESPONSE_CODE);
            orderSearchResponseDto.setResponseMessage("Merchant archive order get success");
            orderSearchResponseDto.setOrderSearchList(orderSearchList);
        } else {
            orderSearchResponseDto.setResponseCode(MerchantConstants.FAILED_RESPONSE_CODE);
            orderSearchResponseDto.setResponseMessage("Merchant order archive not available, empty archive!");
        }
        return orderSearchResponseDto;
    }
}
