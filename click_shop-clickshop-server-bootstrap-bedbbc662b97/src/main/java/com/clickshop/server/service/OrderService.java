package com.clickshop.server.service;

import com.clickshop.server.dto.*;
import com.clickshop.server.dto.response.SearchOrderDetailsResponseDto;
import com.clickshop.server.dto.request.SearchOrderDetailsRequestDto;
import org.springframework.stereotype.Component;

@Component
public interface OrderService {

    public OrderSearchResponseDto findOrder(OrderSearchRequestDto orderSearchRequestDto);

    public OrderStatusSaveResponseDto saveMerchantOrderStatus(OrderStatusSaveRequestDto orderStatusSaveRequestDto);

    public SearchOrderDetailsResponseDto findOrderDetails(SearchOrderDetailsRequestDto searchOrderDetailsRequestDto);

    public OrderSearchResponseDto searchMerchantOrderArchive(OrderArchiveRequestDto orderArchiveRequestDto);

}
