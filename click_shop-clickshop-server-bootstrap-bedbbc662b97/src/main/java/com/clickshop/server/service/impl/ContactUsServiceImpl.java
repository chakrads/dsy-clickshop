package com.clickshop.server.service.impl;

import com.clickshop.server.constants.MerchantConstants;
import com.clickshop.server.dao.ContactUsDao;
import com.clickshop.server.dto.request.ContactUsRequestDto;
import com.clickshop.server.dto.response.ContactUsResponseDto;
import com.clickshop.server.endpoint.ClickshopMerchantItemEndPoint;
import com.clickshop.server.entity.ContactUs;
import com.clickshop.server.entity.Merchant;
import com.clickshop.server.service.ContactUsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ContactUsServiceImpl implements ContactUsService {

    public static final Logger LOGGER = LoggerFactory.getLogger(ClickshopMerchantItemEndPoint.class);

    @Autowired
    ContactUsDao contactUsDao;

    @Override
    public ContactUsResponseDto saveContactUs(ContactUsRequestDto contactUsRequestDto) {
        ContactUsResponseDto contactUsResponseDto = new ContactUsResponseDto();
        ContactUs contactUsEntityData = prepareContactEntityData(contactUsRequestDto);
        ContactUs contactUsData;
        try {
            contactUsData = contactUsDao.createContactUs(contactUsEntityData);
        } catch (Exception e) {
            LOGGER.info(e.getMessage());
            contactUsResponseDto.setResponseCode(MerchantConstants.FAILED_RESPONSE_CODE);
            contactUsResponseDto.setResponseMessage(e.getMessage());
            return contactUsResponseDto;
        }
        if (null != contactUsData) {
            contactUsResponseDto = prepareContactUsResponseData(contactUsData);
        } else {
            contactUsResponseDto.setResponseCode(MerchantConstants.FAILED_RESPONSE_CODE);
            contactUsResponseDto.setResponseMessage("Contact Us save failed");
        }
        return contactUsResponseDto;
    }

    private ContactUsResponseDto prepareContactUsResponseData(ContactUs contactUs) {
        ContactUsResponseDto contactUsResponseDto = new ContactUsResponseDto();

        contactUsResponseDto.setResponseCode(MerchantConstants.SUCCESS_RESPONSE_CODE);
        contactUsResponseDto.setResponseMessage("Contact Us save success");
        contactUsResponseDto.setContactUsId(contactUs.getContactUsId());
        contactUsResponseDto.setMerchantId(contactUs.getMerchant().getMerchantId());
        contactUsResponseDto.setContactUsMessage(contactUs.getContactUsMessage());
        contactUsResponseDto.setContactUsEmail(contactUs.getContactUsEmail());
        contactUsResponseDto.setContactUsMobileNo(contactUs.getContactUsMobileNo());
        contactUsResponseDto.setContactUsName(contactUs.getContactUsName());

        return contactUsResponseDto;
    }

    private ContactUs prepareContactEntityData(ContactUsRequestDto contactUsRequestDto) {
        ContactUs contactUs = new ContactUs();
        Merchant merchant = new Merchant();
        merchant.setMerchantId(contactUsRequestDto.getMerchantId());
        contactUs.setContactUsId(contactUsRequestDto.getContactUsId());
        contactUs.setContactUsEmail(contactUsRequestDto.getContactUsEmail());
        contactUs.setContactUsMessage(contactUsRequestDto.getContactUsMessage());
        contactUs.setContactUsMobileNo(contactUsRequestDto.getContactUsMobileNo());
        contactUs.setContactUsName(contactUsRequestDto.getContactUsName());
        contactUs.setMerchant(merchant);
        return contactUs;
    }
}
