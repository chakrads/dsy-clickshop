package com.clickshop.server.service;

import com.clickshop.server.entity.Item;
import com.clickshop.server.entity.ItemCategory;
import com.clickshop.server.entity.ItemWeight;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by Dell on 7/24/2018.
 */
@Component
public interface ItemService {

    // public Item saveItem(Item item);

    public List<Item> getAllItems();

    public List<Item> getItemsByCategory(ItemCategory itemCategory);

    public List<Item> getItemListRandom(String categoryName);

    public List<ItemWeight> getItemsWeight();

}
