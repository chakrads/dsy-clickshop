package com.clickshop.server.service;

import com.clickshop.server.dto.request.WalletRequestDto;
import com.clickshop.server.dto.response.WalletResponseDto;
import org.springframework.stereotype.Component;

@Component
public interface WalletService {

    public WalletResponseDto getMerchantWalletAmount(WalletRequestDto walletRequestDto);

    public WalletResponseDto updateMerchantWalletAmount(WalletRequestDto walletRequestDto);

}
