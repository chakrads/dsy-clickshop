package com.clickshop.server.service.impl;

import com.clickshop.server.constants.MerchantConstants;
import com.clickshop.server.dao.OfferDao;
import com.clickshop.server.dto.request.OfferRequestDto;
import com.clickshop.server.dto.response.OfferDeleteResponceDto;
import com.clickshop.server.dto.response.OfferResponceDto;
import com.clickshop.server.entity.Merchant;
import com.clickshop.server.entity.Offer;
import com.clickshop.server.service.OfferService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by Dell on 7/22/2018.
 */
@Component
public class OfferServiceImpl implements OfferService {

    @Autowired
    private OfferDao offerDao;

    public OfferResponceDto saveOffer(OfferRequestDto offerRequestDto) {
        Offer offerGetData = offerDao.getItemsOffer(offerRequestDto);
        OfferResponceDto offerResponceDto = new OfferResponceDto();
        if (offerGetData == null) {
            Offer offer = prepareOfferRequest(offerRequestDto);
            Offer offerData = offerDao.saveOffer(offer);
            offerResponceDto = prepareOfferResponse(offerData);
        } else {
            offerResponceDto.setResponseCode(MerchantConstants.FAILED_RESPONSE_CODE);
            offerResponceDto.setResponseMessage("Offer save failed, item offer already exists");
        }
        return offerResponceDto;
    }

    @Override
    public OfferDeleteResponceDto deleteOffer(OfferRequestDto offerRequestDto) {
        OfferDeleteResponceDto offerDeleteResponceDto = new OfferDeleteResponceDto();
        int deleteRecordCount = offerDao.deleteOffer(offerRequestDto);
        if (deleteRecordCount == 1) {
            offerDeleteResponceDto.setResponseCode(MerchantConstants.SUCCESS_RESPONSE_CODE);
            offerDeleteResponceDto.setResponseMessage("Offer delete Success");
        } else {
            offerDeleteResponceDto.setResponseCode(MerchantConstants.FAILED_RESPONSE_CODE);
            offerDeleteResponceDto.setResponseMessage("Offer deletion failed");
        }
        return offerDeleteResponceDto;
    }

    @Override
    public OfferResponceDto getOffer(OfferRequestDto offerRequestDto) {

        Offer offerData = offerDao.getItemsOffer(offerRequestDto);
        OfferResponceDto offerResponceDto = new OfferResponceDto();
        if (null != offerData) {
            offerResponceDto = prepareOfferResponse(offerData);
            offerResponceDto.setResponseMessage("Offer item get success");
        } else {
            offerResponceDto.setResponseCode(MerchantConstants.FAILED_RESPONSE_CODE);
            offerResponceDto.setResponseMessage("Offer item get failed");
        }
        return offerResponceDto;
    }

    @Override
    public OfferResponceDto updateOffer(OfferRequestDto offerRequestDto) {
        Offer offer = prepareOfferRequest(offerRequestDto);
        int result = offerDao.updateOffer(offer);
        OfferResponceDto offerResponceDto = new OfferResponceDto();
        if (result == 1) {
            offerResponceDto.setResponseCode(MerchantConstants.SUCCESS_RESPONSE_CODE);
            offerResponceDto.setResponseMessage("Offer update success");
        } else {
            offerResponceDto.setResponseCode(MerchantConstants.FAILED_RESPONSE_CODE);
            offerResponceDto.setResponseMessage("Offer update failed");
        }
        return offerResponceDto;
    }

    private OfferResponceDto prepareOfferResponse(Offer offerData) {
        OfferResponceDto offerResponceDto = new OfferResponceDto();
        offerResponceDto.setOfferId(offerData.getOfferId());
        offerResponceDto.setMerchantId(offerData.getMerchant().getMerchantId());
        offerResponceDto.setItemId(offerData.getItemId());
        offerResponceDto.setActualPrice(offerData.getActualPrice());
        offerResponceDto.setDiscountPercentage(offerData.getDiscountPercentage());
        offerResponceDto.setDiscountAmount(offerData.getDiscountAmount());
        offerResponceDto.setPriceAfterDiscount(offerData.getPriceAfterDiscount());
        offerResponceDto.setResponseCode(MerchantConstants.SUCCESS_RESPONSE_CODE);
        offerResponceDto.setResponseMessage("Offer save success");
        return offerResponceDto;
    }

    private Offer prepareOfferRequest(OfferRequestDto offerRequestDto) {
        Offer offer = new Offer();
        Merchant merchant = new Merchant();
        merchant.setMerchantId(offerRequestDto.getMerchantId());
        offer.setMerchant(merchant);
        offer.setItemId(offerRequestDto.getItemId());
        offer.setActualPrice(offerRequestDto.getActualPrice());
        offer.setDiscountPercentage(offerRequestDto.getDiscountPercentage());
        offer.setDiscountAmount(offerRequestDto.getDiscountAmount());
        offer.setPriceAfterDiscount(offerRequestDto.getPriceAfterDiscount());
        return offer;
    }
}
