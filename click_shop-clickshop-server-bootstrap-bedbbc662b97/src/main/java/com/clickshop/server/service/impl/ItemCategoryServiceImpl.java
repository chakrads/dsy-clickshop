package com.clickshop.server.service.impl;

import com.clickshop.server.constants.MerchantConstants;
import com.clickshop.server.dao.ItemCategoryDao;
import com.clickshop.server.dto.ItemCategoryDto;
import com.clickshop.server.entity.ItemCategory;
import com.clickshop.server.service.ItemCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ItemCategoryServiceImpl implements ItemCategoryService {

    @Autowired
    private ItemCategoryDao itemCategoryDao;

    @Override
    public List<ItemCategory> getItemsCategory() {
        return itemCategoryDao.getItemsCategory();
    }

    @Override
    public ItemCategoryDto findItemCategoryRandom() {
        ItemCategoryDto itemCategoryDto = new ItemCategoryDto();
        List<ItemCategory> itemCategoryList = itemCategoryDao.getItemsCategoryRandom();
        if (null != itemCategoryList) {
            itemCategoryDto.setResponseCode(MerchantConstants.SUCCESS_RESPONSE_CODE);
            itemCategoryDto.setResponseMessage("Item Category random list get success");
            itemCategoryDto.setData(itemCategoryList);
        } else {
            itemCategoryDto.setResponseCode(MerchantConstants.FAILED_RESPONSE_CODE);
            itemCategoryDto.setResponseMessage("Item Category random list get failed");
        }
        return itemCategoryDto;
    }
}
