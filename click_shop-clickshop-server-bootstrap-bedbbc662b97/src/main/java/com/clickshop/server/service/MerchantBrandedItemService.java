package com.clickshop.server.service;

import com.clickshop.server.dto.request.BrandedItemsOfMerchantRequestDto;
import com.clickshop.server.dto.response.BrandedItemsOfMerchantResponseDto;
import org.springframework.stereotype.Component;

@Component
public interface MerchantBrandedItemService {

    public BrandedItemsOfMerchantResponseDto saveBrandedItem(BrandedItemsOfMerchantRequestDto itemsOfMerchantRequestDto);

}
