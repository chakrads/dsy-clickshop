package com.clickshop.server.service.impl;

import com.clickshop.server.constants.MerchantConstants;
import com.clickshop.server.dao.MerchantDao;
import com.clickshop.server.dao.StoreDao;
import com.clickshop.server.dto.request.StoreRequestDto;
import com.clickshop.server.dto.response.StoreResponseDto;
import com.clickshop.server.entity.Merchant;
import com.clickshop.server.entity.Store;
import com.clickshop.server.service.StoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by Dell on 8/2/2018.
 */
@Component
public class StoreServiceImpl implements StoreService {

    @Autowired
    private StoreDao storeDao;

    @Autowired
    private MerchantDao merchantDao;

    @Override
    public StoreResponseDto saveStore(StoreRequestDto storeRequestDto) {

        Merchant merchant = new Merchant();
        merchant.setMerchantId(storeRequestDto.getMerchantId());
        StoreResponseDto storeResponseDto = new StoreResponseDto();
        Merchant storeMerchant = merchantDao.getMerchantById(merchant);
        if(!storeRequestDto.getFlag().isEmpty()){
            merchantDao.updateMerchantFlag(storeRequestDto.getFlag(), storeRequestDto.getMerchantId());
        }
        if (null != storeMerchant) {
            Store storeEntityData = prepareStoreEntityData(storeRequestDto);
            Store storeData = storeDao.saveStore(storeEntityData);
            if (null != storeData) {
                storeResponseDto = prepareStoreResponseData(storeData);
            } else {
                storeResponseDto.setResponseCode(MerchantConstants.FAILED_RESPONSE_CODE);
                storeResponseDto.setResponseMessage("Store setup save failed");
            }
        }else{
            storeResponseDto.setResponseCode(MerchantConstants.FAILED_RESPONSE_CODE);
            storeResponseDto.setResponseMessage("Merchant is not available, Please register first");
        }
        return storeResponseDto;
    }

    @Override
    public StoreResponseDto updateStore(StoreRequestDto storeRequestDto) {

        Store storeEntityData = prepareStoreEntityData(storeRequestDto);
        StoreResponseDto storeResponseDto = new StoreResponseDto();
        Store storeData = storeDao.updateStore(storeEntityData);
        if (null != storeData) {
            storeResponseDto = prepareStoreResponseData(storeData);
        } else {
            storeResponseDto.setResponseCode(MerchantConstants.FAILED_RESPONSE_CODE);
            storeResponseDto.setResponseMessage("Store setup update failed");
        }
        return storeResponseDto;
    }

    @Override
    public StoreResponseDto getStoreSetup(StoreRequestDto storeRequestDto) {
        Store storeData = storeDao.getStoreSetup(storeRequestDto.getMerchantId());
        StoreResponseDto storeResponseDto =  new StoreResponseDto();
        if(null != storeData){
            storeResponseDto = prepareStoreResponseData(storeData);
            storeResponseDto.setResponseMessage("Store setup get success");
        }else{
            storeResponseDto.setResponseCode(MerchantConstants.FAILED_RESPONSE_CODE);
            storeResponseDto.setResponseMessage("Store setup get failed");
        }
        return storeResponseDto;
    }

    private StoreResponseDto prepareStoreResponseData(Store store) {
        StoreResponseDto storeResponseDto = new StoreResponseDto();

        storeResponseDto.setResponseCode(MerchantConstants.SUCCESS_RESPONSE_CODE);
        storeResponseDto.setResponseMessage("Store setup save/update success");
        storeResponseDto.setStoreId(store.getStoreId());
        storeResponseDto.setMerchantId(store.getMerchant().getMerchantId());
        storeResponseDto.setStoreName(store.getStoreName());
        storeResponseDto.setStoreRating(store.getStoreRating());
        storeResponseDto.setAddress(store.getAddress());
        storeResponseDto.setCity(store.getCity());
        storeResponseDto.setCountry(store.getCountry());
        storeResponseDto.setLatitude(store.getLatitude());
        storeResponseDto.setWorkingHourFrom(store.getWorkingHourFrom());
        storeResponseDto.setWorkingHourTo(store.getWorkingHourTo());
        storeResponseDto.setLongitude(store.getLongitude());
        storeResponseDto.setFlag(store.getFlag());
        storeResponseDto.setDeliveryDuration(store.getDeliveryDuration());
        storeResponseDto.setDeliveryRange(store.getDeliveryRange());
        storeResponseDto.setPinCode(store.getPinCode());
        storeResponseDto.setState(store.getState());
        storeResponseDto.setDeliveryCharge(store.getDeliveryCharge());
        storeResponseDto.setDeliveryFreeAbove(store.getDeliveryFreeAbove());
        return storeResponseDto;
    }

    private Store prepareStoreEntityData(StoreRequestDto storeRequestDto) {
        Store store = new Store();
        Merchant merchant = new Merchant();
        merchant.setMerchantId(storeRequestDto.getMerchantId());
        store.setStoreId(storeRequestDto.getStoreId());
        store.setMerchant(merchant);
        store.setStoreName(storeRequestDto.getStoreName());
        store.setStoreRating(storeRequestDto.getStoreRating());
        store.setAddress(storeRequestDto.getAddress());
        store.setCity(storeRequestDto.getCity());
        store.setCountry(storeRequestDto.getCountry());
        store.setDeliveryDuration(storeRequestDto.getDeliveryDuration());
        store.setDeliveryRange(storeRequestDto.getDeliveryRange());
        store.setFlag(storeRequestDto.getFlag());
        store.setLatitude(storeRequestDto.getLatitude());
        store.setLongitude(storeRequestDto.getLongitude());
        store.setPinCode(storeRequestDto.getPinCode());
        store.setState(storeRequestDto.getState());
        store.setWorkingHourFrom(storeRequestDto.getWorkingHourFrom());
        store.setWorkingHourTo(storeRequestDto.getWorkingHourTo());
        store.setDeliveryCharge(storeRequestDto.getDeliveryCharge());
        store.setDeliveryFreeAbove(storeRequestDto.getDeliveryFreeAbove());
        return store;
    }
}
