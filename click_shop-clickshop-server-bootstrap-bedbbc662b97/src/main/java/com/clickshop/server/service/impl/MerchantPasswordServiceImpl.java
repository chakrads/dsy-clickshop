package com.clickshop.server.service.impl;

import com.clickshop.server.constants.MerchantConstants;
import com.clickshop.server.constants.SecurityConstants;
import com.clickshop.server.dao.MerchantDao;
import com.clickshop.server.dao.MerchantPasswordDao;
import com.clickshop.server.dto.request.PasswordRequestDto;
import com.clickshop.server.dto.response.PasswordResponseDto;
import com.clickshop.server.entity.Merchant;
import com.clickshop.server.entity.MerchantUser;
import com.clickshop.server.service.MerchantPasswordService;
import com.clickshop.server.utils.PasswordUtility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class MerchantPasswordServiceImpl implements MerchantPasswordService {

    @Autowired
    MerchantPasswordDao merchantPasswordDao;

    @Autowired
    MerchantDao merchantDao;

    @Override
    public PasswordResponseDto setNewPassword(PasswordRequestDto passwordRequestDto) {
        PasswordResponseDto passwordResponseDto = new PasswordResponseDto();
        Merchant merchantData = merchantPasswordDao.findMerchantByMobileNo(passwordRequestDto.getMobileNumber());
        if (null != merchantData) {
            if (passwordRequestDto.getNewPassword().equals(passwordRequestDto.getConfirmPassword())) {

                int resultCount = merchantPasswordDao.createNewPassword(passwordRequestDto);
                if (1 == resultCount) {
                    passwordResponseDto.setResponseCode(MerchantConstants.SUCCESS_RESPONSE_CODE);
                    passwordResponseDto.setResponseMessage("Password update successfully");
                } else {
                    passwordResponseDto.setResponseMessage("Password update failed");
                    passwordResponseDto.setResponseCode(MerchantConstants.FAILED_RESPONSE_CODE);
                }
            } else {
                passwordResponseDto.setResponseMessage("New Password and Confirm password does not match");
                passwordResponseDto.setResponseCode(MerchantConstants.FAILED_RESPONSE_CODE);
            }

        } else {
            passwordResponseDto.setResponseMessage("Password update failed due to mobile number does not match");
            passwordResponseDto.setResponseCode(MerchantConstants.FAILED_RESPONSE_CODE);
        }
        return passwordResponseDto;

    }

    @Override
    public PasswordResponseDto resetPassword(PasswordRequestDto passwordRequestDto) {
        PasswordResponseDto passwordResponseDto = new PasswordResponseDto();
        Merchant merchantData = merchantPasswordDao.findMerchantByMobileNo(passwordRequestDto.getMobileNumber());
        if (null != merchantData) {
            if (!passwordRequestDto.getOldPassword().equals(passwordRequestDto.getNewPassword())) {
                String secureMerchantPassword = PasswordUtility.generateSecurePassword(passwordRequestDto.getOldPassword(), SecurityConstants.PASSWORD_ENCRYPTION_SALT);
                passwordRequestDto.setOldPassword(secureMerchantPassword);
                String oldPassword = passwordRequestDto.getOldPassword();
                MerchantUser merchantUserData = merchantDao.findMerchantUserByMobileNumber(merchantData.getMobileNumber(), merchantData.getMerchantId());
                if (merchantUserData.getPassword().equals(oldPassword)) {
                    if (passwordRequestDto.getNewPassword().equals(passwordRequestDto.getConfirmPassword())) {

                        int resultCount = merchantPasswordDao.createNewPassword(passwordRequestDto);
                        if (1 == resultCount) {
                            passwordResponseDto.setResponseCode(MerchantConstants.SUCCESS_RESPONSE_CODE);
                            passwordResponseDto.setResponseMessage("Password update successfully");
                        } else {
                            passwordResponseDto.setResponseMessage("Password update failed");
                            passwordResponseDto.setResponseCode(MerchantConstants.FAILED_RESPONSE_CODE);
                        }
                    } else {
                        passwordResponseDto.setResponseMessage("New Password and Confirm password does not match");
                        passwordResponseDto.setResponseCode(MerchantConstants.FAILED_RESPONSE_CODE);
                    }
                } else {
                    passwordResponseDto.setResponseMessage("Previous password and old password does not match");
                    passwordResponseDto.setResponseCode(MerchantConstants.FAILED_RESPONSE_CODE);
                }
            }else {
                passwordResponseDto.setResponseMessage("New password is same as old password");
                passwordResponseDto.setResponseCode(MerchantConstants.FAILED_RESPONSE_CODE);
            }
        } else {
            passwordResponseDto.setResponseMessage("Password update failed due to mobile number does not match");
            passwordResponseDto.setResponseCode(MerchantConstants.FAILED_RESPONSE_CODE);
        }
        return passwordResponseDto;
    }
}
