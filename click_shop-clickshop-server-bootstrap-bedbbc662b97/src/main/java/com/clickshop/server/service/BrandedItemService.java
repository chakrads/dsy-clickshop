package com.clickshop.server.service;

import com.clickshop.server.entity.BrandedItem;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface BrandedItemService {

    public List<BrandedItem> getAllBrandedItems();

}
