package com.clickshop.server.service.impl;

import com.clickshop.server.constants.MerchantConstants;
import com.clickshop.server.dao.ProfileImageDao;
import com.clickshop.server.dto.ProfileImageDto;
import com.clickshop.server.entity.ProfileImage;
import com.clickshop.server.service.ProfileImageService;
import com.cloudinary.Cloudinary;
import com.cloudinary.utils.ObjectUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.Map;

@Component
public class ProfileImageServiceImpl implements ProfileImageService {

    public static final Logger LOGGER = LoggerFactory.getLogger(ProfileImageServiceImpl.class);

    @Autowired
    private ProfileImageDao profileImageDao;

    @Override
    public ProfileImageDto saveProfileImage(MultipartFile imageFile, Long merchantId) throws IOException {
        ProfileImageDto profileImageDto;
        ProfileImage profileImageGetData = profileImageDao.getProfileImageById(merchantId);
        ProfileImage profileImageResData;
        String imageUrl;
        if (null == profileImageGetData) {
            // Cloudinary image upload start
            imageUrl = uploadProfileImage(imageFile, merchantId);
            // Cloudinary image upload end
            ProfileImage profileImage = new ProfileImage();
            profileImage.setProfilePic(imageUrl);
            profileImage.setMerchantId(merchantId);
            profileImageResData = profileImageDao.saveProfileImage(profileImage);
            profileImageDto = setProfileImageResponse(profileImageResData);
        } else {
            // Cloudinary image upload start
            imageUrl = uploadProfileImage(imageFile, merchantId);
            // Cloudinary image upload end
            ProfileImage profileImage = new ProfileImage();
            profileImage.setProfilePic(imageUrl);
            profileImage.setMerchantId(merchantId);
            profileImageResData = profileImageDao.updateProfileImage(profileImage, profileImageGetData.getImageId());
            profileImageDto = setProfileImageResponse(profileImageResData);
            profileImageDto.setResponseMessage("Image Update success");
        }
        return profileImageDto;
    }

    private String uploadProfileImage(MultipartFile imageFile, Long merchantId) throws IOException {
        File file = multipartToFile(imageFile);
        Cloudinary cloudinary = new Cloudinary(MerchantConstants.CLOUDINARY_URL);
        String filePublicId = String.valueOf(merchantId);
        Map params = ObjectUtils.asMap("public_id", filePublicId);
        Map uploadResult = cloudinary.uploader().upload(file, params);
        return (String) uploadResult.get("url");
    }

    @Override
    public ProfileImageDto getProfileImage(Long merchantId) {
        ProfileImage profileImage = profileImageDao.getProfileImageById(merchantId);
        ProfileImageDto profileImageDto = new ProfileImageDto();
        if (null != profileImage) {
            profileImageDto = setProfileImageResponse(profileImage);
            profileImageDto.setResponseMessage("Image get success");
        } else {
            profileImageDto.setResponseCode(MerchantConstants.FAILED_RESPONSE_CODE);
            profileImageDto.setResponseMessage("Image get failed, because it is not available");
        }
        return profileImageDto;
    }

    private ProfileImageDto setProfileImageResponse(ProfileImage profileImageData) {
        ProfileImageDto profileImageDto = new ProfileImageDto();
        // String encodedProfileImage = new String(Base64.getEncoder().encode(profileImageData.getProfilePic()));
        profileImageDto.setImageId(profileImageData.getImageId());
        profileImageDto.setProfilePic(profileImageData.getProfilePic());
        profileImageDto.setMerchantId(profileImageData.getMerchantId());
        profileImageDto.setResponseCode(MerchantConstants.SUCCESS_RESPONSE_CODE);
        profileImageDto.setResponseMessage("Image save success");
        return profileImageDto;
    }

    public File multipartToFile(MultipartFile multipart) throws IllegalStateException, IOException {
        File convFile = new File(System.getProperty("java.io.tmpdir") + "/" + multipart.getOriginalFilename());
        multipart.transferTo(convFile);
        return convFile;
    }

}
