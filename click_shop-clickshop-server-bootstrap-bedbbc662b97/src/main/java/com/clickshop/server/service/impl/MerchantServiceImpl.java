package com.clickshop.server.service.impl;

import com.clickshop.server.constants.MerchantConstants;
import com.clickshop.server.constants.SecurityConstants;
import com.clickshop.server.dao.MerchantDao;
import com.clickshop.server.dao.WalletDao;
import com.clickshop.server.dto.MerchantRequestDto;
import com.clickshop.server.dto.MerchantResponseDto;
import com.clickshop.server.dto.MerchantSignoutResponseDto;
import com.clickshop.server.endpoint.ClickshopMerchantEndPoint;
import com.clickshop.server.entity.*;
import com.clickshop.server.service.MerchantService;
import com.clickshop.server.utils.PasswordUtility;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by Dell on 7/18/2018.
 */
@Component
public class MerchantServiceImpl implements MerchantService {

    public static final Logger LOGGER = LoggerFactory.getLogger(MerchantServiceImpl.class);

    @Autowired
    private MerchantDao merchantDao;

    @Autowired
    private WalletDao walletDao;

    @Override
    public MerchantResponseDto signinMerchant(MerchantRequestDto merchantRequestDto) {
        Merchant merchantData = merchantDao.findMerchantByMobileNumber(merchantRequestDto.getMobileNumber());
        MerchantResponseDto merchantResponseDto;
        if (null != merchantData) {
            String sessionId = getSessionId(merchantRequestDto.getMobileNumber(), merchantData.getMerchantId());
            if ("-1".equals(sessionId)) {
                MerchantUser merchantUser = merchantDao.findMerchantUserByMobileNumber(merchantData.getMobileNumber(), merchantData.getMerchantId());
                String newSessionId = String.valueOf(System.nanoTime());
                int result = merchantDao.updateSessionId(merchantUser.getMerchantUserId(), newSessionId);
                if (1 == result){
                    LOGGER.info("Session updated successfully!!!");
                }
            }
            MerchantUser merchantUser = merchantDao.findMerchantUserByMobileNumber(merchantData.getMobileNumber(), merchantData.getMerchantId());
            boolean verifyMerchantPassword = PasswordUtility.verifyPassword(merchantRequestDto.getPassword(), merchantUser.getPassword(),
                    SecurityConstants.PASSWORD_ENCRYPTION_SALT);
            if (verifyMerchantPassword) {
                String loginSessionId = merchantDao.getSessionIdByMerchantUserId(merchantUser.getMerchantUserId());
                merchantResponseDto = buildLoginSuccessResponse(merchantData);
                merchantResponseDto.setSessionId(loginSessionId);
                merchantDao.updateFirebaseToken(merchantRequestDto.getFirebaseToken(), merchantData.getMerchantId());
            } else {
                merchantResponseDto = buildWrongPasswordResponse();
            }
        } else {
            merchantResponseDto = buildLoginFailedResponse();
        }
        return merchantResponseDto;
    }

    @Override
    public MerchantResponseDto signupMerchant(MerchantRequestDto merchantRequestDto) {
        Merchant merchantData = merchantDao.findMerchantByMobileNumber(merchantRequestDto.getMobileNumber());
        MerchantResponseDto merchantResponseDto = new MerchantResponseDto();
        if (null == merchantData) {
            Merchant merchant = setMerchantRequestData(merchantRequestDto);
            Merchant merchantSavedData = merchantDao.saveMerchant(merchant);
            if (null != merchantSavedData) {
                //Setup merchant wallet
                MerchantWallet merchantWalletCheck = walletDao.loadMerchantWalletAmount(merchantSavedData.getMerchantId());
                if(null == merchantWalletCheck){
                    MerchantWallet merchantWallet = prepareWalletInsertData(merchantSavedData.getMerchantId());
                    MerchantWallet merchantWalletSaved = walletDao.insertMerchantWalletAmount(merchantWallet);
                    //Enter merchant wallet details
                    if(null != merchantWalletSaved){
                        MerchantWalletDetails merchantWalletDetails = setWalletDetailsRequest(merchantWalletSaved);
                        walletDao.insertMerchantWalletDetails(merchantWalletDetails);
                    }
                }
                // MerchantUser
                MerchantUser merchantUser = setMerchantUserRequestData(merchantRequestDto, merchantSavedData.getMerchantId());
                MerchantUser merchantUserData = merchantDao.saveMerchantUser(merchantUser);
                if (null != merchantUserData) {
                    // MerchantDevice
                    MerchantDevice merchantDevice = setMerchantDeviceRequestData(merchantRequestDto, merchantUserData.getMerchantUserId());
                    MerchantDevice merchantDeviceData = merchantDao.saveMerchantDevice(merchantDevice);
                    if (null != merchantDeviceData) {
                        // MerchantLoginHistory
                        MerchantLoginHistory merchantLoginHistory = setMerchantLoginHistoryRequestData(merchantRequestDto, merchantUserData.getMerchantUserId(), merchantDeviceData.getMerchantDeviceId());
                        MerchantLoginHistory merchantLoginHistoryData = merchantDao.saveMerchantLoginHistory(merchantLoginHistory);
                        merchantResponseDto = buildLoginRegistrationSuccessResponse(merchantSavedData);
                        merchantResponseDto.setSessionId(merchantLoginHistoryData.getSessionId());
                    }

                }
            }
            /*String secureMerchantPassword = PasswordUtility.generateSecurePassword(merchant.getPassword(), SecurityConstants.PASSWORD_ENCRYPTION_SALT);
            merchant.setPassword(secureMerchantPassword);
            Merchant merchantReturnData = merchantDao.saveMerchant(merchant);
            merchantResponseDto = buildLoginRegistrationSuccessResponse(merchantReturnData);*/
        } else {
            merchantResponseDto = buildLoginRegistrationFailedResponse();
        }
        return merchantResponseDto;
    }

    private MerchantWalletDetails setWalletDetailsRequest(MerchantWallet merchantWalletSaved) {
        MerchantWalletDetails merchantWalletDetails = new MerchantWalletDetails();
        MerchantWallet merchantWallet = new MerchantWallet();
        merchantWallet.setMerchantWalletId(merchantWalletSaved.getMerchantWalletId());
        merchantWalletDetails.setMerchantWallet(merchantWallet);
        merchantWalletDetails.setAmount(merchantWalletSaved.getTotalPoint());
        merchantWalletDetails.setModeOfPayment("Online");
        merchantWalletDetails.setApproved("1");
        merchantWalletDetails.setApprovedBy("1");
        return merchantWalletDetails;
    }

    @Override
    public MerchantSignoutResponseDto signoutMerchant(MerchantRequestDto merchantRequestDto) {
        MerchantSignoutResponseDto merchantSignoutResponseDto = new MerchantSignoutResponseDto();
        Merchant merchant = merchantDao.findMerchantByMobileNumber(merchantRequestDto.getMobileNumber());
        if (null != merchant) {
            MerchantUser merchantUser = merchantDao.findMerchantUserByMobileNumber(merchantRequestDto.getMobileNumber(), merchant.getMerchantId());
            int result = merchantDao.updateSessionId(merchantUser.getMerchantUserId(), "-1");
            if (1 == result) {
                merchantSignoutResponseDto.setResponseCode(MerchantConstants.SUCCESS_RESPONSE_CODE);
                merchantSignoutResponseDto.setResponseMessage("Logging out...");
            } else {
                merchantSignoutResponseDto.setResponseCode(MerchantConstants.FAILED_RESPONSE_CODE);
                merchantSignoutResponseDto.setResponseMessage("Logging out Failed...");
            }
        }
        return merchantSignoutResponseDto;
    }

    private MerchantLoginHistory setMerchantLoginHistoryRequestData(MerchantRequestDto merchantRequestDto, Long merchantUserId, Long merchantDeviceId) {
        MerchantUser merchantUser = new MerchantUser();
        merchantUser.setMerchantUserId(merchantUserId);
        MerchantDevice merchantDevice = new MerchantDevice();
        merchantDevice.setMerchantDeviceId(merchantDeviceId);
        MerchantLoginHistory merchantLoginHistory = new MerchantLoginHistory();
        merchantLoginHistory.setMerchantUser(merchantUser);
        merchantLoginHistory.setMerchantDevice(merchantDevice);
        merchantLoginHistory.setLatitude(Double.valueOf(merchantRequestDto.getLatitude()));
        merchantLoginHistory.setLongitude(Double.valueOf(merchantRequestDto.getLongitude()));
        merchantLoginHistory.setSessionId(String.valueOf(System.nanoTime()));
        return merchantLoginHistory;
    }

    private MerchantDevice setMerchantDeviceRequestData(MerchantRequestDto merchantRequestDto, Long merchantUserId) {
        MerchantDevice merchantDevice = new MerchantDevice();
        MerchantUser merchantUser = new MerchantUser();
        merchantUser.setMerchantUserId(merchantUserId);
        merchantDevice.setMerchantUser(merchantUser);
        merchantDevice.setDeviceUniqueId(merchantRequestDto.getDeviceUniqueId());
        merchantDevice.setDeviceType(merchantRequestDto.getDeviceType());
        merchantDevice.setIsActive(1);
        merchantDevice.setIsBlocked(1);
        return merchantDevice;
    }

    private MerchantUser setMerchantUserRequestData(MerchantRequestDto merchantRequestDto, Long merchantId) {
        MerchantUser merchantUser = new MerchantUser();
        Merchant merchant = new Merchant();
        merchant.setMerchantId(merchantId);
        merchantUser.setMerchant(merchant);
        String secureMerchantPassword = PasswordUtility.generateSecurePassword(merchantRequestDto.getPassword(), SecurityConstants.PASSWORD_ENCRYPTION_SALT);
        merchantUser.setPassword(secureMerchantPassword);
        merchantUser.setMobileNumber(merchantRequestDto.getMobileNumber());
        merchantUser.setFirstName(merchantRequestDto.getFirstName());
        merchantUser.setLastName(merchantRequestDto.getLastName());
        merchantUser.setMobileVerified("1");
        merchantUser.setActive("1");
        merchantUser.setFirebaseToken(merchantRequestDto.getFirebaseToken());
        return merchantUser;
    }

    private Merchant setMerchantRequestData(MerchantRequestDto merchantRequestDto) {
        Merchant merchant = new Merchant();
        merchant.setMobileNumber(merchantRequestDto.getMobileNumber());
        merchant.setFirstName(merchantRequestDto.getFirstName());
        merchant.setLastName(merchantRequestDto.getLastName());
        merchant.setAddress(merchantRequestDto.getAddress());
        merchant.setGstNumber(merchantRequestDto.getGstNumber());
        merchant.setStoreNumber(merchantRequestDto.getStoreNumber());
        merchant.setEmail(merchantRequestDto.getEmail());
        merchant.setFlag(merchantRequestDto.getFlag());
        merchant.setFirebaseToken(merchantRequestDto.getFirebaseToken());
        return merchant;
    }

    private MerchantWallet prepareWalletInsertData(Long merchantId) {
        MerchantWallet merchantWallet = new MerchantWallet();
        Merchant merchant = new Merchant();
        merchant.setMerchantId(merchantId);
        merchantWallet.setMerchant(merchant);
        merchantWallet.setTotalPoint("500");
        merchantWallet.setTotalOrderAmount("0");
        merchantWallet.setBalancePoint("500");
        return merchantWallet;
    }

    @Override
    public String getMerchantFlag(Long merchantId) {
        return merchantDao.getMerchantFlag(merchantId);
    }

    private MerchantResponseDto buildLoginRegistrationFailedResponse() {
        MerchantResponseDto merchantResponseDto = new MerchantResponseDto();
        merchantResponseDto.setResponseCode(MerchantConstants.FAILED_RESPONSE_CODE);
        merchantResponseDto.setResponseMessage(MerchantConstants.LOGIN_REGISTRATION_ERROR_MESSAGE);
        return merchantResponseDto;
    }

    private MerchantResponseDto buildLoginRegistrationSuccessResponse(Merchant merchantData) {
        MerchantResponseDto merchantResponseDto = new MerchantResponseDto();
        merchantResponseDto.setMerchantId(merchantData.getMerchantId());
        merchantResponseDto.setMobileNumber(merchantData.getMobileNumber());
        merchantResponseDto.setAddress(merchantData.getAddress());
        merchantResponseDto.setGstNumber(merchantData.getGstNumber());
        merchantResponseDto.setStoreNumber(merchantData.getStoreNumber());
        merchantResponseDto.setEmail(merchantData.getEmail());
        merchantResponseDto.setFirstName(merchantData.getFirstName());
        merchantResponseDto.setLastName(merchantData.getLastName());
        merchantResponseDto.setFlag(merchantData.getFlag());
        merchantResponseDto.setResponseMessage(MerchantConstants.LOGIN_REGISTRATION_SUCCESS_MESSAGE);
        merchantResponseDto.setResponseCode(MerchantConstants.SUCCESS_RESPONSE_CODE);
        return merchantResponseDto;
    }

    private MerchantResponseDto buildLoginSuccessResponse(Merchant merchantData) {
        MerchantResponseDto merchantResponseDto = new MerchantResponseDto();
        merchantResponseDto.setResponseCode(MerchantConstants.SUCCESS_RESPONSE_CODE);
        merchantResponseDto.setResponseMessage(MerchantConstants.LOGIN_SUCCESS_MESSAGE);
        merchantResponseDto.setMerchantId(merchantData.getMerchantId());
        merchantResponseDto.setMobileNumber(merchantData.getMobileNumber());
        merchantResponseDto.setAddress(merchantData.getAddress());
        merchantResponseDto.setGstNumber(merchantData.getGstNumber());
        merchantResponseDto.setStoreNumber(merchantData.getStoreNumber());
        merchantResponseDto.setEmail(merchantData.getEmail());
        merchantResponseDto.setFirstName(merchantData.getFirstName());
        merchantResponseDto.setLastName(merchantData.getLastName());
        merchantResponseDto.setFlag(merchantData.getFlag());
        return merchantResponseDto;
    }

    private MerchantResponseDto buildLoginFailedResponse() {
        MerchantResponseDto merchantResponseDto = new MerchantResponseDto();
        merchantResponseDto.setResponseCode(MerchantConstants.FAILED_RESPONSE_CODE);
        merchantResponseDto.setResponseMessage(MerchantConstants.LOGIN_FAILED_MESSAGE);
        return merchantResponseDto;
    }

    private MerchantResponseDto buildWrongPasswordResponse() {
        MerchantResponseDto merchantResponseDto = new MerchantResponseDto();
        merchantResponseDto.setResponseCode(MerchantConstants.FAILED_RESPONSE_CODE);
        merchantResponseDto.setResponseMessage(MerchantConstants.LOGIN_PASSWORD_ERROR_MESSAGE);
        return merchantResponseDto;
    }

    public String getSessionId(String mobileNumber, Long merchantId) {
        MerchantUser merchantUser = merchantDao.findMerchantUserByMobileNumber(mobileNumber, merchantId);
        String sessionId = merchantDao.getSessionIdByMerchantUserId(merchantUser.getMerchantUserId());
        return sessionId;
    }
}
