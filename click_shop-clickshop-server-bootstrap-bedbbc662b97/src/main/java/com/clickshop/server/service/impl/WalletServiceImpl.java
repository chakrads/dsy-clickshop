package com.clickshop.server.service.impl;

import com.clickshop.server.constants.MerchantConstants;
import com.clickshop.server.dao.WalletDao;
import com.clickshop.server.dto.request.WalletRequestDto;
import com.clickshop.server.dto.response.WalletResponseDto;
import com.clickshop.server.entity.MerchantWallet;
import com.clickshop.server.entity.MerchantWalletDetails;
import com.clickshop.server.service.WalletService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class WalletServiceImpl implements WalletService {

    @Autowired
    private WalletDao walletDao;

    @Override
    public WalletResponseDto getMerchantWalletAmount(WalletRequestDto walletRequestDto) {
        WalletResponseDto walletResponseDto = new WalletResponseDto();
        MerchantWallet merchantWallet = walletDao.loadMerchantWalletAmount(walletRequestDto.getMerchantId());
        if (null != merchantWallet) {
            walletResponseDto = setMerchantWalletResponse(merchantWallet);
        } else {
            walletResponseDto.setResponseCode(MerchantConstants.FAILED_RESPONSE_CODE);
            walletResponseDto.setResponseMessage("Merchant wallet amount load failed");
        }
        return walletResponseDto;
    }

    @Override
    public WalletResponseDto updateMerchantWalletAmount(WalletRequestDto walletRequestDto) {
        WalletResponseDto walletResponseDto = new WalletResponseDto();
        MerchantWallet merchantWallet = walletDao.loadMerchantWalletAmount(walletRequestDto.getMerchantId());
        if (null != merchantWallet){
            int totalPoint = Integer.valueOf(merchantWallet.getTotalPoint()) + Integer.valueOf(walletRequestDto.getAmount());
            String totalAmountPoint = String.valueOf(totalPoint);
            double balancePoint = Double.valueOf(merchantWallet.getBalancePoint()) + Integer.valueOf(walletRequestDto.getAmount());
            String balanceAmountPoint = String.valueOf(balancePoint);
            int result = walletDao.updateMerchantWalletAmount(merchantWallet, walletRequestDto, totalAmountPoint, balanceAmountPoint);
            if(1 == result){
                MerchantWalletDetails merchantWalletDetails = setWalletDetailsRequest(merchantWallet, walletRequestDto.getAmount());
                walletDao.insertMerchantWalletDetails(merchantWalletDetails);
            }
            MerchantWallet updatedMerchantWalletData = walletDao.loadMerchantWalletAmount(walletRequestDto.getMerchantId());
            walletResponseDto = setMerchantWalletResponse(updatedMerchantWalletData);
        }else{
            walletResponseDto.setResponseCode(MerchantConstants.SUCCESS_RESPONSE_CODE);
            walletResponseDto.setResponseMessage("Merchant wallet amount load failed");
        }
        return walletResponseDto;
    }

    private WalletResponseDto setMerchantWalletResponse(MerchantWallet merchantWallet) {
        WalletResponseDto walletResponseDto = new WalletResponseDto();
        walletResponseDto.setResponseCode(MerchantConstants.SUCCESS_RESPONSE_CODE);
        walletResponseDto.setResponseMessage("Merchant wallet amount load success");
        walletResponseDto.setTotalPoint(merchantWallet.getTotalPoint());
        walletResponseDto.setBalancePoint(merchantWallet.getBalancePoint());
        walletResponseDto.setTotalOrderAmount(merchantWallet.getTotalOrderAmount());
        walletResponseDto.setMerchantId(merchantWallet.getMerchant().getMerchantId());
        return walletResponseDto;
    }

    private MerchantWalletDetails setWalletDetailsRequest(MerchantWallet merchantWalletSaved, String updatedAmountPoint) {
        MerchantWalletDetails merchantWalletDetails = new MerchantWalletDetails();
        MerchantWallet merchantWallet = new MerchantWallet();
        merchantWallet.setMerchantWalletId(merchantWalletSaved.getMerchantWalletId());
        merchantWalletDetails.setMerchantWallet(merchantWallet);
        merchantWalletDetails.setAmount(updatedAmountPoint);
        merchantWalletDetails.setModeOfPayment("Online");
        merchantWalletDetails.setApproved("1");
        merchantWalletDetails.setApprovedBy("1");
        return merchantWalletDetails;
    }
}
