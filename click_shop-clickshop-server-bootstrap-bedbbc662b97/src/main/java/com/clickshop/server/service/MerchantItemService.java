package com.clickshop.server.service;

import com.clickshop.server.dto.request.ItemsOfMerchantRequestDto;
import com.clickshop.server.dto.response.ItemsOfMerchantResponseDto;
import com.clickshop.server.dto.response.MerchantItemDeleteResponseDto;
import com.clickshop.server.dto.response.MerchantItemGetResponseDto;
import org.springframework.stereotype.Component;

import java.io.InputStream;

/**
 * Created by Dell on 7/22/2018.
 */
@Component
public interface MerchantItemService {

    public ItemsOfMerchantResponseDto saveItem(ItemsOfMerchantRequestDto itemsOfMerchantRequestDto);

    public MerchantItemDeleteResponseDto deleteItem(ItemsOfMerchantRequestDto itemsOfMerchantRequestDto);

    public ItemsOfMerchantResponseDto updateItem(ItemsOfMerchantRequestDto itemsOfMerchantRequestDto);

    public MerchantItemGetResponseDto getStoreItem(ItemsOfMerchantRequestDto itemsOfMerchantRequestDto);

    public boolean processStroreitem(InputStream fileInputStream) throws  Exception;
}
