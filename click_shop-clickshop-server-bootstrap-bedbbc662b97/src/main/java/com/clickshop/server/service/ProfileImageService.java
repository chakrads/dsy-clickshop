package com.clickshop.server.service;

import com.clickshop.server.dto.ProfileImageDto;
import com.clickshop.server.entity.ProfileImage;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Component
public interface ProfileImageService {

    public ProfileImageDto saveProfileImage(MultipartFile imageFile, Long merchantId) throws IOException;

    public ProfileImageDto getProfileImage(Long merchantId);

}
