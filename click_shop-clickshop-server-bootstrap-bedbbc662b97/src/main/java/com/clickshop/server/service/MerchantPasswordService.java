package com.clickshop.server.service;

import com.clickshop.server.dto.request.PasswordRequestDto;
import com.clickshop.server.dto.response.PasswordResponseDto;
import org.springframework.stereotype.Component;

@Component
public interface MerchantPasswordService {
   public PasswordResponseDto setNewPassword(PasswordRequestDto passwordRequestDto);

  public PasswordResponseDto resetPassword(PasswordRequestDto passwordRequestDto);
}
