package com.clickshop.server.service;

import com.clickshop.server.dto.request.ContactUsRequestDto;
import com.clickshop.server.dto.response.ContactUsResponseDto;
import org.springframework.stereotype.Component;

@Component
public interface ContactUsService {
   public ContactUsResponseDto saveContactUs(ContactUsRequestDto contactUsRequestDto);
}
