package com.clickshop.server.service.impl;

import com.clickshop.server.dao.ItemDao;
import com.clickshop.server.entity.Item;
import com.clickshop.server.entity.ItemCategory;
import com.clickshop.server.entity.ItemWeight;
import com.clickshop.server.service.ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dell on 7/24/2018.
 */
@Component
public class ItemServiceImpl implements ItemService {

    @Autowired
    private ItemDao itemDao;

    /*@Override
    public Item saveItem(Item item) {
        Item itemData = itemDao.saveItem(item);
        return itemData;
    }*/

    @Override
    public List<Item> getAllItems() {
        List<Item> listOfAllItems = itemDao.getAllItems();
        return listOfAllItems;
    }

    @Override
    public List<Item> getItemsByCategory(ItemCategory itemCategory) {
        return itemDao.getItemsByCategory(itemCategory);
    }

    @Override
    public List<ItemWeight> getItemsWeight() {
        return itemDao.getItemsWeight();
    }

    @Override
    public List<Item> getItemListRandom(String categoryName) {
        List<Item> items = itemDao.getItemListRandom(categoryName);
        return items;
    }

}
