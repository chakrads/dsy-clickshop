package com.clickshop.server.service;

import com.clickshop.server.dto.ItemCategoryDto;
import com.clickshop.server.entity.ItemCategory;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface ItemCategoryService {

    public List<ItemCategory> getItemsCategory();
    public ItemCategoryDto findItemCategoryRandom();

}
