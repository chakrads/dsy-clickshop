package com.clickshop.server.service;

import com.clickshop.server.dto.MerchantRequestDto;
import com.clickshop.server.dto.MerchantResponseDto;
import com.clickshop.server.dto.MerchantSignoutResponseDto;
import com.clickshop.server.entity.Merchant;
import org.springframework.stereotype.Component;

/**
 * Created by Dell on 7/18/2018.
 */
@Component
public interface MerchantService {

    public MerchantResponseDto signinMerchant(MerchantRequestDto merchantRequestDto);

    public MerchantResponseDto signupMerchant(MerchantRequestDto merchantRequestDto);

    public MerchantSignoutResponseDto signoutMerchant(MerchantRequestDto merchantRequestDto);

    public String getMerchantFlag(Long merchantId);
}
