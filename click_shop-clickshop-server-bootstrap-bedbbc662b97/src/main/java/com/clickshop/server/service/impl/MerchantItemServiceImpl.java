package com.clickshop.server.service.impl;

import com.clickshop.server.constants.MerchantConstants;
import com.clickshop.server.dao.*;
import com.clickshop.server.dto.request.ItemsOfMerchantRequestDto;
import com.clickshop.server.dto.response.ItemsOfMerchantResponseDto;
import com.clickshop.server.dto.response.MerchantItemDeleteResponseDto;
import com.clickshop.server.dto.response.MerchantItemGetResponseDto;
import com.clickshop.server.dto.response.StoreItemResponseDto;
import com.clickshop.server.entity.*;
import com.clickshop.server.service.MerchantItemService;
import com.clickshop.server.utils.FileUtility;
import com.clickshop.server.utils.Utility;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dell on 7/22/2018.
 */
@Component
public class MerchantItemServiceImpl implements MerchantItemService {

    private static final Logger LOGGER = LoggerFactory.getLogger(MerchantItemServiceImpl.class);
    @Autowired
    MerchantItemDao merchantItemDao;

    @Autowired
    ItemDao itemDao;

    @Autowired
    BrandedItemDao brandedItemDao;

    @Autowired
    MerchantDao merchantDao;

    @Autowired
    ItemCategoryDao itemCategoryDao;

    @Override
    public ItemsOfMerchantResponseDto saveItem(ItemsOfMerchantRequestDto itemsOfMerchantRequestDto) {

        ItemsOfMerchantResponseDto itemsOfMerchantResponseDto = new ItemsOfMerchantResponseDto();
        ItemsOfMerchant itemsOfMerchantItemData = merchantItemDao.getMerchantItemData(itemsOfMerchantRequestDto);
        if (null == itemsOfMerchantItemData) {
            ItemsOfMerchant itemsOfMerchant = getItemsOfMerchant(itemsOfMerchantRequestDto);
            ItemsOfMerchant itemsOfMerchantData = merchantItemDao.saveItem(itemsOfMerchant);
            itemsOfMerchantResponseDto = getItemsOfMerchantResponseDto(itemsOfMerchantData);
        } else {
            itemsOfMerchantResponseDto.setResponseCode(MerchantConstants.FAILED_RESPONSE_CODE);
            itemsOfMerchantResponseDto.setResponseMessage("Merchant Item already available in store");
        }
        return itemsOfMerchantResponseDto;
    }

    @Override
    public MerchantItemGetResponseDto getStoreItem(ItemsOfMerchantRequestDto itemsOfMerchantRequestDto) {

        MerchantItemGetResponseDto merchantItemGetResponseDto = new MerchantItemGetResponseDto();
        List<ItemsOfMerchant> itemsOfMerchantList = merchantItemDao.getStoreItemByMerchantId(itemsOfMerchantRequestDto);
        if (!itemsOfMerchantList.isEmpty()) {
            List<StoreItemResponseDto> itemList = new ArrayList<>();
            itemsOfMerchantList.forEach(itemsOfMerchant -> {
                StoreItemResponseDto storeItemResponseDto = new StoreItemResponseDto();
                //Item item = new Item();
                //Since Item Image is not available in ItemsOfMerchant
                storeItemResponseDto.setType(itemsOfMerchant.getType());
                if ("Brand".equals(itemsOfMerchant.getType())) {
                    BrandedItem brandedItem = brandedItemDao.getBrandedItemById(itemsOfMerchant.getItemId());
                    if (null != brandedItem) {
                        storeItemResponseDto.setItemName(brandedItem.getItemName());
                        storeItemResponseDto.setItemWeight(brandedItem.getItemWeight());
                        storeItemResponseDto.setItemWeightUnit(brandedItem.getItemWeightUnit());
                        storeItemResponseDto.setItemImage(brandedItem.getItemImage());
                        storeItemResponseDto.setItemMRP(brandedItem.getItemPrice());
                    }
                } else {
                    Item item = itemDao.getItemById(itemsOfMerchant.getItemId());
                    if (null != item) {
                        storeItemResponseDto.setItemName(item.getItemName());
                        storeItemResponseDto.setItemWeight(item.getItemWeight());
                        storeItemResponseDto.setItemWeightUnit(item.getItemWeightUnit());
                        storeItemResponseDto.setItemImage(item.getItemImage());
                        storeItemResponseDto.setItemMRP(item.getItemPrice());
                    }
                }

                /*Item currentItem = itemDao.getItemById(itemsOfMerchant.getItemId());
                storeItemResponseDto.setItemImage(currentItem.getItemImage());*/
                //end
                storeItemResponseDto.setItemId(itemsOfMerchant.getItemId());
                //storeItemResponseDto.setItemName(itemsOfMerchant.getItemName());
                storeItemResponseDto.setItemPrice(itemsOfMerchant.getItemPrice());
                //storeItemResponseDto.setItemWeight(itemsOfMerchant.getItemWeight());
                ItemCategory itemCategory = itemCategoryDao.getItemCategory(itemsOfMerchant.getItemCategory().getItemCategoryId());
                storeItemResponseDto.setItemCategory(itemCategory.getItemCategoryName());
                //storeItemResponseDto.setItemMRP(currentItem.getItemPrice());
                itemList.add(storeItemResponseDto);
            });
            merchantItemGetResponseDto.setItemList(itemList);
            merchantItemGetResponseDto.setResponseCode(MerchantConstants.SUCCESS_RESPONSE_CODE);
            merchantItemGetResponseDto.setResponseMessage("Merchant Store item get success");
        } else {
            merchantItemGetResponseDto.setResponseCode(MerchantConstants.FAILED_RESPONSE_CODE);
            merchantItemGetResponseDto.setResponseMessage("Merchant Store item get failed");
        }

        return merchantItemGetResponseDto;
    }

    @Override
    public MerchantItemDeleteResponseDto deleteItem(ItemsOfMerchantRequestDto itemsOfMerchantRequestDto) {
        MerchantItemDeleteResponseDto merchantItemDeleteResponse = new MerchantItemDeleteResponseDto();
        int deleteRecordCount = merchantItemDao.deleteMerchantItem(itemsOfMerchantRequestDto);
        if (deleteRecordCount == 1) {
            merchantItemDeleteResponse.setResponseCode(MerchantConstants.SUCCESS_RESPONSE_CODE);
            merchantItemDeleteResponse.setResponseMessage("Merchant Item deleted Successfully");
        } else {
            merchantItemDeleteResponse.setResponseCode(MerchantConstants.FAILED_RESPONSE_CODE);
            merchantItemDeleteResponse.setResponseMessage("Merchant Item delete failed");
        }
        return merchantItemDeleteResponse;
    }

    @Override
    public ItemsOfMerchantResponseDto updateItem(ItemsOfMerchantRequestDto itemsOfMerchantRequestDto) {

        ItemsOfMerchant itemsOfMerchantItemData = merchantItemDao.getMerchantItemData(itemsOfMerchantRequestDto);
        ItemsOfMerchant itemsOfMerchant;
        int updatedRecordCount;
        ItemsOfMerchantResponseDto itemsOfMerchantResponseDto = new ItemsOfMerchantResponseDto();
        String itemPrice = itemsOfMerchantRequestDto.getItemPrice();
        //String itemWeight = itemsOfMerchantRequestDto.getItemWeight();

        if (null != itemsOfMerchantItemData) {
            updatedRecordCount = merchantItemDao.updateMerchantItemData(itemsOfMerchantItemData, itemPrice/*, itemWeight*/);
            if (updatedRecordCount == 1) {
                //get updated item and return to app
                itemsOfMerchant = merchantItemDao.getMerchantItemData(itemsOfMerchantRequestDto);
                itemsOfMerchantResponseDto = getItemsOfMerchantResponseDto(itemsOfMerchant);
            } else {
                //some error
                itemsOfMerchantResponseDto.setResponseCode(MerchantConstants.FAILED_RESPONSE_CODE);
                itemsOfMerchantResponseDto.setResponseMessage("Error/Multiple item/s updated");
            }
        } else {
            itemsOfMerchantResponseDto.setResponseCode(MerchantConstants.FAILED_RESPONSE_CODE);
            itemsOfMerchantResponseDto.setResponseMessage("Merchant Item not found to update");
        }
        return itemsOfMerchantResponseDto;
    }

    private ItemsOfMerchantResponseDto getItemsOfMerchantResponseDto(ItemsOfMerchant itemsOfMerchantData) {
        ItemsOfMerchantResponseDto itemsOfMerchantResponseDto = new ItemsOfMerchantResponseDto();
        itemsOfMerchantResponseDto.setItemMerchantId(itemsOfMerchantData.getItemMerchantId());
        itemsOfMerchantResponseDto.setItemId(itemsOfMerchantData.getItemId());
        itemsOfMerchantResponseDto.setResponseCode(MerchantConstants.SUCCESS_RESPONSE_CODE);
        itemsOfMerchantResponseDto.setResponseMessage("Merchant item saved/updated successfully");
        if ("Brand".equals(itemsOfMerchantData.getType())) {
            BrandedItem brandedItem = brandedItemDao.getBrandedItemById(itemsOfMerchantData.getItemId());
            if (null != brandedItem) {
                itemsOfMerchantResponseDto.setItemName(itemsOfMerchantData.getItemName());
                itemsOfMerchantResponseDto.setItemWeight(brandedItem.getItemWeight());
            }
        } else {
            Item item = itemDao.getItemById(itemsOfMerchantData.getItemId());
            if (null != item) {
                itemsOfMerchantResponseDto.setItemName(itemsOfMerchantData.getItemName());
                itemsOfMerchantResponseDto.setItemWeight(item.getItemWeight());
            }
        }
        itemsOfMerchantResponseDto.setItemPrice(itemsOfMerchantData.getItemPrice());
        ItemCategory itemCategory = itemCategoryDao.getItemCategory(itemsOfMerchantData.getItemCategory().getItemCategoryId());
        itemsOfMerchantResponseDto.setItemCategory(itemCategory.getItemCategoryName());
        Merchant merchant = merchantDao.getMerchantById(itemsOfMerchantData.getMerchant());
        itemsOfMerchantResponseDto.setMerchantId(merchant.getMerchantId());
        itemsOfMerchantResponseDto.setStoreNumber(merchant.getStoreNumber());
        //itemsOfMerchantResponseDto.setLastUpdatedOn(String.valueOf(itemsOfMerchantData.getLastUpdatedOn()));
        itemsOfMerchantResponseDto.setLastUpdatedBy(itemsOfMerchantData.getLastUpdatedBy());
        itemsOfMerchantResponseDto.setCreatedOn(itemsOfMerchantData.getCreatedOn());
        itemsOfMerchantResponseDto.setCreatedBy(itemsOfMerchantData.getCreatedBy());
        itemsOfMerchantResponseDto.setType(itemsOfMerchantData.getType());
        return itemsOfMerchantResponseDto;
    }

    private ItemsOfMerchant getItemsOfMerchant(ItemsOfMerchantRequestDto itemsOfMerchantRequestDto) {
        ItemsOfMerchant itemsOfMerchant = new ItemsOfMerchant();
        Merchant merchant = new Merchant();
        merchant.setMerchantId(itemsOfMerchantRequestDto.getMerchantId());
        itemsOfMerchant.setMerchant(merchant);
        /*Item item = new Item();
        item.setItemId(itemsOfMerchantRequestDto.getItemId());*/
        itemsOfMerchant.setItemId(itemsOfMerchantRequestDto.getItemId());
        itemsOfMerchant.setCreatedBy(String.valueOf(itemsOfMerchantRequestDto.getMerchantId()));
        itemsOfMerchant.setCreatedOn(Utility.getCurrentDateTime());
        itemsOfMerchant.setLastUpdatedBy(String.valueOf(itemsOfMerchantRequestDto.getMerchantId()));
        //itemsOfMerchant.setLastUpdatedOn(Utility.getCurrentDateTime());
        itemsOfMerchant.setItemName(itemsOfMerchantRequestDto.getItemName());
        itemsOfMerchant.setItemPrice(itemsOfMerchantRequestDto.getItemPrice());
        //itemsOfMerchant.setItemWeight(itemsOfMerchantRequestDto.getItemWeight());
        ItemCategory itemCategory = new ItemCategory();
        itemCategory.setItemCategoryId(itemsOfMerchantRequestDto.getItemCategoryId());
        itemsOfMerchant.setItemCategory(itemCategory);
        itemsOfMerchant.setType(itemsOfMerchantRequestDto.getType());
        return itemsOfMerchant;
    }

    @Override
    public boolean processStroreitem(InputStream fileInputStream) throws Exception {
        boolean status=false;
        List<Item> items = FileUtility.readExcel(fileInputStream);
        int saveItems = itemDao.saveItems(items);
        if(saveItems >0 ) {
            status=true;
        }
        return status;
    }
}
