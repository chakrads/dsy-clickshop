DELIMITER $$
CREATE TRIGGER after_order_status_update
    AFTER UPDATE ON clickshop.t_order
    FOR EACH ROW
BEGIN

    UPDATE clickshop.t_merchant_wallet SET total_order_amount = (OLD.order_amount + total_order_amount),
    balance_point=(balance_point-(0.01*OLD.order_amount))
    where merchant_id=OLD.merchant_id and NEW.order_status='Finished';

END;


-- Related queries
-- drop trigger after_order_status_update;
-- Any updation in trigger, first drop the trigger and then re-create using updated script
-- Trigger will happen only when update/insert happen