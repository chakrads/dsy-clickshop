package com.click.clickshopuser.model

class AddToCart {
    var mId : Int? = 0;
    var mItem_id : Int? = 0;
    var mItem_merchant_id : Int? = 0;
    var mItem_Count : Int? = 0;
    var mItem_Name : String? = null
    var mItem_Catagory: String? = null
    var mItem_Image: String? = null
    var mItem_Price: String? = null
    var mItem_Weight: String? = null
    var mItem_individual_total_amount: Int? = 0;
}