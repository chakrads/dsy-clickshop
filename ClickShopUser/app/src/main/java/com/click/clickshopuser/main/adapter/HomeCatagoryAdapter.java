package com.click.clickshopuser.main.adapter;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.android.volley.toolbox.ImageLoader;
import com.bumptech.glide.Glide;
import com.click.clickshopuser.R;
import com.click.clickshopuser.main.activity.CatagoryItems;
import com.click.clickshopuser.model.Datum;

import java.util.List;

public class HomeCatagoryAdapter extends
        RecyclerView.Adapter<HomeCatagoryAdapter.ViewHolder> {
    private static final String TAG = HomeCatagoryAdapter.class.getSimpleName();
    private Context context;
    private List<Datum> listInAss;
    public ImageView networkImageView;
    public ImageLoader imageLoader;

    public HomeCatagoryAdapter(List<Datum> listInAss, Context context) {
        super();
        this.listInAss = listInAss;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.home_catagory_adapter, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public int getItemCount() {
        return listInAss.size();
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final Datum homeCatagory = listInAss.get(position);
        String image = homeCatagory.getItemCategoryImage();

        Glide.with(context)
                .load(image)
                .override(150, 150)
                .into(networkImageView);



        holder.textViewCatName.setText(homeCatagory.getItemCategoryName());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String mCatagory = homeCatagory.getItemCategoryName();
                Intent intent = new Intent(context, CatagoryItems.class);
                intent.putExtra("itemCategory", mCatagory);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                context.startActivity(intent);
            }
        });


    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private TextView textViewCatName;
        private ViewHolder(View itemView) {
            super(itemView);
            networkImageView = itemView.findViewById(R.id._cat_image);
            textViewCatName = itemView.findViewById(R.id._catagory_name);
            this.setIsRecyclable(false);
        }
    }


}