package com.click.clickshopuser.prelanding;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.click.clickshopuser.R;
import com.click.clickshopuser.appconfig.AppUtils;
import com.click.clickshopuser.appconfig.EndPoint;
import com.click.clickshopuser.network.NetworkUtils;
import com.click.clickshopuser.pushnotification.SharedPrefManager;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.HashMap;


public class RegistrationActivity extends AppCompatActivity implements View.OnClickListener {
    private EditText editTextPhoneNo, editTextPassword, editTextEmail, editTextFirstName,
            editTextLastName, editTextReferralCode, editTextConfirmPassword;
    private Button buttonLogin;
    private AlertDialog alert;
    private static final String TAG = RegistrationActivity.class.getSimpleName();
    private SharedPrefManager sharedPrefManager;
    private String mPass;
    private String mPassConfirm;
    private TelephonyManager telephonyManager;
    private String deviceId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        sharedPrefManager = SharedPrefManager.getInstance(getApplicationContext());
        editTextReferralCode = findViewById(R.id._reffereal_code);
        editTextFirstName = findViewById(R.id._first_name);
        editTextLastName = findViewById(R.id._last_name);
        editTextPhoneNo = findViewById(R.id.phone_no);
        editTextEmail = findViewById(R.id.email);
        editTextPassword = findViewById(R.id.registration_password);
        editTextConfirmPassword = findViewById(R.id.registration_confirt_password);
        buttonLogin = findViewById(R.id.btnLogin);
        buttonLogin.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnLogin:
                mPass = editTextPassword.getText().toString().trim();
                mPassConfirm = editTextConfirmPassword.getText().toString().trim();
                String mEmail = editTextEmail.getText().toString().trim();
                String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
                if (editTextFirstName.getText().length() == 0) {
                    Message("Enter First Name");
                } else if (editTextLastName.getText().length() == 0) {
                    Message("Enter First Name");
                } else if (editTextPhoneNo.getText().length() == 0) {
                    Message("Enter Phone No");
                } else if (editTextPhoneNo.getText().length() < 10) {
                    Message("Enter A Valid Phone No");
                } else if (editTextEmail.getText().length() == 0) {
                    Message("Enter E-mail");
                } else if (!mEmail.matches(emailPattern)) {
                    Message("Enter Valid E-mail");
                } else if (editTextPassword.getText().length() == 0) {
                    Message("Enter Password");
                } else if (editTextConfirmPassword.getText().toString().length() == 0) {
                    Message("Confirm Password");
                } else if (!mPass.equals(mPassConfirm)) {
                    Message("Password Do Not Match");
                } else {
                    if (AppUtils.internetConnectionAvailable(2000)) {
                        submitRegistration();
                    } else {
                        Message("No Internet Connection");
                    }

                }
                break;
        }
    }


    public void submitRegistration() {

        telephonyManager = (TelephonyManager) getSystemService(Context.
                TELEPHONY_SERVICE);
        try {
            deviceId = telephonyManager.getDeviceId();
        }catch (SecurityException e){ e.printStackTrace(); }




        String DeviceName = android.os.Build.DEVICE  ;         // Device
        String DeviceModel = android.os.Build.MODEL ;           // Mode
        String mDeviceType = String.valueOf(new StringBuilder("Device Name : ").append(DeviceName)
                .append("Device Model : ").append(DeviceModel));
        String mFirstName = editTextFirstName.getText().toString().trim();
        String mLastName = editTextLastName.getText().toString().trim();
        String mPhoneNo = editTextPhoneNo.getText().toString().trim();
        String mEmail = editTextEmail.getText().toString().trim();
        String mPassword = editTextPassword.getText().toString().trim();
        String mReferralCode = editTextReferralCode.getText().toString().trim();
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("mobileNumber", mPhoneNo);
        params.put("email", mEmail);
        params.put("password", mPassword);
        params.put("firstName", mFirstName);
        params.put("lastName", mLastName);
        params.put("referralCode", mReferralCode);
        String mFireBaseToken = sharedPrefManager.getDeviceToken();
        params.put("firebaseToken", mFireBaseToken);
        params.put("deviceUniqueId", deviceId);
        params.put("deviceType", mDeviceType);
        params.put("latitude", "0.0");
        params.put("longitude", "0.0");



        Log.d(TAG, "Registrationparams: " + params);
        JsonObjectRequest request_json = new JsonObjectRequest(EndPoint.CLICK_SHOP_USER_SIGN_UP,
                new JSONObject(params),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, "Slot Response: " + response);
                        try {
                            Integer mStatusCode = response.getInt("responseCode");
                            if (mStatusCode == 400) {
                                String mStatusMessage = response.getString("responseMessage");
                                Message(mStatusMessage);
                            } else if (mStatusCode == 200) {
                                Intent intentSuccess = new Intent(
                                        RegistrationActivity.this,
                                        LoginActivity.class);
                                startActivity(intentSuccess);
                                finish();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                try {
                    NetworkUtils.UnableToReachServer(getApplicationContext());
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(request_json);
    }


    public void Message(String message) {
        LayoutInflater layoutInflater = LayoutInflater.from(this);
        View promptView = layoutInflater.inflate(R.layout.alert_message, null);
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setView(promptView);
        TextView textViewMessage = promptView.findViewById(R.id.alert_message_display);
        textViewMessage.setText(message);
        Button buttonblock = (Button) promptView.findViewById(R.id.alert_ok_buton);
        buttonblock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alert.dismiss();
            }
        });
        alert = alertDialogBuilder.create();
        alert.show();
    }


}
