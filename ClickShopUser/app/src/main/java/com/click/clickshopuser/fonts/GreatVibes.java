package com.click.clickshopuser.fonts;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;

import com.click.clickshopuser.volley.AppController;

public class GreatVibes extends AppCompatTextView {

  public GreatVibes(Context context, AttributeSet attrs, int defStyle) {
    super(context, attrs, defStyle);
    init();
  }

  public GreatVibes(Context context, AttributeSet attrs) {
    super(context, attrs);
    init();
  }

  public GreatVibes(Context context) {
    super(context);
    init();
  }

  private void init() {
    if (!isInEditMode()) {
      Typeface custom_font = Typeface.createFromAsset(AppController.getInstance().getAssets(), "GreatVibes-Regular.otf");
      setTypeface(custom_font);
    }
  }
}
