package com.click.clickshopuser.model

class NearByShopFrontPage {
    var storeId : Int? = 0;
    var merchant_Id: Int? = null
    var delivery_duration : String? = null
    var workging_hour_from: String? = null
    var working_hour_to : String? = null
    var store_latitude : String? = null
    var store_longitude : String? = null
    var delivery_range : String? = null
    var address : String? = null
    var city : String? = null
    var pincode : String? = null
    var store_image : String? = null
    var delivery_time : String? = null
    var storeName : String? = null
}