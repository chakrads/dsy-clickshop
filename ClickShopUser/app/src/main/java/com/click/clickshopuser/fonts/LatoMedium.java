package com.click.clickshopuser.fonts;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;

import com.click.clickshopuser.volley.AppController;


public class LatoMedium extends AppCompatTextView {

  public LatoMedium(Context context, AttributeSet attrs, int defStyle) {
    super(context, attrs, defStyle);
    init();
  }

  public LatoMedium(Context context, AttributeSet attrs) {
    super(context, attrs);
    init();
  }

  public LatoMedium(Context context) {
    super(context);
    init();
  }

  private void init() {
    if (!isInEditMode()) {
      Typeface custom_font = Typeface.createFromAsset(AppController.getInstance().getAssets(), "Lato-Medium.ttf");
      setTypeface(custom_font);
    }
  }
}

