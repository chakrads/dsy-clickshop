package com.click.clickshopuser.main.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.click.clickshopuser.R;
import com.click.clickshopuser.main.activity.OrderItemActivity;
import com.click.clickshopuser.model.ProgressPo;


import java.util.List;

public class OrderCardAdapter extends RecyclerView.Adapter<OrderCardAdapter.ViewHolder> {

    private static final String TAG = OrderCardAdapter.class.getSimpleName();
    Context context;
    List<ProgressPo> listInAss;
    String mOrderId;


    public OrderCardAdapter(List<ProgressPo> listInAss, Context context) {
        super();
        this.listInAss = listInAss;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_adapter_progress, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public int getItemCount() {
        return listInAss.size();
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final ProgressPo progressPo = listInAss.get(position);
        holder.textViewOrderId.setText(progressPo.getOrderId());
        holder.textViewAddress.setText(progressPo.getShippingAddress());
        holder.textViewPaymentMode.setText(progressPo.getPaymentMode());
        holder._orderDate.setText(progressPo.getOrderTime());
        holder._store_name.setText(progressPo.getStoreName());
        holder._store_number.setText(progressPo.getStoreMobileNumber());
        holder.textViewTotalAmount.setText(new StringBuilder("Rs. ").append(progressPo.getOrderAmount()));
        holder.delivery_charge.setText(new StringBuilder("Rs. ").append(progressPo.getDeliveryCharge()));


        String mOrderStatus = progressPo.getOrderStatus();

        if (mOrderStatus.equalsIgnoreCase("New")) {
            holder.textViewOrderStatus.setText("Waiting For Approval");
            holder.textViewOrderStatus.setTextColor(Color.parseColor("#1B5E20"));
        } else if (mOrderStatus.equalsIgnoreCase("Progress")) {
            holder.textViewOrderStatus.setText("Order in Progress");
            holder.textViewOrderStatus.setTextColor(Color.parseColor("#FF6F00"));
        } else if (mOrderStatus.equalsIgnoreCase("Finished")) {
            holder.textViewOrderStatus.setText("Delivered");
            holder.textViewOrderStatus.setTextColor(Color.parseColor("#3E2723"));
        } else if (mOrderStatus.equalsIgnoreCase("Archived")) {
            holder.textViewOrderStatus.setText("Order History");
            holder.textViewOrderStatus.setTextColor(Color.parseColor("#0091EA"));
        } else if (mOrderStatus.equalsIgnoreCase("Rejected")) {
            holder.textViewOrderStatus.setText("Rejected by Merchant");
            holder.textViewOrderStatus.setTextColor(Color.parseColor("#D50000"));
        }


        holder.buttonShowItems.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mOrderId = progressPo.getOrderId();

                Intent intent = new Intent(context, OrderItemActivity.class);
                intent.putExtra("OrderItems", mOrderId);
                context.startActivity(intent);


            }
        });


    }


    class ViewHolder extends RecyclerView.ViewHolder {
        TextView textViewOrderId;
        TextView textViewAddress;
        TextView textViewPaymentMode;
        TextView textViewTotalAmount;
        TextView textViewOrderStatus;
        TextView delivery_charge;
        TextView _orderDate;
        TextView _store_name;
        TextView _store_number;

        Button buttonShowItems;

        private ViewHolder(View itemView) {
            super(itemView);
            textViewOrderId = itemView.findViewById(R.id._order_id);
            textViewAddress = itemView.findViewById(R.id._deliver_to);
            textViewPaymentMode = itemView.findViewById(R.id._payment_mode);
            textViewTotalAmount = itemView.findViewById(R.id._total_amount);
            buttonShowItems = itemView.findViewById(R.id._show_items);
            textViewOrderStatus = itemView.findViewById(R.id._order_status);
            delivery_charge = itemView.findViewById(R.id.delivery_charge);
            _orderDate = itemView.findViewById(R.id._orderDate);
            _store_name = itemView.findViewById(R.id._store_name);
            _store_number = itemView.findViewById(R.id._store_number);

            this.setIsRecyclable(false);
        }
    }


}
