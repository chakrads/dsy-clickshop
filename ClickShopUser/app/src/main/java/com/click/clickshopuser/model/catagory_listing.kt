package com.click.clickshopuser.model

class catagory_listing {
    var mCatagoryId : Int? = 0;
    var itemName : String? = null
    var itemImage : String? = null
    var itemPrice : String? = null
    var itemWeight : String? = null
    var itemCategory : String? = null
    var merchantId : String? = null
    var itemId : String? = null
}


// This model is probably not used anywhere. Please check before deleting.