package com.click.clickshopuser.model

class SearchProductCardModel {
    var mStoreId : Int? = 0;
    var mLattitude : String? = null
    var mLongitude: String? = null
    var mDeliveryDuration: String? = null
    var mStoreRatings: String? = null
    var mStoreName: String? = null
    var mItemName: String? = null
    var mItemImage: String? = null
    var mItemPrice: String? = null
    var mItemWeight: String? = null
    var mMerchantId: Int? = 0;
    var mItemId: Int? = 0;
    var mStoreDistance: String? = null
    var mCatagory: String? = null
    var storeitemarray: String? = null
}