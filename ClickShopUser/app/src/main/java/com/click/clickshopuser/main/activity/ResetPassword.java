package com.click.clickshopuser.main.activity;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.click.clickshopuser.R;
import com.click.clickshopuser.appconfig.ApiClientUser;
import com.click.clickshopuser.appconfig.ApiInterface;
import com.click.clickshopuser.appconfig.AppUtils;
import com.click.clickshopuser.appconfig.SessionManager;
import com.click.clickshopuser.model.resetpassword.RequestBodyResetPassword;
import com.click.clickshopuser.model.resetpassword.ResetPasswords;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;

public class ResetPassword extends AppCompatActivity {
    Toolbar toolbar;
    EditText editTextExistingPassword, editTextNewPassword, editTextConfirmPassword;
    Button buttonSubmit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);
        setSupportActionBar(toolbar);

        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }

        toolbar = findViewById(R.id.reset_toolbar);
        setSupportActionBar(toolbar);
        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        getSupportActionBar().setDisplayShowHomeEnabled(false);
        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_arrow_back_black_24dp));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        editTextExistingPassword = findViewById(R.id._enter_exisitng_password);
        editTextNewPassword = findViewById(R.id._enter_new_password);
        editTextConfirmPassword = findViewById(R.id._enter_confirm_pas);
        buttonSubmit = findViewById(R.id.btn_submit);
        buttonSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editTextExistingPassword.getText().length() == 0) {
                    Toast.makeText(getApplicationContext(), "Enter Existing Password",
                            Toast.LENGTH_SHORT).show();
                } else if (editTextNewPassword.getText().length() == 0) {
                    Toast.makeText(getApplicationContext(), "Enter New Password",
                            Toast.LENGTH_SHORT).show();
                } else if (editTextConfirmPassword.getText().length() == 0) {
                    Toast.makeText(getApplicationContext(), "Enter Confirm Password",
                            Toast.LENGTH_SHORT).show();
                } else {
                    if (AppUtils.internetConnectionAvailable(2000)) {
                        resetPassword();
                    } else {
                        Toast.makeText(getApplicationContext(), "No Network Connection",
                                Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });






    }




    public void resetPassword() {
        String mExistingPassword = editTextExistingPassword.getText().toString();
        String mNewPassword = editTextNewPassword.getText().toString();
        String mConfirmPassword = editTextConfirmPassword.getText().toString();
        SessionManager sessionManager = new SessionManager(getApplicationContext());
        final HashMap<String, String> hashMap = sessionManager.getUserDetails();
        String mSessionId = hashMap.get(SessionManager.KEY_SESSION_ID);
        String mPhoneNo = hashMap.get(SessionManager.KEY_PHONE_NUMBER);
        String mDeviceId = hashMap.get(SessionManager.KEY_DEVICE_ID);


        final ApiInterface apiInterfacec = ApiClientUser.getApiClient().create(ApiInterface.class);
        Call<ResetPasswords> call = apiInterfacec.resetPassword(new RequestBodyResetPassword(mPhoneNo, mSessionId, mExistingPassword, mNewPassword, mConfirmPassword, mDeviceId));
        call.enqueue(new Callback<ResetPasswords>() {
            @Override
            public void onResponse(Call<ResetPasswords> call, final retrofit2.Response<ResetPasswords> response) {
                if (response.isSuccessful()) {
                    if (response.body().getResponseCode() == 200) {


                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getApplicationContext());
                        alertDialogBuilder.setMessage("Password Updated Successfully");



                        AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();


                    } else {


                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getApplicationContext());
                        alertDialogBuilder.setMessage("Password Update Failed");


                        AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();

                    }
                } else {
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getApplicationContext());
                    alertDialogBuilder.setMessage("Password Update Failed");



                    AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.show();

                }
            }
            @Override
            public void onFailure(Call<ResetPasswords> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.toString(),
                        Toast.LENGTH_SHORT).show();
            }
        });


    }





}
