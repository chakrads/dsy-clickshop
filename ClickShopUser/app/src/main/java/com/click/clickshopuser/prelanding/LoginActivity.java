package com.click.clickshopuser.prelanding;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.click.clickshopuser.R;
import com.click.clickshopuser.appconfig.AppUtils;
import com.click.clickshopuser.appconfig.Constants;
import com.click.clickshopuser.appconfig.EndPoint;
import com.click.clickshopuser.appconfig.SessionManager;
import com.click.clickshopuser.main.activity.ForgotPassword;
import com.click.clickshopuser.main.activity.MainActivity;
import com.click.clickshopuser.model.AddToCart;
import com.click.clickshopuser.network.ConnectivityDetector;
import com.click.clickshopuser.pushnotification.SharedPrefManager;
import com.click.clickshopuser.storage.DbHandler;
import com.click.clickshopuser.storage.Preferences;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    EditText editTextPhoneNo, editTextPassword;
    Button buttonLogin;
    Button buttonRegister, buttonForgotPassword;
    AlertDialog alert;
    private ProgressDialog progress;
    private static final String TAG = LoginActivity.class.getSimpleName();
    SharedPrefManager sharedPrefManager;
    ConnectivityDetector connectivityDetector;
    private TelephonyManager telephonyManager;
    private String deviceId;
    DbHandler dbHandler;
    JSONArray array;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        dbHandler = DbHandler.getInstance(getApplicationContext());
        sharedPrefManager = SharedPrefManager.getInstance(getApplicationContext());
        editTextPhoneNo = findViewById(R.id.phone_no);
        editTextPassword = findViewById(R.id.password);
        buttonLogin = findViewById(R.id.btnLogin);
        buttonRegister = findViewById(R.id.btnLinkToRegisterScreen);
        buttonForgotPassword = findViewById(R.id.forgot_password);
        buttonLogin.setOnClickListener(this);
        buttonRegister.setOnClickListener(this);
        buttonForgotPassword.setOnClickListener(this);
        connectivityDetector = ConnectivityDetector.getInstance();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnLogin:
                if (editTextPhoneNo.getText().length() == 0) {
                    Message("Enter Phone No");
                } else if (editTextPhoneNo.getText().length() < 10) {
                    Message("Enter A Valid Phone No");
                } else if (editTextPassword.getText().length() == 0) {
                    Message("Enter Password");
                } else {
                    if (AppUtils.internetConnectionAvailable(2000)) {
                        submitLogin();
                    } else {
                        Message("No Internet Connection");
                    }
                }
                break;
            case R.id.btnLinkToRegisterScreen:
                startActivity(new Intent(LoginActivity.this, RegistrationActivity.class));
                break;
            case R.id.forgot_password:
             Intent intentFor = new Intent(LoginActivity.this, ForgotPassword.class);
             startActivity(intentFor);
                break;
        }
    }



    public void submitLogin() {
        progress = ProgressDialog.show(this, Constants.PROGRESS_SIGNING_IN, Constants.PLEASE_WAIT, false, false);
        String mPhoneNo = editTextPhoneNo.getText().toString().trim();
        String mPassword = editTextPassword.getText().toString().trim();

        telephonyManager = (TelephonyManager)getSystemService(Context.
                TELEPHONY_SERVICE);
        try {
            deviceId = telephonyManager.getDeviceId();
        }catch (SecurityException e){ e.printStackTrace(); }


        HashMap<String, String> params = new HashMap<String, String>();
        params.put(Constants.POST_PARAMETER_MOBILE_NUMBER, mPhoneNo);
        params.put(Constants.POST_PARAMETER_PASS, mPassword);
        String mDeviceToken = sharedPrefManager.getDeviceToken();
        params.put(Constants.FIREBASE_TOKEN, mDeviceToken);
        params.put(Constants.DEVICE_UNIQUE_ID, deviceId);
        params.put(Constants.DEVICE_TYPE, "1");  // 1 for Android


        JsonObjectRequest request_json = new JsonObjectRequest(EndPoint.CLICK_SHOP_USER_SIGN_IN, new JSONObject(params),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, "LoginResponse " + response);
                        progress.dismiss();
                        try {
                            Integer mStatusCode = response.getInt(Constants.CLICK_SHOP_RESPONSE_CODE);
                            if (mStatusCode == 400) {
                                Message("User Does Not Exist. Please Register And Retry");
                            } else if (mStatusCode == 200) {
                                String mUserId = response.getString(Constants.CLICK_SHOP_USER_ID);
                                String mPhoneNumber = response.getString(Constants.CLICK_SHOP_MOBILE_NO);
                                String mFirstName = response.getString(Constants.CLICK_SHOP_FIRST_NAME);
                                String mLastName = response.getString(Constants.CLICK_SHOP_LAST_NAME);
                                String mSessionId = response.getString(Constants.SESSION_ID);
                                String mDeviceId = response.getString(Constants.RESPONSE_DEVICE_ID);
                                SessionManager sessionManager = new SessionManager(getApplicationContext());
                                sessionManager.createLoginSession(mUserId, mPhoneNumber,
                                        mFirstName, mLastName, mDeviceId, mSessionId);

                                if (dbHandler.checkIfTableIsEmpty() == 1){
                                    if (AppUtils.internetConnectionAvailable(2000)) {
                                        UpdateCartItems();
                                    } else {
                                        Toast.makeText(getApplicationContext(),
                                                "No Network Connection",
                                                Toast.LENGTH_SHORT).show();
                                    }
                                } else {
                                    Intent intent = new Intent(LoginActivity.this,
                                            MainActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK |
                                            Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(intent);
                                    Toast.makeText(getApplicationContext(),
                                            "Login Successful", Toast.LENGTH_SHORT).show();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.d("LoginErrorResponse",String.valueOf(error.toString()));
                Log.d("LoginErrorResponse",String.valueOf(error.getMessage()));
                Log.d("LoginErrorResponse", String.valueOf(error.getStackTrace()));

                try {
                    progress.dismiss();
                } catch (NullPointerException e) {
                    Toast.makeText(getApplicationContext(), "Login Failed",
                            Toast.LENGTH_LONG).show();
                } catch (IllegalStateException e) {
                    Toast.makeText(getApplicationContext(), "Login Failed",
                            Toast.LENGTH_LONG).show();
                }
            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(request_json);



    }




    public void UpdateCartItems() {
        progress = ProgressDialog.show(this, "", "Please wait...",
                true);
        String mStoreIds = Preferences.getDefaults("postStoreId", getApplicationContext());

        SessionManager sessionManager = new SessionManager(getApplicationContext());
        HashMap<String, String> hashMap = sessionManager.getUserDetails();
        String mUserid = hashMap.get(SessionManager.KEY_USER_ID);
        String mDeviceId = hashMap.get(SessionManager.KEY_DEVICE_ID);
        String mSessionId = hashMap.get(SessionManager.KEY_SESSION_ID);


        try {
            array = new JSONArray();
            List<AddToCart> cartDetails = dbHandler.getCartDetails();
            for (AddToCart addToCart : cartDetails) {
                String mPrice = addToCart.getMItem_Price();
                String mQuantityCount = String.valueOf(addToCart.getMItem_Count());
                String mItemMerchantId = String.valueOf(addToCart.getMItem_merchant_id());
                Integer mTotalPrice = addToCart.getMItem_individual_total_amount();
                JSONObject obj = new JSONObject();
                try {
                    obj.put("itemMerchantId", mItemMerchantId);
                    obj.put("itemPrice", mPrice);
                    obj.put("itemQuantity", mQuantityCount);
                    obj.put("totalPrice", String.valueOf(mTotalPrice));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                array.put(obj);
            }
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            JSONObject jsonBody = new JSONObject();
            jsonBody.put("userId", mUserid);
            jsonBody.put("storeId", mStoreIds);
            jsonBody.put("deviceUniqueId", mDeviceId);
            jsonBody.put("sessionId", mSessionId);
            jsonBody.put("userCartItemList", array);
            final String requestBody = jsonBody.toString();
            StringRequest stringRequest = new StringRequest(Request.Method.POST, EndPoint.SAVE_USER_CART, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    progress.dismiss();
                    Log.d(TAG, "UpdateCartResponse: " + response);
                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    Toast.makeText(getApplicationContext(), "Login Successful", Toast.LENGTH_SHORT).show();

                    // Sending a message to MainActivity to Refresh The Cart Count in Bottom bar
                    Intent intents = new Intent("filter_string");
                    intents.putExtra("key", "value");
                    LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progress.dismiss();
                    Log.e("VOLLEY", error.toString());
                    Intent intent = new Intent(LoginActivity.this,
                            MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK |
                            Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    Toast.makeText(getApplicationContext(),
                            "Login Successful", Toast.LENGTH_SHORT).show();
                }
            }) {
                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                    headers.put("Content-Type", "application/json; charset=utf-8");
                    return headers;
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return requestBody == null ? null : requestBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                        return null;
                    }
                }

                @Override
                protected Response<String> parseNetworkResponse(NetworkResponse response) {
                    String responseString = "";
                    if (response != null) {
                        responseString = String.valueOf(response.statusCode);
                        // can get more details such as response.headers
                    }
                    return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
                }
            };

            requestQueue.add(stringRequest);
        } catch (JSONException e) {
            progress.dismiss();
            e.printStackTrace();
        }
    }

























    public void Message(String message) {
        LayoutInflater layoutInflater = LayoutInflater.from(this);
        View promptView = layoutInflater.inflate(R.layout.alert_message, null);
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setView(promptView);
        TextView textViewMessage = promptView.findViewById(R.id.alert_message_display);
        textViewMessage.setText(message);
        Button buttonblock = (Button) promptView.findViewById(R.id.alert_ok_buton);
        buttonblock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alert.dismiss();
            }
        });
        alert = alertDialogBuilder.create();
        alert.show();
    }


}
