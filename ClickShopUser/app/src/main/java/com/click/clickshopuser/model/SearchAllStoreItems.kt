package com.click.clickshopuser.model

class SearchAllStoreItems {
    var itemId : Int? = 0;
    var itemMerchantId : Int? = 0;
    var merchantId: Int? = null
    var itemName : String? = null
    var itemImage: String? = null
    var itemPrice : String? = null
    var itemWeight : String? = null
    var itemCategory : String? = null
    var itemCount : Int? = 0;
    var mCheckDbCount : String? = null
}