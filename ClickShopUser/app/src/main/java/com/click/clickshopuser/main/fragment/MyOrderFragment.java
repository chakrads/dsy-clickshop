package com.click.clickshopuser.main.fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.click.clickshopuser.R;
import com.click.clickshopuser.appconfig.AppUtils;
import com.click.clickshopuser.appconfig.Constants;
import com.click.clickshopuser.appconfig.EndPoint;
import com.click.clickshopuser.appconfig.SessionManager;
import com.click.clickshopuser.main.adapter.OrderCardAdapter;
import com.click.clickshopuser.model.ProgressPo;
import com.click.clickshopuser.network.NetworkUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;


public class MyOrderFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener{
    private static final String TAG = MyOrderFragment.class.getSimpleName();
    public RelativeLayout relativeLayoutLoading;
    public SwipeRefreshLayout swipeRefreshLayout;
    public RecyclerView recyclerView;
    public List<ProgressPo> orderItems;
    public OrderCardAdapter adapter;
    public LinearLayoutManager linearLayoutManager;
    public MyOrderFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.activity_my_orders, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        relativeLayoutLoading = getActivity().findViewById(R.id._loading_my_order);
        swipeRefreshLayout = getActivity().findViewById(R.id.myorder_refresh);
        swipeRefreshLayout.setOnRefreshListener(this);
        recyclerView = getActivity().findViewById(R.id._recyclerview_myorder);
        linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(linearLayoutManager);
        orderItems = new ArrayList<ProgressPo>();

        if (AppUtils.internetConnectionAvailable(2000)) {
            getMyOrders();
        } else {
            Toast.makeText(getActivity(), "No Network Connection",
                    Toast.LENGTH_SHORT).show();
        }

        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onRefresh() {
        if (orderItems != null) {
            orderItems.clear();
        }
        relativeLayoutLoading.setVisibility(View.GONE);
        if (AppUtils.internetConnectionAvailable(2000)) {
            getMyOrders();
        } else {
            Toast.makeText(getActivity(), "No Network Connection",
                    Toast.LENGTH_SHORT).show();
        }
    }


    public void getMyOrders() {
        SessionManager sessionManager = new SessionManager(getActivity());
        HashMap<String, String> hashMap = sessionManager.getUserDetails();
        String mUserid = hashMap.get(SessionManager.KEY_USER_ID);
        String mDeviceId = hashMap.get(SessionManager.KEY_DEVICE_ID);
        String mSessionId = hashMap.get(SessionManager.KEY_SESSION_ID);



        HashMap<String, String> params = new HashMap<String, String>();
        params.put("userId", mUserid);
        params.put("sessionId", mSessionId);
        params.put("deviceUniqueId", mDeviceId);

        JsonObjectRequest request_json = new JsonObjectRequest(EndPoint.MY_ORDER_DISPLAY, new JSONObject(params),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            int mStatusCode = response.getInt("responseCode");
                            if (mStatusCode == 200) {
                                relativeLayoutLoading.setVisibility(View.GONE);
                                recyclerView.setVisibility(View.VISIBLE);

                                JSONArray array = response.optJSONArray("orderList");
                                parseData(array);

                            } else {
                                relativeLayoutLoading.setVisibility(View.GONE);
                                swipeRefreshLayout.setRefreshing(false);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkUtils.UnableToReachServer(getActivity());
            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(request_json);
    }


    private void parseData(JSONArray array) {
        Log.d("JsonOrderResponse",array.toString());
        for (int i = 0; i < array.length(); i++) {
            ProgressPo progressPo = new ProgressPo();
            JSONObject json;
            try {
                json = array.getJSONObject(i);
                if (json.getString("orderId") != null && !json.getString("orderId").isEmpty()
                        && !json.getString("orderId").equals(Constants.NULL)) {
                    progressPo.setOrderId(json.getString("orderId"));
                }

                if (json.getString("userId") != null && !json.getString("userId").isEmpty()
                        && !json.getString("userId").equals(Constants.NULL)) {
                    progressPo.setUserId((json.getInt("userId")));
                } else {
                    progressPo.setUserId(0);
                }

                if (json.getString("shippingAddress") != null && !json.getString("shippingAddress").isEmpty()
                        && !json.getString("shippingAddress").equals(Constants.NULL)) {
                    progressPo.setShippingAddress((json.getString("shippingAddress")));
                } else {
                    progressPo.setShippingAddress("None");
                }

                if (json.getString("paymentMode") != null && !json.getString("paymentMode").isEmpty()
                        && !json.getString("paymentMode").equals(Constants.NULL)) {
                    progressPo.setPaymentMode((json.getString("paymentMode")));
                } else {
                    progressPo.setPaymentMode("None");
                }

                if (json.getString("orderAmount") != null && !json.getString("orderAmount").isEmpty()
                        && !json.getString("orderAmount").equals(Constants.NULL)) {
                    progressPo.setOrderAmount((json.getString("orderAmount")));
                } else {
                    progressPo.setOrderAmount("Nill");
                }

                if (json.getString("deliveryCharge") != null && !json.getString("deliveryCharge").isEmpty()
                        && !json.getString("deliveryCharge").equals(Constants.NULL)) {
                    progressPo.setDeliveryCharge((json.getString("deliveryCharge")));
                } else {
                    progressPo.setOrderAmount("Nill");
                }

                if (json.getString("storeName") != null && !json.getString("storeName").isEmpty()
                        && !json.getString("storeName").equals(Constants.NULL)) {
                    progressPo.setStoreName((json.getString("storeName")));
                } else {
                    progressPo.setOrderAmount("Nill");
                }

                if (json.getString("storeMobileNumber") != null && !json.getString("storeMobileNumber").isEmpty()
                        && !json.getString("storeMobileNumber").equals(Constants.NULL)) {
                    progressPo.setStoreMobileNumber((json.getString("storeMobileNumber")));
                } else {
                    progressPo.setOrderAmount("Nill");
                }


                if (json.getString("orderTime") != null && !json.getString("orderTime").isEmpty()
                        && !json.getString("orderTime").equals(Constants.NULL)) {
                    progressPo.setOrderTime((json.getString("orderTime")));
                } else {
                    progressPo.setOrderAmount("Nill");
                }




                if (json.getString("merchantId") != null && !json.getString("merchantId").isEmpty()
                        && !json.getString("merchantId").equals(Constants.NULL)) {
                    progressPo.setMerchantId((json.getInt("merchantId")));
                } else {
                    progressPo.setMerchantId(0);
                }

                if (json.getString("orderStatus") != null && !json.getString("orderStatus").isEmpty()
                        && !json.getString("orderStatus").equals(Constants.NULL)) {
                    progressPo.setOrderStatus((json.getString("orderStatus")));
                } else {
                    progressPo.setOrderStatus("None");
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }
            orderItems.add(progressPo);
            // Collections.sort(recordsList);
            Collections.reverse(orderItems);
        }

        adapter = new OrderCardAdapter(orderItems, getActivity());
        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        swipeRefreshLayout.setRefreshing(false);
    }


}
