package com.click.clickshopuser.main.adapter;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.android.volley.toolbox.ImageLoader;
import com.bumptech.glide.Glide;
import com.click.clickshopuser.R;
import com.click.clickshopuser.appconfig.Constants;
import com.click.clickshopuser.main.activity.CatagoryItems;
import com.click.clickshopuser.model.HomeCatagory;

import java.util.List;

public class CatagoryItemActivityCardAdapter extends
        RecyclerView.Adapter<CatagoryItemActivityCardAdapter.ViewHolder> {
    private static final String TAG = CatagoryItemActivityCardAdapter.class.getSimpleName();
    Context context;
    List<HomeCatagory> listInAss;
    public ImageView networkImageView;
    public ImageLoader imageLoader;
    View v;

    public CatagoryItemActivityCardAdapter(List<HomeCatagory> listInAss, Context context) {
        super();
        this.listInAss = listInAss;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.catagory_item_activity_card_adapter, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public int getItemCount() {
        return listInAss.size();
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final HomeCatagory homeCatagory = listInAss.get(position);
        String image = homeCatagory.getMCatagoryImage();



        Glide.with(context)
                .load(image)
                .override(200, 200)
                .into(networkImageView);


        holder.textViewCatName.setText(homeCatagory.getMCatagoryName());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String mCatagory = homeCatagory.getMCatagoryName();
                Intent intent = new Intent(context, CatagoryItems.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                intent.putExtra(Constants.PARAM_ITEM_CATAGORY, mCatagory);
                context.startActivity(intent);
                ((AppCompatActivity)v.getContext()).finish();
            }
        });
    }


    class ViewHolder extends RecyclerView.ViewHolder {
        private TextView textViewCatName;

        private ViewHolder(View itemView) {
            super(itemView);
            networkImageView = itemView.findViewById(R.id._cat_image);
            textViewCatName = itemView.findViewById(R.id._catagory_name);
            this.setIsRecyclable(false);
        }
    }
}