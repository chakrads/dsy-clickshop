
package com.click.clickshopuser.model.categoryitemmodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ItemDataList {

    @SerializedName("itemName")
    @Expose
    private String itemName;
    @SerializedName("itemImage")
    @Expose
    private String itemImage;
    @SerializedName("itemCategory")
    @Expose
    private String itemCategory;

    @SerializedName("itemMerchantId")
    @Expose
    private String itemMerchantId;



    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getItemImage() {
        return itemImage;
    }

    public void setItemImage(String itemImage) {
        this.itemImage = itemImage;
    }

    public String getItemCategory() {
        return itemCategory;
    }

    public void setItemCategory(String itemCategory) {
        this.itemCategory = itemCategory;
    }

    public String getItemMerchantId() {
        return itemMerchantId;
    }

    public void setItemMerchantId(String itemMerchantId) {
        this.itemMerchantId = itemMerchantId;
    }




}
