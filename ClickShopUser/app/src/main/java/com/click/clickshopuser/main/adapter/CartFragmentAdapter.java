package com.click.clickshopuser.main.adapter;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.Volley;
import com.click.clickshopuser.R;
import com.click.clickshopuser.appconfig.AppUtils;
import com.click.clickshopuser.appconfig.EndPoint;
import com.click.clickshopuser.appconfig.SessionManager;
import com.click.clickshopuser.inter.CartRefresh;
import com.click.clickshopuser.model.CartAdapterModel;
import com.click.clickshopuser.network.NetworkUtils;
import com.click.clickshopuser.storage.DbHandler;
import com.click.clickshopuser.storage.Preferences;
import com.click.clickshopuser.volley.CustomVolleyRequest;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.HashMap;
import java.util.List;

public class CartFragmentAdapter extends
        RecyclerView.Adapter<CartFragmentAdapter.ViewHolder> {
    NetworkImageView networkImageView;
    ImageLoader imageLoader;
    static DbHandler dbHandler;
    private ProgressDialog progress;

    private static final String TAG = CartFragmentAdapter.class.getSimpleName();
    Context context;
    List<CartAdapterModel> listInAss;
    CartRefresh cartRefresh;
    private AlertDialog alert;
    int mItemForDeleting;
    String mItemMerchantId;
    View v;

    public CartFragmentAdapter(List<CartAdapterModel> listInAss, Context context, CartRefresh cartRefresh) {
        super();
        this.listInAss = listInAss;
        this.context = context;
        this.cartRefresh = cartRefresh;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cart_fragment_adapter, parent, false);

        dbHandler = DbHandler.getInstance(context);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public int getItemCount() {
        return listInAss.size();
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final CartAdapterModel cartAdapterModel = listInAss.get(position);
        final String image = modifyDropboxUrl(cartAdapterModel.getMItemImage());



        imageLoader = CustomVolleyRequest.getInstance(context).getImageLoader();
        imageLoader.get(image, ImageLoader.getImageListener(networkImageView, R.drawable.noimage,
                R.drawable.noimage));
        networkImageView.setImageUrl(image, imageLoader);
        holder.textViewItemName.setText(cartAdapterModel.getMItemName());
        holder.textViewItemQuantity.setText(new StringBuilder("Quanity :  ").append(cartAdapterModel.getMItemQuantity()).append(" Nos"));
        holder.textViewItemPrice.setText(new StringBuilder("MRP Rs :").append(cartAdapterModel.getMItemPrice()).append(" / ").append(cartAdapterModel.getMItemWeight()));
        holder.textViewItemWeight.setText(cartAdapterModel.getMItemWeight());
        Integer mAmt = Integer.valueOf(cartAdapterModel.getMItemPrice());
        Integer mQty = Integer.valueOf(cartAdapterModel.getMItemQuantity());


        Integer mToAmot = mAmt * mQty;
        holder.textViewAmount.setText(new StringBuilder("Rs : ").append(mToAmot).append("/-"));
        holder.imageViewDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                mItemMerchantId = String.valueOf(cartAdapterModel.getMItemMerchantId());
                mItemForDeleting = cartAdapterModel.getMItemId();
                Message("Are You Sure of Deleting Item in Your Cart ?");
            }
        });
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView textViewItemName;
        TextView textViewItemPrice;
        TextView textViewItemWeight;
        TextView textViewItemQuantity;
        ImageView imageViewDelete;
        TextView textViewAmount;

        private ViewHolder(View itemView) {
            super(itemView);
            networkImageView = itemView.findViewById(R.id._cart_item_image);
            textViewItemName = itemView.findViewById(R.id._cart_item_name);
            textViewItemPrice = itemView.findViewById(R.id._cart_itme_price);
            textViewItemWeight = itemView.findViewById(R.id._cart_itme_weight);
            textViewItemQuantity = itemView.findViewById(R.id._cart_itme_quanity);
            imageViewDelete = itemView.findViewById(R.id._ic_delete);
            textViewAmount = itemView.findViewById(R.id._total_amount);
            this.setIsRecyclable(false);
        }
    }

    public void Message(String message) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View promptView = layoutInflater.inflate(R.layout.alert_message_yes_no, null);
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setView(promptView);
        TextView textViewMessage = promptView.findViewById(R.id.alert_message_display);
        textViewMessage.setText(message);
        Button buttonblock = promptView.findViewById(R.id.alert_ok_buton);
        buttonblock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                SessionManager sessionManager = new SessionManager(context);
                if (sessionManager.isLoggedIn()) {
                    if (AppUtils.internetConnectionAvailable(2000)) {
                        deleteItemInServer(mItemMerchantId, mItemForDeleting);
                    } else {
                        Toast.makeText(context, "No Network Connection",
                                Toast.LENGTH_SHORT).show();
                    }
                } else {
                    dbHandler.deleteCartRow(mItemForDeleting);
                    cartRefresh.callRefresh();
                    Toast.makeText(context, "Deleted Successfully",
                            Toast.LENGTH_SHORT).show();
                }

                alert.dismiss();
            }
        });

        Button cancel = promptView.findViewById(R.id.alert_cancel_button);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alert.dismiss();
            }
        });
        alert = alertDialogBuilder.create();
        alert.show();
    }





    public static String modifyDropboxUrl(String originalUrl) {
        String newUrl = originalUrl.replace("www.dropbox.", "dl.dropboxusercontent.");
        newUrl = newUrl.replace("dropbox.", "dl.dropboxusercontent.");
        return newUrl;
    }




    private void deleteItemInServer(String mItemMerchantId, final Integer mItemfordelete) {
        progress = ProgressDialog.show(v.getContext(), "",
                "Please wait...", false, false);
        SessionManager sessionManager = new SessionManager(context);
        HashMap<String, String> hashMap = sessionManager.getUserDetails();
        String mUserid = hashMap.get(SessionManager.KEY_USER_ID);
        String mDeviceId = hashMap.get(SessionManager.KEY_DEVICE_ID);
        String mSessionId = hashMap.get(SessionManager.KEY_SESSION_ID);

        HashMap<String, String> params = new HashMap<String, String>();
        params.put("userId", mUserid);
        params.put("storeId", Preferences.getDefaults("postStoreId", context));
        params.put("itemMerchantId", mItemMerchantId);
        params.put("deviceUniqueId", mDeviceId);
        params.put("sessionId", mSessionId);

        Log.d(TAG, "selectStoreId: " + params);


        JsonObjectRequest request_json = new JsonObjectRequest(EndPoint.DELETE_CART_ITEM, new JSONObject(params),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, "selectStoreIdRESPONSE: " + response);
                        progress.dismiss();

                        try {
                            Integer mStatusCode = response.getInt("responseCode");
                            if (mStatusCode == 200) {

                                if (dbHandler.getCartCount() == 1){
                                    dbHandler.deleteCartItem();
                                } else {
                                    dbHandler.deleteCartRow(mItemfordelete);
                                }
                                cartRefresh.callRefresh();
                                Toast.makeText(context, "Deleted Successfully",
                                        Toast.LENGTH_SHORT).show();

                            } else if (mStatusCode == 400) {
                                cartRefresh.callRefresh();


                                Toast.makeText(context, "No Item in Cart",
                                        Toast.LENGTH_SHORT).show();
                            }else if (mStatusCode == 402) {

                                dbHandler.deleteCartItem();

                                cartRefresh.callRefresh();
                               Message("The Cart is Cleared Since Another User Through your Login Entered Some Other Store. ");
                            }



                            // Sending a message to MainActivity to Refresh The Cart Count in Bottom bar
                            Intent intent = new Intent("filter_string");
                            intent.putExtra("key", "value");
                            LocalBroadcastManager.getInstance(context).sendBroadcast(intent);







                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progress.dismiss();
                NetworkUtils.UnableToReachServer(context);
            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(request_json);
    }














}