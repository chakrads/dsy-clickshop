package com.click.clickshopuser.network;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import com.click.clickshopuser.R;

/**
 * Created by Yashpal on 21-03-2018.
 */

public class NetworkUtils {
    private static AlertDialog alert;

    public static boolean isInternetAvailable(boolean showErrorMessage, Context context) {
        if (context != null) {
            ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
            boolean isNetworkAvailable = activeNetworkInfo != null && activeNetworkInfo.isConnected();
            if (isNetworkAvailable) {
                return true;
            } else {
                NoInternetConnection(context);
                return false;
            }
        }
        return false;
    }

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static void NoInternetConnection(Context context) {
        try {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View promptView = layoutInflater.inflate(R.layout.alert_no_internet_connection, null);
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setView(promptView);

        Button buttonok = promptView.findViewById(R.id.alert_ok_buton);
        buttonok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alert.dismiss();
            }
        });

        alert = alertDialogBuilder.create();
        alert.show();

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }



    public static void UnableToReachServer(Context context) {
        try {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View promptView = layoutInflater.inflate(R.layout.alert_unable_to_reach_server, null);
        final android.support.v7.app.AlertDialog.Builder alertDialogBuilder = new android.support.v7.app.AlertDialog.Builder(context);
        alertDialogBuilder.setView(promptView);

        Button buttonok = promptView.findViewById(R.id.alert_ok_buton);
        buttonok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alert.dismiss();
            }
        });

        alert = alertDialogBuilder.create();
        alert.show();

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }









}

