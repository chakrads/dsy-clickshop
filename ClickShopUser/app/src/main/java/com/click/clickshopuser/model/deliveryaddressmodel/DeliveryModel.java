
package com.click.clickshopuser.model.deliveryaddressmodel;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DeliveryModel {

    @SerializedName("responseCode")
    @Expose
    private Integer responseCode;
    @SerializedName("responseMessage")
    @Expose
    private String responseMessage;
    @SerializedName("userShippingAddressList")
    @Expose
    private List<UserShippingAddressList> userShippingAddressList = null;

    public Integer getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(Integer responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public List<UserShippingAddressList> getUserShippingAddressList() {
        return userShippingAddressList;
    }

    public void setUserShippingAddressList(List<UserShippingAddressList> userShippingAddressList) {
        this.userShippingAddressList = userShippingAddressList;
    }

}
