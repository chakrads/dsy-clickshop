package com.click.clickshopuser.main.fragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.click.clickshopuser.R;
import com.click.clickshopuser.appconfig.ApiClientMerchant;
import com.click.clickshopuser.appconfig.ApiInterface;
import com.click.clickshopuser.appconfig.AppUtils;
import com.click.clickshopuser.appconfig.EndPoint;
import com.click.clickshopuser.main.activity.CatagoryItemActivity;
import com.click.clickshopuser.main.activity.SearchProducts;
import com.click.clickshopuser.main.adapter.HomeCatagoryAdapter;
import com.click.clickshopuser.main.adapter.NearByShopAdapterFrontPage;
import com.click.clickshopuser.model.CatagoryListing;
import com.click.clickshopuser.model.Datum;
import com.click.clickshopuser.model.GeoLocation;
import com.click.clickshopuser.model.NearByShopFrontPage;
import com.click.clickshopuser.storage.DbHandler;
import com.click.clickshopuser.storage.Preferences;
import com.click.clickshopuser.volley.AppController;
import com.google.android.gms.maps.model.LatLng;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;

public class Home extends Fragment implements SwipeRefreshLayout.OnRefreshListener {
    TextView textViewSearchProduct;
    private static final String TAG = Home.class.getSimpleName();
    double latitude;
    double longitude;
    ImageView imgnostore;
    TextView txtnostore,txtnostore1;
    public RelativeLayout relativeLayoutLoading;
    public SwipeRefreshLayout swipeRefreshLayout;
    public RecyclerView recyclerView;
    public List<NearByShopFrontPage> storeItemList;
    public NearByShopAdapterFrontPage adapter;
    public LinearLayoutManager linearLayoutManager;
    private AlertDialog alertNew;
    String mReceivedLongitude;
    String mReceivedLatitude;
    LatLng mStart, mEnd;
    DbHandler dbHandler;
    RelativeLayout relativeLayoutLoadingCatagory;
    RecyclerView recyclerViewCatagory;
    HomeCatagoryAdapter adapterCatagory;
    LinearLayoutManager linearLayoutManagerCatagory;
    private BroadcastReceiver receiver;
    String mFetchLatitude, mFetchLongitude;
    TextView textViewViewAll;
    CardView cardView;
    RelativeLayout relativeLayoutNoConnection;
    ScrollView scrollView;
    private ApiInterface apiInterface;
    private List<Datum> datum;
    String currentVersion;


    public Home() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v= inflater.inflate(R.layout.fragment_home, container, false);


      //  Toast.makeText(getActivity(), "onCreateView", Toast.LENGTH_SHORT).show();

        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        dbHandler = DbHandler.getInstance(getActivity());
        textViewSearchProduct = getActivity().findViewById(R.id._search);
        textViewSearchProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), SearchProducts.class);
                intent.putExtra("itemName", "None"); // because the same page SearchProduct.class is used in CatagoryItemCardAdapter.class. SearchProduct is gettting navigation from two Acitivyt.
                startActivity(intent);
            }
        });

       // Toast.makeText(getActivity(), "onActivityCreated", Toast.LENGTH_SHORT).show();
        textViewViewAll = getActivity().findViewById(R.id._catagory_view_all);
        txtnostore=getActivity().findViewById(R.id.txtnostore);
        txtnostore1=getActivity().findViewById(R.id.txtnostore1);
        imgnostore=getActivity().findViewById(R.id.imgnostore);
        txtnostore.setVisibility(View.GONE);
        txtnostore1.setVisibility(View.GONE);
        txtnostore.setVisibility(View.GONE);
        imgnostore.setVisibility(View.GONE);
        cardView = getActivity().findViewById(R.id._no_store_available);
        cardView.setVisibility(View.GONE);
        relativeLayoutNoConnection = getActivity().findViewById(R.id._no_connection);
        relativeLayoutNoConnection.setVisibility(View.GONE);
        scrollView = getActivity().findViewById(R.id._scroll_view);
        relativeLayoutLoading = getActivity().findViewById(R.id._loading_store);
        swipeRefreshLayout = getActivity().findViewById(R.id.store_refresh);
        swipeRefreshLayout.setOnRefreshListener(this);
        recyclerView = getActivity().findViewById(R.id._recyclerview_store);
        linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        storeItemList = new ArrayList<>();


        relativeLayoutLoadingCatagory = getActivity().findViewById(R.id._loading_catagory);
        relativeLayoutLoadingCatagory.setVisibility(View.VISIBLE);
        recyclerViewCatagory = getActivity().findViewById(R.id._recyclerview_catagory);
        recyclerViewCatagory.setVisibility(View.GONE);
        linearLayoutManagerCatagory = new LinearLayoutManager(getActivity());
        recyclerViewCatagory.setLayoutManager(new GridLayoutManager(getActivity(), 3));
//        recyclerViewCatagory.getLayoutManager().setMeasurementCacheEnabled(false);



        // Not showing version check dialog beacuse this uses getpackage method which crashes in hawaie and mi

//        try {
//            currentVersion = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0).versionName;
//            Log.d(TAG, "currentVersion  : " + currentVersion);
//        } catch (PackageManager.NameNotFoundException e) {
//            e.printStackTrace();
//        }


        apiInterface = ApiClientMerchant.getApiClient().create(ApiInterface.class);
        if (AppUtils.internetConnectionAvailable(2000)) {
//            new GetVersionCode().execute();
            getCatagory();
        } else {
            Toast.makeText(getActivity(), "No Network Connection",
                    Toast.LENGTH_SHORT).show();
        }


        String mValue = Preferences.getDefaults("Navigation", getActivity());
        if (mValue.equalsIgnoreCase("1")) { // First Time Main Activity Loading
            fetchBradcast();
        } else if (mValue.equalsIgnoreCase("2")) { // Detecting Back Press Useful for Bottom Bar in Main Page.
            fetchPref();
        }

        fetchPref();

        textViewViewAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentCatagoryItem = new Intent(getActivity(), CatagoryItemActivity.class);
                startActivity(intentCatagoryItem);
            }
        });








        // Update cart count in MainActivity
        Intent intent = new Intent("filter_string");
        intent.putExtra("key", "value");
        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);


        super.onActivityCreated(savedInstanceState);
    }


    public void fetchBradcast() {
        relativeLayoutLoading.setVisibility(View.GONE);
        // This method is to detect the data coming from Main Activity
        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String mReceivedData = intent.getStringExtra("home");
                Log.d(TAG, "mReceivedData: " + mReceivedData);
                if (mReceivedData.equalsIgnoreCase("shop")) {
                    if (storeItemList != null) {
                        storeItemList.clear();
                    }
                    relativeLayoutLoading.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.GONE);
                    storeItemList.clear();
                    fetchPref();
                }

            }
        };
        // Registering the Local Broadcast Receiver
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(receiver, new IntentFilter("refresh"));
    }


    public void fetchPref() {
        List<GeoLocation> geoLocation = dbHandler.getGeoLocation();
        for (GeoLocation geoLocation1 : geoLocation) {
            mFetchLatitude = geoLocation1.getMLatitude();
            mFetchLongitude = geoLocation1.getMLongitude();
        }

        if (AppUtils.internetConnectionAvailable(2000)) {
            mFetchNearbyShop();
        } else {
            Toast.makeText(getActivity(), "No Network Connection",
                    Toast.LENGTH_SHORT).show();
        }


    }


    @Override
    public void onRefresh() {
        relativeLayoutLoading.setVisibility(View.GONE);
        storeItemList.clear();


        if (AppUtils.internetConnectionAvailable(2000)) {
            mFetchNearbyShop();
        } else {
            Toast.makeText(getActivity(), "No Network Connection",
                    Toast.LENGTH_SHORT).show();
        }

    }


    public void getCatagory() {
        Call<CatagoryListing> call = apiInterface.getCatagory();
        call.enqueue(new Callback<CatagoryListing>() {
            @Override
            public void onResponse(Call<CatagoryListing> call, retrofit2.Response<CatagoryListing> response) {
                if (response.isSuccessful()) {
                    relativeLayoutLoadingCatagory.setVisibility(View.GONE);
                    recyclerViewCatagory.setVisibility(View.VISIBLE);
                    if (response.body().getResponseCode() == 200) {
                        datum = response.body().getData();
                        adapterCatagory = new HomeCatagoryAdapter(datum, getActivity());
                        recyclerViewCatagory.setAdapter(adapterCatagory);
                    } else {
                        Toast.makeText(getActivity(), "No Data",
                                Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<CatagoryListing> call, Throwable t) {

            }
        });
    }


    public void mFetchNearbyShop() {
//        if (relativeLayoutUnableLayout.getVisibility() == View.VISIBLE) {
//            relativeLayoutUnableLayout.setVisibility(View.GONE);
//        }


        if (storeItemList != null) {
            storeItemList.clear();
        }


        HashMap<String, String> params = new HashMap<String, String>();
        params.put("latitude", mFetchLatitude);
        params.put("longitude", mFetchLongitude);


        Log.d(TAG, "ResponseNearByParmas: " + params);



        JsonObjectRequest request_json = new JsonObjectRequest(EndPoint.CLICK_SHOP_USER_FETCH_NEAR_BY_SHOP, new JSONObject(params),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, "ResponseNearBy: " + response);
                        Log.d(TAG, "ResponseNearBy: " + EndPoint.CLICK_SHOP_USER_FETCH_NEAR_BY_SHOP);
                        relativeLayoutLoading.setVisibility(View.GONE);
                        swipeRefreshLayout.setRefreshing(false);
                        recyclerView.setVisibility(View.VISIBLE);
                        cardView.setVisibility(View.GONE);
                        try {
                            Integer mStatusCode = response.getInt("responseCode");
                            if (mStatusCode == 200) {
                                JSONArray array = response.optJSONArray("storeList");
                                parseData(array);
                            } else if (mStatusCode == 400) {
                                cardView.setVisibility(View.VISIBLE);
                                recyclerView.setVisibility(View.GONE);
                                txtnostore.setVisibility(View.VISIBLE);
                                txtnostore1.setVisibility(View.VISIBLE);
                                imgnostore.setVisibility(View.VISIBLE);

                            } else {
                                cardView.setVisibility(View.VISIBLE);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                try {

                    swipeRefreshLayout.setRefreshing(false);
                    relativeLayoutLoading.setVisibility(View.GONE);
                   // Toast.makeText(getActivity(), "Something Went Wrong. Please Retry",
                     //       Toast.LENGTH_LONG).show();

                } catch (NullPointerException e) {
                    e.printStackTrace();
                }

            }
        });
        AppController.getInstance().addToRequestQueue(request_json,
                "Home");


    }


    private class GetVersionCode extends AsyncTask<Void, String, String> {
        @Override
        protected String doInBackground(Void... voids) {
            String newVersion = null;
            try {
                newVersion = Jsoup.connect("https://play.google.com/store/apps/details?id=com.click.shopuser&hl=en" + getActivity().getPackageName() + "&hl=it").timeout(30000)
                        .userAgent("Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6")
                        .referrer("http://www.google.com")
                        .get()
                        .select("div[itemprop=softwareVersion]")
                        .first()
                        .ownText();
                return newVersion;
            } catch (Exception e) {

                return newVersion;

            }
        }


        @Override

        protected void onPostExecute(String onlineVersion) {
            Log.d(TAG, "onlineVersion  : " + onlineVersion);
            super.onPostExecute(onlineVersion);
            if (onlineVersion != null && !onlineVersion.isEmpty()) {
                if (Float.valueOf(currentVersion) < Float.valueOf(onlineVersion)) {
                    Message("New Version Available in Play Store. Please Update");
                }
            }
            Log.d("update", "Current version " + currentVersion + "  playstore version " + onlineVersion);
        }
    }


    public void Message(String message) {
        LayoutInflater layoutInflater = LayoutInflater.from(this.getActivity());
        View promptView = layoutInflater.inflate(R.layout.alert_message, null);
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setView(promptView);
        TextView textViewMessage = promptView.findViewById(R.id.alert_message_display);
        textViewMessage.setText(message);
        Button buttonblock = promptView.findViewById(R.id.alert_ok_buton);
        buttonblock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertNew.dismiss();
            }
        });
        alertNew = alertDialogBuilder.create();
        alertNew.show();
    }


    private void parseData(JSONArray array) {


        Log.d("arrayResponseNoData",array.toString());
        for (int i = 0; i < array.length(); i++) {
            NearByShopFrontPage store_horizontal = new NearByShopFrontPage();
            JSONObject json;
            try {
                json = array.getJSONObject(i);



                if (json.getString("storeId") != null && !json.getString("storeId").isEmpty()
                        && !json.getString("storeId").equals("storeId")) {
                    store_horizontal.setStoreId((json.getInt("storeId")));
                }
                if (json.getString("storeName") != null && !json.getString("storeName").isEmpty()
                        && !json.getString("storeName").equals("storeName")) {
                    store_horizontal.setStoreName((json.getString("storeName")));
                }
                if (json.getString("merchantId") != null && !json.getString("merchantId").isEmpty()
                        && !json.getString("merchantId").equals("merchantId")) {
                    store_horizontal.setMerchant_Id((json.getInt("merchantId")));
                }
                if (json.getString("deliveryDuration") != null && !json.getString("deliveryDuration").isEmpty()
                        && !json.getString("deliveryDuration").equals("deliveryDuration")) {
                    store_horizontal.setDelivery_duration((json.getString("deliveryDuration")));
                }
                if (json.getString("workingHourFrom") != null && !json.getString("workingHourFrom").isEmpty()
                        && !json.getString("workingHourFrom").equals("workingHourFrom")) {
                    store_horizontal.setWorkging_hour_from(json.getString("workingHourFrom"));
                }
                if (json.getString("workingHourTo") != null && !json.getString("workingHourTo").isEmpty()
                        && !json.getString("workingHourTo").equals("workingHourTo")) {
                    store_horizontal.setWorking_hour_to((json.getString("workingHourTo")));
                }
                if (json.getString("deliveryRange") != null && !json.getString("deliveryRange").isEmpty()
                        && !json.getString("deliveryRange").equals("deliveryRange")) {
                    store_horizontal.setDelivery_range((json.getString("deliveryRange")));
                }
                if (json.getString("latitude") != null && !json.getString("latitude").isEmpty()
                        && !json.getString("latitude").equals("latitude")) {
                    store_horizontal.setStore_latitude((json.getString("latitude")));
                    mReceivedLatitude = json.getString("latitude");
                }
                if (json.getString("longitude") != null && !json.getString("longitude").isEmpty()
                        && !json.getString("longitude").equals("longitude")) {
                    store_horizontal.setStore_longitude((json.getString("longitude")));
                    mReceivedLongitude = json.getString("longitude");
                }
                if (json.getString("address") != null && !json.getString("address").isEmpty()
                        && !json.getString("address").equals("address")) {
                    store_horizontal.setAddress((json.getString("address")));
                }
                if (json.getString("pinCode") != null && !json.getString("pinCode").isEmpty()
                        && !json.getString("pinCode").equals("pinCode")) {
                    store_horizontal.setPincode((json.getString("pinCode")));
                }
                Log.d("NearbyResponse",String.valueOf(json.getString("profilePic")));
                if (json.getString("profilePic") != null && !json.getString("profilePic").isEmpty()
                        && !json.getString("profilePic").equals("profilePic")) {
                    store_horizontal.setStore_image((json.getString("profilePic")));
                }

                mStart = new LatLng(Double.valueOf(mFetchLatitude), Double.valueOf(mFetchLongitude));
                mEnd = new LatLng(Double.valueOf(mReceivedLatitude), Double.valueOf(mReceivedLongitude));
                Location startPoint = new Location("locationB");
                startPoint.setLatitude(latitude);
                startPoint.setLongitude(longitude);
                Location endPoint = new Location("locationA");
                endPoint.setLatitude(Double.valueOf(mReceivedLatitude));
                endPoint.setLongitude(Double.valueOf(mReceivedLongitude));

                double dist = AppUtils.calculationByDistance(mStart, mEnd);

                if (dist == 0.0) {
                    // If the current location and store location is equal the distance
                    // will show as 0.0. Therefore I have added a default nearby distance here

                    store_horizontal.setDelivery_time(String.valueOf(0.10826277856925441));
                } else {

                    Log.d(TAG, "dist: " + dist);
                    store_horizontal.setDelivery_time(String.valueOf(dist));
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
            storeItemList.add(store_horizontal);
        }
        adapter = new NearByShopAdapterFrontPage(storeItemList, getActivity());
        recyclerView.setAdapter(adapter);
        swipeRefreshLayout.setRefreshing(false);
        adapter.notifyDataSetChanged();


        txtnostore.setVisibility(View.GONE);
        txtnostore1.setVisibility(View.GONE);
        txtnostore.setVisibility(View.GONE);
        imgnostore.setVisibility(View.GONE);
    }


}
