package com.click.clickshopuser.main.adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.click.clickshopuser.R;
import com.click.clickshopuser.appconfig.AppUtils;
import com.click.clickshopuser.appconfig.EndPoint;
import com.click.clickshopuser.appconfig.SessionManager;
import com.click.clickshopuser.main.activity.NearByShopFrontCatagory;
import com.click.clickshopuser.model.CheckSelectedStore;
import com.click.clickshopuser.model.NearByShopFrontPage;
import com.click.clickshopuser.network.NetworkUtils;
import com.click.clickshopuser.storage.DbHandler;
import com.click.clickshopuser.storage.Preferences;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;

public class NearByShopAdapterFrontPage extends
        RecyclerView.Adapter<NearByShopAdapterFrontPage.ViewHolder> {
    private static final String TAG = NearByShopAdapterFrontPage.class.getSimpleName();
    private Context context;
    private List<NearByShopFrontPage> listInAss;
    public ImageView networkImageView;
    private Bitmap decodeimage = null;
    private DbHandler dbHandler;
    private AlertDialog alert;
    private int mSelectedStoreId;
    private String cutString;
    private String mStr;
    View v;
    private ProgressDialog progress;

    public NearByShopAdapterFrontPage(List<NearByShopFrontPage> listInAss, Context context) {
        super();
        this.listInAss = listInAss;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.near_by_shop_adapter_front_page, parent, false);
        dbHandler = new DbHandler(context);
        ViewHolder viewHolder = new ViewHolder(v);
        viewHolder.setIsRecyclable(false);
        return viewHolder;
    }

    @Override
    public int getItemCount() {
        return listInAss.size();
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final NearByShopFrontPage getAllItems = listInAss.get(position);
        final String image = getAllItems.getStore_image();
        //stringToBitmap(image);
        Glide.with(context).load(image).asBitmap().into(networkImageView);


        try {
            mStr = getAllItems.getDelivery_time();
        } catch (StringIndexOutOfBoundsException e) {
        } catch (NullPointerException e) {
            Toast.makeText(context, "Error.",
                    Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }

        try {
            cutString = mStr.substring(0, 3);
        } catch (StringIndexOutOfBoundsException e) {
        } catch (NullPointerException e) {
            Toast.makeText(context, "Error.",
                    Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }

        holder.textViewShopName.setText(getAllItems.getStoreName());

        String[] separated = getAllItems.getDelivery_duration().split(":");
        Log.d("SeperatedString", String.valueOf(separated[0].length()));
        Log.d("SeperatedString", String.valueOf(separated[1].length()));
        if(separated[0].length()==1 &&  separated[1].length()==1)
        {
            String from="0"+separated[0];
            String to="0"+separated[1];
            holder.textViewDeliveryTime.setText(new StringBuilder("Delivery in : "+from+":"+to+" Hr"));
        }
        else
            holder.textViewDeliveryTime.setText(new StringBuilder("Delivery in : ").append(getAllItems.getDelivery_duration())+" Hr");
        holder.textViewDeliveryDistance.setText(new StringBuilder("Distance : ").append(cutString).append(" Kms"));

        holder._delivery_hour.setText(new StringBuilder("Working from ").append(getAllItems.getWorkging_hour_from())+" to "+getAllItems.getWorking_hour_to());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Integer mMerchId = getAllItems.getMerchant_Id();
                // Used in CartFragment of fetching Delivery Charge
                if (Preferences.getDefaults("merchantId", context) != null) {
                    Preferences.deletePrefs("merchantId", context);
                }
                Preferences.setDefaults("merchantId", String.valueOf(mMerchId), context);


                // Used in DeliverAddress.Class Activity while posting Orders
                String mStoreIds = String.valueOf(getAllItems.getStoreId());
                if (Preferences.getDefaults("postStoreId", context) != null) {
                    Preferences.deletePrefs("postStoreId", context);
                }
                Preferences.setDefaults("postStoreId", mStoreIds, context);



                int mCheck = dbHandler.getCartCount();


                SessionManager sessionManager = new SessionManager(context);
                if (sessionManager.isLoggedIn()) {
                    if (AppUtils.internetConnectionAvailable(2000)) {

                        String mStore = getAllItems.getStoreName();
                        String mStoreId = String.valueOf(getAllItems.getStoreId());
                        String mStoreName = getAllItems.getStoreName();

                        selectStoreId(mCheck, mStoreIds, mStore,  mStoreId, mStoreName);
                    } else {
                        Toast.makeText(context, "No Network Connection",
                                Toast.LENGTH_SHORT).show();
                    }

                } else {



                    // Saving Seleted Shop Name in Shared Preference . Used NoWhee till now.
                    String mStore = getAllItems.getStoreName();
                    String mStoreId = String.valueOf(getAllItems.getStoreId());
                    String mStoreName = getAllItems.getStoreName();


                    if (mCheck != 0) {
                        List<CheckSelectedStore> cartDetails = dbHandler.getSelectedStore();
                        for (CheckSelectedStore addd : cartDetails) {
                            mSelectedStoreId = addd.getMStoreId();
                        }

                        if (Integer.valueOf(mStoreIds) == mSelectedStoreId) {

                            SharedPreferences preferences1 = PreferenceManager.getDefaultSharedPreferences(context);
                            SharedPreferences.Editor editor1 = preferences1.edit();
                            editor1.putString("storename",  holder.textViewShopName.getText().toString());
                            editor1.apply();

                            Log.d("Storename",preferences1.getString("storename",""));


                            setStoreName(mStore);
                            Intent intent = new Intent(context, NearByShopFrontCatagory.class);
                            intent.putExtra("mStoreId", mStoreId);
                            intent.putExtra("mStoreName", mStoreName);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                            context.startActivity(intent);
                        } else {
                            String mStoreNamee = Preferences.getDefaults(Preferences.SHARE_PREF_CHECK_SELETED_SHOP_NAME, context);
                            Message("You Have Already Selected an Item in " + mStoreNamee + ". Please Complete the Shopping And Proceed. ");
                        }

                    } else if (mCheck == 0) {
                        SharedPreferences preferences1 = PreferenceManager.getDefaultSharedPreferences(context);
                        SharedPreferences.Editor editor1 = preferences1.edit();
                        editor1.putString("storename",  holder.textViewShopName.getText().toString());
                        editor1.apply();
                        Log.d("Storename",preferences1.getString("storename",""));

                        setStoreName(mStore);
                        Intent intent = new Intent(context, NearByShopFrontCatagory.class);
                        intent.putExtra("mStoreId", mStoreId);
                        intent.putExtra("mStoreName", mStoreName);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                        context.startActivity(intent);
                    } else {
                        SharedPreferences preferences1 = PreferenceManager.getDefaultSharedPreferences(context);
                        SharedPreferences.Editor editor1 = preferences1.edit();
                        editor1.putString("storename",  holder.textViewShopName.getText().toString());
                        editor1.apply();
                        Log.d("Storename",preferences1.getString("storename",""));

                        setStoreName(mStore);
                        Intent intent = new Intent(context, NearByShopFrontCatagory.class);
                        intent.putExtra("mStoreId", mStoreId);
                        intent.putExtra("mStoreName", mStoreName);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                        context.startActivity(intent);
                    }







                }


























            }
        });

        holder.setIsRecyclable(false);

    }

    private void stringToBitmap(String str) {
        byte[] bytesImage = Base64.decode(str, Base64.DEFAULT);
        Glide.with(context).load(bytesImage).asBitmap().into(networkImageView);
    }



    @Override
    public int getItemViewType(int position) {
        return position;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView textViewDeliveryTime;
        TextView textViewDeliveryDistance;
        TextView textViewShopName;
        TextView _delivery_hour;

        private ViewHolder(View itemView) {
            super(itemView);
            networkImageView = itemView.findViewById(R.id._store_image);
            textViewDeliveryTime = itemView.findViewById(R.id._delivery_time);
            textViewDeliveryDistance = itemView.findViewById(R.id._delivery_distance);
            textViewShopName = itemView.findViewById(R.id._shope_name);
            _delivery_hour = itemView.findViewById(R.id._delivery_hour);
            this.setIsRecyclable(false);
        }
    }

    private void setStoreName(String mStoreName) {
        if (Preferences.getDefaults(Preferences.SHARE_PREF_CHECK_SELETED_SHOP_NAME, context) != null) {
            Preferences.deletePrefs(Preferences.SHARE_PREF_CHECK_SELETED_SHOP_NAME, context);
        }
        Preferences.setDefaults(Preferences.SHARE_PREF_CHECK_SELETED_SHOP_NAME, mStoreName, context);
    }




    private void selectStoreId(final Integer mCheck, final String mStoreIds, final String mStore, final String mStoreId, final String mStoreName) {
        progress = ProgressDialog.show(v.getContext(), "",
                "Please wait...", false, false);
        SessionManager sessionManager = new SessionManager(context);
        HashMap<String, String> hashMap = sessionManager.getUserDetails();
        String mUserid = hashMap.get(SessionManager.KEY_USER_ID);
        String mDeviceId = hashMap.get(SessionManager.KEY_DEVICE_ID);
        String mSessionId = hashMap.get(SessionManager.KEY_SESSION_ID);

        HashMap<String, String> params = new HashMap<String, String>();
        params.put("userId", mUserid);
        params.put("storeId", Preferences.getDefaults("postStoreId", context));
        params.put("deviceUniqueId", mDeviceId);
        params.put("sessionId", mSessionId);

        Log.d(TAG, "selectStoreId: " + params);


        JsonObjectRequest request_json = new JsonObjectRequest(EndPoint.SELECTED_STORE_ID, new JSONObject(params),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, "selectStoreIdRESPONSE: " + response);
                        progress.dismiss();

                        try {
                            Integer mStatusCode = response.getInt("responseCode");
                            if (mStatusCode == 200) {

                                String mStoreIdReceived = String.valueOf(response.getString("storeId"));

                                if (Preferences.getDefaults("postStoreId", context) != null) {
                                    Preferences.deletePrefs("postStoreId", context);
                                }
                                Preferences.setDefaults("postStoreId", mStoreIdReceived, context);

                                if (dbHandler.checkIfStoreSelected() == 1){
                                    dbHandler.deleteSeletedShop();
                                }


                                CheckSelectedStore checkSelectedStore = new CheckSelectedStore();
                                checkSelectedStore.setMStoreId(Integer.valueOf(mStoreIdReceived));
                                dbHandler.selectedShop(checkSelectedStore);


                                if (mCheck != 0) {
                                    List<CheckSelectedStore> cartDetails = dbHandler.getSelectedStore();
                                    for (CheckSelectedStore addd : cartDetails) {
                                        mSelectedStoreId = addd.getMStoreId();
                                    }

                                    if (Integer.valueOf(mStoreIds) == mSelectedStoreId) {
                                        SharedPreferences preferences1 = PreferenceManager.getDefaultSharedPreferences(context);
                                        SharedPreferences.Editor editor1 = preferences1.edit();
                                        editor1.putString("storename",  mStoreName);
                                        editor1.apply();
                                        Log.d("Storename",preferences1.getString("storename",""));
                                        setStoreName(mStore);
                                        Intent intent = new Intent(context, NearByShopFrontCatagory.class);
                                        intent.putExtra("mStoreId", mStoreId);
                                        intent.putExtra("mStoreName", mStoreName);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                                        context.startActivity(intent);
                                    } else {
                                        String mStoreNamee = Preferences.getDefaults(Preferences.SHARE_PREF_CHECK_SELETED_SHOP_NAME, context);
                                        Message("You Have Already Selected an Item in " + mStoreNamee + ". Please Complete the Shopping And Proceed. ");
                                    }

                                } else if (mCheck == 0) {

                                    SharedPreferences preferences1 = PreferenceManager.getDefaultSharedPreferences(context);
                                    SharedPreferences.Editor editor1 = preferences1.edit();
                                    editor1.putString("storename",  mStoreName);
                                    editor1.apply();

                                    Log.d("Storename",preferences1.getString("storename",""));

                                    setStoreName(mStore);
                                    Intent intent = new Intent(context, NearByShopFrontCatagory.class);
                                    intent.putExtra("mStoreId", mStoreId);
                                    intent.putExtra("mStoreName", mStoreName);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                                    context.startActivity(intent);
                                } else {
                                    SharedPreferences preferences1 = PreferenceManager.getDefaultSharedPreferences(context);
                                    SharedPreferences.Editor editor1 = preferences1.edit();
                                    editor1.putString("storename",  mStoreName);
                                    editor1.apply();

                                    Log.d("Storename",preferences1.getString("storename",""));

                                    setStoreName(mStore);
                                    Intent intent = new Intent(context, NearByShopFrontCatagory.class);
                                    intent.putExtra("mStoreId", mStoreId);
                                    intent.putExtra("mStoreName", mStoreName);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                                    context.startActivity(intent);
                                }



                            } else if (mStatusCode == 400) {

                                Message("You Have Already Selected an Item in Another Store. Please Complete the Shopping And Proceed. ");

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progress.dismiss();
                NetworkUtils.UnableToReachServer(context);
            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(request_json);
    }









    private void Message(String message) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View promptView = layoutInflater.inflate(R.layout.alert_message, null);
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setView(promptView);
        TextView textViewMessage = promptView.findViewById(R.id.alert_message_display);
        textViewMessage.setText(message);
        Button buttonblock = promptView.findViewById(R.id.alert_ok_buton);
        buttonblock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alert.dismiss();
            }
        });
        alert = alertDialogBuilder.create();
        alert.show();
    }


}