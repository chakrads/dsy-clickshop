package com.click.clickshopuser.main.bottombar;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.click.clickshopuser.R;
import com.click.clickshopuser.appconfig.AppUtils;
import com.click.clickshopuser.appconfig.EndPoint;
import com.click.clickshopuser.main.adapter.OfferFragmentCardAdapter;
import com.click.clickshopuser.model.GeoLocation;
import com.click.clickshopuser.model.OfferModelFrag;
import com.click.clickshopuser.network.NetworkUtils;
import com.click.clickshopuser.storage.DbHandler;
import com.google.android.gms.maps.model.LatLng;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class OfferFragment extends Fragment  implements SwipeRefreshLayout.OnRefreshListener {
    private static final String TAG = OfferFragment.class.getSimpleName();
    public RelativeLayout relativeLayoutLoading;
    public SwipeRefreshLayout swipeRefreshLayout;
    public RecyclerView recyclerView;
    public List<OfferModelFrag> storeItemList;
    public OfferFragmentCardAdapter adapter;
    public LinearLayoutManager linearLayoutManager;
    private AlertDialog alert;
    String mReceivedLongitude;
    String mReceivedLatitude;
    LatLng mStart, mEnd;
    DbHandler dbHandler;
    String mLatitude, mLongitude;

    public OfferFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_offer, container, false);
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        dbHandler = DbHandler.getInstance(getActivity());
        relativeLayoutLoading = getActivity().findViewById(R.id._loading_offer);
        relativeLayoutLoading.setVisibility(View.GONE);
        swipeRefreshLayout = getActivity().findViewById(R.id._offer_refresh);
        swipeRefreshLayout.setOnRefreshListener(this);
        recyclerView = getActivity().findViewById(R.id._offer_recyclerview);
        linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(linearLayoutManager);
        storeItemList = new ArrayList<>();

        getData();


        if (AppUtils.internetConnectionAvailable(2000)) {
            SearchProuct();
        } else {
            Toast.makeText(getActivity(), "No Network Connection",
                    Toast.LENGTH_SHORT).show();
        }


        super.onActivityCreated(savedInstanceState);
    }


    @Override
    public void onRefresh() {
        if(storeItemList != null){
            storeItemList.clear();
        }
        recyclerView.setVisibility(View.GONE);
        if (AppUtils.internetConnectionAvailable(2000)) {
            SearchProuct();
        } else {
            Toast.makeText(getActivity(), "No Network Connection",
                    Toast.LENGTH_SHORT).show();
        }

    }



    public void getData() {
        List<GeoLocation> geoLocations = dbHandler.getGeoLocation();
        for (GeoLocation geoLocation : geoLocations) {
            mLatitude = geoLocation.getMLatitude();
            mLongitude = geoLocation.getMLongitude();
        }
    }


    public void SearchProuct() {
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("latitude", mLatitude);
        params.put("longitude", mLongitude);
        Log.d(TAG, "OfferParameters: " + params);

        JsonObjectRequest request_json = new JsonObjectRequest(EndPoint.OFFER_DETAILS, new JSONObject(params),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, "Offer Response: " + response);

                        try {
                            Integer mStatusCode = response.getInt("responseCode");
                            if (mStatusCode == 200) {
                                String mResponseMessage = response.getString("responseMessage");
                                relativeLayoutLoading.setVisibility(View.GONE);
                                recyclerView.setVisibility(View.VISIBLE);
                                JSONArray array = response.optJSONArray("storeList");
                                parseData(array);
                            } else if (mStatusCode == 400) {
                                relativeLayoutLoading.setVisibility(View.GONE);
                                recyclerView.setVisibility(View.GONE);
                                Message("Item Not Available in Any Store Within 3 Kms Range");
                            } else {
                                relativeLayoutLoading.setVisibility(View.GONE);
                                recyclerView.setVisibility(View.GONE);
                                Message("Error. Please Retry..");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                relativeLayoutLoading.setVisibility(View.GONE);
                recyclerView.setVisibility(View.GONE);
                NetworkUtils.UnableToReachServer(getActivity());
            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(request_json);
    }



    private void parseData(JSONArray array) {
        for (int i = 0; i < array.length(); i++) {
            OfferModelFrag offerModelFrag = new OfferModelFrag();
            JSONObject json;
            try {
                json = array.getJSONObject(i);
                if (json.getString("storeId") != null && !json.getString("storeId").isEmpty()
                        && !json.getString("storeId").equals("storeId")) {
                    offerModelFrag.setMStoreId((json.getInt("storeId")));
                }

                if (json.getString("itemId") != null && !json.getString("itemId").isEmpty()
                        && !json.getString("itemId").equals("itemId")) {
                    offerModelFrag.setMItemId((json.getInt("itemId")));
                }

                if (json.getInt("itemMerchantId") != 0 ) {
                    offerModelFrag.setItemMerchantId(json.getInt("itemMerchantId"));
                } else {
                    offerModelFrag.setItemMerchantId(0);
                }

                if (json.getString("latitude") != null && !json.getString("latitude").isEmpty()
                        && !json.getString("latitude").equals("latitude")) {
                    offerModelFrag.setMLattitude((json.getString("latitude")));

                    mReceivedLatitude = json.getString("latitude");
                }
                if (json.getString("longitude") != null && !json.getString("longitude").isEmpty()
                        && !json.getString("longitude").equals("longitude")) {
                    offerModelFrag.setMLongitude((json.getString("longitude")));

                    mReceivedLongitude = json.getString("longitude");
                }
                if (json.getString("deliveryDuration") != null && !json.getString("deliveryDuration").isEmpty()
                        && !json.getString("deliveryDuration").equals("deliveryDuration")) {
                    offerModelFrag.setMDeliveryDuration(json.getString("deliveryDuration"));
                }
                if (json.getString("storeRating") != null && !json.getString("storeRating").isEmpty()
                        && !json.getString("storeRating").equals("storeRating")) {
                    offerModelFrag.setMStoreRatings(json.getString("storeRating"));
                }

                if (json.getString("storeName") != null && !json.getString("storeName").isEmpty()
                        && !json.getString("storeName").equals("storeName")) {
                    offerModelFrag.setMStoreName(json.getString("storeName"));
                }
                if (json.getString("itemName") != null && !json.getString("itemName").isEmpty()
                        && !json.getString("itemName").equals("itemName")) {
                    offerModelFrag.setMItemName(json.getString("itemName"));
                }
                // Price After Discount
                if (json.getString("priceAfterDiscount") != null && !json.getString("priceAfterDiscount").isEmpty()
                        && !json.getString("priceAfterDiscount").equals("priceAfterDiscount")) {
                    offerModelFrag.setMItemPrice(json.getString("priceAfterDiscount"));

                    // Here since there is discount we are passing discounted amount in the path of actual price to display and amount calulation
                }

                if (json.getString("actualPrice") != null && !json.getString("actualPrice").isEmpty()
                        && !json.getString("actualPrice").equals("actualPrice")) {
                    offerModelFrag.setActualPrice(json.getString("actualPrice"));
                }
                if (json.getString("discountPercentage") != null && !json.getString("discountPercentage").isEmpty()
                        && !json.getString("discountPercentage").equals("discountPercentage")) {
                    offerModelFrag.setDiscountPercentage(json.getString("discountPercentage"));
                }

                if (json.getString("itemWeight") != null && !json.getString("itemWeight").isEmpty()
                        && !json.getString("itemWeight").equals("itemWeight")) {
                    offerModelFrag.setMItemWeight(json.getString("itemWeight"));
                }
                if (json.getString("merchantId") != null
                        && !json.getString("merchantId").equals("merchantId")) {
                    offerModelFrag.setMMerchantId(json.getInt("merchantId"));
                }

                if (json.getString("itemImage") != null && !json.getString("itemImage").isEmpty()
                        && !json.getString("itemImage").equals("itemImage")) {
                    String mPath = modifyDropboxUrl(json.getString("itemImage"));
                    offerModelFrag.setMItemImage(mPath);
                }

                mStart = new LatLng(Double.valueOf(mLatitude), Double.valueOf(mLongitude));
                mEnd = new LatLng(Double.valueOf(mReceivedLatitude), Double.valueOf(mReceivedLongitude));

                Location startPoint = new Location("locationB");
                startPoint.setLatitude(Double.valueOf(mLatitude));
                startPoint.setLongitude(Double.valueOf(mLongitude));

                Location endPoint = new Location("locationA");
                endPoint.setLatitude(Double.valueOf(mReceivedLatitude));
                endPoint.setLongitude(Double.valueOf(mReceivedLongitude));

                double distance = startPoint.distanceTo(endPoint);
                double mDis = distance / 1000;

                offerModelFrag.setMStoreDistance(String.valueOf(mDis));
                double dist = CalculationByDistance(mStart, mEnd);

                if (dist == 0.0){

                    // If the current location and store location is equal the distance
                    // will show as 0.0. Therefore I have added a default nearby distance here

                    offerModelFrag.setMStoreDistance(String.valueOf(0.10826277856925441));
                }else {

                    Log.d(TAG, "dist: " + dist);
                    offerModelFrag.setMStoreDistance(String.valueOf(dist));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            storeItemList.add(offerModelFrag);
            // Collections.sort(recordsList);
        }
        adapter = new OfferFragmentCardAdapter(storeItemList, getActivity());
        recyclerView.setAdapter(adapter);
        swipeRefreshLayout.setRefreshing(false);
    }

    public static String modifyDropboxUrl(String originalUrl) {
        String newUrl = originalUrl.replace("www.dropbox.", "dl.dropboxusercontent.");

        //just for sure for case if www is missing in url string
        newUrl = newUrl.replace("dropbox.", "dl.dropboxusercontent.");

        return newUrl;
    }

    public double CalculationByDistance(LatLng StartP, LatLng EndP) {
        int Radius = 6371;// radius of earth in Km
        double lat1 = StartP.latitude;
        double lat2 = EndP.latitude;
        double lon1 = StartP.longitude;
        double lon2 = EndP.longitude;
        double dLat = Math.toRadians(lat2 - lat1);
        double dLon = Math.toRadians(lon2 - lon1);
        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2)
                + Math.cos(Math.toRadians(lat1))
                * Math.cos(Math.toRadians(lat2)) * Math.sin(dLon / 2)
                * Math.sin(dLon / 2);
        double c = 2 * Math.asin(Math.sqrt(a));
        double valueResult = Radius * c;
        double km = valueResult / 1;
        DecimalFormat newFormat = new DecimalFormat("####");
        int kmInDec = Integer.valueOf(newFormat.format(km));
        double meter = valueResult % 1000;
        int meterInDec = Integer.valueOf(newFormat.format(meter));
        Log.i("Radius Value", "" + valueResult + "   KM  " + kmInDec
                + " Meter   " + meterInDec);
        return Radius * c;
    }


    public void Message(String message) {
        LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
        View promptView = layoutInflater.inflate(R.layout.alert_message, null);
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setView(promptView);
        TextView textViewMessage = promptView.findViewById(R.id.alert_message_display);
        textViewMessage.setText(message);
        Button buttonblock = promptView.findViewById(R.id.alert_ok_buton);
        buttonblock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alert.dismiss();
            }
        });
        alert = alertDialogBuilder.create();
        alert.show();
    }

}
