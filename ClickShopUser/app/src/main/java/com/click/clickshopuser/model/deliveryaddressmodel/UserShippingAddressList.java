
package com.click.clickshopuser.model.deliveryaddressmodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserShippingAddressList {

    @SerializedName("userShippingAddressId")
    @Expose
    private Integer userShippingAddressId;
    @SerializedName("userId")
    @Expose
    private Integer userId;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("mobileNumber")
    @Expose
    private String mobileNumber;

    private boolean isSelected;

    public  boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean isSelected) {
        this.isSelected = isSelected;
    }

    public Integer getUserShippingAddressId() {
        return userShippingAddressId;
    }

    public void setUserShippingAddressId(Integer userShippingAddressId) {
        this.userShippingAddressId = userShippingAddressId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

}
