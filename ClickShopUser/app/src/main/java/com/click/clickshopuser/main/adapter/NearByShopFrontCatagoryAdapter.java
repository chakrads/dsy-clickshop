package com.click.clickshopuser.main.adapter;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.android.volley.toolbox.ImageLoader;
import com.bumptech.glide.Glide;
import com.click.clickshopuser.R;
import com.click.clickshopuser.main.activity.SearchStoreItem;
import com.click.clickshopuser.model.HomeCatagory;

import java.util.List;

public class NearByShopFrontCatagoryAdapter extends
        RecyclerView.Adapter<NearByShopFrontCatagoryAdapter.ViewHolder> {

    private static final String TAG = NearByShopFrontCatagoryAdapter.class.getSimpleName();
    Context context;
    List<HomeCatagory> listInAss;
    public ImageView networkImageView;
    public ImageLoader imageLoader;
    View v;


    public NearByShopFrontCatagoryAdapter(List<HomeCatagory> listInAss, Context context) {
        super();
        this.listInAss = listInAss;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_adapter_nearbyshop_front_catagory, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public int getItemCount() {
        return listInAss.size();
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final HomeCatagory homeCatagory = listInAss.get(position);
        String image = homeCatagory.getMCatagoryImage();

        Glide.with(context)
                .load(image)
                .override(200, 200)
                .into(networkImageView);


//        imageLoader = CustomVolleyRequest.getInstance(context).getImageLoader();
//        imageLoader.get(image, ImageLoader.getImageListener(networkImageView, R.drawable.noimage,
//                R.drawable.noimage));
//        networkImageView.setImageUrl(image, imageLoader);



        holder.textViewCatName.setText(homeCatagory.getMCatagoryName());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String mCatagory = String.valueOf(homeCatagory.getMCatagoryId());
                Intent intent = new Intent(context, SearchStoreItem.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                intent.putExtra("itemCategory", mCatagory);
                context.startActivity(intent);
             //   ((AppCompatActivity)v.getContext()).finish();


            }
        });


    }

    private void stringToBitmap(String str) {
        byte[] bytesImage = Base64.decode(str, Base64.DEFAULT);
        Glide.with(context).load(bytesImage).asBitmap().into(networkImageView);
    }



    class ViewHolder extends RecyclerView.ViewHolder {
        private TextView textViewCatName;

        private ViewHolder(View itemView) {
            super(itemView);
            networkImageView = itemView.findViewById(R.id._cat_image);
            textViewCatName = itemView.findViewById(R.id._catagory_name);
            this.setIsRecyclable(false);
        }
    }


}