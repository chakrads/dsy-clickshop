package com.click.clickshopuser.model

class GeoLocation {
    var mId : Int? = 0;
    var mLatitude : String? = null
    var mLongitude: String? = null
    var mAddress: String? = null
    var mCity: String? = null
    var mCountry: String? = null
    var mPinCode: String? = null
}

