package com.click.clickshopuser.prelanding;
import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.provider.Settings
import android.support.v4.app.ActivityCompat
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Toast
import com.click.clickshopuser.R
import com.click.clickshopuser.main.activity.MainActivity


class SplashScreen : AppCompatActivity(), View.OnClickListener {
    private var permissionsRequired = arrayOf(Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.READ_PHONE_STATE, Manifest.permission.WRITE_EXTERNAL_STORAGE

    )
    private val PERMISSION_CALLBACK_CONSTANT = 100
    private val REQUEST_PERMISSION_SETTING = 101
    private var permissionStatus: SharedPreferences? = null
    private var sentToSettings = false

    override fun onClick(v: View?) {
        when (v?.id) {
//            R.id.fab ->
//                Snackbar.make(v, "Android Runtime Permissions Example With Kotlin", Snackbar.LENGTH_LONG).setAction("Action", null).show()
//
//            R.id.btRequestPermission -> {
//                requestPermission()
//            }
        }



    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen_s)


        permissionStatus = getSharedPreferences("permissionStatus", Context.MODE_PRIVATE)


        // Showing Status bar
        if (Build.VERSION.SDK_INT < 17) {
            val i = Intent(this@SplashScreen, MainActivity::class.java)
            startActivity(i)
            finish()
        }else{
            requestPermission()
        }











//        fab.setOnClickListener(this)
//        btRequestPermission.setOnClickListener(this)


    }

    private fun requestPermission() {



            if (ActivityCompat.checkSelfPermission(this, permissionsRequired[0]) != PackageManager.PERMISSION_GRANTED
                    || ActivityCompat.checkSelfPermission(this, permissionsRequired[1]) != PackageManager.PERMISSION_GRANTED
                    || ActivityCompat.checkSelfPermission(this, permissionsRequired[2]) != PackageManager.PERMISSION_GRANTED

                  ) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(this, permissionsRequired[0])
                        || ActivityCompat.shouldShowRequestPermissionRationale(this, permissionsRequired[1])
                        || ActivityCompat.shouldShowRequestPermissionRationale(this, permissionsRequired[2])

                   ) {
                    //Show Information about why you need the permission
                    getAlertDialog()
                } else if (permissionStatus!!.getBoolean(permissionsRequired[0], false)) {
                    //Previously Permission Request was cancelled with 'Dont Ask Again',
                    // Redirect to Settings after showing Information about why you need the permission

                    @Suppress("UNUSED_PARAMETER")
                    val builder = AlertDialog.Builder(this)
                    builder.setTitle("Permission Required")
                    builder.setTitle("Need Multiple Permissions")
                    builder.setMessage("This app needs permissions.")
                    builder.setPositiveButton("Grant") {


                                dialogInterface, i -> dialogInterface.cancel()
                                sentToSettings = true
                                val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
                                val uri = Uri.fromParts("package", packageName, null)
                                intent.data = uri
                                startActivityForResult(intent, REQUEST_PERMISSION_SETTING)
                                Toast.makeText(applicationContext, "Go to Permissions to Grant ", Toast.LENGTH_LONG).show()

                            }

                    builder.setNegativeButton("Cancel") { dialogInterface, i -> dialogInterface.cancel() }
                    val alertDialog = builder.create()
                    alertDialog.show()




                } else {
                    //just request the permission
                    ActivityCompat.requestPermissions(this, permissionsRequired, PERMISSION_CALLBACK_CONSTANT)
                }

                //   txtPermissions.setText("Permissions Required")

                val editor = permissionStatus!!.edit()
                editor.putBoolean(permissionsRequired[0], true)
                editor.commit()
            } else {
                Handler().postDelayed({
                    val i = Intent(this@SplashScreen, MainActivity::class.java)
                    startActivity(i)
                    finish()
                }, SPLASH_TIME_OUT.toLong())


                //You already have the permission, just go ahead.
                Toast.makeText(applicationContext, "Allowed All Permissions", Toast.LENGTH_LONG).show()
            }


    }




    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == PERMISSION_CALLBACK_CONSTANT) {
            //check if all permissions are granted
            var allgranted = false
            for (i in grantResults.indices) {
                if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                    allgranted = true
                } else {
                    allgranted = false
                    break
                }
            }

            if (allgranted) {
                Handler().postDelayed({
                    val i = Intent(this@SplashScreen, MainActivity::class.java)
                    startActivity(i)
                    finish()
                }, SPLASH_TIME_OUT.toLong())




                Toast.makeText(applicationContext, "Allowed All Permissions", Toast.LENGTH_LONG).show()




            } else if (ActivityCompat.shouldShowRequestPermissionRationale(this, permissionsRequired[0])
                    || ActivityCompat.shouldShowRequestPermissionRationale(this, permissionsRequired[1])
                    || ActivityCompat.shouldShowRequestPermissionRationale(this, permissionsRequired[2])
             ) {

                getAlertDialog()
            } else {
                Toast.makeText(applicationContext, "Unable to get Permission", Toast.LENGTH_LONG).show()
            }
        }
    }

    private fun getAlertDialog() {

        @Suppress("UNUSED_PARAMETER")
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Need Multiple Permissions")
        builder.setMessage("This app needs permissions.")
        builder.setPositiveButton("Grant") { dialog, which ->
            dialog.cancel()
            ActivityCompat.requestPermissions(this, permissionsRequired, PERMISSION_CALLBACK_CONSTANT)
        }
        builder.setNegativeButton("Cancel") { dialog, which -> dialog.cancel() }
        builder.show()
    }

    override fun onPostResume() {
        super.onPostResume()
        if (sentToSettings) {
            if (ActivityCompat.checkSelfPermission(this, permissionsRequired[0]) == PackageManager.PERMISSION_GRANTED) {
                Handler().postDelayed({
                    val i = Intent(this@SplashScreen, MainActivity::class.java)
                    startActivity(i)
                    finish()
                }, SPLASH_TIME_OUT.toLong())



                Toast.makeText(applicationContext, "Allowed All Permissions", Toast.LENGTH_LONG).show()
            }
        }
    }

    companion object {
        private val SPLASH_TIME_OUT = 3000

    }
}