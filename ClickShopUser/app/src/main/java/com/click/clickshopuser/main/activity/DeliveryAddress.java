package com.click.clickshopuser.main.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.click.clickshopuser.R;
import com.click.clickshopuser.appconfig.ApiClientUser;
import com.click.clickshopuser.appconfig.ApiInterface;
import com.click.clickshopuser.appconfig.AppUtils;
import com.click.clickshopuser.appconfig.Constants;
import com.click.clickshopuser.appconfig.EndPoint;
import com.click.clickshopuser.appconfig.SessionManager;
import com.click.clickshopuser.inter.DeliveryRefresh;
import com.click.clickshopuser.main.adapter.DeliveryCardAdapter;
import com.click.clickshopuser.model.AddToCart;
import com.click.clickshopuser.model.deliveryaddressmodel.DeliveryModel;
import com.click.clickshopuser.model.deliveryaddressmodel.DeliveryRequestBody;
import com.click.clickshopuser.model.deliveryaddressmodel.UserShippingAddressList;
import com.click.clickshopuser.network.NetworkUtils;
import com.click.clickshopuser.storage.DbHandler;
import com.click.clickshopuser.storage.Preferences;
import com.google.gson.Gson;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import retrofit2.Call;
import retrofit2.Callback;

public class DeliveryAddress extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener, DeliveryRefresh {
    private static final String TAG = DeliveryAddress.class.getSimpleName();
    private Toolbar toolbar;
    public RelativeLayout relativeLayoutLoading, noAddressFound;
    public SwipeRefreshLayout swipeRefreshLayout;
    public RecyclerView recyclerView;
    public DeliveryCardAdapter adapter;
    public LinearLayoutManager linearLayoutManager;
    public AlertDialog alert, alertMessage;
    String mPostAddress;
    String mPostMobileNumber;
    public ProgressDialog progress;
    Button buttonPlaceOrder;
    ArrayList<String> arrayList;
    DbHandler dbHandler;
    JSONArray array;

    private static int mCheckIfRadioButtonClicked;
    Button butAddAddress;
    ApiInterface apiInterfacec;
    List<UserShippingAddressList> datum;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.fadein, R.anim.fadeout);
        setContentView(R.layout.activity_delivery_address);
        mCheckIfRadioButtonClicked = 0;

        // Showing Status bar
        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }

        dbHandler = DbHandler.getInstance(getApplicationContext());
        toolbar = findViewById(R.id.delivery_toolbar_del);
        setSupportActionBar(toolbar);
        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_arrow_back_black_24dp));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        relativeLayoutLoading = findViewById(R.id._loading_delivery);
        relativeLayoutLoading.setVisibility(View.GONE);
        noAddressFound = findViewById(R.id._no_address);
        noAddressFound.setVisibility(View.GONE);
        butAddAddress = findViewById(R.id._add_address);
        butAddAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addAddress();
            }
        });
        buttonPlaceOrder = findViewById(R.id._place_order);
        buttonPlaceOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Integer mCartCount = dbHandler.getCartCount();
                if (mCartCount == 0) {
                    Message("Cart is Empty. Add Items to Cart and then Proceed");
                } else {
                    if (arrayList != null) {
                        arrayList.clear();
                    }
                    arrayList = new ArrayList<String>();
                    for (UserShippingAddressList delivery : datum) {
                        if (delivery != null) {
                            if (delivery.isSelected()) {
                                mPostAddress = delivery.getAddress();
                                mPostMobileNumber = delivery.getMobileNumber();
                                if (mPostAddress != null) {
                                    if (AppUtils.internetConnectionAvailable(2000)) {
                                        postData();
                                    } else {
                                        Toast.makeText(getApplicationContext(), "No Network Connection",
                                                Toast.LENGTH_SHORT).show();
                                    }
                                }
                            }else {
                                Toast.makeText(getApplicationContext(), "Please Select Address",
                                        Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                }
            }
        });


        swipeRefreshLayout = findViewById(R.id.delivery_refresh);
        swipeRefreshLayout.setOnRefreshListener(this);
        recyclerView = findViewById(R.id._recyclerview_delivery);
        linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);


        if (AppUtils.internetConnectionAvailable(2000)) {
            deliveryData();
        } else {
            Toast.makeText(getApplicationContext(), "No Network Connection",
                    Toast.LENGTH_SHORT).show();
        }


    }


    public void postData() {
        progress = ProgressDialog.show(this, "", "Please wait...", true);
        String mModeOfPayment = Preferences.getDefaults(Constants.MODE_OF_PAYMENT, getApplicationContext());
        String mOrderTotalAmount = Preferences.getDefaults(Constants.ORDER_TOTAL_AMOUNT, getApplicationContext());
        String mStoreIds = Preferences.getDefaults("postStoreId", getApplicationContext());
        String mDeliveryCharge = Preferences.getDefaults(Constants.DELIVERY_CHARGE, getApplicationContext());
        SessionManager sessionManager = new SessionManager(getApplicationContext());
        HashMap<String, String> hashMap = sessionManager.getUserDetails();
        String mUserid = hashMap.get(SessionManager.KEY_USER_ID);
        String mDeviceId = hashMap.get(SessionManager.KEY_DEVICE_ID);
        String mSessionId = hashMap.get(SessionManager.KEY_SESSION_ID);
        try {
            array = new JSONArray();
            List<AddToCart> cartDetails = dbHandler.getCartDetails();
            for (AddToCart addToCart : cartDetails) {
                String mPrice = addToCart.getMItem_Price();
                String mQuantityCount = String.valueOf(addToCart.getMItem_Count());
                String mItemMerchantId = String.valueOf(addToCart.getMItem_merchant_id());
                Integer mTotalPrice = addToCart.getMItem_individual_total_amount();
                JSONObject obj = new JSONObject();
                try {
                    obj.put("itemMerchantId", mItemMerchantId);
                    obj.put("itemPrice", mPrice);
                    obj.put("itemQuantityRequested", mQuantityCount);
                    obj.put("totalPrice", String.valueOf(mTotalPrice));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                array.put(obj);
            }
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            JSONObject jsonBody = new JSONObject();
            jsonBody.put("userId", mUserid);
            jsonBody.put("shippingAddress", mPostAddress);
            jsonBody.put("paymentMode", mModeOfPayment);
            jsonBody.put("orderAmount", mOrderTotalAmount);
            jsonBody.put("storeId", mStoreIds);
            jsonBody.put("sessionId", mSessionId);
            jsonBody.put("deviceUniqueId", mDeviceId);
            jsonBody.put("deliveryCharge", mDeliveryCharge);
            jsonBody.put("mobileNumber", mPostMobileNumber);
            jsonBody.put("itemList", array);
            Log.d(TAG, "Place Order: " + jsonBody);

            final String requestBody = jsonBody.toString();
            StringRequest stringRequest = new StringRequest(Request.Method.POST, EndPoint.PLACE_ORDER, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    progress.dismiss();
                    Log.d(TAG, "Placeresponse: " + response);
                    if (response.equalsIgnoreCase("200")) {
                        dbHandler.deleteCartItem();
                        if (Preferences.getDefaults(Constants.ORDER_TOTAL_AMOUNT, getApplicationContext()) != null) {
                            Preferences.deletePrefs(Constants.ORDER_TOTAL_AMOUNT, getApplicationContext());
                        }
                        // Sending a broadcast to Refresh Cart Page
                        Intent intent = new Intent("clear_cart_itmes");
                        intent.putExtra("Cart", "afterPost");
                        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
                        Message("Order is Placed Successfully");

                        SharedPreferences preferences1 = PreferenceManager.getDefaultSharedPreferences(DeliveryAddress.this);
                        SharedPreferences.Editor editor1 = preferences1.edit();
                        editor1.putString("storename",  "");
                        editor1.apply();


                        Intent i=new Intent(DeliveryAddress.this,MainActivity.class);
                        startActivity(i);
                        finish();
                    } else {
                        Message("Placing Order Failed. Please Retry..");
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progress.dismiss();
                    Log.e("VOLLEY", error.toString());
                    Message("Placing Order Failed. Please Retry..");
                }
            }) {
                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                    headers.put("Content-Type", "application/json; charset=utf-8");
                    return headers;
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return requestBody == null ? null : requestBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                        return null;
                    }
                }

                @Override
                protected Response<String> parseNetworkResponse(NetworkResponse response) {
                    String responseString = "";
                    if (response != null) {
                        responseString = String.valueOf(response.statusCode);
                        // can get more details such as response.headers
                    }
                    return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
                }
            };

            requestQueue.add(stringRequest);
        } catch (JSONException e) {
            progress.dismiss();
            e.printStackTrace();
        }
    }


    public void deliveryRefresh() {
        if (datum != null) {
            datum.clear();
        }
        recyclerView.setVisibility(View.GONE);
        if (AppUtils.internetConnectionAvailable(2000)) {
            deliveryData();
        } else {
            Toast.makeText(getApplicationContext(), "No Network Connection",
                    Toast.LENGTH_SHORT).show();
        }


    }


    @Override
    public void onRefresh() {
        if (datum != null) {
            datum.clear();
        }
        relativeLayoutLoading.setVisibility(View.GONE);
        recyclerView.setVisibility(View.GONE);
        if (AppUtils.internetConnectionAvailable(2000)) {
            deliveryData();
        } else {
            Toast.makeText(getApplicationContext(), "No Network Connection",
                    Toast.LENGTH_SHORT).show();
        }

    }































    public void deliveryData() {


        SessionManager sessionManager = new SessionManager(getApplicationContext());
        HashMap<String, String> hashMap = sessionManager.getUserDetails();
        String mUserid = hashMap.get(SessionManager.KEY_USER_ID);
        String mDeviceId = hashMap.get(SessionManager.KEY_DEVICE_ID);
        String mSessionId = hashMap.get(SessionManager.KEY_SESSION_ID);



        apiInterfacec = ApiClientUser.getApiClient().create(ApiInterface.class);
        Call<DeliveryModel> call = apiInterfacec.deliveryAddress(new DeliveryRequestBody(mUserid, mSessionId, mDeviceId));
        call.enqueue(new Callback<DeliveryModel>() {
            @Override
            public void onResponse(Call<DeliveryModel> call, retrofit2.Response<DeliveryModel> response) {
                if (response.isSuccessful()) {
                    relativeLayoutLoading.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.VISIBLE);
                    if (response.body().getResponseCode() == 200) {

                        Log.e("TAG", "DeliveryAddressResp: " + new Gson().toJson(response.body()));

                        noAddressFound.setVisibility(View.GONE);
                        relativeLayoutLoading.setVisibility(View.GONE);
                        recyclerView.setVisibility(View.VISIBLE);
                        datum = response.body().getUserShippingAddressList();
                        adapter = new DeliveryCardAdapter(datum, getApplicationContext(), DeliveryAddress.this);
                        recyclerView.setAdapter(adapter);
                        swipeRefreshLayout.setRefreshing(false);
                    } else {
                        relativeLayoutLoading.setVisibility(View.GONE);
                        noAddressFound.setVisibility(View.VISIBLE);
                        recyclerView.setVisibility(View.GONE);
                        swipeRefreshLayout.setRefreshing(false);
                        Toast.makeText(getApplicationContext(), "No Data",
                                Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<DeliveryModel> call, Throwable t) {
                relativeLayoutLoading.setVisibility(View.GONE);
                recyclerView.setVisibility(View.GONE);
                Toast.makeText(getApplicationContext(), t.toString(),
                        Toast.LENGTH_SHORT).show();
            }
        });
    }


    public void Message(String message) {
        LayoutInflater layoutInflater = LayoutInflater.from(this);
        View promptView = layoutInflater.inflate(R.layout.alert_message, null);
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setView(promptView);
        TextView textViewMessage = promptView.findViewById(R.id.alert_message_display);
        textViewMessage.setText(message);
        Button buttonblock = promptView.findViewById(R.id.alert_ok_buton);
        buttonblock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertMessage.dismiss();
            }
        });
        alertMessage = alertDialogBuilder.create();
        alertMessage.show();
    }


    public void addAddress() {
        LayoutInflater layoutInflater = LayoutInflater.from(this);
        View promptView = layoutInflater.inflate(R.layout.alert_add_address, null);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setView(promptView);

        final EditText editTextAddress = promptView.findViewById(R.id._address_input);
        final EditText editTextMobileNo = promptView.findViewById(R.id.mobile_no);

        Button buttonSubmit = promptView.findViewById(R.id._add_address);
        buttonSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (editTextAddress.getText().length() == 0) {
                    editTextAddress.setError("Enter Address");
                } else if (editTextMobileNo.getText().length() == 0) {
                    editTextMobileNo.setError("Enter Mobile Number");
                } else if (editTextMobileNo.getText().length() <= 9) {
                    editTextMobileNo.setError("Enter Correct Phone Number");
                } else {
                    mPostAddress = editTextAddress.getText().toString().trim();
                    mPostMobileNumber = editTextMobileNo.getText().toString().trim();
                    if (AppUtils.internetConnectionAvailable(2000)) {
                        postAddress();
                    } else {
                        Toast.makeText(getApplicationContext(), "No Network Connection",
                                Toast.LENGTH_SHORT).show();
                    }

                    alert.dismiss();
                }

            }
        });
        alert = alertDialogBuilder.create();
        alert.show();
    }


    public void postAddress() {
        progress = ProgressDialog.show(this, "", "Please wait...", false, false);
        SessionManager sessionManager = new SessionManager(getApplicationContext());
        HashMap<String, String> hashMap = sessionManager.getUserDetails();
        String mUserid = hashMap.get(SessionManager.KEY_USER_ID);
        String mDeviceId = hashMap.get(SessionManager.KEY_DEVICE_ID);
        String mSessionId = hashMap.get(SessionManager.KEY_SESSION_ID);

        HashMap<String, String> params = new HashMap<String, String>();
        params.put("userId", mUserid);
        params.put("address", mPostAddress);
        params.put("mobileNumber", mPostMobileNumber);
        params.put("sessionId", mSessionId);
        params.put("deviceUniqueId", mDeviceId);

        JsonObjectRequest request_json = new JsonObjectRequest(EndPoint.SHIPPING_ADDRESS_POSTING, new JSONObject(params),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, "AddressPostResponse: " + response);
                        progress.dismiss();
                        try {
                            Integer mStatusCode = response.getInt("responseCode");
                            if (mStatusCode == 200) {
                                if (datum != null) {
                                    datum.clear();
                                }
                                if (AppUtils.internetConnectionAvailable(2000)) {
                                    deliveryData();
                                } else {
                                    Toast.makeText(getApplicationContext(), "No Network Connection",
                                            Toast.LENGTH_SHORT).show();
                                }

                                Message("Address Added Successfully");
                            } else if (mStatusCode == 400) {
                                Message("Adding Address Failed");
                            } else {
                                Message("Error Please Retry..");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progress.dismiss();
                NetworkUtils.UnableToReachServer(getApplicationContext());
            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(request_json);
    }


}
