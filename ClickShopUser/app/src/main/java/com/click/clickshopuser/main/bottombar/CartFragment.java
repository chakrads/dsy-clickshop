package com.click.clickshopuser.main.bottombar;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.click.clickshopuser.R;
import com.click.clickshopuser.appconfig.ApiClientUser;
import com.click.clickshopuser.appconfig.ApiInterface;
import com.click.clickshopuser.appconfig.AppUtils;
import com.click.clickshopuser.appconfig.Constants;
import com.click.clickshopuser.appconfig.EndPoint;
import com.click.clickshopuser.appconfig.SessionManager;
import com.click.clickshopuser.inter.CartRefresh;
import com.click.clickshopuser.main.activity.DeliveryAddress;
import com.click.clickshopuser.main.adapter.CartFragmentAdapter;
import com.click.clickshopuser.model.AddToCart;
import com.click.clickshopuser.model.CartAdapterModel;
import com.click.clickshopuser.model.CheckSelectedStore;
import com.click.clickshopuser.model.deliverycharge.DeliveryCharge;
import com.click.clickshopuser.model.deliverycharge.RequestBodyDeliveryCharge;
import com.click.clickshopuser.network.NetworkUtils;
import com.click.clickshopuser.prelanding.LoginActivity;
import com.click.clickshopuser.storage.DbHandler;
import com.click.clickshopuser.storage.Preferences;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;


public class CartFragment extends Fragment implements CartRefresh {
    private static final String TAG = CartFragment.class.getSimpleName();

    private DbHandler dbHandler;
    public RecyclerView recyclerView;
    public ArrayList<CartAdapterModel> cartItemList;
    public CartFragmentAdapter adapter;
    public LinearLayoutManager linearLayoutManager;
    public ImageView imageViewEmpty;
    public TextView textViewAmount;
    Button buttonCheckOUt;
    String mProductName, mQuantityCount, mPrice, mWeight;
    Integer abc, def, ghi;
    public RadioButton radioButtonCashONDelivery, radioButtonPaytm;
    public RadioGroup radioGroup;
    public String mPaymentMode;
    int selectedId;
    RadioButton radioButtoncheck;
    public AlertDialog alertMessage;
    private BroadcastReceiver cartItemReceiver;
    TextView textViewDeliverycharge;
    public static ProgressDialog progress;
    String mDeliveryCharge;
    String mDeliveryFreeUpto;
    SessionManager sessionManager;
    String mStoreIds;


    public CartFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_cart, container, false);
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        dbHandler = new DbHandler(getActivity());
        sessionManager = new SessionManager(getActivity());
        mStoreIds = Preferences.getDefaults("postStoreId", getActivity());  // this was having problem when it was in constants


        textViewAmount = getActivity().findViewById(R.id._amountss);
        textViewDeliverycharge = getActivity().findViewById(R.id._delivery_charges_cart);
        imageViewEmpty = getActivity().findViewById(R.id._shopping_cart_empty);
        imageViewEmpty.setVisibility(View.GONE);
        recyclerView = getActivity().findViewById(R.id._recyclerview_cart_items);
        linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(linearLayoutManager);
        cartItemList = new ArrayList<>();
        mPaymentMode = "COD"; // Default


        ghi = 0;

        fetchData();
        cartAmoutn();


        radioGroup = getActivity().findViewById(R.id.radioPayment);
        radioButtonCashONDelivery = getActivity().findViewById(R.id._cash_on_delivery);
        radioButtonPaytm = getActivity().findViewById(R.id._pay_tm);


        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id._cash_on_delivery) {
                    mPaymentMode = "COD";
                } else if (checkedId == R.id._pay_tm) {
                    mPaymentMode = "PAYTM";
                }
            }
        });


        buttonCheckOUt = getActivity().findViewById(R.id._check_out);
        buttonCheckOUt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (dbHandler.getCartCount() != 0) {
                    selectedId = radioGroup.getCheckedRadioButtonId();
                    radioButtoncheck = getActivity().findViewById(selectedId);
                    if (radioGroup.getCheckedRadioButtonId() == -1) {
                        Toast.makeText(getActivity(), "Select Mode Of Payment",
                                Toast.LENGTH_LONG).show();
                    } else {
                        if (Preferences.getDefaults(Constants.MODE_OF_PAYMENT, getActivity()) != null) {
                            Preferences.deletePrefs(Constants.MODE_OF_PAYMENT, getActivity());
                        }
                        Preferences.setDefaults(Constants.MODE_OF_PAYMENT, mPaymentMode, getActivity());
                        SessionManager sessionManager = new SessionManager(getActivity());
                        if (sessionManager.isLoggedIn()) {


                            if (mDeliveryCharge != null) {
                                Intent intent = new Intent(getActivity(), DeliveryAddress.class);
                                startActivity(intent);
                            } else {
                                Message("Delivery Charge Not Available. Please Reload.");
                            }

                        } else {
                            Intent intent = new Intent(getActivity(), LoginActivity.class);
                            startActivity(intent);
                        }
                    }
                } else {
                    Message("Cart is Empty. Please Select Items of Your Choice");
                }
            }
        });


        // This Local Broadcast receiver is used to clear the Cart Fragment.
        cartItemReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String mReceivedData = intent.getStringExtra("Cart");
                if (mReceivedData.equalsIgnoreCase("Items")) {
                    ghi = 0; // Making Total Amount to 0
                    fetchData();
                    cartAmoutn();
                }
                String mReceivedDat = intent.getStringExtra("Cart");
                if (mReceivedDat.equalsIgnoreCase("afterPost")) {
                    ghi = 0; // Making Total Amount to 0
                    clearData();
                }
            }
        };


        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(cartItemReceiver, new IntentFilter("clear_cart_itmes"));

        super.onActivityCreated(savedInstanceState);
    }


    public void clearData() {
        if (cartItemList != null) {
            cartItemList.clear();
        }

        int mCartCount = dbHandler.getCartCount();
        dbHandler.close();

        if (mCartCount != 0) {
            List<AddToCart> addToCarts = dbHandler.getCartDetails();
            for (AddToCart addToCart : addToCarts) {
                CartAdapterModel cartAdapterModel = new CartAdapterModel();
                cartAdapterModel.setMItemId(addToCart.getMItem_id());
                cartAdapterModel.setMItemMerchantId(addToCart.getMItem_merchant_id());
                cartAdapterModel.setMItemName(addToCart.getMItem_Name());
                cartAdapterModel.setMItemImage(addToCart.getMItem_Image());
                cartAdapterModel.setMItemPrice(addToCart.getMItem_Price());
                cartAdapterModel.setMItemQuantity(String.valueOf(addToCart.getMItem_Count()));
                cartAdapterModel.setMItemWeight(addToCart.getMItem_Weight());
                cartItemList.add(cartAdapterModel);
            }
            adapter = new CartFragmentAdapter(cartItemList, getActivity(), this);
            recyclerView.setAdapter(adapter);
        } else {
            recyclerView.setVisibility(View.GONE);
            imageViewEmpty.setVisibility(View.VISIBLE);
        }

        // Broadcasting to MainActivity to update the cart count
        Intent intent = new Intent("filter_string");
        intent.putExtra("key", "value");
        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);


    }


    public void Message(String message) {
        LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
        View promptView = layoutInflater.inflate(R.layout.alert_message, null);
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setView(promptView);
        TextView textViewMessage = promptView.findViewById(R.id.alert_message_display);
        textViewMessage.setText(message);
        Button buttonblock = promptView.findViewById(R.id.alert_ok_buton);
        buttonblock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertMessage.dismiss();
            }
        });
        alertMessage = alertDialogBuilder.create();
        alertMessage.show();
    }


    public void getDeliveryCharge() {

//        progress = ProgressDialog.show(getActivity(), "Loading...", "Please wait...", true);

        ApiInterface apiInterfacec = ApiClientUser.getApiClient().create(ApiInterface.class);
        Call<DeliveryCharge> call = apiInterfacec.getDeliverycharge(new RequestBodyDeliveryCharge(mStoreIds));
        call.enqueue(new Callback<DeliveryCharge>() {
            @Override
            public void onResponse(Call<DeliveryCharge> call, retrofit2.Response<DeliveryCharge> response) {
                if (response.isSuccessful()) {
                    String mDel = new Gson().toJson(response.body());
                    try {


                        JSONObject jsonObject = new JSONObject(mDel);
                        Log.d("JsonRersponse",jsonObject.toString());
                        Integer mResponseCode = jsonObject.getInt("responseCode");
                        if (mResponseCode == 200) {
                            mDeliveryCharge = jsonObject.getString("deliveryCharge");
                            mDeliveryFreeUpto = jsonObject.getString("deliveryFreeAbove");

                            Log.d(TAG, "mDeliveryCharge " + mDeliveryCharge);
                            Log.d(TAG, "mDeliveryFreeUpto " + mDeliveryFreeUpto);
                            if (mDeliveryCharge != null) {
                                if (Integer.valueOf(ghi) <= Integer.valueOf(mDeliveryFreeUpto)) {
                                    textViewDeliverycharge.setText(String.valueOf(new StringBuilder("Delivery Amt. Rs : ").append(mDeliveryCharge).append(" Upto Rs : ").append(mDeliveryFreeUpto)));
                                    try {
                                        if (Preferences.getDefaults(Constants.DELIVERY_CHARGE, getActivity()) != null) {
                                            Preferences.deletePrefs(Constants.DELIVERY_CHARGE, getActivity());
                                        }
                                        Preferences.setDefaults(Constants.DELIVERY_CHARGE, mDeliveryCharge, getActivity());
                                    } catch (NullPointerException e) {
                                        e.printStackTrace();
                                    }

                                } else {
                                    try {
                                        if (Preferences.getDefaults(Constants.DELIVERY_CHARGE, getActivity()) != null) {
                                            Preferences.deletePrefs(Constants.DELIVERY_CHARGE, getActivity());
                                        }
                                        Preferences.setDefaults(Constants.DELIVERY_CHARGE, "0", getActivity());
                                    } catch (NullPointerException e) {
                                        e.printStackTrace();
                                    }
                                    textViewDeliverycharge.setText(String.valueOf(new StringBuilder("Delivery Charges : ").append("Free Delivery")));
                                }
                            }
                        } else {
                            mDeliveryCharge = "None";
                            mDeliveryFreeUpto = "None";
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<DeliveryCharge> call, Throwable t) {
                Toast.makeText(getActivity(), "Fsiled", Toast.LENGTH_SHORT).show();
                mDeliveryCharge = "None";
                mDeliveryFreeUpto = "None";
            }
        });
    }


    // Refreshing Cart Amount here. Also getting the delivery charge for the logged in User
    // and displaying in the Cart Fragment.

    public void cartAmoutn() {
        List<AddToCart> cartDetails = dbHandler.getCartDetails();
        for (AddToCart addToCart : cartDetails) {
            mProductName = addToCart.getMItem_Name();
            mPrice = addToCart.getMItem_Price();
            mQuantityCount = String.valueOf(addToCart.getMItem_Count());
            mWeight = addToCart.getMItem_Weight();

            abc = Integer.valueOf(mPrice) * Integer.valueOf(mQuantityCount);
            ghi = abc + ghi;
        }
        textViewAmount.setText(new StringBuilder("Total Amount : Rs. ").append(String.valueOf(ghi)));

        if (AppUtils.internetConnectionAvailable(2000)) {
            getDeliveryCharge();
        } else {
            Toast.makeText(getActivity(), "No Network Connection",
                    Toast.LENGTH_SHORT).show();
        }


        try {
            if (Preferences.getDefaults(Constants.ORDER_TOTAL_AMOUNT, getActivity()) != null) {
                Preferences.deletePrefs(Constants.ORDER_TOTAL_AMOUNT, getActivity());
            }
            Preferences.setDefaults(Constants.ORDER_TOTAL_AMOUNT, String.valueOf(ghi), getActivity());
        } catch (NullPointerException e) {
            e.printStackTrace();
        }


    }


    public void fetchData() {
        if (cartItemList != null) {
            cartItemList.clear();
        }


        SessionManager sessionManager = new SessionManager(getActivity());
        if (sessionManager.isLoggedIn()) {
            if (AppUtils.internetConnectionAvailable(2000)) {
                getCartItems();
            } else {
                Toast.makeText(getActivity(), "No Network Connection",
                        Toast.LENGTH_SHORT).show();
            }

        } else {
            int mCartCount = dbHandler.getCartCount();
            dbHandler.close();

            if (mCartCount != 0) {
                List<AddToCart> addToCarts = dbHandler.getCartDetails();
                for (AddToCart addToCart : addToCarts) {
                    CartAdapterModel cartAdapterModel = new CartAdapterModel();
                    cartAdapterModel.setMItemId(addToCart.getMItem_id());
                    cartAdapterModel.setMItemMerchantId(addToCart.getMItem_merchant_id());
                    cartAdapterModel.setMItemName(addToCart.getMItem_Name());
                    cartAdapterModel.setMItemImage(addToCart.getMItem_Image());
                    cartAdapterModel.setMItemPrice(addToCart.getMItem_Price());
                    cartAdapterModel.setMItemQuantity(String.valueOf(addToCart.getMItem_Count()));
                    cartAdapterModel.setMItemWeight(addToCart.getMItem_Weight());
                    cartItemList.add(cartAdapterModel);
                }
                adapter = new CartFragmentAdapter(cartItemList, getActivity(), this);
                recyclerView.setAdapter(adapter);
            } else {
                recyclerView.setVisibility(View.GONE);
                imageViewEmpty.setVisibility(View.VISIBLE);
            }

        }


    }


    public void getCartItems() {


        SessionManager sessionManager = new SessionManager(getActivity());
        HashMap<String, String> hashMap = sessionManager.getUserDetails();
        String mUserid = hashMap.get(SessionManager.KEY_USER_ID);
        String mDeviceId = hashMap.get(SessionManager.KEY_DEVICE_ID);
        String mSessionId = hashMap.get(SessionManager.KEY_SESSION_ID);

        HashMap<String, String> params = new HashMap<String, String>();
        params.put("userId", mUserid);
        params.put("deviceUniqueId", mDeviceId);
        params.put("sessionId", mSessionId);


        Log.d(TAG, "CartFragParams " + params);

        JsonObjectRequest request_json = new JsonObjectRequest(EndPoint.GET_USER_CART_ITEMS, new JSONObject(params),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, "getCartFragmentResp: " + response);

                        dbHandler.deleteCartItem();


                        // Deleting all the items in cart and store Id and loading the fresh
                        // data which is saved in the server.
                        try {
                            Integer mStoreId = response.getInt("storeId");
                            if (mStoreId == 0) {

                                if (Preferences.getDefaults("postStoreId", getActivity()) != null) {
                                    Preferences.deletePrefs("postStoreId", getActivity());
                                }

                                if (dbHandler.checkIfStoreSelected() == 1) {
                                    dbHandler.deleteSeletedShop();
                                }

                                if (AppUtils.internetConnectionAvailable(2000)) {
                                    deleteSelectedStore();
                                } else {
                                    Toast.makeText(getActivity(), "No Network Connection",
                                            Toast.LENGTH_SHORT).show();
                                }


                            } else {


                                if (Preferences.getDefaults("postStoreId", getActivity()) != null) {
                                    Preferences.deletePrefs("postStoreId", getActivity());
                                }
                                Preferences.setDefaults("postStoreId", String.valueOf(mStoreId), getActivity());

                                if (dbHandler.checkIfStoreSelected() == 1) {
                                    dbHandler.deleteSeletedShop();
                                }

                                CheckSelectedStore checkSelectedStore = new CheckSelectedStore();
                                checkSelectedStore.setMStoreId(mStoreId);
                                dbHandler.selectedShop(checkSelectedStore);

                            }


                            // Sending a message to MainActivity to Refresh The Cart Count in Bottom bar
                            Intent intent = new Intent("filter_string");
                            intent.putExtra("key", "value");
                            LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);


                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (NullPointerException e) {
                            e.printStackTrace();
                        }


                        try {
                            Integer mStatusCode = response.getInt("responseCode");

                            if (mStatusCode == 200) {
                                JSONArray array = response.optJSONArray("userCartItemList");

                                if (array != null) {
                                    for (int i = 0; i < array.length(); i++) {
                                        JSONObject json = array.getJSONObject(i);

                                        String mItemId = json.getString("itemId");
                                        String itemMerchantId = json.getString("itemMerchantId");
                                        String mItemName = json.getString("itemName");
                                        String mItemImage = json.getString("itemImage");
                                        String itemPrice = json.getString("itemPrice");
                                        String itemQuantity = json.getString("itemQuantity");
                                        String totalPrice = json.getString("totalPrice");
                                        String itemWeight = json.getString("itemWeight");


                                        // Saving Values in Sqlite Database
                                        AddToCart addToCart = new AddToCart();
                                        addToCart.setMItem_id(Integer.valueOf(mItemId));
                                        addToCart.setMItem_merchant_id(Integer.valueOf(itemMerchantId));
                                        addToCart.setMItem_Count(Integer.valueOf(itemQuantity));
                                        addToCart.setMItem_Name(mItemName);
                                        addToCart.setMItem_Catagory("null");
                                        addToCart.setMItem_Image(mItemImage);
                                        addToCart.setMItem_Price(itemPrice);
                                        addToCart.setMItem_Weight(itemWeight);
//                                        addToCart.setMItem_individual_total_amount(Integer.valueOf(totalPrice));
                                        addToCart.setMItem_individual_total_amount(420);
                                        dbHandler.addToCart(addToCart);
                                    }
                                }


                                int mCartCount = dbHandler.getCartCount();
                                dbHandler.close();

                                if (mCartCount != 0) {
                                    List<AddToCart> addToCarts = dbHandler.getCartDetails();
                                    for (AddToCart addToCart : addToCarts) {
                                        CartAdapterModel cartAdapterModel = new CartAdapterModel();
                                        cartAdapterModel.setMItemId(addToCart.getMItem_id());
                                        cartAdapterModel.setMItemMerchantId(addToCart.getMItem_merchant_id());
                                        cartAdapterModel.setMItemName(addToCart.getMItem_Name());
                                        cartAdapterModel.setMItemImage(addToCart.getMItem_Image());
                                        cartAdapterModel.setMItemPrice(addToCart.getMItem_Price());
                                        cartAdapterModel.setMItemQuantity(String.valueOf(addToCart.getMItem_Count()));
                                        cartAdapterModel.setMItemWeight(addToCart.getMItem_Weight());
                                        cartItemList.add(cartAdapterModel);
                                    }
                                    adapter = new CartFragmentAdapter(cartItemList, getActivity(), CartFragment.this);
                                    recyclerView.setAdapter(adapter);
                                } else {
                                    recyclerView.setVisibility(View.GONE);
                                    imageViewEmpty.setVisibility(View.VISIBLE);
                                }

                                refreshCartAmount();
                            } else if (mStatusCode == 400) {
                                if (AppUtils.internetConnectionAvailable(2000)) {
                                    deleteSelectedStore();
                                } else {
                                    Toast.makeText(getActivity(), "No Network Connection",
                                            Toast.LENGTH_SHORT).show();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkUtils.UnableToReachServer(getActivity());
            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(request_json);
    }


    private void deleteSelectedStore() {
        try {
            SessionManager sessionManager = new SessionManager(getActivity());
            HashMap<String, String> hashMap = sessionManager.getUserDetails();
            String mUserid = hashMap.get(SessionManager.KEY_USER_ID);
            String mDeviceId = hashMap.get(SessionManager.KEY_DEVICE_ID);
            String mSessionId = hashMap.get(SessionManager.KEY_SESSION_ID);
            HashMap<String, String> params = new HashMap<String, String>();
            params.put("userId", mUserid);
            params.put("deviceUniqueId", mDeviceId);
            params.put("sessionId", mSessionId);
            Log.d(TAG, "DeleteStoreIdParams: " + params);


            JsonObjectRequest request_json = new JsonObjectRequest(EndPoint.DELETE_SELECTED_STORE, new JSONObject(params),
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.d(TAG, "DelteStoreIdResponse: " + response);

                            try {
                                Integer mStatusCode = response.getInt("responseCode");
                                if (mStatusCode == 200) {
                                    if (cartItemList != null) {
                                        cartItemList.clear();
                                    }
                                    // Deleting the local cart items as soon as the items are deleted in the cart
                                    dbHandler.deleteCartItem();
                                    dbHandler.close();
                                    int mCartCount = dbHandler.getCartCount();


                                    if (mCartCount != 0) {
                                        List<AddToCart> addToCarts = dbHandler.getCartDetails();
                                        for (AddToCart addToCart : addToCarts) {
                                            CartAdapterModel cartAdapterModel = new CartAdapterModel();
                                            cartAdapterModel.setMItemId(addToCart.getMItem_id());
                                            cartAdapterModel.setMItemMerchantId(addToCart.getMItem_merchant_id());
                                            cartAdapterModel.setMItemName(addToCart.getMItem_Name());
                                            cartAdapterModel.setMItemImage(addToCart.getMItem_Image());
                                            cartAdapterModel.setMItemPrice(addToCart.getMItem_Price());
                                            cartAdapterModel.setMItemQuantity(String.valueOf(addToCart.getMItem_Count()));
                                            cartAdapterModel.setMItemWeight(addToCart.getMItem_Weight());
                                            cartItemList.add(cartAdapterModel);
                                        }
                                        adapter = new CartFragmentAdapter(cartItemList, getActivity(), CartFragment.this);
                                        recyclerView.setAdapter(adapter);
                                    } else {
                                        recyclerView.setVisibility(View.GONE);
                                        imageViewEmpty.setVisibility(View.VISIBLE);
                                    }


                                } else if (mStatusCode == 400) {
                                    recyclerView.setVisibility(View.GONE);
                                    imageViewEmpty.setVisibility(View.VISIBLE);
                                }


                                refreshCartAmount();

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    recyclerView.setVisibility(View.GONE);
                    imageViewEmpty.setVisibility(View.VISIBLE);
                    NetworkUtils.UnableToReachServer(getActivity());
                }
            });
            try {
                RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
                requestQueue.add(request_json);
            } catch (NullPointerException e) {
                e.printStackTrace();
            }

        }catch (NullPointerException e) {

            Intent intent = new Intent(getActivity(), LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);


            e.printStackTrace();}


    }

    public void refreshCartAmount() {
        abc = 0;
        def = 0;
        ghi = 0;
        cartAmoutn();
        Intent intent = new Intent("filter_string");
        intent.putExtra("key", "value");
        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
    }


    public void callRefresh() {
        abc = 0;
        def = 0;
        ghi = 0;
        fetchData();
        cartAmoutn();
        Intent intent = new Intent("filter_string");
        intent.putExtra("key", "value");
        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
    }
}
