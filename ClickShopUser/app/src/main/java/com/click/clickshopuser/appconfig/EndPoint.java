package com.click.clickshopuser.appconfig;

public interface EndPoint {


//    Server

    //String USER_APP_URL = "http://3.17.138.169:9093/";
    String USER_APP_URL = "http://18.222.124.117:9093/";
    //String USER_APP_URL = "http://3.17.172.163:9093/";
    String MERCHANT_APP_URL = "http://18.222.124.117:9091/";
//    public static String CATGRY = "http://192.168.0.113/clickshopcatagory/";


    //    Local

//    String USER_APP_URL = "http://192.168.0.108:9091/";
//    String MERCHANT_APP_URL = "http://192.168.0.108:8080/";
//

    String CLICK_SHOP_USER_SIGN_IN = USER_APP_URL + "user/signIn";
    String CLICK_SHOP_USER_SIGN_UP = USER_APP_URL + "user/signUp";
    String CLICK_SHOP_USER_FETCH_NEAR_BY_SHOP = USER_APP_URL + "user/storeLocator";
    String IN_Store = USER_APP_URL + "user/searchItemInStore";
    String CLICK_SHOP_USER_STORE_ITMES = USER_APP_URL + "user/searchAllItemsInStore";
    String CLICK_SHOP_USER_RECEIVE_ADDRESS = USER_APP_URL + "user/getAddressType";
    String CLICK_SHOP_USER_SEND_ADDRESS = USER_APP_URL + "user/saveAddressType";
    String SHIPPING_ADDRESS_GET = USER_APP_URL + "user/getUserShippingAddress";
    String SHIPPING_ADDRESS_POSTING = USER_APP_URL + "user/saveUserShippingAddress";
    String SHIPPING_ADDRESS_DELETE = USER_APP_URL + "user/deleteUserShippingAddress";
    String PLACE_ORDER = USER_APP_URL + "user/placeOrder";
    String VIEW_MY_ORDRES = USER_APP_URL + "user/getUserCurrentOrders";
    String OFFER_DETAILS = USER_APP_URL + "user/searchOfferOnStoreItemItem";
    String MY_ORDER_DISPLAY = USER_APP_URL + "user/getUserOrderList";
    String ORDER_ITEMS = USER_APP_URL + "user/searchOrder";
    String SEARCH_ALL_ITEM_IN_STORE_BY_CATAGORY = USER_APP_URL + "user/searchAllItemsInStoreByCategory";
    String CATAGORY_SEARCH = USER_APP_URL + "user/searchItemInGalleryByCategory";
    String CATEGORY_LISTING = MERCHANT_APP_URL + "clickshop/getItemsCategory";
    String ADD_CART_ITEM = USER_APP_URL + "user/addItemUserCart";
    String GET_USER_CART_ITEMS = USER_APP_URL + "user/getUserCart";
    String SAVE_USER_CART = USER_APP_URL + "user/saveUserCart";
    String SELECTED_STORE_ID = USER_APP_URL + "user/saveUserSelectedStore";
    String DELETE_CART_ITEM = USER_APP_URL + "user/deleteUserCartItem";
    String DELETE_SELECTED_STORE = USER_APP_URL + "user/deleteUserSelectedStore";
    String FORGOT_PASSWORD = USER_APP_URL + "user/forgetPassword";


}




