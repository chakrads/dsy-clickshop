package com.click.clickshopuser.main.activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.click.clickshopuser.R;
import com.click.clickshopuser.appconfig.AppUtils;
import com.click.clickshopuser.appconfig.Constants;
import com.click.clickshopuser.appconfig.EndPoint;
import com.click.clickshopuser.main.adapter.NearByShopFrontCatagoryAdapter;
import com.click.clickshopuser.main.bottombar.CartActivity;
import com.click.clickshopuser.model.HomeCatagory;
import com.click.clickshopuser.model.SearchAllStoreItems;
import com.click.clickshopuser.network.NetworkUtils;
import com.click.clickshopuser.storage.Preferences;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class NearByShopFrontCatagory extends AppCompatActivity  implements SwipeRefreshLayout.OnRefreshListener{
    private static final String TAG = NearByShopFrontCatagory.class.getSimpleName();
    private Toolbar toolbar;
    EditText editTextSearch;
    private String newquery, mSubString;
    public RelativeLayout relativeLayoutLoadingCatagory;
    public NearByShopFrontCatagoryAdapter adapterCatagory;
    public LinearLayoutManager linearLayoutManager;
    public SwipeRefreshLayout swipeRefreshLayout;
    public RecyclerView recyclerView;
    public List<HomeCatagory> catagoryListing;
    String mStoreId;
    String mStoreName;
    ImageView _user_login;
    public List<SearchAllStoreItems> storeItemList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.fadein, R.anim.fadeout);
        setContentView(R.layout.activity_near_by_shop_front_catagory);


        storeItemList=new ArrayList<>();

        // Showing Status bar
        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }

        toolbar = findViewById(R.id.catagoryactivity_toolbar);
        setSupportActionBar(toolbar);
        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        getSupportActionBar().setDisplayShowHomeEnabled(false);
        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_arrow_back_black_24dp));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        _user_login=(ImageView)findViewById(R.id._user_login);

        _user_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i =new Intent(NearByShopFrontCatagory.this,CartActivity.class);
                startActivity(i);

            }
        });
        relativeLayoutLoadingCatagory = findViewById(R.id._loading_catagory);
        swipeRefreshLayout = findViewById(R.id.cat_refres);
        swipeRefreshLayout.setOnRefreshListener(this);
        recyclerView = findViewById(R.id._recyclerview_cat);
        linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 3));
        catagoryListing = new ArrayList<>();

        editTextSearch = findViewById(R.id._search);
        editTextSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence query, int start, int before, int count) {
                if (count >= 2) {
                    mSubString = String.valueOf(query);
                    newquery = mSubString.substring(0, 1).toUpperCase() + mSubString.substring(1);
                } else {
                    mSubString = String.valueOf(query);
                    newquery = query.toString().toUpperCase();
                }

                final List<HomeCatagory> filteredList = new ArrayList<>();

                if (catagoryListing != null) {
                    for (int i = 0; i < catagoryListing.size(); i++) {
                        try {
                            final String text = catagoryListing.get(i).getMCatagoryName();
                            if (text.contains(newquery)) {
                                filteredList.add(catagoryListing.get(i));
                                updateRecyclerView(filteredList, false);
                            }
                        } catch (NullPointerException e) {
                            e.printStackTrace();
                        }
                    }
                }


            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        if (AppUtils.internetConnectionAvailable(2000)) {
            mFetchCatagory();
        } else {
            Toast.makeText(getApplicationContext(), "No Network Connection",
                    Toast.LENGTH_SHORT).show();
        }


        getInent();
    }
    public void getInent() {
        Intent intent = getIntent();
        try {
            mStoreId = intent.getStringExtra("mStoreId");
            mStoreName = intent.getStringExtra("mStoreName");

        } catch (NullPointerException e) {
            e.printStackTrace();
        }

    }

    public void updateRecyclerView(List<HomeCatagory> lst, boolean swipeStatus) {
        recyclerView = findViewById(R.id._recyclerview_cat);
        recyclerView.setHasFixedSize(true);
        linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 3));
        adapterCatagory = new NearByShopFrontCatagoryAdapter(lst, this);
        recyclerView.setAdapter(adapterCatagory);
        adapterCatagory.notifyDataSetChanged();
        swipeRefreshLayout.setRefreshing(swipeStatus);
    }

    @Override
    public void onRefresh() {
        if (catagoryListing != null) {
            catagoryListing.clear();
        }
        relativeLayoutLoadingCatagory.setVisibility(View.GONE);
        recyclerView.setVisibility(View.GONE);
        if (AppUtils.internetConnectionAvailable(2000)) {
            mFetchCatagory();
        } else {
            Toast.makeText(getApplicationContext(), "No Network Connection",
                    Toast.LENGTH_SHORT).show();
        }

    }

    public void mFetchCatagory() {
        if (catagoryListing != null) {
            catagoryListing.clear();
        }

        HashMap<String, String> params = new HashMap<String, String>();
        // params.put("userId", "1");


        Log.d(TAG, "params Catagory: " + params);

        JsonObjectRequest request_json = new JsonObjectRequest(EndPoint.CATEGORY_LISTING, new JSONObject(params),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, "responseCatagory: " + response);
                        relativeLayoutLoadingCatagory.setVisibility(View.GONE);
                        recyclerView.setVisibility(View.VISIBLE);
                        try {
                            Integer mStatusCode = response.getInt("responseCode");
                            if (mStatusCode == 200) {
                                JSONArray arrays = response.optJSONArray("data");
                                parseCatagory(arrays);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                relativeLayoutLoadingCatagory.setVisibility(View.GONE);
                NetworkUtils.UnableToReachServer(getApplicationContext());
            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(request_json);
    }


    private void parseCatagory(JSONArray array) {




        for (int i = 0; i < array.length(); i++) {
            final HomeCatagory homeCatagory = new HomeCatagory();
            final JSONObject json;
            try {
                json = array.getJSONObject(i);





                if (storeItemList != null) {
                    storeItemList.clear();

                }

                String mStoreId = Preferences.getDefaults("postStoreId", getApplicationContext());
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("storeId", mStoreId);
                params.put("itemCategoryId", String.valueOf(json.getInt("itemCategoryId")));

                Log.d(TAG, "ProductStoreItmes: " + params);

                Log.d("CategoryIds",mStoreId);
                Log.d("CategoryIds",String.valueOf(json.getInt("itemCategoryId")));

                JsonObjectRequest request_json = new JsonObjectRequest(EndPoint.SEARCH_ALL_ITEM_IN_STORE_BY_CATAGORY, new JSONObject(params),
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                Log.d(TAG, "product response: " + response);
                                recyclerView.setVisibility(View.VISIBLE);

                                try {
                                    Integer mStatusCode = response.getInt("responseCode");
                                    if (mStatusCode == 200) {
                                      //  JSONArray array = response.optJSONArray("storeSearchItemDataList");



                                        if (json.getString("itemCategoryId") != null && !json.getString("itemCategoryId").isEmpty()
                                                && !json.getString("itemCategoryId").equals("itemCategoryId")) {
                                            homeCatagory.setMCatagoryId((json.getInt("itemCategoryId")));
                                        }

                                        if (json.getString("itemCategoryName") != null && !json.getString("itemCategoryName").isEmpty()
                                                && !json.getString("itemCategoryName").equals("itemCategoryName")) {
                                            homeCatagory.setMCatagoryName((json.getString("itemCategoryName")));
                                        }

                                        if (json.getString("itemCategoryImage") != null && !json.getString("itemCategoryImage").isEmpty()
                                                && !json.getString("itemCategoryImage").equals("itemCategoryImage")) {

                                            homeCatagory.setMCatagoryImage(json.getString(Constants.GET_FILTER_CATEGORY_IMAGE));
                                        }

                                        catagoryListing.add(homeCatagory);



                                    } else if (mStatusCode == 400) {
                                       // Toast.makeText(NearByShopFrontCatagory.this, "No Items in the Store", Toast.LENGTH_SHORT).show();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }


                                adapterCatagory = new NearByShopFrontCatagoryAdapter(catagoryListing, getApplicationContext());
                                recyclerView.setAdapter(adapterCatagory);
                                swipeRefreshLayout.setRefreshing(false);
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        recyclerView.setVisibility(View.GONE);
                        NetworkUtils.UnableToReachServer(getApplicationContext());
                    }
                });
                RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
                requestQueue.add(request_json);









            } catch (JSONException e) {
                e.printStackTrace();
            }


        }

    }








    public void SearchStoreItems() {
    }

}
