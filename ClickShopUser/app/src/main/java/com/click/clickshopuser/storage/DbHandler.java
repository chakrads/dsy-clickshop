package com.click.clickshopuser.storage;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import com.click.clickshopuser.model.AddToCart;
import com.click.clickshopuser.model.CheckSelectedStore;
import com.click.clickshopuser.model.GeoLocation;
import java.util.ArrayList;

public class DbHandler extends SQLiteOpenHelper {

    private static DbHandler mInstance;
    private static final String TAG = DbHandler.class.getSimpleName();
    private static final int DATABASE_VERSION = 3;
    private static final String DATABASE_NAME = "click_shop_shopping";
    private static final String TABLE_CART = "click_shop_cart";
    private static final String INCREMENT_ID = "increment_id";
    private static final String ITEM_ID = "item_id";
    private static final String ITEM_MERCHANT_ID = "item_merchant_id";
    private static final String ITEM_COUNT = "item_count";
    private static final String ITEM_NAME = "item_name";
    private static final String ITEM_CATAGORY = "item_catagory";
    private static final String ITEM_IMAGE_NAME = "itmephoto";
    private static final String ITEM_PRICE = "item_price";
    private static final String ITEM_WEIGHT = "item_weight";
    private static final String ITEM_INDIVIDUAL_TOTAL_AMOUNT = "item_individual_total_amount";
    private static final String TABLE_SELECTED_SHOP = "click_shop_selected";
    private static final String STORE_ID = "selected_store_id";
    private static final String STORE_NAME = "selected_store_name";
    private static final String TABLE_GEO_LOCATION = "table_geo_loation";
    private static final String GEO_ID = "geo_id";
    private static final String GEO_LATITUDE = "geo_latitude";
    private static final String GEO_LONGITUDE = "geo_longitude";
    private static final String GEO_ADDRESS = "geo_address";
    private static final String GEO_CITY = "geo_city";
    private static final String GEO_COUNTRY = "geo_country";
    private static final String GEO_PINCODE = "geo_pincode";

    public static synchronized DbHandler getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new DbHandler(context.getApplicationContext());
        }
        return mInstance;
    }

    public DbHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String CREATE_USER = "CREATE TABLE IF NOT EXISTS " + TABLE_CART + "(" +
                INCREMENT_ID + " INTEGER PRIMARY KEY, " +
                ITEM_ID + " VARCHAR(233)  , " +
                ITEM_MERCHANT_ID + " VARCHAR(233)  , " +
                ITEM_COUNT + " INTEGER , " +
                ITEM_NAME + " VARCHAR(233)  , " +
                ITEM_CATAGORY + " VARCHAR(233)  , " +
                ITEM_IMAGE_NAME + " VARCHAR(233)  , " +
                ITEM_PRICE + " VARCHAR(233)  , " +
                ITEM_WEIGHT + " VARCHAR(233)  , " +
                ITEM_INDIVIDUAL_TOTAL_AMOUNT + " VARCHAR(233)  );";
        sqLiteDatabase.execSQL(CREATE_USER);

        String CREATE_SELECTED_SHOP = "CREATE TABLE IF NOT EXISTS " + TABLE_SELECTED_SHOP + "(" +
                STORE_ID + " VARCHAR(233)  NOT NULL);";
        sqLiteDatabase.execSQL(CREATE_SELECTED_SHOP);

        String CREATE_GEO_LOCATION = "CREATE TABLE IF NOT EXISTS " + TABLE_GEO_LOCATION + "(" +
                GEO_LATITUDE + " VARCHAR(233)  , " +
                GEO_LONGITUDE + " VARCHAR(233) , " +
                GEO_ADDRESS + " VARCHAR(233)  , " +
                GEO_CITY + " VARCHAR(233)  , " +
                GEO_COUNTRY + " VARCHAR(233)  , " +
                GEO_PINCODE + " VARCHAR(233)  );";
        sqLiteDatabase.execSQL(CREATE_GEO_LOCATION);
    }


    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_CART);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_SELECTED_SHOP);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_GEO_LOCATION);
        onCreate(sqLiteDatabase);

    }


    public void addGeoLocation(GeoLocation geoLocation) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(GEO_LATITUDE, geoLocation.getMLatitude());
        values.put(GEO_LONGITUDE, geoLocation.getMLongitude());
        values.put(GEO_ADDRESS, geoLocation.getMAddress());
        values.put(GEO_CITY, geoLocation.getMCity());
        values.put(GEO_COUNTRY, geoLocation.getMCountry());
        values.put(GEO_PINCODE, geoLocation.getMPinCode());

        long id = db.insert(TABLE_GEO_LOCATION, null, values);
        Log.d(TAG, "Data inserted successfully - Geo Location: " + id);
        db.close();
    }


    public ArrayList<GeoLocation> getGeoLocation() {
        ArrayList<GeoLocation> geoLocations = new ArrayList<GeoLocation>();
        String selectedQuery = "SELECT * FROM " + TABLE_GEO_LOCATION;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectedQuery, null);
        if (cursor.moveToFirst()) {
            do {
                GeoLocation GeoLocation = new GeoLocation();
                GeoLocation.setMLatitude(cursor.getString(0));
                GeoLocation.setMLongitude(cursor.getString(1));
                GeoLocation.setMAddress(cursor.getString(2));
                GeoLocation.setMCity(cursor.getString(3));
                GeoLocation.setMCountry(cursor.getString(4));
                GeoLocation.setMPinCode(cursor.getString(5));
                geoLocations.add(GeoLocation);

            } while (cursor.moveToNext());
            cursor.close();
        }
        return geoLocations;
    }



    public int checkLocationExists() {
        int rowUserCount;
        String selectQuery = "SELECT  * FROM " + TABLE_GEO_LOCATION;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor != null && cursor.getCount() >= 0) {
            rowUserCount = 1;
        } else {
            rowUserCount = 0;
        }
        db.close();
        return rowUserCount;
    }


    public void deleteGeoLocation() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_GEO_LOCATION, null, null);
        db.close();
    }


    public void addToCart(AddToCart add_to_cart) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(ITEM_ID, add_to_cart.getMItem_id());
        values.put(ITEM_MERCHANT_ID, add_to_cart.getMItem_merchant_id());
        values.put(ITEM_COUNT, add_to_cart.getMItem_Count());
        values.put(ITEM_NAME, add_to_cart.getMItem_Name());
        values.put(ITEM_CATAGORY, add_to_cart.getMItem_Catagory());
        values.put(ITEM_IMAGE_NAME, add_to_cart.getMItem_Image());
        values.put(ITEM_PRICE, add_to_cart.getMItem_Price());
        values.put(ITEM_WEIGHT, add_to_cart.getMItem_Weight());
        values.put(ITEM_INDIVIDUAL_TOTAL_AMOUNT, add_to_cart.getMItem_individual_total_amount());

        long id = db.insert(TABLE_CART, null, values);
        Log.d(TAG, "Data inserted successfully - Pricing: " + id);
        db.close();
    }


    public ArrayList<AddToCart> getCartDetails() {
        ArrayList<AddToCart> userProfilesList = new ArrayList<AddToCart>();
        String selectedQuery = "SELECT * FROM " + TABLE_CART;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectedQuery, null);
        if (cursor.moveToFirst()) {
            do {
                AddToCart add_to_cart = new AddToCart();
                add_to_cart.setMId(cursor.getInt(0));
                add_to_cart.setMItem_id(cursor.getInt(1));
                add_to_cart.setMItem_merchant_id(cursor.getInt(2));
                add_to_cart.setMItem_Count(cursor.getInt(3));
                add_to_cart.setMItem_Name(cursor.getString(4));
                add_to_cart.setMItem_Catagory(cursor.getString(5));
                add_to_cart.setMItem_Image(cursor.getString(6));
                add_to_cart.setMItem_Price(cursor.getString(7));
                add_to_cart.setMItem_Weight(cursor.getString(8));
                add_to_cart.setMItem_individual_total_amount(cursor.getInt(9));
                userProfilesList.add(add_to_cart);
            } while (cursor.moveToNext());
            cursor.close();
        }
        return userProfilesList;
    }


    public ArrayList<AddToCart> getParticualrColoumn(Integer count) {
        ArrayList<AddToCart> userProfilesList = new ArrayList<AddToCart>();
        String sql = "SELECT * FROM click_shop_cart WHERE item_id = '" + count + "'";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(sql, null);

        if (cursor.moveToFirst()) {
            do {
                AddToCart add_to_cart = new AddToCart();
                add_to_cart.setMId(cursor.getInt(0));
                add_to_cart.setMItem_id(cursor.getInt(1));
                add_to_cart.setMItem_merchant_id(cursor.getInt(2));
                add_to_cart.setMItem_Count(cursor.getInt(3));
                add_to_cart.setMItem_Name(cursor.getString(4));
                add_to_cart.setMItem_Catagory(cursor.getString(5));
                add_to_cart.setMItem_Image(cursor.getString(6));
                add_to_cart.setMItem_Price(cursor.getString(7));
                add_to_cart.setMItem_Weight(cursor.getString(8));
                 add_to_cart.setMItem_individual_total_amount(cursor.getInt(9));
                userProfilesList.add(add_to_cart);

            } while (cursor.moveToNext());
            cursor.close();
        }
        return userProfilesList;
    }


    public void updateitem(int id, int mCount) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("UPDATE click_shop_cart SET item_count= " + mCount + " WHERE item_id=" + id + " ");

    }

    public void updateIndividualItemPrice(int id, int mItemTotalAmt) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("UPDATE click_shop_cart SET item_individual_total_amount= " + mItemTotalAmt + " WHERE item_id=" + id + " ");

    }

    public void deleteCartRow(int mItemId) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM " + TABLE_CART + " WHERE " + ITEM_ID + "='" + mItemId + "'");
        db.close();
    }


    public void deleteCartItem() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_CART, null, null);
        db.close();
    }


    public int checkIfTableIsEmpty() {
        int rowUserCount;
        String selectQuery = "SELECT  * FROM " + TABLE_CART;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor != null && cursor.getCount() > 0) {
            rowUserCount = 1;
        } else {
            rowUserCount = 0;
        }
        db.close();
        return rowUserCount;
    }

    public int getCartCount() {
        String countQuery = "SELECT  * FROM " + TABLE_CART;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        int count = cursor.getCount();
        cursor.close();
        return count;
    }


    public void selectedShop(CheckSelectedStore checkSelectedStore) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(STORE_ID, checkSelectedStore.getMStoreId());
        long id = db.insert(TABLE_SELECTED_SHOP, null, values);
        Log.d(TAG, "SELECTED STORE: " + id);
        db.close();
    }

    public ArrayList<CheckSelectedStore> getSelectedStore() {
        ArrayList<CheckSelectedStore> selectedStores = new ArrayList<CheckSelectedStore>();
        String selectedQuery = "SELECT * FROM " + TABLE_SELECTED_SHOP;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectedQuery, null);
        if (cursor.moveToFirst()) {
            do {
                CheckSelectedStore selectedStore = new CheckSelectedStore();
                selectedStore.setMStoreId(cursor.getInt(0));
                selectedStores.add(selectedStore);

            } while (cursor.moveToNext());
            cursor.close();
        }
        return selectedStores;
    }

    public int checkIfStoreSelected() {
        int rowUserCount;
        String selectQuery = "SELECT  * FROM " + TABLE_SELECTED_SHOP;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor != null && cursor.getCount() > 0) {
            rowUserCount = 1;
        } else {
            rowUserCount = 0;
        }
        db.close();
        return rowUserCount;
    }


    public int checkRowCountSelectedShop() {
        int rowUserCount;
        String selectQuery = "SELECT  * FROM " + TABLE_SELECTED_SHOP;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor != null && cursor.getCount() == 0) {
            rowUserCount = 1;
        } else {
            rowUserCount = 0;
        }
        db.close();
        return rowUserCount;
    }

    public void deleteSeletedShop() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_SELECTED_SHOP, null, null);
        db.close();
    }


}