
package com.click.clickshopuser.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Datum {

    @SerializedName("itemCategoryId")
    @Expose
    private int itemCategoryId;
    @SerializedName("itemCategoryName")
    @Expose
    private String itemCategoryName;
    @SerializedName("itemCategoryImage")
    @Expose
    private String itemCategoryImage;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Datum() {
    }

    /**
     * 
     * @param itemCategoryName
     * @param itemCategoryImage
     * @param itemCategoryId
     */
    public Datum(int itemCategoryId, String itemCategoryName, String itemCategoryImage) {
        super();
        this.itemCategoryId = itemCategoryId;
        this.itemCategoryName = itemCategoryName;
        this.itemCategoryImage = itemCategoryImage;
    }

    public int getItemCategoryId() {
        return itemCategoryId;
    }

    public void setItemCategoryId(int itemCategoryId) {
        this.itemCategoryId = itemCategoryId;
    }

    public String getItemCategoryName() {
        return itemCategoryName;
    }

    public void setItemCategoryName(String itemCategoryName) {
        this.itemCategoryName = itemCategoryName;
    }

    public String getItemCategoryImage() {
        return itemCategoryImage;
    }

    public void setItemCategoryImage(String itemCategoryImage) {
        this.itemCategoryImage = itemCategoryImage;
    }

}
