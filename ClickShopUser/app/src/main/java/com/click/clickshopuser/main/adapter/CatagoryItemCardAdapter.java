package com.click.clickshopuser.main.adapter;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.click.clickshopuser.R;
import com.click.clickshopuser.main.activity.SearchProducts;
import com.click.clickshopuser.model.categoryitemmodel.ItemDataList;
import com.click.clickshopuser.storage.DbHandler;
import com.click.clickshopuser.volley.CustomVolleyRequest;
import java.util.List;

public class CatagoryItemCardAdapter extends RecyclerView.Adapter<CatagoryItemCardAdapter.ViewHolder> {
    NetworkImageView networkImageView;
    ImageLoader imageLoader;
    DbHandler dbHandler;
    View v;

    private static final String TAG = CatagoryItemCardAdapter.class.getSimpleName();
    Context context;
    List<ItemDataList> listInAss;

    public CatagoryItemCardAdapter(List<ItemDataList> listInAss, Context context) {
        super();
        this.listInAss = listInAss;
        this.context = context;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cart_item_card_adapter, parent, false);
        dbHandler = DbHandler.getInstance(context);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public int getItemCount() {
        return listInAss.size();
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final ItemDataList cartAdapterModel = listInAss.get(position);

        final String image = modifyDropboxUrl(cartAdapterModel.getItemImage());
        imageLoader = CustomVolleyRequest.getInstance(context).getImageLoader();
        imageLoader.get(image, ImageLoader.getImageListener(networkImageView, R.drawable.noimage,
                R.drawable.noimage));
        networkImageView.setImageUrl(image, imageLoader);
        holder.textViewItemName.setText(cartAdapterModel.getItemName());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String mItemName = cartAdapterModel.getItemName();
                Intent intent = new Intent(context, SearchProducts.class);
                intent.putExtra("itemName", mItemName);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                context.startActivity(intent);
                ((AppCompatActivity)v.getContext()).finish();
            }
        });

    }


    public static String modifyDropboxUrl(String originalUrl) {
        String newUrl = originalUrl.replace("www.dropbox.", "dl.dropboxusercontent.");
        newUrl = newUrl.replace("dropbox.", "dl.dropboxusercontent.");
        return newUrl;
    }


    class ViewHolder extends RecyclerView.ViewHolder {
        TextView textViewItemName;
        private ViewHolder(View itemView) {
            super(itemView);
            networkImageView = itemView.findViewById(R.id._cat_item_image);
            textViewItemName = itemView.findViewById(R.id._catagory_Item_name);
            this.setIsRecyclable(false);
        }
    }
}





