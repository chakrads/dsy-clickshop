package com.click.clickshopuser.appconfig;

/**
 * Created by Yashpal on 12/8/2017.
 */

public interface Constants {

    // Used for Notifications
    String CHANNEL_ID = "my_channel_01";
    String CHANNEL_NAME = "ClickShop";
    String CHANNEL_DESCRIPTION = "www.clickshop.com";

    String CLICK_SHOP_RESPONSE_CODE = "responseCode";
    String CLICK_SHOP_RESPONSE_MESSAGE = "responseMessage";
    String CLICK_SHOP_USER_ID = "userId";
    String CLICK_SHOP_MOBILE_NO = "mobileNumber";
    String CLICK_SHOP_E_MAIL = "email";
    String CLICK_SHOP_FIRST_NAME = "firstName";
    String CLICK_SHOP_LAST_NAME = "lastName";
    String CLICK_SHOP_REFERRAL_CODE = "referralCode";
    String GET_FILTER_CATEGORY_IMAGE= "itemCategoryImage";
    String SESSION_ID = "sessionId";
    String RESPONSE_DEVICE_ID = "deviceUniqueId";

    String NULL = "null";

    // Login Activity
    String PROGRESS_SIGNING_IN = "Signing in...";
    String PLEASE_WAIT = "Please wait...";
    String POST_PARAMETER_MOBILE_NUMBER = "mobileNumber";
    String POST_PARAMETER_PASS = "password";
    String FIREBASE_TOKEN = "firebaseToken";
    String DEVICE_UNIQUE_ID = "deviceUniqueId";
    String DEVICE_TYPE = "deviceType";


    // CartFragment
    String DELIVERY_CHARGE = "deliverycharge";


    //  adapter/CatagoryItemActivityCardAdapter
    String PARAM_ITEM_CATAGORY = "itemCategory";

    //  adapter/OfferFragmentCardAdapter
    String POST_STORE_ID = "postStoreId";

    // DeliveryAddress
    String MODE_OF_PAYMENT = "ModeOfPayment";
    String ORDER_TOTAL_AMOUNT = "OrderTotalAmount";

    // SearchAllStoreItemAdapter
    String ITEM_ID = "mItemId";













}
