
package com.click.clickshopuser.model.deliverycharge;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DeliveryCharge {

    @SerializedName("responseCode")
    @Expose
    private Integer responseCode;
    @SerializedName("responseMessage")
    @Expose
    private String responseMessage;
    @SerializedName("deliveryCharge")
    @Expose
    private Object deliveryCharge;
    @SerializedName("deliveryFreeAbove")
    @Expose
    private Object deliveryFreeAbove;

    public Integer getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(Integer responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public Object getDeliveryCharge() {
        return deliveryCharge;
    }

    public void setDeliveryCharge(Object deliveryCharge) {
        this.deliveryCharge = deliveryCharge;
    }

    public Object getDeliveryFreeAbove() {
        return deliveryFreeAbove;
    }

    public void setDeliveryFreeAbove(Object deliveryFreeAbove) {
        this.deliveryFreeAbove = deliveryFreeAbove;
    }

}
