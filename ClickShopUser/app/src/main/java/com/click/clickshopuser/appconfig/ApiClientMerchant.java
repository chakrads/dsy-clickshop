package com.click.clickshopuser.appconfig;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClientMerchant {


    // Server
    public static final String BASE_URL = "http://18.222.124.117:9091/clickshop/";


    // Local
//        public static final String BASE_URL = "http://192.168.0.108:9091/clickshop/";



    public static Retrofit retrofit = null;


    public static Retrofit getApiClient () {
        if (retrofit == null){
            retrofit = new Retrofit.Builder().baseUrl(BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        }
        return retrofit;
    }



}
