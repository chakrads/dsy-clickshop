package com.click.clickshopuser.main.activity;
import android.content.Intent;
import android.location.Location;
import android.os.Build;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.click.clickshopuser.R;
import com.click.clickshopuser.appconfig.AppUtils;
import com.click.clickshopuser.appconfig.EndPoint;
import com.click.clickshopuser.main.adapter.SearchProductCardAdapter;
import com.click.clickshopuser.model.GeoLocation;
import com.click.clickshopuser.model.SearchProductCardModel;
import com.click.clickshopuser.network.NetworkUtils;
import com.click.clickshopuser.storage.DbHandler;
import com.google.android.gms.maps.model.LatLng;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class SearchProducts extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {
    private static final String TAG = SearchProducts.class.getSimpleName();
    Toolbar toolbar;
    String mLatitude, mLongitude, mAddress, mCity, mCountry, mPinCode;
    EditText editTextSearch;
    Button buttonSearch;
    private AlertDialog alert;
    public RelativeLayout relativeLayoutLoading;
    public SwipeRefreshLayout swipeRefreshLayout;
    public RecyclerView recyclerView;
    public List<SearchProductCardModel> storeItemList;
    public SearchProductCardAdapter adapter;
    public LinearLayoutManager linearLayoutManager;
    public RelativeLayout relativeLayoutShopping;
    String mReceivedLongitude;
    String mReceivedLatitude;
    LatLng mStart, mEnd;
    DbHandler dbHandler;
    String mItemName;
    String mSearch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.fadein, R.anim.fadeout);
        setContentView(R.layout.activity_search_products);
        dbHandler = DbHandler.getInstance(getApplicationContext());

        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }

        toolbar = findViewById(R.id.search_product_toolbar);
        setSupportActionBar(toolbar);
        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_arrow_back_black_24dp));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        getSharedPrefData();
        getInent();

        editTextSearch = findViewById(R.id._search);
        buttonSearch = findViewById(R.id._button_search);
        buttonSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mItemName = "Nill";

                AppUtils.hideKeyboard(SearchProducts.this);
                if (editTextSearch.getText().length() == 0) {
                    Message("Please Enter..");
                } else {
                    if (AppUtils.internetConnectionAvailable(2000)) {
                        SearchProuct();
                    } else {
                        Toast.makeText(getApplicationContext(), "No Network Connection",
                                Toast.LENGTH_SHORT).show();
                    }

                }
            }
        });

        relativeLayoutLoading = findViewById(R.id._loading_store_items);
        relativeLayoutLoading.setVisibility(View.GONE);
        swipeRefreshLayout = findViewById(R.id.store_item_refresh);
        relativeLayoutShopping = findViewById(R.id._loading_shopping);
        swipeRefreshLayout.setOnRefreshListener(this);
        recyclerView = findViewById(R.id._recyclerview_store_items);
        linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        storeItemList = new ArrayList<>();
        if (mItemName.equalsIgnoreCase("None")) {

        } else {
            mSearch = mItemName;
            if (AppUtils.internetConnectionAvailable(2000)) {
                SearchProuct();
            } else {
                Toast.makeText(getApplicationContext(), "No Network Connection",
                        Toast.LENGTH_SHORT).show();
            }

        }
    }

    public void getInent() {
        Intent intent = getIntent();
        try {
            mItemName = intent.getStringExtra("itemName");
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRefresh() {
        relativeLayoutLoading.setVisibility(View.GONE);
        recyclerView.setVisibility(View.GONE);
        storeItemList.clear();
        if (AppUtils.internetConnectionAvailable(2000)) {
            SearchProuct();
        } else {
            Toast.makeText(getApplicationContext(), "No Network Connection",
                    Toast.LENGTH_SHORT).show();
        }

    }

    public void getSharedPrefData() {
        List<GeoLocation> geoLocations = dbHandler.getGeoLocation();
        for (GeoLocation geoLocation : geoLocations) {
            mLatitude = geoLocation.getMLatitude();
            mLongitude = geoLocation.getMLongitude();
            mAddress = geoLocation.getMAddress();
            mCity = geoLocation.getMCity();
            mCountry = geoLocation.getMCountry();
            mPinCode = geoLocation.getMPinCode();
        }
    }

    public void SearchProuct() {
        relativeLayoutShopping.setVisibility(View.GONE);
        if (storeItemList != null) {
            storeItemList.clear();
        }
        relativeLayoutLoading.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.GONE);

        if (mItemName.equalsIgnoreCase("Nill")) {
            mSearch = editTextSearch.getText().toString().trim();
        }

        HashMap<String, String> params = new HashMap<String, String>();
        params.put("itemName", mSearch);
        params.put("latitude", mLatitude);
        params.put("longitude", mLongitude);
        Log.d(TAG, "ProductParamenterPosting: " + params);

        JsonObjectRequest request_json = new JsonObjectRequest(EndPoint.IN_Store, new JSONObject(params),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, "ProductResponse: " + response);

                        try {
                            Integer mStatusCode = response.getInt("responseCode");
                            if (mStatusCode == 200) {
                                String mResponseMessage = response.getString("responseMessage");
                                Toast.makeText(getApplicationContext(), mResponseMessage, Toast.LENGTH_SHORT).show();
                                relativeLayoutLoading.setVisibility(View.GONE);
                                recyclerView.setVisibility(View.VISIBLE);
                                JSONArray array = response.optJSONArray("storeList");
                                parseData(array);
                            } else if (mStatusCode == 400) {
                                relativeLayoutLoading.setVisibility(View.GONE);
                                recyclerView.setVisibility(View.GONE);
                                relativeLayoutShopping.setVisibility(View.VISIBLE);
                                Message("Item Not Available in Any Store Within 3 Kms Range");
                            } else {
                                relativeLayoutLoading.setVisibility(View.GONE);
                                recyclerView.setVisibility(View.GONE);
                                Message("Error. Please Retry..");
                                relativeLayoutShopping.setVisibility(View.VISIBLE);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                relativeLayoutLoading.setVisibility(View.GONE);
                recyclerView.setVisibility(View.GONE);
                relativeLayoutShopping.setVisibility(View.VISIBLE);
                NetworkUtils.UnableToReachServer(getApplicationContext());
            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(request_json);
    }


    private void parseData(JSONArray array) {
        for (int i = 0; i < array.length(); i++) {
            SearchProductCardModel showDifferentStoreItems = new SearchProductCardModel();
            JSONObject json;
            try {
                json = array.getJSONObject(i);
                if (json.getString("storeId") != null && !json.getString("storeId").isEmpty()
                        && !json.getString("storeId").equals("storeId")) {
                    showDifferentStoreItems.setMStoreId((json.getInt("storeId")));
                }
                if (json.getString("latitude") != null && !json.getString("latitude").isEmpty()
                        && !json.getString("latitude").equals("latitude")) {
                    showDifferentStoreItems.setMLattitude((json.getString("latitude")));
                    mReceivedLatitude = json.getString("latitude");
                }
                if (json.getString("longitude") != null && !json.getString("longitude").isEmpty()
                        && !json.getString("longitude").equals("longitude")) {
                    showDifferentStoreItems.setMLongitude((json.getString("longitude")));
                    mReceivedLongitude = json.getString("longitude");
                }
                if (json.getString("deliveryDuration") != null && !json.getString("deliveryDuration").isEmpty()
                        && !json.getString("deliveryDuration").equals("deliveryDuration")) {
                    showDifferentStoreItems.setMDeliveryDuration(json.getString("deliveryDuration"));
                }
                if (json.getString("storeRating") != null && !json.getString("storeRating").isEmpty()
                        && !json.getString("storeRating").equals("storeRating")) {
                    showDifferentStoreItems.setMStoreRatings(json.getString("storeRating"));
                }
                if (json.getString("storeName") != null && !json.getString("storeName").isEmpty()
                        && !json.getString("storeName").equals("storeName")) {
                    showDifferentStoreItems.setMStoreName(json.getString("storeName"));
                }
                if (json.getJSONArray("storeSearchItemData") != null && !json.getJSONArray("storeSearchItemData").equals("null")) {
                    try {
                        JSONArray jsonArray = json.getJSONArray("storeSearchItemData");
                        showDifferentStoreItems.setStoreitemarray(jsonArray.toString());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                mStart = new LatLng(Double.valueOf(mLatitude), Double.valueOf(mLongitude));
                mEnd = new LatLng(Double.valueOf(mReceivedLatitude), Double.valueOf(mReceivedLongitude));

                Location startPoint = new Location("locationB");
                startPoint.setLatitude(Double.valueOf(mLatitude));
                startPoint.setLongitude(Double.valueOf(mLongitude));

                Location endPoint = new Location("locationA");
                endPoint.setLatitude(Double.valueOf(mReceivedLatitude));
                endPoint.setLongitude(Double.valueOf(mReceivedLongitude));

                double distance = startPoint.distanceTo(endPoint);
                double mDis = distance / 1000;

                showDifferentStoreItems.setMStoreDistance(String.valueOf(mDis));
                double dist = AppUtils.calculationByDistance(mStart, mEnd);

                if (dist == 0.0){

                    // If the current location and store location is equal the distance
                    // will show as 0.0. Therefore I have added a default nearby distance here

                    showDifferentStoreItems.setMStoreDistance(String.valueOf(0.10826277856925441));
                }else {

                    Log.d(TAG, "dist: " + dist);
                    showDifferentStoreItems.setMStoreDistance(String.valueOf(dist));
                }



            } catch (JSONException e) {
                e.printStackTrace();
            }
            storeItemList.add(showDifferentStoreItems);
            // Collections.sort(recordsList);
        }
        adapter = new SearchProductCardAdapter(storeItemList, getApplicationContext());
        recyclerView.setAdapter(adapter);
        swipeRefreshLayout.setRefreshing(false);
    }





    public void Message(String message) {
        LayoutInflater layoutInflater = LayoutInflater.from(this);
        View promptView = layoutInflater.inflate(R.layout.alert_message, null);
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setView(promptView);
        TextView textViewMessage = promptView.findViewById(R.id.alert_message_display);
        textViewMessage.setText(message);
        Button buttonblock = promptView.findViewById(R.id.alert_ok_buton);
        buttonblock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alert.dismiss();
            }
        });
        alert = alertDialogBuilder.create();
        alert.show();
    }
}
