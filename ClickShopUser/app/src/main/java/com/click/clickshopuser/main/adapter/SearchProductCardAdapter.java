package com.click.clickshopuser.main.adapter;
import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.click.clickshopuser.R;
import com.click.clickshopuser.model.PricingPojo;
import com.click.clickshopuser.model.SearchProductCardModel;
import com.click.clickshopuser.storage.DbHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.List;

public class SearchProductCardAdapter extends
        RecyclerView.Adapter<SearchProductCardAdapter.ViewHolder> {

    private static final String TAG = SearchProductCardAdapter.class.getSimpleName();
    Context context;
    List<SearchProductCardModel> listInAss;
    public NetworkImageView networkImageView;
    public ImageLoader imageLoader;
    DbHandler dbHandler;
    String cutString;
    private RecyclerView recyclerView;
    private PricingAdapter pricingAdapter;
    private LinearLayoutManager linearLayoutManager;
    public List<PricingPojo> pricingPojos;

    public SearchProductCardAdapter(List<SearchProductCardModel> listInAss, Context context) {
        super();
        this.listInAss = listInAss;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_adapter_search_product, parent, false);
        dbHandler = DbHandler.getInstance(context);

        recyclerView = v.findViewById(R.id._recyclerview_product);
        linearLayoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(linearLayoutManager);
        pricingPojos = new ArrayList<>();

        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public int getItemCount() {
        return listInAss.size();
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final SearchProductCardModel showDifferentStoreItems = listInAss.get(position);

        if (pricingPojos != null){
            pricingPojos.clear();
        }

        holder.textViewStoreName.setText(showDifferentStoreItems.getMStoreName());
        holder.textViewDuration.setText(new StringBuilder(showDifferentStoreItems.getMDeliveryDuration()).append(" hr"));
        String mStr = showDifferentStoreItems.getMStoreDistance();
        try {
            cutString = mStr.substring(0, 4);
        }catch (StringIndexOutOfBoundsException e){
            e.printStackTrace();
        }
        holder.textViewStoreDistance.setText(new StringBuilder(cutString).append(" Kms"));

        String mJon = showDifferentStoreItems.getStoreitemarray();

        try {
            JSONArray jsonArr = new JSONArray(mJon);
            parsePricing(jsonArr);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView textViewStoreName;
        TextView textViewDuration;
        TextView textViewStoreDistance;
        Button buttonShopNow;

        public ViewHolder(View itemView) {
            super(itemView);
            networkImageView = itemView.findViewById(R.id._store_item_image);
            textViewStoreName = itemView.findViewById(R.id._store_name);
            textViewDuration = itemView.findViewById(R.id._delivery_duration);
            textViewStoreDistance = itemView.findViewById(R.id._store_distance);
            buttonShopNow = itemView.findViewById(R.id._shop_now);

            this.setIsRecyclable(false);
        }
    }


    private void parsePricing(JSONArray arrs) {
        for (int i = 0; i < arrs.length(); i++) {
            PricingPojo pricingPojo = new PricingPojo();
            JSONObject json;
            try {
                json = arrs.getJSONObject(i);
                if (json.getString("storeId") != null && !json.getString("storeId").isEmpty() && !json.getString("storeId").equals("null")) {
                    pricingPojo.setStoreId(json.getInt("storeId"));
                }

                if (json.getInt("itemMerchantId") != 0 ) {
                    pricingPojo.setItemMerchantId(json.getInt("itemMerchantId"));
                } else {
                    pricingPojo.setItemMerchantId(0);
                }

                if (json.getString("storeName") != null && !json.getString("storeName").isEmpty() && !json.getString("storeName").equals("null")) {
                    pricingPojo.setMStoreName(json.getString("storeName"));
                }

                if (json.getString("itemId") != null && !json.getString("itemId").isEmpty() && !json.getString("itemId").equals("null")) {
                    pricingPojo.setItemId(json.getInt("itemId"));
                }
                if (json.getString("merchantId") != null && !json.getString("merchantId").isEmpty() && !json.getString("merchantId").equals("null")) {
                    pricingPojo.setMerchantId(json.getInt("merchantId"));
                }
                if (json.getString("itemName") != null && !json.getString("itemName").isEmpty() && !json.getString("itemName").equals("null")) {
                    pricingPojo.setItemName(json.getString("itemName"));
                }
                if (json.getString("itemImage") != null && !json.getString("itemImage").isEmpty() && !json.getString("itemImage").equals("null")) {
                    String mImag = modifyDropboxUrl(json.getString("itemImage"));
                    pricingPojo.setItemImage(mImag);
                }
                if (json.getString("itemPrice") != null && !json.getString("itemPrice").isEmpty() && !json.getString("itemPrice").equals("null")) {
                    pricingPojo.setItemPrice(json.getString("itemPrice"));
                }
                if (json.getString("itemWeight") != null && !json.getString("itemWeight").isEmpty() && !json.getString("itemWeight").equals("null")) {
                    pricingPojo.setItemWeight(json.getString("itemWeight"));
                }
                if (json.getString("itemCategory") != null && !json.getString("itemCategory").isEmpty() && !json.getString("itemCategory").equals("null")) {
                    pricingPojo.setItemCategory(json.getString("itemCategory"));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            pricingPojos.add(pricingPojo);
        }
        pricingAdapter = new PricingAdapter(pricingPojos, context);
        recyclerView.setAdapter(pricingAdapter);
    }


    public static String modifyDropboxUrl(String originalUrl) {
        String newUrl = originalUrl.replace("www.dropbox.", "dl.dropboxusercontent.");
        newUrl = newUrl.replace("dropbox.", "dl.dropboxusercontent.");
        return newUrl;
    }


























//    public void Message(String message) {
//        LayoutInflater layoutInflater = LayoutInflater.from(context);
//        View promptView = layoutInflater.inflate(R.layout.alert_message, null);
//        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
//        alertDialogBuilder.setView(promptView);
//        TextView textViewMessage = promptView.findViewById(R.id.alert_message_display);
//        textViewMessage.setText(message);
//        Button buttonblock = promptView.findViewById(R.id.alert_ok_buton);
//        buttonblock.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                alert.dismiss();
//            }
//        });
//        alert = alertDialogBuilder.create();
//        alert.show();
//    }
}