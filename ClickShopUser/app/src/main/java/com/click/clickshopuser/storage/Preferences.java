package com.click.clickshopuser.storage;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class Preferences {

    public static void setDefaults(String key, String value, Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(key, value);
        editor.apply();
    }

    public static String getDefaults(String key, Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString(key, null);
    }

    public static void deletePrefs(String key, Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.remove(key);
        editor.apply();
    }



  // SeachAllStoreItemAdaper
    public static String SHARE_PREF_POSITION = "positionCard";
    public static String SHARE_PREF_CHECK_SELETED_SHOP = "checkSeletedShop";
    public static String SHARE_PREF_CHECK_SELETED_SHOP_NAME = "checkSeletedShopName";
}
