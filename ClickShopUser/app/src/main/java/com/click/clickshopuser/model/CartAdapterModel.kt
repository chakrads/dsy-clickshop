package com.click.clickshopuser.model

class CartAdapterModel {
    var mId : Int? = 0;
    var mItemId : Int? = 0;
    var mItemMerchantId : Int? = 0;
    var mItemName : String? = null
    var mItemImage : String? = null
    var mItemPrice : String? = null
    var mItemQuantity : String? = null
    var mItemWeight : String? = null
}