package com.click.clickshopuser.main.bottombar;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.click.clickshopuser.R;
import com.click.clickshopuser.appconfig.AppUtils;
import com.click.clickshopuser.appconfig.EndPoint;
import com.click.clickshopuser.appconfig.SessionManager;
import com.click.clickshopuser.main.adapter.NearByShopAdapter;
import com.click.clickshopuser.model.GeoLocation;
import com.click.clickshopuser.model.store_horizontal;
import com.click.clickshopuser.network.NetworkUtils;
import com.click.clickshopuser.storage.DbHandler;
import com.google.android.gms.maps.model.LatLng;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class NearbyShop extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    private static final String TAG = NearbyShop.class.getSimpleName();
    private String latitude;
    private String longitude;
    private double mDis;

    private RelativeLayout relativeLayoutLoading;
    private SwipeRefreshLayout swipeRefreshLayout;
    private RecyclerView recyclerView;
    private List<store_horizontal> storeItemList;
    private NearByShopAdapter adapter;
    private LinearLayoutManager linearLayoutManager;
    private AlertDialog alert;

    private String mReceivedLongitude;
    private String mReceivedLatitude;
    private LatLng mStart, mEnd;
    private DbHandler dbHandler;

    public NearbyShop() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_nearby_shop, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        dbHandler = DbHandler.getInstance(getActivity());
        fetchPref();

        relativeLayoutLoading = getActivity().findViewById(R.id._loading_store);
        swipeRefreshLayout = getActivity().findViewById(R.id.store_refresh);
        swipeRefreshLayout.setOnRefreshListener(this);
        recyclerView = getActivity().findViewById(R.id._recyclerview_store);
        linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(linearLayoutManager);
        storeItemList = new ArrayList<>();
        super.onActivityCreated(savedInstanceState);
    }


    @Override
    public void onRefresh() {
        recyclerView.setVisibility(View.GONE);
        relativeLayoutLoading.setVisibility(View.GONE);
        storeItemList.clear();
        fetchPref();
    }


    public void fetchPref() {
        List<GeoLocation> geoLocation = dbHandler.getGeoLocation();
        for (GeoLocation geoLocation1 : geoLocation) {
            latitude = geoLocation1.getMLatitude();
            longitude = geoLocation1.getMLongitude();
        }

        if (AppUtils.internetConnectionAvailable(2000)) {
            mFetchNearbyShop();
        } else {
            Toast.makeText(getActivity(), "No Network Connection",
                    Toast.LENGTH_SHORT).show();
        }



    }


    public void mFetchNearbyShop() {
        SessionManager sessionManager = new SessionManager(getActivity());
        HashMap<String, String> hashMap = sessionManager.getUserDetails();
        String mUserId = hashMap.get(SessionManager.KEY_USER_ID);

        HashMap<String, String> params = new HashMap<String, String>();
        params.put("userId", mUserId);
        params.put("latitude", latitude);
        params.put("longitude", longitude);

        Log.d(TAG, "params: " + params);

        JsonObjectRequest request_json = new JsonObjectRequest(EndPoint.CLICK_SHOP_USER_FETCH_NEAR_BY_SHOP, new JSONObject(params),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, "response: " + response);
                        relativeLayoutLoading.setVisibility(View.GONE);
                        recyclerView.setVisibility(View.VISIBLE);
                        try {
                            Integer mStatusCode = response.getInt("responseCode");
                            Log.d( "ResponseErrors: ",String.valueOf(mStatusCode));
                            if (mStatusCode == 200) {
                                JSONArray array = response.optJSONArray("storeList");
                                parseData(array);
                            } else {
                                Message("No Shops Around");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

               // Log.d("ResponseErrors",error.getMessage());
                try{
                    relativeLayoutLoading.setVisibility(View.GONE);
                    NetworkUtils.UnableToReachServer(getActivity());
                }catch (NullPointerException e){
                    e.printStackTrace();
                }catch (IllegalStateException e){
                    e.printStackTrace();
                }

            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(request_json);
    }


    private void parseData(JSONArray array) {
        for (int i = 0; i < array.length(); i++) {
            store_horizontal store_horizontal = new store_horizontal();
            JSONObject json;
            try {
                json = array.getJSONObject(i);
                if (json.getString("storeId") != null && !json.getString("storeId").isEmpty()
                        && !json.getString("storeId").equals("storeId")) {
                    store_horizontal.setStoreId((json.getInt("storeId")));
                }
                if (json.getString("merchantId") != null && !json.getString("merchantId").isEmpty()
                        && !json.getString("merchantId").equals("merchantId")) {
                    store_horizontal.setMerchant_Id((json.getInt("merchantId")));
                }

                if (json.getString("storeName") != null && !json.getString("storeName").isEmpty()
                        && !json.getString("storeName").equals("storeName")) {
                    store_horizontal.setStore_name((json.getString("storeName")));
                }
                if (json.getString("deliveryDuration") != null && !json.getString("deliveryDuration").isEmpty()
                        && !json.getString("deliveryDuration").equals("deliveryDuration")) {
                    store_horizontal.setDelivery_duration((json.getString("deliveryDuration")));
                }
                if (json.getString("workingHourFrom") != null && !json.getString("workingHourFrom").isEmpty()
                        && !json.getString("workingHourFrom").equals("workingHourFrom")) {
                    store_horizontal.setWorkging_hour_from(json.getString("workingHourFrom"));
                }
                if (json.getString("workingHourTo") != null && !json.getString("workingHourTo").isEmpty()
                        && !json.getString("workingHourTo").equals("workingHourTo")) {
                    store_horizontal.setWorking_hour_to((json.getString("workingHourTo")));
                }
                if (json.getString("deliveryRange") != null && !json.getString("deliveryRange").isEmpty()
                        && !json.getString("deliveryRange").equals("deliveryRange")) {
                    store_horizontal.setDelivery_range((json.getString("deliveryRange")));
                }
                if (json.getString("latitude") != null && !json.getString("latitude").isEmpty()
                        && !json.getString("latitude").equals("latitude")) {
                    store_horizontal.setStore_latitude((json.getString("latitude")));
                    mReceivedLatitude = json.getString("latitude");
                }
                if (json.getString("longitude") != null && !json.getString("longitude").isEmpty()
                        && !json.getString("longitude").equals("longitude")) {
                    store_horizontal.setStore_longitude((json.getString("longitude")));
                    mReceivedLongitude = json.getString("longitude");
                }
                if (json.getString("address") != null && !json.getString("address").isEmpty()
                        && !json.getString("address").equals("address")) {
                    store_horizontal.setAddress((json.getString("address")));
                }
                if (json.getString("pinCode") != null && !json.getString("pinCode").isEmpty()
                        && !json.getString("pinCode").equals("pinCode")) {
                    store_horizontal.setPincode((json.getString("pinCode")));
                }
                if (json.getString("profilePic") != null && !json.getString("profilePic").isEmpty()
                        && !json.getString("profilePic").equals("profilePic")) {
                    store_horizontal.setStore_image((json.getString("profilePic")));
                }

                mStart = new LatLng(Double.valueOf(latitude), Double.valueOf(longitude));
                mEnd = new LatLng(Double.valueOf(mReceivedLatitude), Double.valueOf(mReceivedLongitude));
                Location startPoint = new Location("locationB");
                startPoint.setLatitude(Double.valueOf(latitude));
                startPoint.setLongitude(Double.valueOf(longitude));
                Location endPoint = new Location("locationA");
                endPoint.setLatitude(Double.valueOf(mReceivedLatitude));
                endPoint.setLongitude(Double.valueOf(mReceivedLongitude));
                double distance = startPoint.distanceTo(endPoint);
                mDis = distance / 1000;

                double dist = CalculationByDistance(mStart, mEnd);

                if (dist == 0.0){

                    // If the current location and store location is equal the distance
                    // will show as 0.0. Therefore I have added a default nearby distance here

                    store_horizontal.setDelivery_time(String.valueOf(0.10826277856925441));
                }else {

                    Log.d(TAG, "dist: " + dist);
                    store_horizontal.setDelivery_time(String.valueOf(dist));
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }
            storeItemList.add(store_horizontal);
            // Collections.sort(recordsList);
        }
        adapter = new NearByShopAdapter(storeItemList, getActivity());
        recyclerView.setAdapter(adapter);
        swipeRefreshLayout.setRefreshing(false);
    }


    public double CalculationByDistance(LatLng StartP, LatLng EndP) {
        int Radius = 6371;// radius of earth in Km
        double lat1 = StartP.latitude;
        double lat2 = EndP.latitude;
        double lon1 = StartP.longitude;
        double lon2 = EndP.longitude;
        double dLat = Math.toRadians(lat2 - lat1);
        double dLon = Math.toRadians(lon2 - lon1);
        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2)
                + Math.cos(Math.toRadians(lat1))
                * Math.cos(Math.toRadians(lat2)) * Math.sin(dLon / 2)
                * Math.sin(dLon / 2);
        double c = 2 * Math.asin(Math.sqrt(a));
        double valueResult = Radius * c;
        double km = valueResult / 1;
        DecimalFormat newFormat = new DecimalFormat("####");
        int kmInDec = Integer.valueOf(newFormat.format(km));
        double meter = valueResult % 1000;
        int meterInDec = Integer.valueOf(newFormat.format(meter));
        Log.i("Radius Value", "" + valueResult + "   KM  " + kmInDec
                + " Meter   " + meterInDec);
        return Radius * c;
    }


    public void Message(String message) {
        LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
        View promptView = layoutInflater.inflate(R.layout.alert_message, null);
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setView(promptView);
        TextView textViewMessage = promptView.findViewById(R.id.alert_message_display);
        textViewMessage.setText(message);
        Button buttonblock = (Button) promptView.findViewById(R.id.alert_ok_buton);
        buttonblock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alert.dismiss();
            }
        });
        alert = alertDialogBuilder.create();
        alert.show();
    }


}
