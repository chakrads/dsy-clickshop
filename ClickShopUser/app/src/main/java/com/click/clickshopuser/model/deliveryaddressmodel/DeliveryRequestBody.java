package com.click.clickshopuser.model.deliveryaddressmodel;

public class DeliveryRequestBody {

    String userId, sessionId, deviceUniqueId;
    public DeliveryRequestBody(String userId, String sessionId, String deviceUniqueId) {
        this.userId = userId;
        this.sessionId = sessionId;
        this.deviceUniqueId = deviceUniqueId;
    }
}
