package com.click.clickshopuser.model

class ProgressPo {
    var orderId: String? = null
    var userId: Int? = 0;
    var shippingAddress: String? = null
    var paymentMode: String? = null
    var orderAmount: String? = null
    var deliveryCharge: String? = null
    var merchantId: Int? = 0;
    var orderStatus: String? = null
    var storeName: String?=null
    var storeMobileNumber: String?=null
    var orderTime: String?=null

}