package com.click.clickshopuser.appconfig;
import com.click.clickshopuser.model.categoryitemmodel.CatagoryItemsMod;
import com.click.clickshopuser.model.categoryitemmodel.RequestBody;
import com.click.clickshopuser.model.CatagoryListing;
import com.click.clickshopuser.model.deliveryaddressmodel.DeliveryModel;
import com.click.clickshopuser.model.deliveryaddressmodel.DeliveryRequestBody;
import com.click.clickshopuser.model.deliverycharge.DeliveryCharge;
import com.click.clickshopuser.model.deliverycharge.RequestBodyDeliveryCharge;
import com.click.clickshopuser.model.resetpassword.RequestBodyResetPassword;
import com.click.clickshopuser.model.resetpassword.ResetPasswords;
import com.click.clickshopuser.model.signout.RequestBodySignOut;
import com.click.clickshopuser.model.signout.SignOut;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface ApiInterface {

    @GET("findItemCategoryRandomly")
    Call<CatagoryListing> getCatagory();

    @POST("searchItemInGalleryByCategory")
    Call<CatagoryItemsMod> searchitems(@Body RequestBody requestBody);

    @POST("getUserShippingAddress")
    Call<DeliveryModel> deliveryAddress (@Body DeliveryRequestBody deliveryRequestBody);

    @POST("resetPassword")
    Call<ResetPasswords> resetPassword (@Body RequestBodyResetPassword requestBodyResetPassword);

    @POST("signOut")
    Call<SignOut> signOut (@Body RequestBodySignOut requestBodySignOut);

    @POST("getOrderDeliveryCharge")
    Call<DeliveryCharge> getDeliverycharge (@Body RequestBodyDeliveryCharge requestBodySignOut);



}



