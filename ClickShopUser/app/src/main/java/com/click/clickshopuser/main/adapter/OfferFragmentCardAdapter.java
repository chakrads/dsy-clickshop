package com.click.clickshopuser.main.adapter;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.Volley;
import com.click.clickshopuser.R;
import com.click.clickshopuser.appconfig.AppUtils;
import com.click.clickshopuser.appconfig.EndPoint;
import com.click.clickshopuser.appconfig.SessionManager;
import com.click.clickshopuser.main.activity.SearchProducts;
import com.click.clickshopuser.main.activity.SearchStoreItemOneSelect;
import com.click.clickshopuser.model.AddToCart;
import com.click.clickshopuser.model.CheckSelectedStore;
import com.click.clickshopuser.model.OfferModelFrag;
import com.click.clickshopuser.network.NetworkUtils;
import com.click.clickshopuser.storage.DbHandler;
import com.click.clickshopuser.storage.Preferences;
import com.click.clickshopuser.volley.CustomVolleyRequest;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.HashMap;
import java.util.List;


public class OfferFragmentCardAdapter extends
        RecyclerView.Adapter<OfferFragmentCardAdapter.ViewHolder> {

    private static final String TAG = OfferFragmentCardAdapter.class.getSimpleName();
    Context context;
    List<OfferModelFrag> listInAss;
    public NetworkImageView networkImageView;
    public ImageLoader imageLoader;
    static SearchProducts activity;
    DbHandler dbHandler;
    int mMerchtId;
    private AlertDialog alert;
    String cutString;
    Integer mActualPrice;
    private int mSelectedStoreid;
    public static ProgressDialog progress;
    View v;


    public OfferFragmentCardAdapter(List<OfferModelFrag> listInAss, Context context) {
        super();
        this.listInAss = listInAss;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
         v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.offer_fragment_card_adapter, parent, false);

        dbHandler = DbHandler.getInstance(context);

        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public int getItemCount() {
        return listInAss.size();
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final OfferModelFrag showDifferentStoreItems = listInAss.get(position);


        String mNa = showDifferentStoreItems.getMItemName();
        String cap = mNa.substring(0, 1).toUpperCase() + mNa.substring(1);
        holder.textViewItemName.setText(cap);

        final String image = showDifferentStoreItems.getMItemImage();
        imageLoader = CustomVolleyRequest.getInstance(context).getImageLoader();
        imageLoader.get(image, ImageLoader.getImageListener(networkImageView, R.drawable.noimage,
                R.drawable.noimage));
        networkImageView.setImageUrl(image, imageLoader);

        holder.textViewStoreName.setText(showDifferentStoreItems.getMStoreName());
        holder.textViewDuration.setText(new StringBuilder(showDifferentStoreItems.getMDeliveryDuration()).append(" hr"));


        holder.textViewOfferActualPrice.setText(new StringBuilder("Rs. ").append(showDifferentStoreItems.getActualPrice()));
        holder.textViewOfferPercantage.setText(new StringBuilder(showDifferentStoreItems.getDiscountPercentage()).append(" %"));
        holder.textViewOfferPrice.setText(new StringBuilder("Rs. ").append(showDifferentStoreItems.getMItemPrice()));
        // Here in OfferFratment We are using the itemPrice field to show offer price


        String mStr = showDifferentStoreItems.getMStoreDistance();
        try {
            cutString = mStr.substring(0, 4);
        }catch (StringIndexOutOfBoundsException e){
            e.printStackTrace();
        }
        holder.textViewStoreDistance.setText(new StringBuilder(cutString).append(" Kms"));


        holder.buttonShopNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                // Used in CartFragment of fetching Delivery Charge
                if (Preferences.getDefaults("merchantId", context) != null) {
                    Preferences.deletePrefs("merchantId", context);
                }
                Preferences.setDefaults("merchantId", String.valueOf(mMerchtId), context);


                // Saving the selected store id in the shared preference.
                String mStoreIds = String.valueOf(showDifferentStoreItems.getMStoreId());
                if (Preferences.getDefaults("postStoreId", context) != null) {
                    Preferences.deletePrefs("postStoreId", context);
                }
                Preferences.setDefaults("postStoreId", mStoreIds, context);


                String mStoreId = String.valueOf(showDifferentStoreItems.getMStoreId());
                String mStoreName = showDifferentStoreItems.getMStoreName();
                String mItemName = showDifferentStoreItems.getMItemName();
                String mItemPrice = showDifferentStoreItems.getMItemPrice();
                String mItemWeight = showDifferentStoreItems.getMItemWeight();
                String mItemImage = showDifferentStoreItems.getMItemImage();
                Integer mItemId = showDifferentStoreItems.getMItemId();
                String mMerchantId = String.valueOf(showDifferentStoreItems.getMMerchantId());
                mMerchtId = showDifferentStoreItems.getMMerchantId();


                // Getting the Size of the Cart.
                int mCheck = dbHandler.getCartCount();


                // This condition is checking if the cart is having any item. If the cart is
                // having any item. Then check which store the item belogns and then allow the
                // user only to that store where the user has already selected.
                // User is not allowed to enter other store when he has already selected a store.

                if (mCheck != 0) {
                    List<CheckSelectedStore> cartDetails = dbHandler.getSelectedStore();
                    for (CheckSelectedStore addd : cartDetails) {
                        mSelectedStoreid = addd.getMStoreId();
                    }

                    //This condition does not allow you to enter other store if the store is
                    // is already selected. You will be allowed to go to other store only when
                    // the cart is cleared from other store.


                    if (Integer.valueOf(mStoreId) == mSelectedStoreid) {

                        String mStore = showDifferentStoreItems.getMStoreName();
                        setStoreName(mStore);

                        Intent intents = new Intent(context, SearchStoreItemOneSelect.class);
                        intents.putExtra("mStoreId", mStoreId);
                        intents.putExtra("mItemId", mItemId);
                        intents.putExtra("mStoreName", mStoreName);
                        intents.putExtra("mItemName", mItemName);
                        intents.putExtra("mItemPrice", mItemPrice);
                        intents.putExtra("mItemWeight", mItemWeight);
                        intents.putExtra("mItemImage", mItemImage);
                        intents.putExtra("mMerchantId", mMerchantId);
                        intents.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                        context.startActivity(intents);
                    } else {
                        String mSTr = Preferences.getDefaults(Preferences.SHARE_PREF_CHECK_SELETED_SHOP_NAME, context);
                        Toast.makeText(context, "You Have Already Selected an Item in " + mSTr + ". Please Complete the Shopping And Proceed. ",
                                Toast.LENGTH_LONG).show();
                    }


                } else if (mCheck == 0) {

                    // If the Cart size 0 then allow the user to select a store and simuultaneously
                    // add the item in the cart.


                    int mCount = 1;

                    String mStore = showDifferentStoreItems.getMStoreName();
                    setStoreName(mStore);

                    String mItemNameDB = showDifferentStoreItems.getMItemName();
                    String mItemImageDB = showDifferentStoreItems.getMItemImage();
                    String mItemPriceDB = showDifferentStoreItems.getMItemPrice();
                    String mItemWeightDB = showDifferentStoreItems.getMItemWeight();
                    Integer mItemIdDB = showDifferentStoreItems.getMItemId();
                    Integer mItemMerchantId = showDifferentStoreItems.getItemMerchantId();
                    Integer mMerchantIds = showDifferentStoreItems.getMMerchantId();


                    SessionManager sessionManager = new SessionManager(context);
                    if (sessionManager.isLoggedIn()) {
                        if (AppUtils.internetConnectionAvailable(2000)) {
                            add(mStoreIds, mStore, mItemIdDB, mItemMerchantId, mCount, mItemNameDB, mItemImageDB, mItemPriceDB, mItemWeightDB, mMerchantIds);
                        } else {
                            Toast.makeText(context, "No Network Connection",
                                    Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        // Saving Values in Sqlite Database
                        AddToCart addToCart = new AddToCart();
                        addToCart.setMItem_id(mItemIdDB);
                        addToCart.setMItem_Count(mCount);
                        addToCart.setMItem_Name(mItemName);
                        addToCart.setMItem_Catagory("null");  // not used anywhere
                        addToCart.setMItem_Image(mItemImage);
                        addToCart.setMItem_Price(mItemPriceDB);
                        addToCart.setMItem_Weight(mItemWeightDB);
                        dbHandler.addToCart(addToCart);




                        // Saving the Seleted Store id in Sqlite Database.
                        CheckSelectedStore checkSelectedStore = new CheckSelectedStore();
                        checkSelectedStore.setMStoreId(Integer.valueOf(mStoreId));
                        dbHandler.selectedShop(checkSelectedStore);


                        // Sending a message to MainActivity to Refresh The Cart Count in Bottom bar
                        Intent intent = new Intent("filter_string");
                        intent.putExtra("key", "value");
                        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);


                        Intent intentM = new Intent(context, SearchStoreItemOneSelect.class);
                        intentM.putExtra("mStoreId", mStoreId);
                        intentM.putExtra("mItemId", String.valueOf(mItemIdDB));
                        intentM.putExtra("mStoreName", mStoreName);
                        intentM.putExtra("mItemName", mItemName);
                        intentM.putExtra("mItemPrice", mItemPriceDB);
                        intentM.putExtra("mItemWeight", mItemWeightDB);
                        intentM.putExtra("mItemImage", mItemImage);
                        intentM.putExtra("mMerchantId", mMerchantId);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                        context.startActivity(intentM);


                    }


                }
            }
        });
    }


    private void setStoreName(String mStoreName) {
        if (Preferences.getDefaults(Preferences.SHARE_PREF_CHECK_SELETED_SHOP_NAME, context) != null) {
            Preferences.deletePrefs(Preferences.SHARE_PREF_CHECK_SELETED_SHOP_NAME, context);
        }
        Preferences.setDefaults(Preferences.SHARE_PREF_CHECK_SELETED_SHOP_NAME, mStoreName, context);
    }


    class ViewHolder extends RecyclerView.ViewHolder {
        TextView textViewItemName;
        TextView textViewStoreName;
        TextView textViewDuration;
        TextView textViewStoreDistance;
        Button buttonShopNow;

        TextView textViewOfferActualPrice;
        TextView textViewOfferPercantage;
        TextView textViewOfferPrice;

        public ViewHolder(View itemView) {
            super(itemView);
            networkImageView = itemView.findViewById(R.id._store_item_image);
            textViewItemName = itemView.findViewById(R.id._store_item_name);

            textViewStoreName = itemView.findViewById(R.id._store_name);
            textViewDuration = itemView.findViewById(R.id._delivery_range);
            textViewStoreDistance = itemView.findViewById(R.id._store_distance);
            buttonShopNow = itemView.findViewById(R.id._shop_now);

            textViewOfferActualPrice = itemView.findViewById(R.id._offer_actual_price);
            textViewOfferPercantage = itemView.findViewById(R.id._offer_percentage);
            textViewOfferPrice = itemView.findViewById(R.id._offer_amt);


            this.setIsRecyclable(false);
        }
    }

    public void Message(String message) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View promptView = layoutInflater.inflate(R.layout.alert_message, null);
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setView(promptView);
        TextView textViewMessage = promptView.findViewById(R.id.alert_message_display);
        textViewMessage.setText(message);
        Button buttonblock = promptView.findViewById(R.id.alert_ok_buton);
        buttonblock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alert.dismiss();
            }
        });
        alert = alertDialogBuilder.create();
        alert.show();
    }











    private  void add(final String mStoreId, final String mStoreName, final Integer mItemIdAdd, final Integer mItemMerchantIdAdd,
                      final Integer mItemQuantityAdd, final String mItemName, final String mItemImage, final String mItemPriceAdd,
                      final String mItemWeightDB, final Integer mMerchantId

    ) {
        progress = ProgressDialog.show(v.getContext(), "",
                "Please wait...", false, false);
        SessionManager sessionManager = new SessionManager(context);
        HashMap<String, String> hashMap = sessionManager.getUserDetails();
        String mUserid = hashMap.get(SessionManager.KEY_USER_ID);
        String mDeviceId = hashMap.get(SessionManager.KEY_DEVICE_ID);
        String mSessionId = hashMap.get(SessionManager.KEY_SESSION_ID);

        HashMap<String, String> params = new HashMap<String, String>();
        params.put("userId", mUserid);
        params.put("storeId", Preferences.getDefaults("postStoreId", context));
        params.put("itemMerchantId", String.valueOf(mItemMerchantIdAdd));
        params.put("itemQuantity", String.valueOf(mItemQuantityAdd));
        params.put("itemPrice", mItemPriceAdd);
        params.put("totalPrice", mItemPriceAdd);
        params.put("deviceUniqueId", mDeviceId);
        params.put("sessionId", mSessionId);

        Log.d(TAG, "AddingItem: " + params);


        JsonObjectRequest request_json = new JsonObjectRequest(EndPoint.ADD_CART_ITEM, new JSONObject(params),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, "StoreDiffAddResp: " + response);
                        progress.dismiss();

                        try {
                            Integer mStatusCode = response.getInt("responseCode");
                            if (mStatusCode == 200) {


                                // Saving Values in Sqlite Database
                                AddToCart addToCart = new AddToCart();
                                addToCart.setMItem_id(mItemIdAdd);
                                addToCart.setMItem_Count(mItemQuantityAdd);
                                addToCart.setMItem_Name(mItemName);
                                addToCart.setMItem_Catagory("null");  // not used anywhere
                                addToCart.setMItem_Image(mItemImage);
                                addToCart.setMItem_Price(mItemPriceAdd);
                                addToCart.setMItem_Weight(mItemWeightDB);
                                dbHandler.addToCart(addToCart);


                                // Saving the Seleted Store id in Sqlite Database.
                                CheckSelectedStore checkSelectedStore = new CheckSelectedStore();
                                checkSelectedStore.setMStoreId(Integer.valueOf(mStoreId));
                                dbHandler.selectedShop(checkSelectedStore);


                                // Sending a message to MainActivity to Refresh The Cart Count in Bottom bar
                                Intent intent = new Intent("filter_string");
                                intent.putExtra("key", "value");
                                LocalBroadcastManager.getInstance(context).sendBroadcast(intent);


                                Intent intentM = new Intent(context, SearchStoreItemOneSelect.class);
                                intentM.putExtra("mStoreId", mStoreId);
                                intentM.putExtra("mItemId", String.valueOf(mItemIdAdd));
                                intentM.putExtra("mStoreName", mStoreName);
                                intentM.putExtra("mItemName", mItemName);
                                intentM.putExtra("mItemPrice", mItemPriceAdd);
                                intentM.putExtra("mItemWeight", mItemWeightDB);
                                intentM.putExtra("mItemImage", mItemImage);
                                intentM.putExtra("mMerchantId", mMerchantId);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                                context.startActivity(intentM);


                            } else if (mStatusCode == 400) {

                                Toast.makeText(context, "Adding Item to Cart Failed..",
                                        Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progress.dismiss();
                NetworkUtils.UnableToReachServer(context);
            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(request_json);
    }



}
