package com.click.clickshopuser.main.activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;
import com.click.clickshopuser.R;
import com.click.clickshopuser.appconfig.ApiClientUser;
import com.click.clickshopuser.appconfig.ApiInterface;
import com.click.clickshopuser.appconfig.AppUtils;
import com.click.clickshopuser.appconfig.Constants;
import com.click.clickshopuser.main.adapter.CatagoryItemCardAdapter;
import com.click.clickshopuser.model.categoryitemmodel.CatagoryItemsMod;
import com.click.clickshopuser.model.categoryitemmodel.ItemDataList;
import com.click.clickshopuser.model.categoryitemmodel.RequestBody;
import java.util.ArrayList;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CatagoryItems extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {
    private static final String TAG = CatagoryItems.class.getSimpleName();
    Toolbar toolbar;
    EditText editTextSearch;
    public RelativeLayout relativeLayoutLoading;
    public SwipeRefreshLayout swipeRefreshLayout;
    public RecyclerView recyclerView;
    public CatagoryItemCardAdapter adapter;
    public LinearLayoutManager linearLayoutManager;
    String mCatagory;
    private String newquery, mSubString;
    ApiInterface apiInterfacec;
    List<ItemDataList> datum;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.fadein, R.anim.fadeout);
        setContentView(R.layout.activity_catagory_items);
        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }

        toolbar = findViewById(R.id.catagory_item_toolbar);
        setSupportActionBar(toolbar);
        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_arrow_back_black_24dp));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        editTextSearch = findViewById(R.id._search);
        relativeLayoutLoading = findViewById(R.id._loading_catagory);
        swipeRefreshLayout = findViewById(R.id.catagory_refresh);
        swipeRefreshLayout.setOnRefreshListener(this);
        recyclerView = findViewById(R.id._catagory_store);
        linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 2));
        datum = new ArrayList<>();


        getInent();

        editTextSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence query, int start, int before, int count) {
                if (count >= 2) {
                    mSubString = String.valueOf(query);
                    newquery = mSubString.substring(0, 1).toUpperCase() + mSubString.substring(1);
                } else {
                    mSubString = String.valueOf(query);
                    newquery = query.toString().toUpperCase();
                }
                final List<ItemDataList> filteredList = new ArrayList<>();
                if (datum != null) {
                    for (int i = 0; i < datum.size(); i++) {
                        try {
                            final String text = datum.get(i).getItemName();
                            if (text.contains(newquery)) {
                                filteredList.add(datum.get(i));
                                updateRecyclerView(filteredList, false);
                            }
                        } catch (NullPointerException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        if (AppUtils.internetConnectionAvailable(2000)) {
            mSearchByCatagory();
        } else {
            Toast.makeText(getApplicationContext(), "No Network Connection",
                    Toast.LENGTH_SHORT).show();
        }
    }


    public void updateRecyclerView(List<ItemDataList> lst, boolean swipeStatus) {
        recyclerView = findViewById(R.id._catagory_store);
        recyclerView.setHasFixedSize(true);
        linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 2));
        adapter = new CatagoryItemCardAdapter(lst, this);
        recyclerView.setAdapter(adapter);
        swipeRefreshLayout.setRefreshing(swipeStatus);
    }

    public void getInent() {
        Intent intent = getIntent();
        try {
            mCatagory = intent.getStringExtra(Constants.PARAM_ITEM_CATAGORY);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

    }


    @Override
    public void onRefresh() {
        if (datum != null) {
            datum.clear();
        }
        relativeLayoutLoading.setVisibility(View.GONE);
        recyclerView.setVisibility(View.GONE);
        if (AppUtils.internetConnectionAvailable(2000)) {
            mSearchByCatagory();
        } else {
            Toast.makeText(getApplicationContext(), "No Network Connection",
                    Toast.LENGTH_SHORT).show();
        }
    }

    public void mSearchByCatagory() {
        apiInterfacec = ApiClientUser.getApiClient().create(ApiInterface.class);
        Call<CatagoryItemsMod> call = apiInterfacec.searchitems(new RequestBody(mCatagory));
        call.enqueue(new Callback<CatagoryItemsMod>() {
            @Override
            public void onResponse(Call<CatagoryItemsMod> call, Response<CatagoryItemsMod> response) {
                if (response.isSuccessful()) {
                    relativeLayoutLoading.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.VISIBLE);
                    if (response.body().getResponseCode() == 200) {
                        datum = response.body().getItemDataList();
                        adapter = new CatagoryItemCardAdapter(datum, getApplicationContext());
                        recyclerView.setAdapter(adapter);
                        swipeRefreshLayout.setRefreshing(false);
                    } else {
                        Toast.makeText(getApplicationContext(), "No Data",
                                Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<CatagoryItemsMod> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.toString(),
                        Toast.LENGTH_SHORT).show();
            }
        });
    }


}
