package com.click.clickshopuser.fonts;
import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;
import com.click.clickshopuser.volley.AppController;

public class CaviarDreams extends AppCompatTextView {

    public CaviarDreams(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public CaviarDreams(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CaviarDreams(Context context) {
        super(context);
        init();
    }

    private void init() {
        if (!isInEditMode()) {
            Typeface custom_font = Typeface.createFromAsset(AppController.getInstance().getAssets(), "CaviarDreams.ttf");
            setTypeface(custom_font);
        }
    }
}
