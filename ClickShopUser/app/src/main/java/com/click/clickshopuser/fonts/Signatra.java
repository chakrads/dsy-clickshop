package com.click.clickshopuser.fonts;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;

import com.click.clickshopuser.volley.AppController;

public class Signatra extends AppCompatTextView {

    public Signatra(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public Signatra(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public Signatra(Context context) {
        super(context);
        init();
    }

    private void init() {
        if (!isInEditMode()) {
            Typeface custom_font = Typeface.createFromAsset(AppController.getInstance().getAssets(), "Signatra.ttf");
            setTypeface(custom_font);
        }
    }
}