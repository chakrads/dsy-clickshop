package com.click.clickshopuser.pushnotification;
import android.util.Log;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;


public class MyFirebaseMessagingService extends FirebaseMessagingService {

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        Log.d("MyNotification", remoteMessage.getNotification().toString());
        Log.d("MyNotification", remoteMessage.getNotification().toString());

//        if (remoteMessage.getData().size() > 0) {
//            //handle the data message here
//        }

        //getting the title and the body
        String title = remoteMessage.getNotification().getTitle();
        String body = remoteMessage.getNotification().getBody();


        Log.d("body", body + title );

        MyNotificationManager.getInstance(getApplicationContext()).displayNotification(title, body);

        if (remoteMessage.getData().size() > 0) {
            Log.d("Notifiy", "Message data payload: " + remoteMessage.getData());
        }


    }
}
