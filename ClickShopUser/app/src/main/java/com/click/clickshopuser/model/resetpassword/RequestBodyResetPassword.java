package com.click.clickshopuser.model.resetpassword;

public class RequestBodyResetPassword {

    String mobileNumber, sessionId,  oldPassword, newPassword, confirmPassword, deviceUniqueId;

    public RequestBodyResetPassword(String mobileNumber, String sessionId, String oldPassword,
                                    String newPassword, String confirmPassword, String deviceUniqueId) {
        this.mobileNumber = mobileNumber;
        this.sessionId = sessionId;
        this.oldPassword = oldPassword;
        this.newPassword = newPassword;
        this.confirmPassword = confirmPassword;
        this.deviceUniqueId = deviceUniqueId;

    }
}
