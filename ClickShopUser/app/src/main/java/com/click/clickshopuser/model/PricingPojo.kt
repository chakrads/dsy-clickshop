package com.click.clickshopuser.model

class PricingPojo {
    var itemId : Int? = 0;
    var merchantId : Int? = 0;
    var itemMerchantId : Int? = 0;
    var storeId : Int? = 0;
    var itemName: String? = null
    var itemImage: String? = null
    var itemPrice: String? = null
    var itemWeight: String? = null
    var itemCategory: String? = null
    var mStoreName: String? = null
}