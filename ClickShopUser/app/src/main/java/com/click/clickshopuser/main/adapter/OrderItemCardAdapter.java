package com.click.clickshopuser.main.adapter;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.bumptech.glide.Glide;
import com.click.clickshopuser.R;
import com.click.clickshopuser.model.OrderItems;

import java.util.List;

public class OrderItemCardAdapter extends RecyclerView.Adapter<OrderItemCardAdapter.ViewHolder> {

    private static final String TAG = OrderItemCardAdapter.class.getSimpleName();
    Context context;
    List<OrderItems> orderItems;
    ImageView networkImageView;
    public ImageLoader imageLoader;
    String image;

    public OrderItemCardAdapter(List<OrderItems> orderItems, Context context) {
        super();
        this.orderItems = orderItems;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.order_item_cart_adapter, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public int getItemCount() {
        return orderItems.size();
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final OrderItems orderItems1 = orderItems.get(position);
        holder.textViewItemName.setText(orderItems1.getItemName());
        holder.textViewItemPrice.setText(new StringBuilder("Rs .").append(orderItems1.getItemPrice()));
        holder.textViewItemWeight.setText(orderItems1.getItemWeight());
        holder.textViewItemQuantity.setText(new StringBuilder(orderItems1.getItemQuantity()).append(" Nos."));
//       holder.textViewAmount.setText(new StringBuilder("Rs. ").append(orderItems1.getTotalPrice()));


        try {
            image = modifyDropboxUrl(orderItems1.getItemImage());
        }catch (NullPointerException e){
            e.printStackTrace();
        }


        Glide.with(context)
                .load(image)
                .override(200, 200)
                .into(networkImageView);




    }

    public static String modifyDropboxUrl(String originalUrl) {
        String newUrl = originalUrl.replace("www.dropbox.", "dl.dropboxusercontent.");
        newUrl = newUrl.replace("dropbox.", "dl.dropboxusercontent.");
        return newUrl;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView textViewItemName;
        TextView textViewItemPrice;
        TextView textViewItemWeight;
        TextView textViewItemQuantity;
        TextView textViewAmount;


        private ViewHolder(View itemView) {
            super(itemView);
            networkImageView = itemView.findViewById(R.id._cart_item_image);
            textViewItemName = itemView.findViewById(R.id._cart_item_name);
            textViewItemPrice = itemView.findViewById(R.id._cart_itme_price);
            textViewItemWeight = itemView.findViewById(R.id._cart_itme_weight);
            textViewItemQuantity = itemView.findViewById(R.id._cart_itme_quanity);
            textViewAmount = itemView.findViewById(R.id._total_amount);

            this.setIsRecyclable(false);
        }
    }




}
