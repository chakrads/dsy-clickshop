package com.click.clickshopuser.appconfig;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.click.clickshopuser.main.activity.MainActivity;
import com.click.clickshopuser.prelanding.LoginActivity;

import java.util.HashMap;

public class SessionManager {

    public static SharedPreferences pref;
    public static Editor editor;
    private static Context _context;
    int PRIVATE_MODE = 0;
    public static String PREF_NAME = "ClickShopUser";
    public static String IS_LOGIN = "IsLoggedIn";
    public static String KEY_USER_ID = "userid";
    public static String KEY_PHONE_NUMBER = "phonenumber";
    public static String KEY_FIRST_NAME = "firstname";
    public static String KEY_LAST_NAME = "lastname";
    public static String KEY_SESSION_ID = "mSessionId";
    public static String KEY_DEVICE_ID = "mDeviceId";


    public SessionManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void createLoginSession(String userId, String phone, String firstName, String lastName, String mDeviceId, String mSessionId) {
        try {
            editor.putBoolean(IS_LOGIN, true);
            editor.putString(KEY_USER_ID, userId);
            editor.putString(KEY_PHONE_NUMBER, phone);
            editor.putString(KEY_FIRST_NAME, firstName);
            editor.putString(KEY_LAST_NAME, lastName);
            editor.putString(KEY_DEVICE_ID, mDeviceId);
            editor.putString(KEY_SESSION_ID, mSessionId);

            editor.commit();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    public void checkLogin() {
        if (!this.isLoggedIn()) {
            Intent i = new Intent(_context, LoginActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            _context.startActivity(i);
        }
    }

    public HashMap<String, String> getUserDetails() {
        HashMap<String, String> user = new HashMap<String, String>();
        user.put(KEY_USER_ID, pref.getString(KEY_USER_ID, null));
        user.put(KEY_PHONE_NUMBER, pref.getString(KEY_PHONE_NUMBER, null));
        user.put(KEY_FIRST_NAME, pref.getString(KEY_FIRST_NAME, null));
        user.put(KEY_LAST_NAME, pref.getString(KEY_LAST_NAME, null));
        user.put(KEY_DEVICE_ID, pref.getString(KEY_DEVICE_ID, null));
        user.put(KEY_SESSION_ID, pref.getString(KEY_SESSION_ID, null));

        return user;
    }

    public static void logoutUser() {
        editor.clear();
        editor.commit();
        Intent i = new Intent(_context, MainActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        _context.startActivity(i);
    }

    public boolean isLoggedIn() {
        return pref.getBoolean(IS_LOGIN, false);
    }
}