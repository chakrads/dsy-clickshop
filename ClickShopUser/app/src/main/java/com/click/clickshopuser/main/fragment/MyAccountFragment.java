package com.click.clickshopuser.main.fragment;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;
import com.click.clickshopuser.R;
import com.click.clickshopuser.appconfig.ApiClientUser;
import com.click.clickshopuser.appconfig.ApiInterface;
import com.click.clickshopuser.appconfig.AppUtils;
import com.click.clickshopuser.appconfig.SessionManager;
import com.click.clickshopuser.fonts.CorbelTextView;
import com.click.clickshopuser.main.activity.ResetPassword;
import com.click.clickshopuser.model.signout.RequestBodySignOut;
import com.click.clickshopuser.model.signout.SignOut;
import java.util.HashMap;
import retrofit2.Call;
import retrofit2.Callback;


public class MyAccountFragment extends Fragment implements View.OnClickListener {
    SessionManager sessionManager;
    ProgressDialog progress;
    CorbelTextView corbelTextView_user, corbelTextView_phone, corbelTextViewLogout;
    TextView textViewReset;

    public MyAccountFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_my_account, container, false);
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        corbelTextView_user = getActivity().findViewById(R.id._user);
        corbelTextView_phone = getActivity().findViewById(R.id._phone_no);

        corbelTextViewLogout = getActivity().findViewById(R.id._logout);
        corbelTextViewLogout.setOnClickListener(this);
        sessionManager = new SessionManager(getActivity());
        HashMap<String, String> mUserDetails = sessionManager.getUserDetails();
        String mFirstName = mUserDetails.get(SessionManager.KEY_FIRST_NAME);
        String mLastName = mUserDetails.get(SessionManager.KEY_LAST_NAME);
        String mPhone = mUserDetails.get(SessionManager.KEY_PHONE_NUMBER);

        corbelTextView_user.setText(new StringBuilder(mFirstName).append(" ").append(mLastName));
        corbelTextView_phone.setText(mPhone);
        textViewReset = getActivity().findViewById(R.id._resettext);
        textViewReset.setOnClickListener(this);

        super.onActivityCreated(savedInstanceState);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id._logout:
                if (AppUtils.internetConnectionAvailable(2000)) {
                    progress = ProgressDialog.show(getActivity(), "Logging Out...", "Please wait...", false, false);
                    sessionLogOut();
                } else {
                    Toast.makeText(getActivity(), "No Network Connection",
                            Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id._resettext:
                Intent intent = new Intent(getActivity(), ResetPassword.class);
                startActivity(intent);
                break;
        }
    }


    public void sessionLogOut() {
        SessionManager sessionManager = new SessionManager(getActivity());
        HashMap<String, String> hashMap = sessionManager.getUserDetails();
        String mPhone = hashMap.get(SessionManager.KEY_PHONE_NUMBER);
        String mDeviceId = hashMap.get(SessionManager.KEY_DEVICE_ID);

        ApiInterface apiInterfacec = ApiClientUser.getApiClient().create(ApiInterface.class);
        Call<SignOut> call = apiInterfacec.signOut(new RequestBodySignOut(mPhone, mDeviceId));
        call.enqueue(new Callback<SignOut>() {
            @Override
            public void onResponse(Call<SignOut> call, retrofit2.Response<SignOut> response) {

                if (response.isSuccessful()) {
                    Toast.makeText(getActivity(), "Successfully Logged Out",
                            Toast.LENGTH_SHORT).show();
                    progress.dismiss();
                    SessionManager.logoutUser();
                    getActivity().finish();
                }else {
                    progress.dismiss();
                    SessionManager.logoutUser();
                    getActivity().finish();

                }
            }

            @Override
            public void onFailure(Call<SignOut> call, Throwable t) {
                SessionManager.logoutUser();
                getActivity().finish();
                progress.dismiss();
            }
        });
    }

}
