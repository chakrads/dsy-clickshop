package com.click.clickshopuser.appconfig;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClientUser {

    // UserApp Base URL Server
    public static final String BASE_URL = "http://18.222.124.117:9093/user/";


    // Local
//    public static final String BASE_URL = "http://.108:9091/user/";


    public static Retrofit retrofit = null;

    public static Retrofit getApiClient () {
        if (retrofit == null){
            retrofit = new Retrofit.Builder().baseUrl(BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        }
        return retrofit;
    }
}
