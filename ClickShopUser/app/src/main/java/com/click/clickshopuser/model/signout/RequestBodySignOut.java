package com.click.clickshopuser.model.signout;

public class RequestBodySignOut {
    String mobileNumber, deviceUniqueId ;

    public RequestBodySignOut(String mobileNumber, String deviceUniqueId) {
        this.mobileNumber = mobileNumber;
        this.deviceUniqueId = deviceUniqueId;

    }
}
