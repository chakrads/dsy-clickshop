package com.click.clickshopuser.main.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.Volley;
import com.click.clickshopuser.R;
import com.click.clickshopuser.appconfig.AppUtils;
import com.click.clickshopuser.appconfig.EndPoint;
import com.click.clickshopuser.appconfig.SessionManager;
import com.click.clickshopuser.fonts.CorbelTextView;
import com.click.clickshopuser.inter.ShopRefresh;
import com.click.clickshopuser.main.adapter.SearchAllStoreItemAdapter;
import com.click.clickshopuser.model.AddToCart;
import com.click.clickshopuser.model.SearchAllStoreItems;
import com.click.clickshopuser.network.NetworkUtils;
import com.click.clickshopuser.storage.DbHandler;
import com.click.clickshopuser.storage.Preferences;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class SearchStoreItemOneSelect extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener, ShopRefresh {
    private static final String TAG = SearchStoreItemOneSelect.class.getSimpleName();
    Toolbar toolbar;
    DbHandler dbHandler;
    private TextView textViewStoreName, textViewCardCount;
    EditText editTextSearch;
    private String newquery, mSubString;
    String mStoreId, mStoreName, mItemId, mItemPrice, mItemWeight, mItemImage, mItemName, mMerchantId;
    public RelativeLayout relativeLayoutLoading;
    public SwipeRefreshLayout swipeRefreshLayout;
    public RecyclerView recyclerView;
    public List<SearchAllStoreItems> storeItemList;
    public SearchAllStoreItemAdapter adapter;
    public LinearLayoutManager linearLayoutManager;
    private AlertDialog alert;
    public NetworkImageView networkImageView;
    public ImageLoader imageLoader;
    CorbelTextView textViewContitueShopping;
    TextView textViewPriceDisplay;
    public static int index = -1;
    public static int top = -1;


    String mProductName, mQuantityCount, mPrice, mWeight;
    Integer abc, def, ghi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_store_item_one_select);


        Log.d("OneSelect","ss");
        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }

        ghi = 0;

        toolbar = findViewById(R.id.search_store_toolbar);
        setSupportActionBar(toolbar);
        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_arrow_back_black_24dp));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        dbHandler = DbHandler.getInstance(getApplicationContext());
        textViewStoreName = findViewById(R.id._shop_name);
        editTextSearch = findViewById(R.id._search);
        textViewCardCount = findViewById(R.id.cart_badge_store_item);
        textViewContitueShopping = findViewById(R.id._continue_shopping);
        textViewPriceDisplay = findViewById(R.id._price_display);
        editTextSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence query, int start, int before, int count) {
                if (count >= 2) {
                    mSubString = String.valueOf(query);
                    newquery = mSubString.substring(0, 1).toUpperCase() + mSubString.substring(1);
                } else {
                    mSubString = String.valueOf(query);
                    newquery = query.toString().toUpperCase();
                }

                final List<SearchAllStoreItems> filteredList = new ArrayList<>();
                if (storeItemList != null) {
                    for (int i = 0; i < storeItemList.size(); i++) {
                        try {
                            final String text = storeItemList.get(i).getItemName();
                            if (text.contains(newquery)) {
                                filteredList.add(storeItemList.get(i));
                                updateRecyclerView(filteredList, false);
                            }
                        } catch (NullPointerException e) {
                            e.printStackTrace();
                        }
                    }
                }

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        relativeLayoutLoading = findViewById(R.id._loading_store_its);
        swipeRefreshLayout = findViewById(R.id.store_item_refresh);
        swipeRefreshLayout.setOnRefreshListener(this);
        recyclerView = findViewById(R.id._recyclerview_store_items);
        linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        storeItemList = new ArrayList<>();

        getInent();
        serviceCall();


        SharedPreferences preferences1 = PreferenceManager.getDefaultSharedPreferences(SearchStoreItemOneSelect.this);
        textViewStoreName.setText("You are Shopping in "+preferences1.getString("storename",""));
        Log.d("Storename",preferences1.getString("storename",""));
        textViewContitueShopping.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
















    }

    public void serviceCall() {



        SessionManager sessionManager = new SessionManager(getApplicationContext());
        if (sessionManager.isLoggedIn()) {
            if (AppUtils.internetConnectionAvailable(2000)) {
                getCartItems();
            } else {
                Toast.makeText(getApplicationContext(), "No Network Connection",
                        Toast.LENGTH_SHORT).show();
            }
        } else {
            if (AppUtils.internetConnectionAvailable(2000)) {
                SearchStoreItems();
            } else {
                Toast.makeText(getApplicationContext(), "No Network Connection",
                        Toast.LENGTH_SHORT).show();
            }
        }
    }


    public void getCartItems() {

        // Getting the Cart item which is saved in the cart and saving it in the sqlite.
        // Before saving it to sqlite, we are deleting the cart in the local and then save.


        SessionManager sessionManager = new SessionManager(getApplicationContext());
        HashMap<String, String> hashMap = sessionManager.getUserDetails();
        String mUserid = hashMap.get(SessionManager.KEY_USER_ID);
        String mDeviceId = hashMap.get(SessionManager.KEY_DEVICE_ID);
        String mSessionId = hashMap.get(SessionManager.KEY_SESSION_ID);

        HashMap<String, String> params = new HashMap<String, String>();
        params.put("userId", mUserid);
        params.put("deviceUniqueId", mDeviceId);
        params.put("storeId", Preferences.getDefaults("postStoreId", getApplicationContext()));
        params.put("sessionId", mSessionId);


        Log.d(TAG, "getCartItemsParams " + params);

        JsonObjectRequest request_json = new JsonObjectRequest(EndPoint.GET_USER_CART_ITEMS, new JSONObject(params),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, "getCartItemsParamsresponse: " + response);

                        dbHandler.deleteCartItem(); // deleting the local cart
                        storeItemList.clear();  // deleting the arraylist

                        try {
                            Integer mStatusCode = response.getInt("responseCode");
                            if (mStatusCode == 200) {
                                JSONArray array = response.optJSONArray("userCartItemList");

                                if (array != null) {
                                    for (int i = 0; i < array.length(); i++) {
                                        JSONObject json = array.getJSONObject(i);

                                        String mItemId = json.getString("itemId");
                                        String itemMerchantId = json.getString("itemMerchantId");
                                        String mItemName = json.getString("itemName");
                                        String mItemImage = json.getString("itemImage");
                                        String itemPrice = json.getString("itemPrice");
                                        String itemQuantity = json.getString("itemQuantity");
                                        String totalPrice = json.getString("totalPrice");
                                        String itemWeight = json.getString("itemWeight");


                                        // Saving Values in Sqlite Database
                                        AddToCart addToCart = new AddToCart();
                                        addToCart.setMItem_id(Integer.valueOf(mItemId));
                                        addToCart.setMItem_merchant_id(Integer.valueOf(itemMerchantId));
                                        addToCart.setMItem_Count(Integer.valueOf(itemQuantity));
                                        addToCart.setMItem_Name(mItemName);
                                        addToCart.setMItem_Catagory("null");
                                        addToCart.setMItem_Image(mItemImage);
                                        addToCart.setMItem_Price(itemPrice);
                                        addToCart.setMItem_Weight(itemWeight);
                                        addToCart.setMItem_individual_total_amount(Integer.valueOf(totalPrice));
                                        dbHandler.addToCart(addToCart);


                                        // Broadcasting to MainActivity to update the cart count
                                        Intent intent = new Intent("filter_string");
                                        intent.putExtra("key", "value");
                                        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);


                                    }
                                }

                                if (AppUtils.internetConnectionAvailable(2000)) {
                                    SearchStoreItems();
                                } else {
                                    Toast.makeText(getApplicationContext(), "No Network Connection",
                                            Toast.LENGTH_SHORT).show();
                                }


                            } else if (mStatusCode == 400) {

                                if (AppUtils.internetConnectionAvailable(2000)) {
                                    SearchStoreItems();
                                } else {
                                    Toast.makeText(getApplicationContext(), "No Network Connection",
                                            Toast.LENGTH_SHORT).show();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkUtils.UnableToReachServer(getApplicationContext());
            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(request_json);
    }





    public void callRefresh() {
        saving();
        storeItemList.clear();
        serviceCall();
    }


    public void saving() {
        //read current recyclerview position
        index = linearLayoutManager.findFirstVisibleItemPosition();
        View v = recyclerView.getChildAt(0);
        top = (v == null) ? 0 : (v.getTop() - recyclerView.getPaddingTop());
    }

    public void retreiving() {
        if (index != -1) {
            linearLayoutManager.scrollToPositionWithOffset(index, top);
        }
    }


    public void getInent() {
        Intent intent = getIntent();
        try {
            mStoreId = intent.getStringExtra("mStoreId");
            mStoreName = intent.getStringExtra("mStoreName");
            mItemId = intent.getStringExtra("mItemId");
            mItemName = intent.getStringExtra("mItemName");
            mItemImage = intent.getStringExtra("mItemImage");
            mItemPrice = intent.getStringExtra("mItemPrice");
            mItemWeight = intent.getStringExtra("mItemWeight");
            mMerchantId = intent.getStringExtra("mMerchantId");
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        textViewStoreName.setText(mStoreName);
    }


    public void updateRecyclerView(List<SearchAllStoreItems> lst, boolean swipeStatus) {
        recyclerView = findViewById(R.id._recyclerview_store_items);
        recyclerView.setHasFixedSize(true);
        linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        adapter = new SearchAllStoreItemAdapter(lst, this, this);
        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        swipeRefreshLayout.setRefreshing(swipeStatus);
    }


    @Override
    public void onRefresh() {
        recyclerView.setVisibility(View.GONE);
        storeItemList.clear();

        serviceCall();

    }

    public void SearchStoreItems() {
        if (storeItemList != null) {
            storeItemList.clear();
        }

        HashMap<String, String> params = new HashMap<String, String>();
        params.put("storeId", mStoreId);

        Log.d(TAG, "Product Store Itmes: " + params);

        JsonObjectRequest request_json = new JsonObjectRequest(EndPoint.CLICK_SHOP_USER_STORE_ITMES, new JSONObject(params),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, "SearchStoreItemOnSelectResponse: " + response);
                        recyclerView.setVisibility(View.VISIBLE);
                        try {
                            Integer mStatusCode = response.getInt("responseCode");
                            if (mStatusCode == 200) {
                                JSONArray array = response.optJSONArray("storeSearchItemDataList");
                                parseData(array);
                            } else if (mStatusCode == 400) {
                                Message("No Items In The Store");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                recyclerView.setVisibility(View.GONE);
                NetworkUtils.UnableToReachServer(getApplicationContext());
            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(request_json);
    }


    private void parseData(JSONArray array) {
        for (int i = 0; i < array.length(); i++) {
            SearchAllStoreItems searchAllStoreItems = new SearchAllStoreItems();
            JSONObject json;
            try {
                json = array.getJSONObject(i);
                if (json.getString("itemName") != null && !json.getString("itemName").isEmpty()
                        && !json.getString("itemName").equals("itemName")) {
                    searchAllStoreItems.setItemName(json.getString("itemName"));
                } else {
                    searchAllStoreItems.setItemName("None");
                }

                if (json.getInt("itemMerchantId") != 0) {
                    searchAllStoreItems.setItemMerchantId(json.getInt("itemMerchantId"));
                } else {
                    searchAllStoreItems.setItemMerchantId(0);
                }


                if (json.getString("itemPrice") != null && !json.getString("itemPrice").isEmpty()
                        && !json.getString("itemPrice").equals("itemPrice")) {
                    searchAllStoreItems.setItemPrice(json.getString("itemPrice"));
                } else {
                    searchAllStoreItems.setItemPrice("0");
                }

                if (json.getString("itemWeight") != null && !json.getString("itemWeight").isEmpty()
                        && !json.getString("itemWeight").equals("itemWeight")) {
                    searchAllStoreItems.setItemWeight(json.getString("itemWeight"));
                } else {
                    searchAllStoreItems.setItemWeight("0");
                }

                if (json.getString("itemCategory") != null && !json.getString("itemCategory").isEmpty()
                        && !json.getString("itemCategory").equals("itemCategory")) {
                    searchAllStoreItems.setItemCategory(json.getString("itemCategory"));
                } else {
                    searchAllStoreItems.setItemCategory("None");
                }

                if (json.getString("merchantId") != null && !json.getString("merchantId").isEmpty()
                        && !json.getString("merchantId").equals("merchantId")) {
                    searchAllStoreItems.setMerchantId(json.getInt("merchantId"));
                } else {
                    searchAllStoreItems.setMerchantId(0);
                }

                if (json.getString("itemId") != null && !json.getString("itemId").isEmpty()
                        && !json.getString("itemId").equals("itemId")) {
                    searchAllStoreItems.setItemId(json.getInt("itemId"));

                    if (dbHandler.checkIfTableIsEmpty() == 1) {
                        searchAllStoreItems.setMCheckDbCount("1");

                        Integer mItemIdS = json.getInt("itemId");
                        List<AddToCart> addToCarts = dbHandler.getParticualrColoumn(mItemIdS);
                        for (AddToCart addToCart : addToCarts) {
                            Integer mCount = addToCart.getMItem_Count();
                            searchAllStoreItems.setItemCount(mCount);
                        }
                    } else {
                        searchAllStoreItems.setMCheckDbCount("0");
                    }
                } else {
                    searchAllStoreItems.setItemId(0);
                }

                if (json.getString("itemImage") != null && !json.getString("itemImage").isEmpty()
                        && !json.getString("itemImage").equals("itemImage")) {
                    String mPath = modifyDropboxUrl(json.getString("itemImage"));
                    searchAllStoreItems.setItemImage(mPath);
                } else {
                    searchAllStoreItems.setItemImage("None");
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }
            storeItemList.add(searchAllStoreItems);
            // Collections.sort(recordsList);
        }
        adapter = new SearchAllStoreItemAdapter(storeItemList, getApplicationContext(), this);
        recyclerView.setAdapter(adapter);
        swipeRefreshLayout.setRefreshing(false);
        adapter.notifyDataSetChanged();

        mCartCountDisplay();
        retreiving();


    }


    public static String modifyDropboxUrl(String originalUrl) {
        String newUrl = originalUrl.replace("www.dropbox.", "dl.dropboxusercontent.");
        newUrl = newUrl.replace("dropbox.", "dl.dropboxusercontent.");
        return newUrl;
    }

    public void mCartCountDisplay() {
        abc = 0;
        def = 0;
        ghi = 0;



        int mCartCount = dbHandler.getCartCount();
        dbHandler.close();
        textViewCardCount.setText(String.valueOf(mCartCount));


        List<AddToCart> cartDetails = dbHandler.getCartDetails();
        for (AddToCart addToCart : cartDetails) {
            mProductName = addToCart.getMItem_Name();
            mPrice = addToCart.getMItem_Price();
            mQuantityCount = String.valueOf(addToCart.getMItem_Count());
            mWeight = addToCart.getMItem_Weight();

            abc = Integer.valueOf(mPrice) * Integer.valueOf(mQuantityCount);
            ghi = abc + ghi;
        }
        textViewPriceDisplay.setText(new StringBuilder("Rs. ").append(String.valueOf(ghi)));


    }


    public void Message(String message) {
        LayoutInflater layoutInflater = LayoutInflater.from(this);
        View promptView = layoutInflater.inflate(R.layout.alert_message, null);
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setView(promptView);
        TextView textViewMessage = promptView.findViewById(R.id.alert_message_display);
        textViewMessage.setText(message);
        Button buttonblock = promptView.findViewById(R.id.alert_ok_buton);
        buttonblock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alert.dismiss();
            }
        });
        alert = alertDialogBuilder.create();
        alert.show();
    }


    public void onClickFinish(View v){

        finish();
    }


}
