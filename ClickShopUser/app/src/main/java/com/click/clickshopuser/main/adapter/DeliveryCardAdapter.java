package com.click.clickshopuser.main.adapter;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.click.clickshopuser.R;
import com.click.clickshopuser.appconfig.AppUtils;
import com.click.clickshopuser.appconfig.EndPoint;
import com.click.clickshopuser.appconfig.SessionManager;
import com.click.clickshopuser.inter.DeliveryRefresh;
import com.click.clickshopuser.model.deliveryaddressmodel.UserShippingAddressList;
import com.click.clickshopuser.network.NetworkUtils;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.HashMap;
import java.util.List;

public class DeliveryCardAdapter extends RecyclerView.Adapter<DeliveryCardAdapter.ViewHolder> {
    private static final String TAG = DeliveryCardAdapter.class.getSimpleName();
    private Context context;
    private List<UserShippingAddressList> listInAss;
    private DeliveryRefresh deliveryRefresh;
    private ProgressDialog progress;
    private int selectedPosition = -1;
    private Integer mShoppingAddressId;
    private static AlertDialog alert;
    View v;

    public DeliveryCardAdapter(List<UserShippingAddressList> listInAss, Context context, DeliveryRefresh deliveryRefresh) {
        super();
        this.listInAss = listInAss;
        this.context = context;
        this.deliveryRefresh = deliveryRefresh;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.delivery_card_adapter, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public int getItemCount() {
        return listInAss.size();
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final UserShippingAddressList delivery = listInAss.get(position);
        holder.textViewAddress.setText(delivery.getAddress());
        holder.textViewPhoneNumber.setText(delivery.getMobileNumber());
//        holder.textViewAddress.setText("Ankyah Infinity, Mysuru");
//        holder.textViewPhoneNumber.setText("+91-9164374686");
        holder.imageViewDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mShoppingAddressId = delivery.getUserShippingAddressId();
                message("Are You Sure Deleting the Selected Address ?");
            }
        });

        holder.radioButton.setOnCheckedChangeListener(null);
        holder.radioButton.setChecked(listInAss.get(position).isSelected());
        if (position == selectedPosition) {
            holder.radioButton.setChecked(true);
        } else {
            holder.radioButton.setChecked(false);
            holder.radioButton.setOnClickListener(onStateChangedListener(holder.radioButton, position));
            holder.radioButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    listInAss.get(holder.getAdapterPosition()).setSelected(isChecked);
                }
            });
        }
    }

    private View.OnClickListener onStateChangedListener(final RadioButton radioButton, final int position) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (radioButton.isChecked()) {
                    selectedPosition = position;
                } else {
                    selectedPosition = -1;
                }
                notifyDataSetChanged();
            }
        };
    }


    class ViewHolder extends RecyclerView.ViewHolder {
        private TextView textViewAddress, textViewPhoneNumber;
        private RadioButton radioButton;
        private ImageView imageViewDelete;

        private ViewHolder(View itemView) {
            super(itemView);
            textViewAddress = itemView.findViewById(R.id._address_display);
            textViewPhoneNumber = itemView.findViewById(R.id._shipping_phone);
            radioButton = itemView.findViewById(R.id._raido_butttons);
            imageViewDelete = itemView.findViewById(R.id._ic_delete__button);
            this.setIsRecyclable(false);
        }
    }


    private void deleteAddress() {
        progress = ProgressDialog.show(v.getContext(), "", "Please wait...", false, false);
        SessionManager sessionManager = new SessionManager(context);
        HashMap<String, String> hashMap = sessionManager.getUserDetails();
        String mUserid = hashMap.get(SessionManager.KEY_USER_ID);
        String mDeviceId = hashMap.get(SessionManager.KEY_DEVICE_ID);
        String mSessionId = hashMap.get(SessionManager.KEY_SESSION_ID);


        HashMap<String, String> params = new HashMap<String, String>();
        params.put("userId", mUserid);
        params.put("userShippingAddressId", String.valueOf(mShoppingAddressId));
        params.put("sessionId", mSessionId);
        params.put("deviceUniqueId", mDeviceId);


        JsonObjectRequest request_json = new JsonObjectRequest(EndPoint.SHIPPING_ADDRESS_DELETE, new JSONObject(params),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, "AddressPostResponse: " + response);
                        progress.dismiss();

                        try {
                            Integer mStatusCode = response.getInt("responseCode");
                            if (mStatusCode == 200) {
                                deliveryRefresh.deliveryRefresh();
                            } else if (mStatusCode == 400) {
                                deliveryRefresh.deliveryRefresh();
                                Toast.makeText(context, "No Address Found",
                                        Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progress.dismiss();
                NetworkUtils.UnableToReachServer(context);
            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(request_json);
    }

    private void message(String message) {
        try {
            LayoutInflater layoutInflater = LayoutInflater.from(v.getContext());
            View promptView = layoutInflater.inflate(R.layout.alert_message_are_your_sure, null);
            final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(v.getContext());
            alertDialogBuilder.setView(promptView);
            TextView textViewMessage = promptView.findViewById(R.id.alert_message_display);
            textViewMessage.setText(message);
            Button buttonblock = promptView.findViewById(R.id.alert_ok_buton);
            buttonblock.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (AppUtils.internetConnectionAvailable(2000)) {
                        deleteAddress();
                    } else {
                        Toast.makeText(context, "No Network Connection",
                                Toast.LENGTH_SHORT).show();
                    }

                    alert.dismiss();
                }
            });
            Button buttoncancel = promptView.findViewById(R.id.alert_cancel);
            buttoncancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    alert.dismiss();

                }
            });
            alert = alertDialogBuilder.create();
            alert.show();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}