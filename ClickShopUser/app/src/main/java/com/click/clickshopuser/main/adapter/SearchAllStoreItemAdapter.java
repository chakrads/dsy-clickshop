package com.click.clickshopuser.main.adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.Volley;
import com.click.clickshopuser.R;
import com.click.clickshopuser.appconfig.AppUtils;
import com.click.clickshopuser.appconfig.Constants;
import com.click.clickshopuser.appconfig.EndPoint;
import com.click.clickshopuser.appconfig.SessionManager;
import com.click.clickshopuser.inter.ShopRefresh;
import com.click.clickshopuser.model.AddToCart;
import com.click.clickshopuser.model.CheckSelectedStore;
import com.click.clickshopuser.model.SearchAllStoreItems;
import com.click.clickshopuser.network.NetworkUtils;
import com.click.clickshopuser.storage.DbHandler;
import com.click.clickshopuser.storage.Preferences;
import com.click.clickshopuser.volley.CustomVolleyRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;

public class SearchAllStoreItemAdapter extends
        RecyclerView.Adapter<SearchAllStoreItemAdapter.ViewHolder> {

    private static final String TAG = SearchAllStoreItemAdapter.class.getSimpleName();
    Context context;
    List<SearchAllStoreItems> listInAss;
    public NetworkImageView networkImageView;
    public ImageLoader imageLoader;
    private static DbHandler dbHandler;
    final ShopRefresh shopRefresh;
    private ProgressDialog progress;
    View v;

    Integer mItemTogalAmountupdate;
    Integer mItemPrice;

    private int mSelectedStoreId;


    public SearchAllStoreItemAdapter(List<SearchAllStoreItems> listInAss, Context context, ShopRefresh shopRefresh) {
        super();
        this.listInAss = listInAss;
        this.context = context;
        this.shopRefresh = shopRefresh;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.search_all_store_item_adapter, parent, false);
        dbHandler = DbHandler.getInstance(context);

        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public int getItemCount() {
        return listInAss.size();
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        final SearchAllStoreItems SearchAllStoreItems = listInAss.get(position);
        final String image = SearchAllStoreItems.getItemImage();
        imageLoader = CustomVolleyRequest.getInstance(context).getImageLoader();
        imageLoader.get(image, ImageLoader.getImageListener(networkImageView, R.drawable.noimage,
                R.drawable.noimage));
        networkImageView.setImageUrl(image, imageLoader);
        holder.textViewItemName.setText(SearchAllStoreItems.getItemName());
        holder.textViewItemPrice.setText(new StringBuilder("MRP. Rs ").append(SearchAllStoreItems.getItemPrice()));
        holder.textViewItmeWeight.setText(new StringBuilder("Qty - ").append(SearchAllStoreItems.getItemWeight()));


        // Checking the Database Count here. If row is Available then First Condition else Next Condition Works.
        // Next Checking No of Rows in Cart and Executing the function.

        if (SearchAllStoreItems.getMCheckDbCount().equalsIgnoreCase("1")) {
            if (SearchAllStoreItems.getItemCount() >= 1) {
                holder.buttonAdd.setVisibility(View.GONE);
                holder.imageViewButtonAdd.setVisibility(View.VISIBLE);
                holder.imageViewSubtractButton.setVisibility(View.VISIBLE);
                holder.textViewCountDisplay.setVisibility(View.VISIBLE);

                String mCount = String.valueOf(SearchAllStoreItems.getItemCount());
                holder.textViewCountDisplay.setText(mCount);
            } else {
                holder.textViewCountDisplay.setVisibility(View.GONE);
                holder.imageViewButtonAdd.setVisibility(View.GONE);
                holder.imageViewSubtractButton.setVisibility(View.GONE);
                holder.buttonAdd.setVisibility(View.VISIBLE);
            }
        } else {
            holder.textViewCountDisplay.setVisibility(View.GONE);
            holder.imageViewButtonAdd.setVisibility(View.GONE);
            holder.imageViewSubtractButton.setVisibility(View.GONE);
            holder.buttonAdd.setVisibility(View.VISIBLE);
        }


        holder.imageViewButtonAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = holder.getAdapterPosition();

                // Saving the position of the Seletec Card Adapter.
                cardPosition(position);

                // Addition is done here
                int mCount = Integer.valueOf(SearchAllStoreItems.getItemCount());
                int mCountAddOne = mCount + 1;

                Integer mItemId = SearchAllStoreItems.getItemId();
                Integer mItemMerchantId = SearchAllStoreItems.getItemMerchantId();
                String mItemPric = SearchAllStoreItems.getItemPrice();

                SessionManager sessionManager = new SessionManager(context);
                if (sessionManager.isLoggedIn()) {
                    if (AppUtils.internetConnectionAvailable(2000)) {

                        Integer mActualPrice = Integer.valueOf(SearchAllStoreItems.getItemPrice());
                        Integer mItemIdP = SearchAllStoreItems.getItemId();
                        List<AddToCart> addToCarts = dbHandler.getParticualrColoumn(mItemIdP);
                        for (AddToCart addToCart : addToCarts) {
                            mItemPrice = addToCart.getMItem_individual_total_amount();
                            mItemTogalAmountupdate = mItemPrice + mActualPrice;
                        }

                        add(mItemId, mItemMerchantId, mCountAddOne, String.valueOf(mItemPric), String.valueOf(mItemTogalAmountupdate));

                    } else {
                        Toast.makeText(context, "No Network Connection",
                                Toast.LENGTH_SHORT).show();
                    }

                } else {

                    Integer mActualPrice = Integer.valueOf(SearchAllStoreItems.getItemPrice());
                    Integer mItemIdP = SearchAllStoreItems.getItemId();
                    List<AddToCart> addToCarts = dbHandler.getParticualrColoumn(mItemIdP);
                    for (AddToCart addToCart : addToCarts) {
                        Integer mItemPrice = addToCart.getMItem_individual_total_amount();
                        int mItemTogalAmount = mItemPrice + mActualPrice;
                        dbHandler.updateIndividualItemPrice(mItemId, mItemTogalAmount);
                    }
                    dbHandler.updateitem(mItemId, mCountAddOne);
                    shopRefresh.callRefresh();
                }


            }
        });


        holder.imageViewSubtractButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = holder.getAdapterPosition();

                if (Preferences.getDefaults(Preferences.SHARE_PREF_POSITION, context) != null) {
                    Preferences.deletePrefs(Preferences.SHARE_PREF_POSITION, context);
                }
                Preferences.setDefaults(Preferences.SHARE_PREF_POSITION, String.valueOf(position), context);

                Integer mItemId = SearchAllStoreItems.getItemId();
                if (Preferences.getDefaults(Constants.ITEM_ID, context) != null) {
                    Preferences.deletePrefs(Constants.ITEM_ID, context);
                }
                Preferences.setDefaults(Constants.ITEM_ID, String.valueOf(mItemId), context);

                // Subtraction done here
                int mCount = Integer.valueOf(SearchAllStoreItems.getItemCount());
                int mCountSub = mCount - 1;

                Integer mItemMerchantId = SearchAllStoreItems.getItemMerchantId();
                String mItemPrics = SearchAllStoreItems.getItemPrice();


                SessionManager sessionManager = new SessionManager(context);
                if (sessionManager.isLoggedIn()) {
                    if (AppUtils.internetConnectionAvailable(2000)) {

                        Integer mActualPrice = Integer.valueOf(SearchAllStoreItems.getItemPrice());
                        Integer mItemIdP = SearchAllStoreItems.getItemId();
                        List<AddToCart> addToCarts = dbHandler.getParticualrColoumn(mItemIdP);
                        for (AddToCart addToCart : addToCarts) {
                            Integer mItemPricesub = addToCart.getMItem_individual_total_amount();
                            Integer mItemTogalAmountupdatesub = mItemPricesub - mActualPrice;
                            subtract(mItemId, mItemMerchantId, mCountSub, mItemPrics, String.valueOf(mItemTogalAmountupdatesub), mActualPrice);
                        }

                    } else {
                        Toast.makeText(context, "No Network Connection",
                                Toast.LENGTH_SHORT).show();
                    }
                } else {


                    if (mCountSub == 0) {
                        Integer mSeletedItem = SearchAllStoreItems.getItemId();
                        dbHandler.deleteCartRow(mSeletedItem);

                        // Sending a message to MainActivity to Refresh The Cart Count in Bottom bar
                        Intent intent = new Intent("filter_string");
                        intent.putExtra("key", "value");
                        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
                    }

                    Integer mActualPrice = Integer.valueOf(SearchAllStoreItems.getItemPrice());
                    Integer mItemIdP = SearchAllStoreItems.getItemId();
                    List<AddToCart> addToCarts = dbHandler.getParticualrColoumn(mItemIdP);
                    for (AddToCart addToCart : addToCarts) {
                        Integer mItemPrice = addToCart.getMItem_individual_total_amount();
                        int mItemTogalAmount = mItemPrice - mActualPrice;
                        dbHandler.updateIndividualItemPrice(mItemId, mItemTogalAmount);
                    }
                    dbHandler.updateitem(mItemId, mCountSub);
                    shopRefresh.callRefresh();


                }
            }
        });


        holder.buttonAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = holder.getAdapterPosition();
                if (Preferences.getDefaults(Preferences.SHARE_PREF_POSITION, context) != null) {
                    Preferences.deletePrefs(Preferences.SHARE_PREF_POSITION, context);
                }
                Preferences.setDefaults(Preferences.SHARE_PREF_POSITION, String.valueOf(position), context);

                Integer mStoreIdCheck = Integer.valueOf(Preferences.getDefaults("postStoreId", context));

                int mCount = 1;

                Integer mItemId = SearchAllStoreItems.getItemId();
                String mItemName = SearchAllStoreItems.getItemName();
                String mItemCatagory = SearchAllStoreItems.getItemCategory();
                String mItemImageee = SearchAllStoreItems.getItemImage();
                String mItemPrice = SearchAllStoreItems.getItemPrice();
                String mItemWeight = SearchAllStoreItems.getItemWeight();
                Integer mItemMerchantId = SearchAllStoreItems.getItemMerchantId();


                SessionManager sessionManager = new SessionManager(context);
                if (sessionManager.isLoggedIn()) {
                    if (AppUtils.internetConnectionAvailable(2000)) {
                        addItemToCart(mItemId, mItemMerchantId, mCount, mItemPrice, mItemPrice,
                                mItemName, mItemCatagory, mItemImageee, mItemWeight);
                    } else {
                        Toast.makeText(context, "No Network Connection",
                                Toast.LENGTH_SHORT).show();
                    }


                    CheckSelectedStore checkSelectedStore = new CheckSelectedStore();
                    checkSelectedStore.setMStoreId(mStoreIdCheck);
                    dbHandler.selectedShop(checkSelectedStore);


                } else {


                    // Saving Values in Sqlite Database
                    AddToCart addToCart = new AddToCart();
                    addToCart.setMItem_id(mItemId);
                    addToCart.setMItem_merchant_id(mItemMerchantId);
                    addToCart.setMItem_Count(mCount);
                    addToCart.setMItem_Name(mItemName);
                    addToCart.setMItem_Catagory(mItemCatagory);
                    addToCart.setMItem_Image(mItemImageee);
                    addToCart.setMItem_Price(mItemPrice);
                    addToCart.setMItem_Weight(mItemWeight);
                    addToCart.setMItem_individual_total_amount(Integer.valueOf(mItemPrice));
                    dbHandler.addToCart(addToCart);

                    // Saving the Seleted Store id in Sqlite Database.
                    CheckSelectedStore checkSelectedStore = new CheckSelectedStore();
                    checkSelectedStore.setMStoreId(mStoreIdCheck);
                    dbHandler.selectedShop(checkSelectedStore);

                    // Sending a message to MainActivity to Refresh The Cart Count in Bottom bar
                    Intent intent = new Intent("filter_string");
                    intent.putExtra("key", "value");
                    LocalBroadcastManager.getInstance(context).sendBroadcast(intent);

                    // Calling method in Activity to refresh Recyclerview through interface.
                    shopRefresh.callRefresh();


                }


            }
        });

        holder.setIsRecyclable(false);
    }


    private void cardPosition(int position) {
        // This is created to get the position of the adapter and displaying the same cardview after refresh // not yet done..
        if (Preferences.getDefaults(Preferences.SHARE_PREF_POSITION, context) != null) {
            Preferences.deletePrefs(Preferences.SHARE_PREF_POSITION, context);
        }
        Preferences.setDefaults(Preferences.SHARE_PREF_POSITION, String.valueOf(position), context);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private TextView textViewItemName;
        private TextView textViewItemPrice;
        private TextView textViewItmeWeight;
        private Button imageViewButtonAdd;
        private Button imageViewSubtractButton;
        private TextView textViewCountDisplay;
        private Button buttonAdd;

        private ViewHolder(View itemView) {
            super(itemView);
            networkImageView = itemView.findViewById(R.id._store_item_image);
            textViewItemName = itemView.findViewById(R.id._store_item_name);
            textViewItemPrice = itemView.findViewById(R.id._itme_price);
            textViewItmeWeight = itemView.findViewById(R.id._itme_weight);
            imageViewButtonAdd = itemView.findViewById(R.id._add_button);
            imageViewSubtractButton = itemView.findViewById(R.id._subtract_button);
            textViewCountDisplay = itemView.findViewById(R.id._count_display);
            buttonAdd = itemView.findViewById(R.id._add_first_button);

            this.setIsRecyclable(false);
        }
    }


    private void addItemToCart(final Integer mItemId, final Integer mItemMerchantId,
                               final Integer mItemQuantity, final String mItemPrice,
                               final String mTotalPrice,
                               final String mItemName,
                               final String mItemCatagory,
                               final String mItemImageee,
                               final String mItemWeight
    ) {
        progress = ProgressDialog.show(v.getContext(), "",
                "Please wait...", false, false);
        SessionManager sessionManager = new SessionManager(context);
        HashMap<String, String> hashMap = sessionManager.getUserDetails();
        String mUserid = hashMap.get(SessionManager.KEY_USER_ID);
        String mDeviceId = hashMap.get(SessionManager.KEY_DEVICE_ID);
        String mSessionId = hashMap.get(SessionManager.KEY_SESSION_ID);


        HashMap<String, String> params = new HashMap<String, String>();
        params.put("userId", mUserid);
        params.put("storeId", Preferences.getDefaults("postStoreId", context));
        params.put("itemMerchantId", String.valueOf(mItemMerchantId));
        params.put("itemQuantity", String.valueOf(mItemQuantity));
        params.put("itemPrice", mItemPrice);
        params.put("totalPrice", mTotalPrice);
        params.put("deviceUniqueId", mDeviceId);
        params.put("sessionId", mSessionId);

        Log.d(TAG, "AddressPostparams: " + params);


        JsonObjectRequest request_json = new JsonObjectRequest(EndPoint.ADD_CART_ITEM, new JSONObject(params),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, "AddressPostResponse: " + response);
                        progress.dismiss();

                        try {
                            Integer mStatusCode = response.getInt("responseCode");
                            if (mStatusCode == 200) {
                                // Saving Values in Sqlite Database
                                AddToCart addToCart = new AddToCart();
                                addToCart.setMItem_id(mItemId);
                                addToCart.setMItem_merchant_id(mItemMerchantId);
                                addToCart.setMItem_Count(mItemQuantity);
                                addToCart.setMItem_Name(mItemName);
                                addToCart.setMItem_Catagory(mItemCatagory);
                                addToCart.setMItem_Image(mItemImageee);
                                addToCart.setMItem_Price(mItemPrice);
                                addToCart.setMItem_Weight(mItemWeight);
                                addToCart.setMItem_individual_total_amount(Integer.valueOf(mItemPrice));
                                dbHandler.addToCart(addToCart);

                                // Sending a message to MainActivity to Refresh The Cart Count in Bottom bar
                                Intent intent = new Intent("filter_string");
                                intent.putExtra("key", "value");
                                LocalBroadcastManager.getInstance(context).sendBroadcast(intent);

                                // Calling method in Activity to refresh Recyclerview through interface.
                                shopRefresh.callRefresh();
                            } else if (mStatusCode == 400) {
                                Toast.makeText(context, "Adding Item to Cart Failed..",
                                        Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progress.dismiss();
                NetworkUtils.UnableToReachServer(context);
            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(request_json);
    }


    private void add(final Integer mItemIdAdd, final Integer mItemMerchantIdAdd,
                     final Integer mItemQuantityAdd, final String mItemPriceAdd,
                     final String mTotalPriceAdd

    ) {
        progress = ProgressDialog.show(v.getContext(), "",
                "Please wait...", false, false);
        SessionManager sessionManager = new SessionManager(context);
        HashMap<String, String> hashMap = sessionManager.getUserDetails();
        String mUserid = hashMap.get(SessionManager.KEY_USER_ID);
        String mDeviceId = hashMap.get(SessionManager.KEY_DEVICE_ID);
        String mSessionId = hashMap.get(SessionManager.KEY_SESSION_ID);


        HashMap<String, String> params = new HashMap<String, String>();
        params.put("userId", mUserid);
        params.put("storeId", Preferences.getDefaults("postStoreId", context));
        params.put("itemMerchantId", String.valueOf(mItemMerchantIdAdd));
        params.put("itemQuantity", String.valueOf(mItemQuantityAdd));
        params.put("itemPrice", mItemPriceAdd);
        params.put("totalPrice", mTotalPriceAdd);
        params.put("deviceUniqueId", mDeviceId);
        params.put("sessionId", mSessionId);

        Log.d(TAG, "AddressPostparams: " + params);


        JsonObjectRequest request_json = new JsonObjectRequest(EndPoint.ADD_CART_ITEM, new JSONObject(params),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, "AddressPostResponse: " + response);
                        progress.dismiss();

                        try {
                            Integer mStatusCode = response.getInt("responseCode");
                            if (mStatusCode == 200) {

                                dbHandler.updateIndividualItemPrice(mItemIdAdd, Integer.valueOf(mTotalPriceAdd));
                                dbHandler.updateitem(mItemIdAdd, mItemQuantityAdd);

                                // Sending a message to MainActivity to Refresh The Cart Count in Bottom bar
                                Intent intent = new Intent("filter_string");
                                intent.putExtra("key", "value");
                                LocalBroadcastManager.getInstance(context).sendBroadcast(intent);

                                // Calling method in Activity to refresh Recyclerview through interface.
                                shopRefresh.callRefresh();
                            } else if (mStatusCode == 400) {
                                Toast.makeText(context, "Adding Item to Cart Failed..",
                                        Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progress.dismiss();
                NetworkUtils.UnableToReachServer(context);
            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(request_json);
    }


    private void subtract(final Integer mItemIdS, final Integer mItemMerchantIdSubtract,
                          final Integer mCountSubtract, final String mItemPriceSubtract,
                          final String mTotalPriceSubtract,
    final Integer mActualPrice
    ) {
        progress = ProgressDialog.show(v.getContext(), "",
                "Please wait...", false, false);
        SessionManager sessionManager = new SessionManager(context);
        HashMap<String, String> hashMap = sessionManager.getUserDetails();
        String mUserid = hashMap.get(SessionManager.KEY_USER_ID);
        String mDeviceId = hashMap.get(SessionManager.KEY_DEVICE_ID);
        String mSessionId = hashMap.get(SessionManager.KEY_SESSION_ID);


        HashMap<String, String> params = new HashMap<String, String>();
        params.put("userId", mUserid);
        params.put("storeId", Preferences.getDefaults("postStoreId", context));
        params.put("itemMerchantId", String.valueOf(mItemMerchantIdSubtract));
        params.put("itemQuantity", String.valueOf(mCountSubtract));
        params.put("itemPrice", mItemPriceSubtract);
        params.put("totalPrice", mTotalPriceSubtract);
        params.put("deviceUniqueId", mDeviceId);
        params.put("sessionId", mSessionId);


        JsonObjectRequest request_json = new JsonObjectRequest(EndPoint.ADD_CART_ITEM, new JSONObject(params),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, "AddressPostResponse: " + response);
                        progress.dismiss();

                        try {
                            Integer mStatusCode = response.getInt("responseCode");
                            if (mStatusCode == 200) {


                                if (mCountSubtract == 0) {

                                    dbHandler.deleteCartRow(mItemIdS);

                                    // Sending a message to MainActivity to Refresh The Cart Count in Bottom bar
                                    Intent intent = new Intent("filter_string");
                                    intent.putExtra("key", "value");
                                    LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
                                }


                                List<AddToCart> addToCarts = dbHandler.getParticualrColoumn(mItemIdS);
                                for (AddToCart addToCart : addToCarts) {
                                    Integer mItemPrice = addToCart.getMItem_individual_total_amount();
                                    int mItemTogalAmount = mItemPrice - mActualPrice;
                                    dbHandler.updateIndividualItemPrice(mItemIdS, mItemTogalAmount);
                                }

                                dbHandler.updateIndividualItemPrice(mItemIdS, Integer.valueOf(mTotalPriceSubtract));
                                dbHandler.updateitem(mItemIdS, mCountSubtract);

                                Integer mCartCount = dbHandler.getCartCount();

                                if (mCartCount == 0) {
                                    if (AppUtils.internetConnectionAvailable(2000)) {
                                        deleteSelectedStore();
                                    } else {
                                        Toast.makeText(context, "No Network Connection",
                                                Toast.LENGTH_SHORT).show();
                                    }
                                }else {
                                    shopRefresh.callRefresh();
                                }



                            } else if (mStatusCode == 400) {
                                Toast.makeText(context, "Subtract Item to Cart Failed..",
                                        Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progress.dismiss();
                NetworkUtils.UnableToReachServer(context);
            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(request_json);
    }


    private void deleteSelectedStore() {
        SessionManager sessionManager = new SessionManager(context);
        HashMap<String, String> hashMap = sessionManager.getUserDetails();
        String mUserid = hashMap.get(SessionManager.KEY_USER_ID);
        String mDeviceId = hashMap.get(SessionManager.KEY_DEVICE_ID);
        String mSessionId = hashMap.get(SessionManager.KEY_SESSION_ID);

        HashMap<String, String> params = new HashMap<String, String>();
        params.put("userId", mUserid);
        params.put("deviceUniqueId", mDeviceId);
        params.put("sessionId", mSessionId);

        Log.d(TAG, "DeleteStoreIdParams: " + params);

        JsonObjectRequest request_json = new JsonObjectRequest(EndPoint.DELETE_SELECTED_STORE, new JSONObject(params),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, "DelteStoreIdResponse: " + response);

                        try {
                            Integer mStatusCode = response.getInt("responseCode");
                            if (mStatusCode == 200) {
                                shopRefresh.callRefresh();


                            } else if (mStatusCode == 400) {
                                shopRefresh.callRefresh();

                            }


                            // Sending a message to MainActivity to Refresh The Cart Count in Bottom bar
                            Intent intent = new Intent("filter_string");
                            intent.putExtra("key", "value");
                            LocalBroadcastManager.getInstance(context).sendBroadcast(intent);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                NetworkUtils.UnableToReachServer(context);
            }
        });
        try {
            RequestQueue requestQueue = Volley.newRequestQueue(context);
            requestQueue.add(request_json);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }


}




