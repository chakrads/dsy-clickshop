package com.click.clickshopuser.main.activity;
import android.Manifest;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.provider.Settings;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.click.clickshopuser.R;
import com.click.clickshopuser.appconfig.ApiClientUser;
import com.click.clickshopuser.appconfig.ApiInterface;
import com.click.clickshopuser.appconfig.AppUtils;
import com.click.clickshopuser.appconfig.CustomTypefaceSpan;
import com.click.clickshopuser.appconfig.EndPoint;
import com.click.clickshopuser.appconfig.GPSTracker;
import com.click.clickshopuser.appconfig.SessionManager;
import com.click.clickshopuser.main.bottombar.CartActivity;
import com.click.clickshopuser.main.bottombar.CartFragment;
import com.click.clickshopuser.main.bottombar.NearbyShop;
import com.click.clickshopuser.main.bottombar.OfferFragment;
import com.click.clickshopuser.main.fragment.ContactUsFragment;
import com.click.clickshopuser.main.fragment.Home;
import com.click.clickshopuser.main.fragment.MyAccountFragment;
import com.click.clickshopuser.main.fragment.MyOrderFragment;
import com.click.clickshopuser.main.fragment.PrivacyPolicy;
import com.click.clickshopuser.model.GeoLocation;
import com.click.clickshopuser.model.signout.RequestBodySignOut;
import com.click.clickshopuser.model.signout.SignOut;
import com.click.clickshopuser.network.NetworkUtils;
import com.click.clickshopuser.places.PlacesActivity;
import com.click.clickshopuser.prelanding.LoginActivity;
import com.click.clickshopuser.pushnotification.SharedPrefManager;
import com.click.clickshopuser.storage.DbHandler;
import com.click.clickshopuser.storage.Preferences;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener  {
    private static final int REQUEST_CODE_GET_ADDRESS = 5;
    private static final String TAG = MainActivity.class.getSimpleName();
    TextView textViewfragmenttitle;
    TextView textViewDisplay;
    ImageView imageViewSignIn;
    Geocoder geocoder;
    double latitude;
    double longitude;
    String postlatitude;
    String postlongitude;
    String postaddress;
    String postcity;
    String poststate;
    String postcountry;
    String postpinCode;
    List<Address> addresses;
    LinearLayout linearLayoutPickUpLocation;
    private AlertDialog alert;
    Menu menu;
    Boolean doubleBackToExitPressedOnce = false;
    ImageView home_image, nearby_image, offer_image, cart_image;
    RelativeLayout relativeLayoutHome, relativeLayoutNearby, relativeLayoutOffer, relativeLayoutCart;
    TextView textViewHome, textViewNearby, textViewOffer, textViewCart;
    public Fragment fragment;
    String mCheck;
    private BroadcastReceiver receiver, cartItemReceiver;
    private LocalBroadcastManager lbm;
    private DbHandler dbHandler;
    private TextView textViewCartCount;
    int mNullcheck;
    GPSTracker gpsTracker;
    SharedPrefManager sharedPrefManager;

    public LocationManager locationManager;
    public Criteria criteria;
    public String bestProvider;
    Location location;



    private static final int PERMISSION_REQUEST_CODE = 1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.fadein, R.anim.fadeout);
        setContentView(R.layout.activity_main);





        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        fragment = null;
        mCheck = null;
        mNullcheck = 0;
        dbHandler = DbHandler.getInstance(getApplicationContext());
        gpsTracker = new GPSTracker(MainActivity.this);
        geocoder = new Geocoder(this, Locale.getDefault());

        relativeLayoutHome = findViewById(R.id._bottom_button_home);
        relativeLayoutNearby = findViewById(R.id._bottom_nearby_shop);
        relativeLayoutOffer = findViewById(R.id._bottom_offer);
        relativeLayoutCart = findViewById(R.id._bottom_cart);

        relativeLayoutHome.setOnClickListener(this);
        relativeLayoutNearby.setOnClickListener(this);
        relativeLayoutOffer.setOnClickListener(this);
        relativeLayoutCart.setOnClickListener(this);

        home_image = findViewById(R.id._home_image);
        nearby_image = findViewById(R.id._nearby_image);
        offer_image = findViewById(R.id._offer_image);
        cart_image = findViewById(R.id._cart_image);

        textViewHome = findViewById(R.id._home_text);
        textViewNearby = findViewById(R.id._nearby_text);
        textViewOffer = findViewById(R.id._offer_text);
        textViewCart = findViewById(R.id._cart_text);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        //add this line to display menu1 when the activity is loaded
        displaySelectedScreen(R.id.nav_menu1);

        textViewCartCount = findViewById(R.id.cart_badge_man_activity);
        textViewfragmenttitle = findViewById(R.id._fragment_title);
        textViewfragmenttitle.setText("Your Location");

        textViewDisplay = findViewById(R.id._location_display);
        textViewDisplay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, PlacesActivity.class);
                startActivityForResult(i, REQUEST_CODE_GET_ADDRESS);
            }
        });


        textViewDisplay.setText("Type Store Location here..");
        imageViewSignIn = findViewById(R.id._user_login);
        imageViewSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                SessionManager sessionManager = new SessionManager(getApplicationContext());
//                if (sessionManager.isLoggedIn()) {
//                    fragment = new MyAccountFragment();
//                    mCheck = "MyAccountFragment";
//
//                    if (fragment != null) {
//                        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
//                        ft.setTransition(FragmentTransaction.TRANSIT_ENTER_MASK);
//                        ft.replace(R.id.content_frame, fragment, mCheck);
//                        ft.addToBackStack(mCheck);
//                        ft.commit();
//                        clickNone();
//                    }
//                } else {
//                    Intent intentSignIn = new Intent(MainActivity.this, LoginActivity.class);
//                    startActivity(intentSignIn);
//                }


                Intent i =new Intent(MainActivity.this,CartActivity.class);
                startActivity(i);

            }
        });

        linearLayoutPickUpLocation = findViewById(R.id._pick_location);
        linearLayoutPickUpLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (AppUtils.internetConnectionAvailable(2000)) {
                    if (!gpsTracker.canGetLocation()) {
                        try {
                            showSettingsAlert("GPS is Not Enabled. Do You Want To Go To Settings Menu?");
                        }catch (RuntimeException e){
                            e.printStackTrace();
                        }

                    } else {
                        Intent i = new Intent(MainActivity.this, PlacesActivity.class);
                        startActivityForResult(i, Activity.RESULT_OK);
                    }
                } else {
                    Message("No Internet Connection");
                }

            }
        });

        menu = navigationView.getMenu();
        for (int i = 0; i < menu.size(); i++) {
            MenuItem mi = menu.getItem(i);
            SubMenu subMenu = mi.getSubMenu();
            if (subMenu != null && subMenu.size() > 0) {
                for (int j = 0; j < subMenu.size(); j++) {
                    MenuItem subMenuItem = subMenu.getItem(j);
                    applyFontToMenuItem(subMenuItem);
                }
            }
            applyFontToMenuItem(mi);
        }


        SessionManager sessionManager = new SessionManager(getApplicationContext());
        if (sessionManager.isLoggedIn()) {
            if (AppUtils.internetConnectionAvailable(2000)) {
                getAddress();
            } else {
                Toast.makeText(getApplicationContext(), "No Network Connection",
                        Toast.LENGTH_SHORT).show();
            }
        } else {
            checkPermission();
        }



        if (dbHandler.checkLocationExists() == 1) {
            dbHandler.deleteGeoLocation();
        }


        getCount();
        Navigation("1");

        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String mReceivedData = intent.getStringExtra("key");
                if (mReceivedData.equalsIgnoreCase("value")) {
                    int mCartCount = dbHandler.getCartCount();
                    dbHandler.close();
                    textViewCartCount.setText(String.valueOf(mCartCount));

                }
            }
        };


        LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver(receiver, new IntentFilter("filter_string"));
        cartItemReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String mReceivedData = intent.getStringExtra("Cart");
                if (mReceivedData.equalsIgnoreCase("Items")) {
                    int mCartCount = dbHandler.getCartCount();
                    dbHandler.close();
                    textViewCartCount.setText(String.valueOf(mCartCount));

                }
            }
        };


        // Registering the Local Broadcast Receiver
        LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver(cartItemReceiver, new IntentFilter("clear_cart_itmes"));
        sharedPrefManager = SharedPrefManager.getInstance(getApplicationContext());
        String mToken = sharedPrefManager.getDeviceToken();



    }



    private void requestPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this, Manifest.permission.ACCESS_FINE_LOCATION)) {
            Message("GPS permission allows us to access location data. Please allow in App Settings for additional functionality.");
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_REQUEST_CODE);
        }
    }

    private boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
        if (result == PackageManager.PERMISSION_GRANTED) {
            if (AppUtils.internetConnectionAvailable(2000)) {
                new FetchLocationTask().execute();
            } else {
                Toast.makeText(getApplicationContext(), "No Network Connection",
                        Toast.LENGTH_SHORT).show();
            }
            return true;
        } else {

            requestPermission();
            return false;
        }
    }


    public void getCount() {
        Integer mCartCount = dbHandler.getCartCount();
        dbHandler.close();
        textViewCartCount.setText(String.valueOf(mCartCount));
    }


    public void getAddress() {
        SessionManager sessionManager = new SessionManager(getApplicationContext());
        HashMap<String, String> hashMap = sessionManager.getUserDetails();
        String mUserid = hashMap.get(SessionManager.KEY_USER_ID);
        String mSessionId = hashMap.get(SessionManager.KEY_SESSION_ID);
        String mDeviceId = hashMap.get(SessionManager.KEY_DEVICE_ID);

        HashMap<String, String> params = new HashMap<String, String>();
        params.put("userId", mUserid);
        params.put("deviceUniqueId", mDeviceId);
        params.put("sessionId", mSessionId);
        Log.d(TAG, "Post Address: " + params);

        JsonObjectRequest request_json = new JsonObjectRequest(EndPoint.CLICK_SHOP_USER_RECEIVE_ADDRESS, new JSONObject(params),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, "Address Response: " + response);
                        try {
                            Integer mStatusCode = response.getInt("responseCode");
                            if (mStatusCode == 200) {
                                postaddress = response.getString("address");
                                double lats = response.getDouble("latitude");
                                double longs = response.getDouble("longitude");
                                postlatitude = String.valueOf(lats);
                                postlongitude = String.valueOf(longs);
                                postcity = response.getString("city");
                                postcountry = response.getString("country");
                                postpinCode = response.getString("pinCode");
                                savingAddressInSharedPreference(postlatitude, postlongitude, postaddress, postcity, postcountry, postpinCode);
                            } else if (mStatusCode == 400) {
                                if (AppUtils.internetConnectionAvailable(2000)) {

                                    new FetchLocationTask().execute();
                                } else {
                                    Toast.makeText(getApplicationContext(), "No Network Connection",
                                            Toast.LENGTH_SHORT).show();
                                }

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkUtils.UnableToReachServer(getApplicationContext());
            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(request_json);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id._bottom_button_home:
                fragment = new Home();
                clickHome();
                clearBackStack();
                mCheck = "Home";
                break;
            case R.id._bottom_nearby_shop:
                fragment = new NearbyShop();
                clickNearby();
                mCheck = "NearbyShop";
                break;
            case R.id._bottom_offer:
                fragment = new OfferFragment();
                clickOffer();
                mCheck = "OfferModelFrag";
                break;
            case R.id._bottom_cart:
                fragment = new CartFragment();
                clickCart();
                mCheck = "CartFragment";
                break;
        }

        //replacing the fragment
        if (fragment != null) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.setTransition(FragmentTransaction.TRANSIT_ENTER_MASK);
            ft.replace(R.id.content_frame, fragment, mCheck);
            ft.addToBackStack(mCheck);
            ft.commit();
        }
    }


    private void applyFontToMenuItem(MenuItem mi) {
        Typeface font = Typeface.createFromAsset(getAssets(), "corbel-1361520258.ttf");
        SpannableString mNewTitle = new SpannableString(mi.getTitle());
        mNewTitle.setSpan(new CustomTypefaceSpan("", font), 0, mNewTitle.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        mi.setTitle(mNewTitle);
    }

    public void Message(String message) {
        LayoutInflater layoutInflater = LayoutInflater.from(this);
        View promptView = layoutInflater.inflate(R.layout.alert_message_retry, null);
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setView(promptView);
        TextView textViewMessage = promptView.findViewById(R.id.alert_message_display);
        textViewMessage.setText(message);
        Button buttonblock = (Button) promptView.findViewById(R.id.alert_ok_buton);
        buttonblock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alert.dismiss();
            }
        });
        alert = alertDialogBuilder.create();
        alert.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            Double mLatitude = Double.valueOf(data.getStringExtra("latitude"));
            Double mLongitude = Double.valueOf(data.getStringExtra("longitude"));
            try {
                try {
                    addresses = geocoder.getFromLocation(mLatitude, mLongitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                } catch (IOException e) {
                    e.printStackTrace();
                }

                if (addresses != null) {
                    postaddress = addresses.get(0).getAddressLine(0);
                    latitude = addresses.get(0).getLatitude();
                    longitude = addresses.get(0).getLongitude();
                    postcity = addresses.get(0).getLocality();
                    poststate = addresses.get(0).getAdminArea();
                    postcountry = addresses.get(0).getCountryName();
                    postpinCode = addresses.get(0).getPostalCode();
                    savingAddressInSharedPreference(String.valueOf(latitude), String.valueOf(longitude), postaddress, postcity, postcountry, postpinCode);
                }
            } catch (IndexOutOfBoundsException e) {
                e.printStackTrace();
            } catch (NullPointerException e) {
                e.printStackTrace();
            }

        }
        if (resultCode == Activity.RESULT_CANCELED) {
        }
    }





    public class FetchLocationTask extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... params) {

            //Note To Developer:
            // If there is any creash or difficulty in fetching the current location then, mostly
            // it is the issue with google api or GPS Tracker.
            // This might be the isssue becuase. the app min sdk version is 16 and max is 28
            // Its a huge gap and targes most of the devices. Therfore we will have to handle the
            // situation for different devices.

            try {


                geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
                if (!gpsTracker.canGetLocation()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            // this will run in the main thread
                            try {
                                showSettingsAlert("GPS is Not Enabled. Do You Want To Go To Settings Menu?");  //
                            }catch (RuntimeException e){e.printStackTrace();}
                        }
                    });
                } else {
                    latitude = gpsTracker.getLatitude();
                    longitude = gpsTracker.getLongitude();
                }

                postlatitude = String.valueOf(latitude);
                postlongitude = String.valueOf(longitude);

                Log.d(TAG, "postlatitude: " + postlatitude);
                Log.d(TAG, "postlongitude: " + postlongitude);


                postlatitude = String.valueOf(latitude);
                postlongitude = String.valueOf(longitude);

                Log.d(TAG, "postlatitude: " + postlatitude);
                Log.d(TAG, "postlongitude: " + postlongitude);

                addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                if (addresses != null) {
                    if (latitude == 0.0){
                        deflatlon ();  // This is the default value set this could be changed later.
                    }else {
                        if (addresses.get(0).getAddressLine(0) != null) {
                            postaddress = addresses.get(0).getAddressLine(0);
                        } else {
                            postaddress = "NA";
                        }
                        if (addresses.get(0).getLocality() != null) {
                            postcity = addresses.get(0).getLocality();
                        } else {
                            postcity = "NA";
                        }
                        if (addresses.get(0).getAdminArea() != null) {
                            poststate = addresses.get(0).getAdminArea();
                        } else {
                            poststate = "NA";
                        }
                        if (addresses.get(0).getCountryName() != null) {
                            postcountry = addresses.get(0).getCountryName();
                        } else {
                            postcountry = "NA";
                        }
                        if (addresses.get(0).getPostalCode() != null) {
                            postpinCode = addresses.get(0).getPostalCode();
                        } else {
                            postpinCode = "NA";
                        }
                    }
                }
            } catch (SecurityException e) {
                mNullcheck = 1;
                e.printStackTrace();
            } catch (IndexOutOfBoundsException e) {
                mNullcheck = 1;
                e.printStackTrace();
            } catch (NullPointerException e) {
                mNullcheck = 1;
                e.printStackTrace();
            } catch (IOException e) {
                mNullcheck = 1;
                e.printStackTrace();
            }

            return "";
        }

        @Override
        protected void onPostExecute(String s) {
            if (mNullcheck == 1) {
                Message("Unable To Fetch Your Present Location. Please Try Typing Store Location Manually ");
            } else {
                savingAddressInSharedPreference(postlatitude, postlongitude, postaddress, postcity, postcountry, postpinCode);
                // Saving all data in Sqlite
            }
        }
    }



    public void showSettingsAlert(String message) {
        LayoutInflater layoutInflater = LayoutInflater.from(this);
        View promptView = layoutInflater.inflate(R.layout.showsettingalert, null);
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setView(promptView);
        TextView textViewMessage = promptView.findViewById(R.id.alert_message_display);
        textViewMessage.setText(message);
        Button buttonblock = promptView.findViewById(R.id.alert_ok_buton);
        buttonblock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
                alert.dismiss();
            }
        });

        Button buttonbcancel = promptView.findViewById(R.id.alert_cancel_buton);
        buttonbcancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                alert.dismiss();
            }
        });

        alert = alertDialogBuilder.create();
        alert.show();
    }



    // Default Lat long to avoid wierd behaviour of app. Change it later as per requirement
    public void deflatlon (){
        latitude = 13.651325;
        longitude = 77.049836;
        postaddress = "Type Store Location..";
        postcity = "NA";
        poststate = "NA";
        postcountry = "NA";
        postpinCode = "NA";
    }


    @Override
    protected void onStop() {
        gpsTracker.stopUsingGPS();  // stopping gps tracker
        super.onStop();
    }

    public void savingAddressInSharedPreference(String postlatitude, String postlongitude, String postaddress, String postcity, String postcountry, String postpinCode) {
        if (dbHandler.checkLocationExists() == 1) {
            dbHandler.deleteGeoLocation();
        }

        GeoLocation geoLocation = new GeoLocation();
        if (postlatitude != null) {
            geoLocation.setMLatitude(postlatitude);
        } else {
            geoLocation.setMLatitude("0.0");
        }
        if (postlongitude != null) {
            geoLocation.setMLongitude(postlongitude);
        } else {
            geoLocation.setMLongitude("0.0");
        }
        if (postaddress != null) {
            geoLocation.setMAddress(postaddress);
        } else {
            geoLocation.setMAddress("Not Available");
        }
        if (postcity != null) {
            geoLocation.setMCity("NA"); // for few location has no city
        } else {
            geoLocation.setMCity("Not Available");
        }
        if (postcountry != null) {
            geoLocation.setMCountry(postcountry);
        } else {
            geoLocation.setMCountry("Not Available");
        }
        if (postpinCode != null) {
            geoLocation.setMPinCode(postpinCode);
        } else {
            geoLocation.setMPinCode("Not Available");
        }

        dbHandler.addGeoLocation(geoLocation);
        textViewDisplay.setText(postaddress);

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent("refresh");
                intent.putExtra("home", "shop");
                LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
            }
        }, 2000);

    }


    public void Navigation(String value) {
        if (Preferences.getDefaults("Navigation", getApplicationContext()) != null) {
            Preferences.deletePrefs("Navigation", getApplicationContext());
        }
        Preferences.setDefaults("Navigation", value, getApplicationContext());
    }


    private void displaySelectedScreen(int itemId) {
        switch (itemId) {

            case R.id.nav_menu0:
                clickHome();
                clearBackStack();
                fragment = new ContactUsFragment();
                mCheck = "Home";
                break;


            case R.id.nav_menu1:
                clickHome();
                clearBackStack();
                fragment = new Home();
                mCheck = "Home";
                break;

            case R.id.nav_menu:
//                clickHome();
//                clearBackStack();
//                fragment = new CartFragment();
//                mCheck = "Home";

                                SessionManager sessionManager = new SessionManager(getApplicationContext());
                if (sessionManager.isLoggedIn()) {
                    fragment = new MyAccountFragment();
                    mCheck = "MyAccountFragment";

                    if (fragment != null) {
                        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                        ft.setTransition(FragmentTransaction.TRANSIT_ENTER_MASK);
                        ft.replace(R.id.content_frame, fragment, mCheck);
                        ft.addToBackStack(mCheck);
                        ft.commit();
                        clickNone();
                    }
                } else {
                    Intent intentSignIn = new Intent(MainActivity.this, LoginActivity.class);
                    startActivity(intentSignIn);
                }

                break;


            case R.id.nav_menu3:
                clickNone();
                SessionManager sessionManager1 = new SessionManager(getApplicationContext());
                if (sessionManager1.isLoggedIn()) {
                    fragment = new MyOrderFragment();
                    mCheck = "MyOrderFragment";
                    clickNone();
                } else {
                    Intent intentSignIn = new Intent(MainActivity.this, LoginActivity.class);
                    startActivity(intentSignIn);
                }
                break;
            case R.id.nav_menu2:
                fragment = new PrivacyPolicy();
                clickNone();
                mCheck = "PrivacyPolicy";
                break;
            case R.id._share:
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                String shareBody = "Download Click Shop Android App on Google Play Store - https://play.google.com/store/apps/details?id=com.click.shopuser";
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Click Shop App");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                startActivity(Intent.createChooser(sharingIntent, "Share via"));

                clickNone();
                break;

            case R.id._logout:


                sessionLogOut();
                break;

        }

        //replacing the fragment
        if (fragment != null) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.setTransition(FragmentTransaction.TRANSIT_ENTER_MASK);
            ft.replace(R.id.content_frame, fragment, mCheck);
            ft.addToBackStack(mCheck);
            ft.commit();
        }
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        displaySelectedScreen(item.getItemId());
        return true;
    }



    public void sessionLogOut() {
        SessionManager sessionManager = new SessionManager(MainActivity.this);
        HashMap<String, String> hashMap = sessionManager.getUserDetails();
        String mPhone = hashMap.get(SessionManager.KEY_PHONE_NUMBER);
        String mDeviceId = hashMap.get(SessionManager.KEY_DEVICE_ID);

        ApiInterface apiInterfacec = ApiClientUser.getApiClient().create(ApiInterface.class);
        Call<SignOut> call = apiInterfacec.signOut(new RequestBodySignOut(mPhone, mDeviceId));
        call.enqueue(new Callback<SignOut>() {
            @Override
            public void onResponse(Call<SignOut> call, retrofit2.Response<SignOut> response) {

                if (response.isSuccessful()) {
                    Toast.makeText( MainActivity.this, "Successfully Logged Out",
                            Toast.LENGTH_SHORT).show();

                    SessionManager.logoutUser();
                   finish();
                }else {

                    SessionManager.logoutUser();
                    finish();

                }
            }

            @Override
            public void onFailure(Call<SignOut> call, Throwable t) {
                SessionManager.logoutUser();
                finish();

            }
        });
    }

    @Override
    public void onBackPressed() {
        Navigation("2");

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            int fragments = getSupportFragmentManager().getBackStackEntryCount();
            if (fragments == 1) {
                if (doubleBackToExitPressedOnce) {
                    System.exit(0);
                    return;
                }
                this.doubleBackToExitPressedOnce = true;
                Toast.makeText(this, "Please click BACK again to exit",
                        Toast.LENGTH_SHORT).show();
            } else {
                if (getFragmentManager().getBackStackEntryCount() > 1) {
                    getFragmentManager().popBackStack();
                } else {
                    super.onBackPressed();
                }
            }
        }

        FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment currentFragment = fragmentManager.findFragmentById(R.id.content_frame);
        if (currentFragment.getTag().equals("Home")) {
            clickHome();
        } else if (currentFragment.getTag().equals("NearbyShop")) {
            clickNearby();
        } else if (currentFragment.getTag().equals("OfferModelFrag")) {
            clickOffer();
        } else if (currentFragment.getTag().equals("CartFragment")) {
            clickCart();
        } else {
            clickNone();
        }
    }


    private void clearBackStack() {
        FragmentManager manager = getSupportFragmentManager();
        if (manager.getBackStackEntryCount() > 0) {
            FragmentManager.BackStackEntry first = manager.getBackStackEntryAt(0);
            manager.popBackStack(first.getId(), FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
    }


//    md_grey_500

    public void clickHome() {
        home_image.getBackground().setColorFilter(getResources().getColor(R.color.colorPrimary), android.graphics.PorterDuff.Mode.SRC_IN);
        nearby_image.getBackground().setColorFilter(getResources().getColor(R.color.md_grey_800), android.graphics.PorterDuff.Mode.SRC_IN);
        offer_image.getBackground().setColorFilter(getResources().getColor(R.color.md_grey_800), android.graphics.PorterDuff.Mode.SRC_IN);
        cart_image.getBackground().setColorFilter(getResources().getColor(R.color.md_grey_800), android.graphics.PorterDuff.Mode.SRC_IN);
        textViewHome.setTextColor(getResources().getColor(R.color.colorPrimary));
        textViewNearby.setTextColor(getResources().getColor(R.color.md_grey_800));
        textViewOffer.setTextColor(getResources().getColor(R.color.md_grey_800));
        textViewCart.setTextColor(getResources().getColor(R.color.md_grey_800));
    }

    public void clickNearby() {
        home_image.getBackground().setColorFilter(getResources().getColor(R.color.md_grey_800), android.graphics.PorterDuff.Mode.SRC_IN);
        nearby_image.getBackground().setColorFilter(getResources().getColor(R.color.colorPrimary), android.graphics.PorterDuff.Mode.SRC_IN);
        offer_image.getBackground().setColorFilter(getResources().getColor(R.color.md_grey_800), android.graphics.PorterDuff.Mode.SRC_IN);
        cart_image.getBackground().setColorFilter(getResources().getColor(R.color.md_grey_800), android.graphics.PorterDuff.Mode.SRC_IN);
        textViewHome.setTextColor(getResources().getColor(R.color.md_grey_800));
        textViewNearby.setTextColor(getResources().getColor(R.color.colorPrimary));
        textViewOffer.setTextColor(getResources().getColor(R.color.md_grey_800));
        textViewCart.setTextColor(getResources().getColor(R.color.md_grey_800));
    }

    public void clickOffer() {
        home_image.getBackground().setColorFilter(getResources().getColor(R.color.md_grey_800), android.graphics.PorterDuff.Mode.SRC_IN);
        nearby_image.getBackground().setColorFilter(getResources().getColor(R.color.md_grey_800), android.graphics.PorterDuff.Mode.SRC_IN);
        offer_image.getBackground().setColorFilter(getResources().getColor(R.color.colorPrimary), android.graphics.PorterDuff.Mode.SRC_IN);
        cart_image.getBackground().setColorFilter(getResources().getColor(R.color.md_grey_800), android.graphics.PorterDuff.Mode.SRC_IN);
        textViewHome.setTextColor(getResources().getColor(R.color.md_grey_800));
        textViewNearby.setTextColor(getResources().getColor(R.color.md_grey_800));
        textViewOffer.setTextColor(getResources().getColor(R.color.colorPrimary));
        textViewCart.setTextColor(getResources().getColor(R.color.md_grey_800));
    }

    public void clickCart() {
        home_image.getBackground().setColorFilter(getResources().getColor(R.color.md_grey_800), android.graphics.PorterDuff.Mode.SRC_IN);
        nearby_image.getBackground().setColorFilter(getResources().getColor(R.color.md_grey_800), android.graphics.PorterDuff.Mode.SRC_IN);
        offer_image.getBackground().setColorFilter(getResources().getColor(R.color.md_grey_800), android.graphics.PorterDuff.Mode.SRC_IN);
        cart_image.getBackground().setColorFilter(getResources().getColor(R.color.colorPrimary), android.graphics.PorterDuff.Mode.SRC_IN);
        textViewHome.setTextColor(getResources().getColor(R.color.md_grey_800));
        textViewNearby.setTextColor(getResources().getColor(R.color.md_grey_800));
        textViewOffer.setTextColor(getResources().getColor(R.color.md_grey_800));
        textViewCart.setTextColor(getResources().getColor(R.color.colorPrimary));
    }


    public void clickNone() {
        home_image.getBackground().setColorFilter(getResources().getColor(R.color.md_grey_800), android.graphics.PorterDuff.Mode.SRC_IN);
        nearby_image.getBackground().setColorFilter(getResources().getColor(R.color.md_grey_800), android.graphics.PorterDuff.Mode.SRC_IN);
        offer_image.getBackground().setColorFilter(getResources().getColor(R.color.md_grey_800), android.graphics.PorterDuff.Mode.SRC_IN);
        cart_image.getBackground().setColorFilter(getResources().getColor(R.color.md_grey_800), android.graphics.PorterDuff.Mode.SRC_IN);
        textViewHome.setTextColor(getResources().getColor(R.color.md_grey_800));
        textViewNearby.setTextColor(getResources().getColor(R.color.md_grey_800));
        textViewOffer.setTextColor(getResources().getColor(R.color.md_grey_800));
        textViewCart.setTextColor(getResources().getColor(R.color.md_grey_800));
    }
}
