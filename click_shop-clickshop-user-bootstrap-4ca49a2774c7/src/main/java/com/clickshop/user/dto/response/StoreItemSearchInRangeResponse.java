package com.clickshop.user.dto.response;

import com.clickshop.user.dto.StoreItmSearchAttributeResponse;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
public class StoreItemSearchInRangeResponse implements Serializable {

    private Long responseCode;
    private String responseMessage;
    private List<StoreItmSearchAttributeResponse> storeList;

}
