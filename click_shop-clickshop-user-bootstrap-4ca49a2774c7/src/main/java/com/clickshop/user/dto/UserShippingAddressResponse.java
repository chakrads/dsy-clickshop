package com.clickshop.user.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class UserShippingAddressResponse implements Serializable {

    private Long responseCode;
    private String responseMessage;
    private Long userShippingAddressId;
    private Long userId;
    private String address;
    private String mobileNumber;

}
