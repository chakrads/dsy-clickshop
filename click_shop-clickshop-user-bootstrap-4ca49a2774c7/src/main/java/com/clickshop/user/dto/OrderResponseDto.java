package com.clickshop.user.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class OrderResponseDto implements Serializable {

    private Long responseCode;
    private String responseMessage;
    private String orderId;
    private String orderStatus;

}
