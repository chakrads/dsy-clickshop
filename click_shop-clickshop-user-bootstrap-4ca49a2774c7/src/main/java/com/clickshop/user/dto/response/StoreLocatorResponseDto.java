package com.clickshop.user.dto.response;

import com.clickshop.user.dto.StoreAttributeResponse;
import com.clickshop.user.entity.Store;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
public class StoreLocatorResponseDto implements Serializable {

    private Long responseCode;
    private String responseMessage;
    private List<StoreAttributeResponse> storeList;

}
