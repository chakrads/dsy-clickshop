package com.clickshop.user.dto.request;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class UserAddressTypeRequestDto implements Serializable {

    private Long userId;

    //private String addressType;

    private double latitude;

    private double longitude;

    private String address;

    private String city;

    private String state;

    private String country;

    private String pinCode;

    private String deviceUniqueId;

    private String sessionId;

}
