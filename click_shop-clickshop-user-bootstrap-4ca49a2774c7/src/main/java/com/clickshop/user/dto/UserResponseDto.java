package com.clickshop.user.dto;

import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

import java.io.Serializable;

@Getter
@Setter
@Component
public class UserResponseDto implements Serializable {

    private Long responseCode;

    private String responseMessage;

    private Long userId;

    private String mobileNumber;

    private String email;

    private String firstName;

    private String lastName;

    private String referralCode;

    private String deviceUniqueId;

    private String sessionId;

}
