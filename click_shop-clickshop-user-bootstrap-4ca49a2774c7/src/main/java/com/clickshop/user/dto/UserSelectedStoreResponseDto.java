package com.clickshop.user.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class UserSelectedStoreResponseDto implements Serializable {

    private Long responseCode;
    private String responseMessage;
    private Long userSelectedStoreId;
    private Long userId;
    private Long storeId;

}
