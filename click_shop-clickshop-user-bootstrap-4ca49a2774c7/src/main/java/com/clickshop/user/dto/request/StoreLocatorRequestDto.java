package com.clickshop.user.dto.request;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class StoreLocatorRequestDto implements Serializable {

    private Long userId;
    private Long storeId;
    private String itemName;
    private String itemCategory;
    private Long itemCategoryId;
    private Double latitude;
    private Double longitude;

}
