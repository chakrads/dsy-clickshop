package com.clickshop.user.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class OrderItemDto implements Serializable {

    /*private Long itemId;
    private String itemName;
    private String itemImage;*/
    private Long itemMerchantId;
    private String itemPrice;
    private String itemQuantityRequested;
    private String totalPrice;
    //private String itemWeight;

}
