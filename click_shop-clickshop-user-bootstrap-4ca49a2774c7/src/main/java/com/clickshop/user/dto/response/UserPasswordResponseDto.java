package com.clickshop.user.dto.response;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;


@Getter
@Setter

public class UserPasswordResponseDto implements Serializable {

    private Long responseCode;
    private String responseMessage;
}
