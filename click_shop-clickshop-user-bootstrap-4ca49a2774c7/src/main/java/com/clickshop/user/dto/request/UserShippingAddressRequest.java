package com.clickshop.user.dto.request;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class UserShippingAddressRequest implements Serializable {

    private Long userShippingAddressId;
    private Long userId;
    private String address;
    private String mobileNumber;
    private String deviceUniqueId;
    private String sessionId;

}
