package com.clickshop.user.dto.response;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.sql.Blob;

@Getter
@Setter
public class StoreItemSearchResponse implements Serializable {

    private Long responseCode;
    private String responseMessage;
    //store
    private Long storeId;
    private double latitude;
    private double longitude;
    private String deliveryDuration;
    private Double storeRating;
    private String storeName;
    //item
    private String itemName;
    private String itemImage;
    private String itemPrice;
    private String itemWeight;
    private String itemCategory;
    //offer
    private String actualPrice;
    private String discountPercentage;
    private String discountAmount;
    private String priceAfterDiscount;

}
