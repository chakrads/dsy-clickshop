package com.clickshop.user.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
public class StoreItmSearchAttributeResponse implements Serializable {

    private Long storeId;
    private double latitude;
    private double longitude;
    private String deliveryDuration;
    private Double storeRating;
    private String storeName;
    // Old impl start
    //private byte[] profilePic;
    /* private Long itemId;
    private String itemCategory;
    private String itemName;
    private String itemImage;
    private String itemPrice;
    private String itemWeight;
    private Long merchantId;*/
    // Old Impl end
    //New impl start
    private List<StoreSearchItemData> storeSearchItemData;
    //New impl end
}
