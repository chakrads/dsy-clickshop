package com.clickshop.user.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class UserCartItemListResponseDto implements Serializable {

    private Long itemMerchantId;
    private Long itemId;
    private String itemName;
    private String itemImage;
    private String itemPrice;
    private String itemQuantity;
    private String totalPrice;
    private String itemWeight;

}
