package com.clickshop.user.dto.response;

import com.clickshop.user.entity.Order;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
public class FindOrderResponseDto implements Serializable {
    private Long responseCode;
    private String responseMessage;
    private List<OrderSearchListResponseDto> orderList;
}
