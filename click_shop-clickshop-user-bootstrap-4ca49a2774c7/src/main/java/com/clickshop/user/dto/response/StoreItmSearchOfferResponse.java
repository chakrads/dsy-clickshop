package com.clickshop.user.dto.response;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class StoreItmSearchOfferResponse implements Serializable {

    private Long storeId;
    private double latitude;
    private double longitude;
    private String deliveryDuration;
    private Double storeRating;
    private String storeName;
    //private byte[] profilePic;
    private Long itemMerchantId;
    private Long itemId;
    private String itemCategory;
    private String itemName;
    private String itemImage;
    private String itemPrice;
    private String itemMRP;
    private String itemWeight;
    private Long merchantId;
    //offer
    private String actualPrice;
    private String discountPercentage;
    private String discountAmount;
    private String priceAfterDiscount;

}
