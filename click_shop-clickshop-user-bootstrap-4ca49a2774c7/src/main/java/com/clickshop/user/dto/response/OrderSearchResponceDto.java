package com.clickshop.user.dto.response;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
public class OrderSearchResponceDto implements Serializable {

    private Long responseCode;
    private String responseMessage;
    private String orderId;
    private Long userId;
    private String shippingAddress;
    private String mobileNumber;
    private String paymentMode;
    private String orderAmount;
    private Long storeId;
    private String orderStatus;
    private String deliveryCharge;
    private List<OrderDetailsListResponseDto> orderDetailsList;

}
