package com.clickshop.user.dto.response;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
public class OrderSearchListResponseDto implements Serializable {

    private String orderId;
    private Long userId;
    private String shippingAddress;
    private String mobileNumber;
    private String paymentMode;
    private String orderAmount;
    private Long merchantId;
    private String orderStatus;
    private String deliveryCharge;
    private String storeName;
    private String storeMobileNumber;
    private Date orderTime;

}
