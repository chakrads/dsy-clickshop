package com.clickshop.user.dto.response;

import com.clickshop.user.dto.StoreSearchItemData;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
public class SearchAllStoreItemResponse implements Serializable {

    private Long responseCode;
    private String responseMessage;
    List<StoreSearchItemData> storeSearchItemDataList;

}
