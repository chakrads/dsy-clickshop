package com.clickshop.user.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class UserSignOutResponseDto implements Serializable {

    private String responseMessage;
    private Long responseCode;

}
