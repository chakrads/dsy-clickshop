package com.clickshop.user.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class StoreSearchItemData implements Serializable {
    private Long itemMerchantId;
    private Long storeId;
    private String storeName;
    private String itemName;
    private String itemImage;
    private String itemPrice;
    private String itemMRP;
    private String itemWeight;
    private String itemCategory;
    private Long merchantId;
    private Long itemId;
}
