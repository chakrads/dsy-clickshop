package com.clickshop.user.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class StoreAttributeResponse implements Serializable {

    private Long storeId;
    private Long merchantId;
    private String deliveryDuration;
    private String workingHourFrom;
    private String workingHourTo;
    private String deliveryRange;
    private String storeName;
    private double latitude;
    private double longitude;
    private String address;
    private String city;
    private String state;
    private String country;
    private String pinCode;
    private String flag;
    private String profilePic;


}
