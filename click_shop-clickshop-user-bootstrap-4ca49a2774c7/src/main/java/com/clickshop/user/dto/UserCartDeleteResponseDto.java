package com.clickshop.user.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class UserCartDeleteResponseDto implements Serializable {

    private Long responseCode;
    private String responseMessage;

}
