package com.clickshop.user.dto.request;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class OrderSearchRequestDto implements Serializable {
    private String orderId;
}
