package com.clickshop.user.dto.response;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
public class StoreItemSearchOfferInRangeResponse implements Serializable {

    private Long responseCode;
    private String responseMessage;
    private List<StoreItmSearchOfferResponse> storeList;

}
