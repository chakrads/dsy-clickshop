package com.clickshop.user.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class UserCartItemListRequestDto implements Serializable {

    private Long itemMerchantId;
    private String itemQuantity;
    private String itemPrice;
    private String totalPrice;

}
