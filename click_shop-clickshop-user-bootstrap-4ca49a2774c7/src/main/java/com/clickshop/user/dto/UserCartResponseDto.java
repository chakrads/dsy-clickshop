package com.clickshop.user.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
public class UserCartResponseDto implements Serializable {

    private Long responseCode;
    private String responseMessage;
    private Long storeId;
    private List<UserCartItemListResponseDto> userCartItemList;

}
