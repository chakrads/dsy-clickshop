package com.clickshop.user.dto;

import com.clickshop.user.dto.request.UserShippingAddressRequest;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
public class UserShippingAddressListResponse implements Serializable {

    private Long responseCode;
    private String responseMessage;
    private List<UserShippingAddressRequest> userShippingAddressList;

}
