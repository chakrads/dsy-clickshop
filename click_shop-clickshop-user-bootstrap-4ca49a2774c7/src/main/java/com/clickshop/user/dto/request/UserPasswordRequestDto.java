package com.clickshop.user.dto.request;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class UserPasswordRequestDto implements Serializable {
    private String mobileNumber;
    private String newPassword;
    private String confirmPassword;
    private String oldPassword;
    private String deviceUniqueId;
    private String sessionId;
}
