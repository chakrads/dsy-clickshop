package com.clickshop.user.dto.response;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class UserAddressTypeResponseDto implements Serializable {

    private Long responseCode;

    private String responseMessage;

    private Long userAddressTypeId;

    private Long userId;

    //private String addressType;

    private double latitude;

    private double longitude;

    private String address;

    private String city;

    private String state;

    private String country;

    private String pinCode;

}
