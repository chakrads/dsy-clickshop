package com.clickshop.user.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
public class OrderRequestDto implements Serializable {

    private Long userId;
    private String shippingAddress;
    private String mobileNumber;
    private String paymentMode;
    private String orderAmount;
    private String deliveryCharge;
    private Long storeId;
    private List<OrderItemDto> itemList;
    private String deviceUniqueId;
    private String sessionId;

}
