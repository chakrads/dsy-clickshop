package com.clickshop.user.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
public class UserCartRequestDto implements Serializable {

    private Long userId;
    private Long storeId;
    private List<UserCartItemListRequestDto> userCartItemList;
    private String deviceUniqueId;
    private String sessionId;

}
