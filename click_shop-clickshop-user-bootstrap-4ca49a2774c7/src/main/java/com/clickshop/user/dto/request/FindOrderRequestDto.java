package com.clickshop.user.dto.request;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class FindOrderRequestDto implements Serializable {
    private Long userId;
    private String deviceUniqueId;
    private String sessionId;
}
