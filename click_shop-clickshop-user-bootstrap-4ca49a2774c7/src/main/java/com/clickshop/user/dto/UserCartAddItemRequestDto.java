package com.clickshop.user.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class UserCartAddItemRequestDto implements Serializable {

    private Long userId;
    private Long storeId;
    //private Long userCartSize;
    private Long itemMerchantId;
    private String itemQuantity;
    private String itemPrice;
    private String totalPrice;
    private String deviceUniqueId;
    private String sessionId;

}
