package com.clickshop.user.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class SearchItemInGalleryDto implements Serializable {

    private String itemName;
    private String itemImage;
    private String itemCategory;
    private String itemMRP;

}
