package com.clickshop.user.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class UserRequestDto implements Serializable {

    private String mobileNumber;
    private String email;
    private String password;
    private String firstName;
    private String lastName;
    private String referralCode;
    private String firebaseToken;
    private String deviceUniqueId;
    private String deviceType;
    private String latitude;
    private String longitude;

}
