package com.clickshop.user.fcm;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.stereotype.Component;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

@Component
public class SendOrderNotifs {

    public String sendOrderNotifsToMerchant(String firebaseToken, String orderId) {

        JSONObject body = new JSONObject();
        body.put("to", firebaseToken);
        body.put("priority", "high");

        JSONObject notification = new JSONObject();
        notification.put("title", "New Order from User!");
        notification.put("body", "OrderId: " + orderId);
        notification.put("sound", "default");

        body.put("notification", notification);
        HttpEntity<String> request = new HttpEntity<>(body.toString());

        try {
            AndroidPushNotificationsService androidPushNotificationsService = new AndroidPushNotificationsService();
            CompletableFuture<String> pushNotification = androidPushNotificationsService.send(request);
            CompletableFuture.allOf(pushNotification).join();
            String firebaseResponse = pushNotification.get();
            return firebaseResponse;
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        return null;
    }
}
