package com.clickshop.user.entity;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Dell on 8/2/2018.
 */
@Entity
@Table(name = "t_offer")
@Getter
@Setter
public class Offer implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "offer_id")
    private Long offerId;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "merchant_id")
    private Merchant merchant;

    @Column(name = "item_Id")
    private Long itemId;

    @Column(name = "actual_price")
    private String actualPrice;

    //Pass either P/R i,e: 10P/50R
    @Column(name = "discount_percentage")
    private String discountPercentage;

    @Column(name = "discount_amount")
    private String discountAmount;

    @Column(name = "price_after_discount")
    private String priceAfterDiscount;

}
