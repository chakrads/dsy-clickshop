package com.clickshop.user.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "t_order_details")
@Getter
@Setter
public class OrderDetails implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "order_details_id")
    private Long orderDetailsId;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "order_id")
    private Order order;

    /*@Column(name = "item_id")
    private Long itemId;

    @Column(name = "item_name")
    private String itemName;

    @Column(name = "item_image")
    private String itemImage;

    @Column(name = "item_weight")
    private String itemWeight;*/

    /*@OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "item_merchant_id")*/
    @Column(name = "item_merchant_id")
    private Long itemMerchantId;

    @Column(name = "item_price")
    private String itemPrice;

    @Column(name = "total_price")
    private String totalPrice;

    @Column(name = "item_quantity_requested")
    private String itemQuantityRequested;

    @Column(name = "item_quantity_approved")
    private String itemQuantityApproved;

}
