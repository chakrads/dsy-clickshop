package com.clickshop.user.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "t_user_cart")
@Getter
@Setter
public class UserCart implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "user_cart_id")
    private Long userCartId;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "item_merchant_id")
    private ItemsOfMerchant itemMerchant;

    @Column(name = "item_quantity")
    private String itemQuantity;

    @Column(name = "item_price")
    private String itemPrice;

    @Column(name = "total_price")
    private String totalPrice;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user;

    @Column(name = "store_id")
    private Long storeId;

}
