package com.clickshop.user.entity;

import com.clickshop.user.entity.ItemCategory;
import com.clickshop.user.entity.Merchant;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by Dell on 7/21/2018.
 */
@Entity
@Table(name = "t_item_merchant")
@Getter
@Setter
public class ItemsOfMerchant implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "item_merchant_id")
    private Long itemMerchantId;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "merchant_id")
    private Merchant merchant;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "item_category_id")
    private ItemCategory itemCategory;

    @Column(name = "item_id")
    private Long itemId;

    @Column(name = "type")
    private String type;

    @Column(name = "item_price")
    private String itemPrice;

    @Column(name = "item_name")
    private String itemName;


   /* @Column(name = "item_weight")
    private String itemWeight;*/

    @Column(name = "created_on")
    private String createdOn;

    @Column(name = "created_by")
    private String createdBy;

    @Column(name = "last_updated_by")
    private String lastUpdatedBy;

    @Temporal(TemporalType.DATE)
    @Column(name = "last_updated_on", updatable = false, insertable = false, columnDefinition = "TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP")
    private Date lastUpdatedOn;

    @Column(name = "status")
    private String status;

    @Column(name = "is_blocked")
    private String isBlocked;

    @Column(name = "approved")
    private String approved;

    @Column(name = "approved_by")
    private String approvedBy;
}
