package com.clickshop.user.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "t_user_device")
@Getter
@Setter
public class UserDevice implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "user_device_id")
    private Long userDeviceId;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user;

    @Column(name = "device_unique_id")
    private String deviceUniqueId;

    @Column(name = "device_type")
    private String deviceType;

    @Column(name = "is_active")
    private String isActive;

    @Column(name = "is_blocked")
    private String isBlocked;

}
