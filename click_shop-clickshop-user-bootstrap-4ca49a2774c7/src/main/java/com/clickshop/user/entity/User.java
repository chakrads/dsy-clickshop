package com.clickshop.user.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "t_user")
@Getter
@Setter
public class User implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "user_id")
    private Long userId;

    @Column(name = "mobile_number")
    private String mobileNumber;

    @Column(name = "mobile_verified")
    private String mobileVerified;

    @Column(name = "email")
    private String email;

    @Column(name = "email_verified")
    private String emailVerified;

    @Column(name = "password")
    private String password;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "referral_code")
    private String referralCode;

    @Column(name = "firebase_token")
    private String firebaseToken;

    @Column(name = "active")
    private String active;

    @Temporal(TemporalType.DATE)
    @Column(name = "registered_date", updatable = false, insertable = false, columnDefinition = "TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP")
    private Date registeredDate;

}
