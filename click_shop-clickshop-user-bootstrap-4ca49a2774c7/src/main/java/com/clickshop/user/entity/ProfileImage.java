package com.clickshop.user.entity;

import lombok.Getter;


import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

/*
 * 
 * mysql> desc t_profile_image ;
+-------------+--------------+------+-----+---------+----------------+
| Field       | Type         | Null | Key | Default | Extra          |
+-------------+--------------+------+-----+---------+----------------+
| image_id    | bigint(20)   | NO   | PRI | NULL    | auto_increment |
| merchant_id | bigint(20)   | YES  |     | NULL    |                |
| profile_pic | longblob     | YES  |     | NULL    |                |
| image_url   | varchar(255) | YES  |     | NULL    |                |
+-------------+--------------+------+-----+---------+----------------+

 * 
 */



@Entity
@Table(name = "t_profile_image")
@Getter
@Setter
public class ProfileImage implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "imageId")
    private Long imageId;
    
    //.... This is inserted by Ankyah
    // @Lob
    @Column(name = "image_url")
    // private byte[] profilePic;
    private String profilePicUrl;

   
	/*
	 * @Lob
	 * 
	 * @Column(name = "profile_pic") private byte[] profilePic;
	 */
    
    
    @Column(name = "merchant_Id")
    private Long merchantId;

}
