package com.clickshop.user.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "t_user_address_type")
@Getter
@Setter
public class UserAddressType implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "user_address_type_Id")
    private Long userAddressTypeId;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_Id")
    private User userId;

    /*@Column(name = "address_type")
    private String addressType;*/

    @Column(name = "latitude")
    private double latitude;

    @Column(name = "longitude")
    private double longitude;

    @Column(name = "address")
    private String address;

    @Column(name = "city")
    private String city;

    @Column(name = "state")
    private String state;

    @Column(name = "country")
    private String country;

    @Column(name = "pin_code")
    private String pinCode;

}
