package com.clickshop.user.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Dell on 7/18/2018.
 */
@Entity
@Table(name = "t_merchant")
@Getter
@Setter
public class Merchant implements Serializable   {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "merchant_id")
    private Long merchantId;

    @Column(name = "mobile_number")
    private String mobileNumber;

    /*@Column(name = "password")
    private String password;*/

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "address")
    private String address;

    @Column(name = "gst_number")
    private String gstNumber;

    @Column(name = "store_number")
    private String storeNumber;

    @Column(name = "email")
    private String email;

    @Column(name = "flag")
    private String flag;

    @Column(name = "firebase_token")
    private String firebaseToken;

}
