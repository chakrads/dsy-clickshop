package com.clickshop.user.endpoint;

import com.clickshop.user.constants.UserConstants;
import com.clickshop.user.dao.UserDao;
import com.clickshop.user.dto.*;
import com.clickshop.user.entity.UserDevice;
import com.clickshop.user.service.UserCartService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/user")
@RestController
public class ClickshopUserCartEndpoint {

    public static final Logger LOGGER = LoggerFactory.getLogger(ClickshopUserOrderEndpoint.class);

    @Autowired
    private UserCartService userCartService;

    @Autowired
    private UserDao userDao;

    @RequestMapping(value = "/saveUserCart", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<UserCartResponseDto> saveUserCart(@RequestBody UserCartRequestDto userCartRequestDto) {
        UserCartResponseDto userCartResponseDto = new UserCartResponseDto();
        ResponseEntity<UserCartResponseDto> responseStatus = null;
        String sessionId = getSessionId(userCartRequestDto.getUserId(), userCartRequestDto.getDeviceUniqueId());
        if (!(userCartRequestDto.getSessionId()).equals(sessionId)) {
            userCartResponseDto.setResponseCode(UserConstants.INVALID_SESSION_CODE);
            userCartResponseDto.setResponseMessage(UserConstants.INVALID_SESSION_MESSAGE);
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(userCartResponseDto);
        }
        try {
            userCartResponseDto = userCartService.saveUserCart(userCartRequestDto);
            responseStatus = ResponseEntity.status(HttpStatus.OK).body(userCartResponseDto);
        } catch (Exception ex) {
            LOGGER.error(ex.toString());
            responseStatus = ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        } finally {
            return responseStatus;
        }
    }

    @RequestMapping(value = "/getUserCart", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<UserCartResponseDto> getUserCart(@RequestBody UserCartRequestDto userCartRequestDto) {
        UserCartResponseDto userCartResponseDto = new UserCartResponseDto();
        ResponseEntity<UserCartResponseDto> responseStatus = null;
        String sessionId = getSessionId(userCartRequestDto.getUserId(), userCartRequestDto.getDeviceUniqueId());
        if (!(userCartRequestDto.getSessionId()).equals(sessionId)) {
            userCartResponseDto.setResponseCode(UserConstants.INVALID_SESSION_CODE);
            userCartResponseDto.setResponseMessage(UserConstants.INVALID_SESSION_MESSAGE);
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(userCartResponseDto);
        }
        try {
            userCartResponseDto = userCartService.getUserCart(userCartRequestDto);
            responseStatus = ResponseEntity.status(HttpStatus.OK).body(userCartResponseDto);
        } catch (Exception ex) {
            LOGGER.error(ex.toString());
            responseStatus = ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        } finally {
            return responseStatus;
        }
    }

    @RequestMapping(value = "/deleteUserCart", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<UserCartDeleteResponseDto> deleteUserCart(@RequestBody UserCartRequestDto userCartRequestDto) {
        UserCartDeleteResponseDto userCartDeleteResponseDto = new UserCartDeleteResponseDto();
        ResponseEntity<UserCartDeleteResponseDto> responseStatus = null;
        String sessionId = getSessionId(userCartRequestDto.getUserId(), userCartRequestDto.getDeviceUniqueId());
        if (!(userCartRequestDto.getSessionId()).equals(sessionId)) {
            userCartDeleteResponseDto.setResponseCode(UserConstants.INVALID_SESSION_CODE);
            userCartDeleteResponseDto.setResponseMessage(UserConstants.INVALID_SESSION_MESSAGE);
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(userCartDeleteResponseDto);
        }
        try {
            userCartDeleteResponseDto = userCartService.deleteUserCart(userCartRequestDto);
            responseStatus = ResponseEntity.status(HttpStatus.OK).body(userCartDeleteResponseDto);
        } catch (Exception ex) {
            LOGGER.error(ex.toString());
            responseStatus = ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        } finally {
            return responseStatus;
        }
    }

    @RequestMapping(value = "/addItemUserCart", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<UserCartResponseDto> updateUserCart(@RequestBody UserCartAddItemRequestDto userCartAddItemRequestDto) {
        UserCartResponseDto userCartResponseDto = new UserCartResponseDto();
        ResponseEntity<UserCartResponseDto> responseStatus = null;
        String sessionId = getSessionId(userCartAddItemRequestDto.getUserId(), userCartAddItemRequestDto.getDeviceUniqueId());
        if (!(userCartAddItemRequestDto.getSessionId()).equals(sessionId)) {
            userCartResponseDto.setResponseCode(UserConstants.INVALID_SESSION_CODE);
            userCartResponseDto.setResponseMessage(UserConstants.INVALID_SESSION_MESSAGE);
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(userCartResponseDto);
        }
        try {
            userCartResponseDto = userCartService.addItemUserCart(userCartAddItemRequestDto);
            responseStatus = ResponseEntity.status(HttpStatus.OK).body(userCartResponseDto);
        } catch (Exception ex) {
            LOGGER.error(ex.toString());
            responseStatus = ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        } finally {
            return responseStatus;
        }
    }

    @RequestMapping(value = "/deleteUserSelectedStore", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<UserCartDeleteResponseDto> deleteUserSelectedStore(@RequestBody UserCartRequestDto userCartRequestDto) {
        UserCartDeleteResponseDto userCartDeleteResponseDto = new UserCartDeleteResponseDto();
        ResponseEntity<UserCartDeleteResponseDto> responseStatus = null;
        String sessionId = getSessionId(userCartRequestDto.getUserId(), userCartRequestDto.getDeviceUniqueId());
        if (!(userCartRequestDto.getSessionId()).equals(sessionId)) {
            userCartDeleteResponseDto.setResponseCode(UserConstants.INVALID_SESSION_CODE);
            userCartDeleteResponseDto.setResponseMessage(UserConstants.INVALID_SESSION_MESSAGE);
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(userCartDeleteResponseDto);
        }
        try {
            userCartDeleteResponseDto = userCartService.deleteUserSelectedStore(userCartRequestDto);
            responseStatus = ResponseEntity.status(HttpStatus.OK).body(userCartDeleteResponseDto);
        } catch (Exception ex) {
            LOGGER.error(ex.toString());
            responseStatus = ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        } finally {
            return responseStatus;
        }
    }

    @RequestMapping(value = "/deleteUserCartItem", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<UserCartDeleteResponseDto> deleteUserCartItem(@RequestBody UserCartAddItemRequestDto userCartAddItemRequestDto) {
        UserCartDeleteResponseDto userCartDeleteResponseDto = new UserCartDeleteResponseDto();
        ResponseEntity<UserCartDeleteResponseDto> responseStatus = null;
        String sessionId = getSessionId(userCartAddItemRequestDto.getUserId(), userCartAddItemRequestDto.getDeviceUniqueId());
        if (!(userCartAddItemRequestDto.getSessionId()).equals(sessionId)) {
            userCartDeleteResponseDto.setResponseCode(UserConstants.INVALID_SESSION_CODE);
            userCartDeleteResponseDto.setResponseMessage(UserConstants.INVALID_SESSION_MESSAGE);
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(userCartDeleteResponseDto);
        }
        try {
            userCartDeleteResponseDto = userCartService.deleteUserCartItem(userCartAddItemRequestDto);
            responseStatus = ResponseEntity.status(HttpStatus.OK).body(userCartDeleteResponseDto);
        } catch (Exception ex) {
            LOGGER.error(ex.toString());
            responseStatus = ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        } finally {
            return responseStatus;
        }
    }

    @RequestMapping(value = "/saveUserSelectedStore", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<UserSelectedStoreResponseDto> saveUserSelectedStore(@RequestBody UserCartAddItemRequestDto userCartAddItemRequestDto) {
        UserSelectedStoreResponseDto userSelectedStoreResponseDto = new UserSelectedStoreResponseDto();
        ResponseEntity<UserSelectedStoreResponseDto> responseStatus = null;
        String sessionId = getSessionId(userCartAddItemRequestDto.getUserId(), userCartAddItemRequestDto.getDeviceUniqueId());
        if (!(userCartAddItemRequestDto.getSessionId()).equals(sessionId)) {
            userSelectedStoreResponseDto.setResponseCode(UserConstants.INVALID_SESSION_CODE);
            userSelectedStoreResponseDto.setResponseMessage(UserConstants.INVALID_SESSION_MESSAGE);
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(userSelectedStoreResponseDto);
        }
        try {
            userSelectedStoreResponseDto = userCartService.saveUserSelectedStore(userCartAddItemRequestDto);
            responseStatus = ResponseEntity.status(HttpStatus.OK).body(userSelectedStoreResponseDto);
        } catch (Exception ex) {
            LOGGER.error(ex.toString());
            responseStatus = ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        } finally {
            return responseStatus;
        }
    }

    private String getSessionId(Long userId, String deviceUniqueId) {
        UserDevice userDevice = userDao.getUserDeviceByType(userId, deviceUniqueId);
        String sessionId = userDao.getSessionIdByUserDeviceId(userDevice.getUserDeviceId());
        return sessionId;
    }
}
