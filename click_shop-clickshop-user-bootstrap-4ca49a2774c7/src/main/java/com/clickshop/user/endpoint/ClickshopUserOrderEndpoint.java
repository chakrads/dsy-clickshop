package com.clickshop.user.endpoint;

import com.clickshop.user.constants.UserConstants;
import com.clickshop.user.dao.UserDao;
import com.clickshop.user.dto.OrderRequestDto;
import com.clickshop.user.dto.OrderResponseDto;
import com.clickshop.user.dto.request.FindOrderRequestDto;
import com.clickshop.user.dto.request.OrderSearchRequestDto;
import com.clickshop.user.dto.request.OrderStatusRequestDto;
import com.clickshop.user.dto.response.*;
import com.clickshop.user.entity.UserDevice;
import com.clickshop.user.service.OrderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/user")
@RestController
public class ClickshopUserOrderEndpoint {

    public static final Logger LOGGER = LoggerFactory.getLogger(ClickshopUserOrderEndpoint.class);

    @Autowired
    private OrderService orderService;

    @Autowired
    private UserDao userDao;

    @RequestMapping(value = "/placeOrder", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<OrderResponseDto> createOrder(@RequestBody OrderRequestDto orderRequestDto) {
        OrderResponseDto orderResponseDto = new OrderResponseDto();
        ResponseEntity<OrderResponseDto> responseStatus = null;
        String sessionId = getSessionId(orderRequestDto.getUserId(), orderRequestDto.getDeviceUniqueId());
        if(!(orderRequestDto.getSessionId()).equals(sessionId)){
            orderResponseDto.setResponseCode(UserConstants.INVALID_SESSION_CODE);
            orderResponseDto.setResponseMessage(UserConstants.INVALID_SESSION_MESSAGE);
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(orderResponseDto);
        }
        try {
            orderResponseDto = orderService.createOrder(orderRequestDto);
            responseStatus = ResponseEntity.status(HttpStatus.OK).body(orderResponseDto);
        } catch (Exception ex) {
            LOGGER.error(ex.toString());
            responseStatus = ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        } finally {
            return responseStatus;
        }
    }

    @RequestMapping(value = "/searchOrder", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<OrderSearchResponceDto> findOrder(@RequestBody OrderSearchRequestDto orderSearchRequestDto){
        ResponseEntity<OrderSearchResponceDto> responseStatus = null;
        try {
            OrderSearchResponceDto orderSearchResponceDto = orderService.findOrder(orderSearchRequestDto);
            responseStatus = ResponseEntity.status(HttpStatus.OK).body(orderSearchResponceDto);
        } catch (Exception ex) {
            LOGGER.error(ex.toString());
            responseStatus = ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        } finally {
            return responseStatus;
        }
    }

    @RequestMapping(value = "/getOrderStatus", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<OrderStatusResponseDto> findOrderStatus(@RequestBody OrderStatusRequestDto orderStatusRequestDto){
        OrderStatusResponseDto orderStatusResponseDto = new OrderStatusResponseDto();
        ResponseEntity<OrderStatusResponseDto> responseStatus = null;
        String sessionId = getSessionId(orderStatusRequestDto.getUserId(), orderStatusRequestDto.getDeviceUniqueId());
        if(!(orderStatusRequestDto.getSessionId()).equals(sessionId)){
            orderStatusResponseDto.setResponseCode(UserConstants.INVALID_SESSION_CODE);
            orderStatusResponseDto.setResponseMessage(UserConstants.INVALID_SESSION_MESSAGE);
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(orderStatusResponseDto);
        }
        try {
            orderStatusResponseDto = orderService.findOrderStatus(orderStatusRequestDto);
            responseStatus = ResponseEntity.status(HttpStatus.OK).body(orderStatusResponseDto);
        } catch (Exception ex) {
            LOGGER.error(ex.toString());
            responseStatus = ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        } finally {
            return responseStatus;
        }
    }

    @RequestMapping(value = "/getUserCurrentOrders", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<OrderSearchResponseDto> getUserCurrentOrders(@RequestBody OrderStatusRequestDto orderStatusRequestDto){
        OrderSearchResponseDto orderSearchResponseDto = new OrderSearchResponseDto();
        ResponseEntity<OrderSearchResponseDto> responseStatus = null;
        String sessionId = getSessionId(orderStatusRequestDto.getUserId(), orderStatusRequestDto.getDeviceUniqueId());
        if(!(orderStatusRequestDto.getSessionId()).equals(sessionId)){
            orderSearchResponseDto.setResponseCode(UserConstants.INVALID_SESSION_CODE);
            orderSearchResponseDto.setResponseMessage(UserConstants.INVALID_SESSION_MESSAGE);
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(orderSearchResponseDto);
        }
        try {
            orderSearchResponseDto = orderService.getUserCurrentOrders(orderStatusRequestDto);
            responseStatus = ResponseEntity.status(HttpStatus.OK).body(orderSearchResponseDto);
        } catch (Exception ex) {
            LOGGER.error(ex.toString());
            responseStatus = ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        } finally {
            return responseStatus;
        }
    }

    @RequestMapping(value = "/getUserOrderList", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<FindOrderResponseDto> findOrderList(@RequestBody FindOrderRequestDto findOrderRequestDto){
        FindOrderResponseDto findOrderResponseDto = new FindOrderResponseDto();
        ResponseEntity<FindOrderResponseDto> responseStatus = null;
        String sessionId = getSessionId(findOrderRequestDto.getUserId(), findOrderRequestDto.getDeviceUniqueId());
        if(!(findOrderRequestDto.getSessionId()).equals(sessionId)){
            findOrderResponseDto.setResponseCode(UserConstants.INVALID_SESSION_CODE);
            findOrderResponseDto.setResponseMessage(UserConstants.INVALID_SESSION_MESSAGE);
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(findOrderResponseDto);
        }
        try {
            findOrderResponseDto = orderService.findOrderList(findOrderRequestDto);
            responseStatus = ResponseEntity.status(HttpStatus.OK).body(findOrderResponseDto);
        } catch (Exception ex) {
            LOGGER.error(ex.toString());
            responseStatus = ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        } finally {
            return responseStatus;
        }
    }

    @RequestMapping(value = "/getOrderDeliveryCharge", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<OrderDeliveryChargeResponseDto> getOrderDeliveryCharge(@RequestBody OrderRequestDto orderRequestDto){
        OrderDeliveryChargeResponseDto orderDeliveryChargeResponseDto;
        ResponseEntity<OrderDeliveryChargeResponseDto> responseStatus = null;
        /*String sessionId = getSessionId(orderRequestDto.getUserId(), orderRequestDto.getDeviceUniqueId());
        if(!(orderRequestDto.getSessionId()).equals(sessionId)){
            orderDeliveryChargeResponseDto.setResponseCode(UserConstants.INVALID_SESSION_CODE);
            orderDeliveryChargeResponseDto.setResponseMessage(UserConstants.INVALID_SESSION_MESSAGE);
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(orderDeliveryChargeResponseDto);
        }*/
        try {
            orderDeliveryChargeResponseDto = orderService.getOrderDeliveryCharge(orderRequestDto);
            responseStatus = ResponseEntity.status(HttpStatus.OK).body(orderDeliveryChargeResponseDto);
        } catch (Exception ex) {
            LOGGER.error(ex.toString());
            responseStatus = ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        } finally {
            return responseStatus;
        }
    }

    private String getSessionId(Long userId, String deviceUniqueId){
        UserDevice userDevice = userDao.getUserDeviceByType(userId, deviceUniqueId);
        String sessionId = userDao.getSessionIdByUserDeviceId(userDevice.getUserDeviceId());
        return sessionId;
    }
}
