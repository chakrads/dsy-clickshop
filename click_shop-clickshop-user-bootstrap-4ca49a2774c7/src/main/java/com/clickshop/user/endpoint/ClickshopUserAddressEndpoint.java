package com.clickshop.user.endpoint;

import com.clickshop.user.constants.UserConstants;
import com.clickshop.user.dao.UserDao;
import com.clickshop.user.dto.UserShippingAddressListResponse;
import com.clickshop.user.dto.UserShippingAddressResponse;
import com.clickshop.user.dto.request.UserShippingAddressRequest;
import com.clickshop.user.entity.User;
import com.clickshop.user.entity.UserDevice;
import com.clickshop.user.service.UserAddressService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/user")
@RestController
public class ClickshopUserAddressEndpoint {

    public static final Logger LOGGER = LoggerFactory.getLogger(ClickshopUserEndpoint.class);

    @Autowired
    private UserAddressService userAddressService;

    @Autowired
    private UserDao userDao;

    @RequestMapping(value = "/saveUserShippingAddress", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<UserShippingAddressResponse> saveUserShippingAddress(@RequestBody UserShippingAddressRequest userShippingAddressRequest) {
        UserShippingAddressResponse userShippingAddressResponse = new UserShippingAddressResponse();
        ResponseEntity<UserShippingAddressResponse> responseStatus = null;
        String sessionId = getSessionId(userShippingAddressRequest.getUserId(), userShippingAddressRequest.getDeviceUniqueId());
        if(!(userShippingAddressRequest.getSessionId()).equals(sessionId)){
            userShippingAddressResponse.setResponseCode(UserConstants.INVALID_SESSION_CODE);
            userShippingAddressResponse.setResponseMessage(UserConstants.INVALID_SESSION_MESSAGE);
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(userShippingAddressResponse);
        }
        try {
            userShippingAddressResponse = userAddressService.saveUserShippingAddress(userShippingAddressRequest);
            responseStatus = ResponseEntity.status(HttpStatus.OK).body(userShippingAddressResponse);
        } catch (Exception ex) {
            LOGGER.error(ex.toString());
            responseStatus = ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        } finally {
            return responseStatus;
        }
    }

    @RequestMapping(value = "/getUserShippingAddress", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<UserShippingAddressListResponse> getUserShippingAddress(@RequestBody UserShippingAddressRequest userShippingAddressRequest) {
        UserShippingAddressListResponse userShippingAddressListResponse = new UserShippingAddressListResponse();
        ResponseEntity<UserShippingAddressListResponse> responseStatus = null;
        String sessionId = getSessionId(userShippingAddressRequest.getUserId(), userShippingAddressRequest.getDeviceUniqueId());
        if(!(userShippingAddressRequest.getSessionId()).equals(sessionId)){
            userShippingAddressListResponse.setResponseCode(UserConstants.INVALID_SESSION_CODE);
            userShippingAddressListResponse.setResponseMessage(UserConstants.INVALID_SESSION_MESSAGE);
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(userShippingAddressListResponse);
        }
        try {
            userShippingAddressListResponse = userAddressService.getUserShippingAddress(userShippingAddressRequest);
            responseStatus = ResponseEntity.status(HttpStatus.OK).body(userShippingAddressListResponse);
        } catch (Exception ex) {
            LOGGER.error(ex.toString());
            responseStatus = ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        } finally {
            return responseStatus;
        }
    }

    @RequestMapping(value = "/deleteUserShippingAddress", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<UserShippingAddressListResponse> deleteUserShippingAddress(@RequestBody UserShippingAddressRequest userShippingAddressRequest) {
        UserShippingAddressListResponse userShippingAddressListResponse = new UserShippingAddressListResponse();
        ResponseEntity<UserShippingAddressListResponse> responseStatus = null;
        String sessionId = getSessionId(userShippingAddressRequest.getUserId(), userShippingAddressRequest.getDeviceUniqueId());
        if(!(userShippingAddressRequest.getSessionId()).equals(sessionId)){
            userShippingAddressListResponse.setResponseCode(UserConstants.INVALID_SESSION_CODE);
            userShippingAddressListResponse.setResponseMessage(UserConstants.INVALID_SESSION_MESSAGE);
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(userShippingAddressListResponse);
        }
        try {
            userShippingAddressListResponse = userAddressService.deleteUserShippingAddress(userShippingAddressRequest);
            responseStatus = ResponseEntity.status(HttpStatus.OK).body(userShippingAddressListResponse);
        } catch (Exception ex) {
            LOGGER.error(ex.toString());
            responseStatus = ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        } finally {
            return responseStatus;
        }
    }

    private String getSessionId(Long userId, String deviceUniqueId){
        UserDevice userDevice = userDao.getUserDeviceByType(userId, deviceUniqueId);
        String sessionId = userDao.getSessionIdByUserDeviceId(userDevice.getUserDeviceId());
        return sessionId;
    }

}
