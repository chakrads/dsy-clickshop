package com.clickshop.user.endpoint;

import com.clickshop.user.dto.request.StoreLocatorRequestDto;
import com.clickshop.user.dto.response.StoreItemSearchOfferInRangeResponse;
import com.clickshop.user.service.UserOfferService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/user")
@RestController
public class ClickshopUserOfferEndpoint {

    public static final Logger LOGGER = LoggerFactory.getLogger(ClickshopUserOfferEndpoint.class);

    @Autowired
    UserOfferService userOfferService;

    @RequestMapping(value = "/searchOfferOnStoreItemItem", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<StoreItemSearchOfferInRangeResponse> searchOfferOnStoreItemItem(@RequestBody StoreLocatorRequestDto storeLocatorRequestDto) {
        ResponseEntity<StoreItemSearchOfferInRangeResponse> responseStatus = null;
        try {
            StoreItemSearchOfferInRangeResponse itemSearchOfferInRangeResponse = userOfferService.findOfferOnItem(storeLocatorRequestDto);
            responseStatus = ResponseEntity.status(HttpStatus.OK).body(itemSearchOfferInRangeResponse);
        } catch (Exception ex) {
            LOGGER.error(ex.toString());
            responseStatus = ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        } finally {
            return responseStatus;
        }
    }
}
