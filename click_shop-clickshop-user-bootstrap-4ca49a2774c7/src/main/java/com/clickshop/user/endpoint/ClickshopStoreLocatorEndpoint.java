package com.clickshop.user.endpoint;

import com.clickshop.user.dto.request.StoreLocatorRequestDto;
import com.clickshop.user.dto.response.*;
import com.clickshop.user.service.StoreLocatorService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/user")
@RestController
public class ClickshopStoreLocatorEndpoint {

    public static final Logger LOGGER = LoggerFactory.getLogger(ClickshopUserEndpoint.class);

    @Autowired
    private StoreLocatorService storeLocatorService;

    @RequestMapping(value = "/storeLocator", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<StoreLocatorResponseDto> findStoreInRange(@RequestBody StoreLocatorRequestDto storeLocatorRequestDto) {
        ResponseEntity<StoreLocatorResponseDto> responseStatus = null;
        try {
            StoreLocatorResponseDto storeLocatorResponseDto = storeLocatorService.findStoreInRange(storeLocatorRequestDto);
            responseStatus = ResponseEntity.status(HttpStatus.OK).body(storeLocatorResponseDto);
        } catch (Exception ex) {
            LOGGER.error(ex.toString());
            responseStatus = ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        } finally {
            return responseStatus;
        }
    }

    // Discuss -- Added itemName in t_merchant_item table -- done
    @RequestMapping(value = "/searchItemInStore", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<StoreItemSearchInRangeResponse> findItemInStoreInRange(@RequestBody StoreLocatorRequestDto storeLocatorRequestDto) {
        ResponseEntity<StoreItemSearchInRangeResponse> responseStatus = null;
        try {
            StoreItemSearchInRangeResponse storeItemSearchResponse = storeLocatorService.findItemInStoreInRange(storeLocatorRequestDto);
            responseStatus = ResponseEntity.status(HttpStatus.OK).body(storeItemSearchResponse);
        } catch (Exception ex) {
            LOGGER.error(ex.toString());
            responseStatus = ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        } finally {
            return responseStatus;
        }
    }

    @RequestMapping(value = "/searchItemInGalleryByCategory", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<SearchItemInGalleryByCategoryResponse> searchItemInGalleryByCategory(@RequestBody StoreLocatorRequestDto storeLocatorRequestDto) {
        ResponseEntity<SearchItemInGalleryByCategoryResponse> responseStatus = null;
        try {
            SearchItemInGalleryByCategoryResponse storeItemSearchResponse = storeLocatorService.searchItemInGalleryByCategory(storeLocatorRequestDto);
            responseStatus = ResponseEntity.status(HttpStatus.OK).body(storeItemSearchResponse);
        } catch (Exception ex) {
            LOGGER.error(ex.toString());
            responseStatus = ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        } finally {
            return responseStatus;
        }
    }

    // Discuss -- Added itemName in t_merchant_item table -- done
    @RequestMapping(value = "/searchStoreItem", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<StoreItemSearchResponse> findStoreItem(@RequestBody StoreLocatorRequestDto storeLocatorRequestDto) {
        ResponseEntity<StoreItemSearchResponse> responseStatus = null;
        try {
            StoreItemSearchResponse storeItemSearchResponse = storeLocatorService.findStoreItem(storeLocatorRequestDto);
            responseStatus = ResponseEntity.status(HttpStatus.OK).body(storeItemSearchResponse);
        } catch (Exception ex) {
            LOGGER.error(ex.toString());
            responseStatus = ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        } finally {
            return responseStatus;
        }
    }

    @RequestMapping(value = "/searchAllItemsInStore", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<SearchAllStoreItemResponse> searchAllItemsInStore(@RequestBody StoreLocatorRequestDto storeLocatorRequestDto) {
        ResponseEntity<SearchAllStoreItemResponse> responseStatus = null;
        try {
            SearchAllStoreItemResponse searchAllItemsInStoreResponse = storeLocatorService.findAllItemsInStore(storeLocatorRequestDto);
            responseStatus = ResponseEntity.status(HttpStatus.OK).body(searchAllItemsInStoreResponse);
        } catch (Exception ex) {
            LOGGER.error(ex.toString());
            responseStatus = ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        } finally {
            return responseStatus;
        }
    }

    @RequestMapping(value = "/searchAllItemsInStoreByCategory", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<SearchAllStoreItemResponse> searchAllItemsInStoreByCategory(@RequestBody StoreLocatorRequestDto storeLocatorRequestDto) {
        ResponseEntity<SearchAllStoreItemResponse> responseStatus = null;
        try {
            SearchAllStoreItemResponse searchAllItemsInStoreResponse = storeLocatorService.findAllItemsInStoreByCategory(storeLocatorRequestDto);
            responseStatus = ResponseEntity.status(HttpStatus.OK).body(searchAllItemsInStoreResponse);
        } catch (Exception ex) {
            LOGGER.error(ex.toString());
            responseStatus = ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        } finally {
            return responseStatus;
        }
    }
}
