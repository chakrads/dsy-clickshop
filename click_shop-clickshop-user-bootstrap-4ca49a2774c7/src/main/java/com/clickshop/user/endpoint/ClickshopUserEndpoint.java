package com.clickshop.user.endpoint;

import com.clickshop.user.constants.UserConstants;
import com.clickshop.user.dao.UserDao;
import com.clickshop.user.dto.UserRequestDto;
import com.clickshop.user.dto.UserResponseDto;
import com.clickshop.user.dto.UserSignOutResponseDto;
import com.clickshop.user.dto.request.UserAddressTypeRequestDto;
import com.clickshop.user.dto.response.UserAddressTypeResponseDto;
import com.clickshop.user.entity.UserDevice;
import com.clickshop.user.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/user")
@RestController
public class ClickshopUserEndpoint {

    public static final Logger LOGGER = LoggerFactory.getLogger(ClickshopUserEndpoint.class);

    @Autowired
    private UserService userService;

    @Autowired
    private UserDao userDao;

    @RequestMapping(value = "/signUp", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<UserResponseDto> signUpUser(@RequestBody UserRequestDto userRequestDto) {
        ResponseEntity<UserResponseDto> responseStatus = null;
        try {
            UserResponseDto userResponseDto = userService.signUpUser(userRequestDto);
            responseStatus = ResponseEntity.status(HttpStatus.OK).body(userResponseDto);
        } catch (Exception ex) {
            LOGGER.error(ex.toString());
            responseStatus = ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        } finally {
            return responseStatus;
        }
    }

    @RequestMapping(value = "/signIn", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<UserResponseDto> signInUser(@RequestBody UserRequestDto userRequestDto) {

        ResponseEntity<UserResponseDto> responseStatus = null;
        try {
            UserResponseDto userResponseDto = userService.signInUser(userRequestDto);
            responseStatus = ResponseEntity.status(HttpStatus.OK).body(userResponseDto);
        } catch (Exception ex) {
            LOGGER.error(ex.toString());
            responseStatus = ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        } finally {
            return responseStatus;
        }
    }

    @RequestMapping(value = "/signOut", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<UserSignOutResponseDto> signOutUser(@RequestBody UserRequestDto userRequestDto) {

        ResponseEntity<UserSignOutResponseDto> responseStatus = null;
        try {
            UserSignOutResponseDto userDto = userService.signOutUser(userRequestDto);
            responseStatus = ResponseEntity.status(HttpStatus.OK).body(userDto);
        } catch (Exception ex) {
            LOGGER.error(ex.toString());
            responseStatus = ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        } finally {
            return responseStatus;
        }
    }

    @RequestMapping(value = "/saveAddressType", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<UserAddressTypeResponseDto> saveAddressType(@RequestBody UserAddressTypeRequestDto userAddressTypeRequestDto) {
        UserAddressTypeResponseDto userAddressTypeResponseDto = new UserAddressTypeResponseDto();
        ResponseEntity<UserAddressTypeResponseDto> responseStatus = null;
        String sessionId = getSessionId(userAddressTypeRequestDto.getUserId(), userAddressTypeRequestDto.getDeviceUniqueId());
        if(!(userAddressTypeRequestDto.getSessionId()).equals(sessionId)){
            userAddressTypeResponseDto.setResponseCode(UserConstants.INVALID_SESSION_CODE);
            userAddressTypeResponseDto.setResponseMessage(UserConstants.INVALID_SESSION_MESSAGE);
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(userAddressTypeResponseDto);
        }
        try {
            userAddressTypeResponseDto = userService.saveAddressType(userAddressTypeRequestDto);
            responseStatus = ResponseEntity.status(HttpStatus.OK).body(userAddressTypeResponseDto);
        } catch (Exception ex) {
            LOGGER.error(ex.toString());
            responseStatus = ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        } finally {
            return responseStatus;
        }
    }

    @RequestMapping(value = "/getAddressType", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<UserAddressTypeResponseDto> getAddressType(@RequestBody UserAddressTypeRequestDto userAddressTypeRequestDto) {
        UserAddressTypeResponseDto userAddressTypeResponseDto = new UserAddressTypeResponseDto();
        ResponseEntity<UserAddressTypeResponseDto> responseStatus = null;
        String sessionId = getSessionId(userAddressTypeRequestDto.getUserId(), userAddressTypeRequestDto.getDeviceUniqueId());
        if(!(userAddressTypeRequestDto.getSessionId()).equals(sessionId)){
            userAddressTypeResponseDto.setResponseCode(UserConstants.INVALID_SESSION_CODE);
            userAddressTypeResponseDto.setResponseMessage(UserConstants.INVALID_SESSION_MESSAGE);
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(userAddressTypeResponseDto);
        }
        try {
            userAddressTypeResponseDto = userService.getAddressType(userAddressTypeRequestDto);
            responseStatus = ResponseEntity.status(HttpStatus.OK).body(userAddressTypeResponseDto);
        } catch (Exception ex) {
            LOGGER.error(ex.toString());
            responseStatus = ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        } finally {
            return responseStatus;
        }
    }

    private String getSessionId(Long userId, String deviceUniqueId){
        UserDevice userDevice = userDao.getUserDeviceByType(userId, deviceUniqueId);
        String sessionId = userDao.getSessionIdByUserDeviceId(userDevice.getUserDeviceId());
        return sessionId;
    }

    @RequestMapping(value = "/testUserEndpointUrl", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<String> testEndpointUrl() {

        ResponseEntity<String> responseStatus = null;
        try {
            responseStatus = ResponseEntity.status(HttpStatus.OK).body("User Endpoint is working!");
        } catch (Exception ex) {
            LOGGER.error(ex.toString());
            responseStatus = ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        } finally {
            return responseStatus;
        }
    }

}
