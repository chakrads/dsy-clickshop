package com.clickshop.user.endpoint;


import com.clickshop.user.constants.UserConstants;
import com.clickshop.user.dao.UserDao;
import com.clickshop.user.dto.request.UserPasswordRequestDto;
import com.clickshop.user.dto.response.UserPasswordResponseDto;
import com.clickshop.user.entity.User;
import com.clickshop.user.entity.UserDevice;
import com.clickshop.user.service.UserPasswordService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/user")
@RestController
public class ClickshopUserPasswordEndpoint {

    public static final Logger LOGGER = LoggerFactory.getLogger(ClickshopUserPasswordEndpoint.class);

    @Autowired
    UserPasswordService userPasswordService;

    @Autowired
    private UserDao userDao;

    @RequestMapping(value = "/forgetPassword", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<UserPasswordResponseDto> setNewPassword(@RequestBody UserPasswordRequestDto userPasswordRequestDto) {
        UserPasswordResponseDto userPasswordResponseDto = new UserPasswordResponseDto();
        ResponseEntity<UserPasswordResponseDto> responseStatus = null;
        /*String sessionId = getSessionId(userPasswordRequestDto.getMobileNumber(), userPasswordRequestDto.getDeviceUniqueId());
        if(!(userPasswordRequestDto.getSessionId()).equals(sessionId)){
            userPasswordResponseDto.setResponseCode(UserConstants.INVALID_SESSION_CODE);
            userPasswordResponseDto.setResponseMessage(UserConstants.INVALID_SESSION_MESSAGE);
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(userPasswordResponseDto);
        }*/
        try {
            userPasswordResponseDto = userPasswordService.createNewPassword(userPasswordRequestDto);
            responseStatus = ResponseEntity.status(HttpStatus.OK).body(userPasswordResponseDto);
        } catch (Exception ex) {
            LOGGER.error(ex.toString());
            responseStatus = ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        } finally {
            return responseStatus;
        }
    }

    @RequestMapping(value = "/resetPassword", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<UserPasswordResponseDto> updateNewPassword(@RequestBody UserPasswordRequestDto userPasswordRequestDto) {
        UserPasswordResponseDto userPasswordResponseDto = new UserPasswordResponseDto();
        ResponseEntity<UserPasswordResponseDto> responseStatus = null;
        String sessionId = getSessionId(userPasswordRequestDto.getMobileNumber(), userPasswordRequestDto.getDeviceUniqueId());
        if(!(userPasswordRequestDto.getSessionId()).equals(sessionId)){
            userPasswordResponseDto.setResponseCode(UserConstants.INVALID_SESSION_CODE);
            userPasswordResponseDto.setResponseMessage(UserConstants.INVALID_SESSION_MESSAGE);
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(userPasswordResponseDto);
        }
        try {
            userPasswordResponseDto = userPasswordService.updateNewPassword(userPasswordRequestDto);
            responseStatus = ResponseEntity.status(HttpStatus.OK).body(userPasswordResponseDto);
        } catch (Exception ex) {
            LOGGER.error(ex.toString());
            responseStatus = ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        } finally {
            return responseStatus;
        }
    }

    private String getSessionId(String mobileNumber, String deviceUniqueId){
        User user = userDao.getUserByMobileNumber(mobileNumber);
        UserDevice userDevice = userDao.getUserDeviceByType(user.getUserId(), deviceUniqueId);
        String sessionId = userDao.getSessionIdByUserDeviceId(userDevice.getUserDeviceId());
        return sessionId;
    }
}