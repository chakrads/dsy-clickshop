package com.clickshop.user.utils;

import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SessionImplementor;
import org.hibernate.id.IdentifierGenerator;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.UUID;

public class OrderUtility implements IdentifierGenerator {

    // Generate unique order id
    @Override
    public Serializable generate(SessionImplementor sessionImplementor, Object o) throws HibernateException {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("MMddHH:mm:ss");
        LocalDateTime now = LocalDateTime.now();
        String dateTimeStampNow = dtf.format(now);
        dateTimeStampNow = dateTimeStampNow.replace(":", "");
        UUID uuid = UUID.randomUUID();
        String shortRandomDigit = uuid.toString().toUpperCase().substring(0, 5);
        String orderNumber = "O" + shortRandomDigit + dateTimeStampNow;
        return orderNumber;
    }
}
