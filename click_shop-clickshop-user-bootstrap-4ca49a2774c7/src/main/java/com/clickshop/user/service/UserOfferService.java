package com.clickshop.user.service;

import com.clickshop.user.dto.request.StoreLocatorRequestDto;
import com.clickshop.user.dto.response.StoreItemSearchOfferInRangeResponse;
import org.springframework.stereotype.Component;

@Component
public interface UserOfferService {

   public StoreItemSearchOfferInRangeResponse findOfferOnItem(StoreLocatorRequestDto userOfferOnItemRequestDto);

}
