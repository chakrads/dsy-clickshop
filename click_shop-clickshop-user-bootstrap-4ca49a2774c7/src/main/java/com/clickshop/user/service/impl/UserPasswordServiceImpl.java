package com.clickshop.user.service.impl;

import com.clickshop.user.constants.SecurityConstants;
import com.clickshop.user.constants.UserConstants;
import com.clickshop.user.dao.UserPasswordDao;
import com.clickshop.user.dto.request.UserPasswordRequestDto;
import com.clickshop.user.dto.response.UserPasswordResponseDto;
import com.clickshop.user.entity.User;
import com.clickshop.user.service.UserPasswordService;
import com.clickshop.user.utils.PasswordUtility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
public class UserPasswordServiceImpl implements UserPasswordService {

    @Autowired
    UserPasswordDao userPasswordDao;


    @Override
    public UserPasswordResponseDto createNewPassword(UserPasswordRequestDto userPasswordRequestDto) {
        UserPasswordResponseDto userPasswordResponseDto = new UserPasswordResponseDto();
        User userData = userPasswordDao.getUserByMobileNumber(userPasswordRequestDto.getMobileNumber());
        if (null != userData) {
            if (userPasswordRequestDto.getNewPassword().equals(userPasswordRequestDto.getConfirmPassword())) {

                int resultCount = userPasswordDao.createUserNewPassword(userPasswordRequestDto);
                if (1 == resultCount) {
                    userPasswordResponseDto.setResponseCode(UserConstants.SUCCESS_RESPONSE_CODE);
                    userPasswordResponseDto.setResponseMessage("Password update successfully");
                } else {
                    userPasswordResponseDto.setResponseMessage("Password update failed");
                    userPasswordResponseDto.setResponseCode(UserConstants.FAILED_RESPONSE_CODE);
                }
            } else {
                userPasswordResponseDto.setResponseMessage("New Password and Confirm password does not match");
                userPasswordResponseDto.setResponseCode(UserConstants.FAILED_RESPONSE_CODE);
            }

        } else {
            userPasswordResponseDto.setResponseMessage("Password update failed due to user's mobile number does not match");
            userPasswordResponseDto.setResponseCode(UserConstants.FAILED_RESPONSE_CODE);
        }
        return userPasswordResponseDto;

    }

    @Transactional
    @Override
    public UserPasswordResponseDto updateNewPassword(UserPasswordRequestDto userPasswordRequestDto) {
        UserPasswordResponseDto passwordResponseDto = new UserPasswordResponseDto();
        User userData = userPasswordDao.getUserByMobileNumber(userPasswordRequestDto.getMobileNumber());
        if (null != userData) {
            if (!userPasswordRequestDto.getOldPassword().equals(userPasswordRequestDto.getNewPassword())) {
                String secureMerchantPassword = PasswordUtility.generateSecurePassword(userPasswordRequestDto.getOldPassword(), SecurityConstants.PASSWORD_ENCRYPTION_SALT);
                userPasswordRequestDto.setOldPassword(secureMerchantPassword);
                String oldPassword = userPasswordRequestDto.getOldPassword();
                if (userData.getPassword().equals(oldPassword)) {
                    if (userPasswordRequestDto.getNewPassword().equals(userPasswordRequestDto.getConfirmPassword())) {

                        int resultCount = userPasswordDao.createUserNewPassword(userPasswordRequestDto);
                        if (1 == resultCount) {
                            passwordResponseDto.setResponseCode(UserConstants.SUCCESS_RESPONSE_CODE);
                            passwordResponseDto.setResponseMessage("Password update successfully");
                        } else {
                            passwordResponseDto.setResponseMessage("Password update failed");
                            passwordResponseDto.setResponseCode(UserConstants.FAILED_RESPONSE_CODE);
                        }
                    } else {
                        passwordResponseDto.setResponseMessage("New Password and Confirm password does not match");
                        passwordResponseDto.setResponseCode(UserConstants.FAILED_RESPONSE_CODE);
                    }
                } else {
                    passwordResponseDto.setResponseMessage("Previous password and old password does not match");
                    passwordResponseDto.setResponseCode(UserConstants.FAILED_RESPONSE_CODE);
                }
            } else {
                passwordResponseDto.setResponseMessage("New password is same as old password");
                passwordResponseDto.setResponseCode(UserConstants.FAILED_RESPONSE_CODE);
            }
        } else {
            passwordResponseDto.setResponseMessage("Password update failed due to mobile number does not match");
            passwordResponseDto.setResponseCode(UserConstants.FAILED_RESPONSE_CODE);
        }
        return passwordResponseDto;

    }
}

