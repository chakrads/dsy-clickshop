package com.clickshop.user.service.impl;


import com.clickshop.user.constants.UserConstants;
import com.clickshop.user.dao.OrderDao;
import com.clickshop.user.dao.StoreLocatorDao;
import com.clickshop.user.dao.UserOfferDao;
import com.clickshop.user.dto.request.StoreLocatorRequestDto;
import com.clickshop.user.dto.response.StoreItemSearchOfferInRangeResponse;
import com.clickshop.user.dto.response.StoreItmSearchOfferResponse;
import com.clickshop.user.entity.Item;
import com.clickshop.user.entity.ItemsOfMerchant;
import com.clickshop.user.entity.Offer;
import com.clickshop.user.entity.Store;
import com.clickshop.user.service.UserOfferService;
import com.clickshop.user.utils.DistanceUtility;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class UserOfferServiceImpl implements UserOfferService {

    public static final Logger LOGGER = LoggerFactory.getLogger(UserOfferServiceImpl.class);

    @Autowired
    UserOfferDao userOfferDao;

    @Autowired
    StoreLocatorDao storeLocatorDao;

    @Autowired
    OrderDao orderDao;


    @Override
    public StoreItemSearchOfferInRangeResponse findOfferOnItem(StoreLocatorRequestDto storeLocatorRequestDto) {
        StoreItemSearchOfferInRangeResponse storeItemSearchOfferInRangeResponse = new StoreItemSearchOfferInRangeResponse();
        List<StoreItmSearchOfferResponse> storeListInRange = new ArrayList<>();
        double userLatitude = storeLocatorRequestDto.getLatitude();
        double userLongitude = storeLocatorRequestDto.getLongitude();
        List<Store> allStores = storeLocatorDao.getAllStores();
        if (null != allStores) {
            allStores.forEach(store -> {
                long distanceInKm = DistanceUtility.distance(userLatitude, store.getLatitude(), userLongitude, store.getLongitude());
                if (distanceInKm <= 3) {
                    List<ItemsOfMerchant> itemsOfMerchants = getAllMerchantItems(store.getMerchant().getMerchantId());
                    itemsOfMerchants.forEach(itemsOfMerchant -> {
                        Offer offer = userOfferDao.getItemsOffer(itemsOfMerchant.getMerchant().getMerchantId(), itemsOfMerchant.getItemId());
                        if (null != offer) {
                            StoreItmSearchOfferResponse storeItmSearchOfferResponse = new StoreItmSearchOfferResponse();
                            storeItmSearchOfferResponse.setActualPrice(offer.getActualPrice());
                            storeItmSearchOfferResponse.setDiscountAmount(offer.getDiscountAmount());
                            storeItmSearchOfferResponse.setDiscountPercentage(offer.getDiscountPercentage());
                            storeItmSearchOfferResponse.setPriceAfterDiscount(offer.getPriceAfterDiscount());
                            storeItmSearchOfferResponse.setItemId(itemsOfMerchant.getItemId());
                            storeItmSearchOfferResponse.setItemCategory(itemsOfMerchant.getItemCategory().getItemCategoryName());
                            //Get image from Item table
                            storeItmSearchOfferResponse.setItemMerchantId(itemsOfMerchant.getItemMerchantId());
                            Item item = storeLocatorDao.getItemById(itemsOfMerchant.getItemId());
                            storeItmSearchOfferResponse.setItemName(item.getItemName());
                            storeItmSearchOfferResponse.setItemImage(item.getItemImage());
                            storeItmSearchOfferResponse.setActualPrice(itemsOfMerchant.getItemPrice());
                            storeItmSearchOfferResponse.setItemWeight(item.getItemWeight());
                            storeItmSearchOfferResponse.setMerchantId(itemsOfMerchant.getMerchant().getMerchantId());
                            storeItmSearchOfferResponse.setItemMRP(item.getItemPrice());
                            //Get store from store table
                            Store storeData = orderDao.getStoreByMerchantId(itemsOfMerchant.getMerchant().getMerchantId());
                            storeItmSearchOfferResponse.setStoreId(storeData.getStoreId());
                            storeItmSearchOfferResponse.setLatitude(storeData.getLatitude());
                            storeItmSearchOfferResponse.setLongitude(storeData.getLongitude());
                            storeItmSearchOfferResponse.setDeliveryDuration(storeData.getDeliveryDuration());
                            storeItmSearchOfferResponse.setStoreName(storeData.getStoreName());
                            storeItmSearchOfferResponse.setStoreRating(storeData.getStoreRating());
                            storeListInRange.add(storeItmSearchOfferResponse);
                        }
                    });
                }
            });
            if (0 != storeListInRange.size()) {
                storeItemSearchOfferInRangeResponse.setResponseCode(UserConstants.SUCCESS_RESPONSE_CODE);
                storeItemSearchOfferInRangeResponse.setResponseMessage("Offer on store item get success");
                storeItemSearchOfferInRangeResponse.setStoreList(storeListInRange);
            } else {
                storeItemSearchOfferInRangeResponse.setResponseCode(UserConstants.FAILED_RESPONSE_CODE);
                storeItemSearchOfferInRangeResponse.setResponseMessage("There are no offer on store item, Empty offer!");
            }
        }
        return storeItemSearchOfferInRangeResponse;

    }

    private List<ItemsOfMerchant> getAllMerchantItems(Long merchantId) {
        List<ItemsOfMerchant> itemsOfMerchants;
        try {
            itemsOfMerchants = storeLocatorDao.getAllItemsFromStore(merchantId);
        } catch (Exception e) {
            LOGGER.info(e.getMessage());
            return null;
        }
        return itemsOfMerchants;
    }

}
