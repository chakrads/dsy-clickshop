package com.clickshop.user.service.impl;

import com.clickshop.user.constants.UserConstants;
import com.clickshop.user.dao.StoreLocatorDao;
import com.clickshop.user.dto.SearchItemInGalleryDto;
import com.clickshop.user.dto.StoreAttributeResponse;
import com.clickshop.user.dto.StoreItmSearchAttributeResponse;
import com.clickshop.user.dto.StoreSearchItemData;
import com.clickshop.user.dto.request.StoreLocatorRequestDto;
import com.clickshop.user.dto.response.*;
import com.clickshop.user.endpoint.ClickshopUserEndpoint;
import com.clickshop.user.entity.*;
import com.clickshop.user.service.StoreLocatorService;
import com.clickshop.user.utils.DistanceUtility;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class StoreLocatorServiceImpl implements StoreLocatorService {

    public static final Logger LOGGER = LoggerFactory.getLogger(ClickshopUserEndpoint.class);

    @Autowired
    private StoreLocatorDao storeLocatorDao;

    @Override
    public StoreLocatorResponseDto findStoreInRange(StoreLocatorRequestDto storeLocatorRequestDto) {
        List<StoreAttributeResponse> storeListInRange = new ArrayList<>();
        StoreLocatorResponseDto storeLocatorResponseDto = new StoreLocatorResponseDto();
        double userLatitude = storeLocatorRequestDto.getLatitude();
        double userLongitude = storeLocatorRequestDto.getLongitude();
        List<Store> allStores = storeLocatorDao.getAllStores();
        if (null != allStores) {
            allStores.forEach(store -> {
            	
                long distanceInKm = DistanceUtility.distance(userLatitude, store.getLatitude(), userLongitude, store.getLongitude());
//                if (distanceInKm <= 3) {
                	if (distanceInKm <= Long.parseLong(store.getDeliveryRange())) {
                    ProfileImage storeProfileImage = storeLocatorDao.getStoreProfileImageById(store.getMerchant().getMerchantId());
                    StoreAttributeResponse storeAttributeResponse = prepareStoreData(store);
                    if(null != storeProfileImage)
                        storeAttributeResponse.setProfilePic(storeProfileImage.getProfilePicUrl());
                    storeListInRange.add(storeAttributeResponse);
                }
            });
            if (0 != storeListInRange.size()) {
                storeLocatorResponseDto.setStoreList(storeListInRange);
                storeLocatorResponseDto.setResponseCode(UserConstants.SUCCESS_RESPONSE_CODE);
                storeLocatorResponseDto.setResponseMessage("Store list within 3 KM range");
            } else {
                storeLocatorResponseDto.setResponseCode(UserConstants.FAILED_RESPONSE_CODE);
                storeLocatorResponseDto.setResponseMessage("No stores available within 3 KM range");
            }
        } else {
            storeLocatorResponseDto.setResponseCode(UserConstants.FAILED_RESPONSE_CODE);
            storeLocatorResponseDto.setResponseMessage("No stores available on clickshop");
        }
        return storeLocatorResponseDto;
    }

    @Override
    public SearchItemInGalleryByCategoryResponse searchItemInGalleryByCategory(StoreLocatorRequestDto storeLocatorRequestDto) {
        SearchItemInGalleryByCategoryResponse searchItemInGalleryByCategoryResponse = new SearchItemInGalleryByCategoryResponse();
        List<SearchItemInGalleryDto> itemDataList = new ArrayList<>();
        List<Item> itemList = storeLocatorDao.getItemByCategory(storeLocatorRequestDto.getItemCategory());
        if (0 != itemList.size()) {
            itemList.forEach(item -> {
                SearchItemInGalleryDto searchItemInGalleryDto = new SearchItemInGalleryDto();
                searchItemInGalleryDto.setItemName(item.getItemName());
                searchItemInGalleryDto.setItemImage(item.getItemImage());
                searchItemInGalleryDto.setItemCategory(item.getItemCategory());
                searchItemInGalleryDto.setItemMRP(item.getItemPrice());

                itemDataList.add(searchItemInGalleryDto);
            });
            searchItemInGalleryByCategoryResponse.setResponseCode(UserConstants.SUCCESS_RESPONSE_CODE);
            searchItemInGalleryByCategoryResponse.setResponseMessage("Gallery item get success");
            searchItemInGalleryByCategoryResponse.setItemDataList(itemDataList);
        } else {
            searchItemInGalleryByCategoryResponse.setResponseCode(UserConstants.FAILED_RESPONSE_CODE);
            searchItemInGalleryByCategoryResponse.setResponseMessage("Gallery item get failed");
        }
        return searchItemInGalleryByCategoryResponse;
    }

    @Override
    public StoreItemSearchInRangeResponse findItemInStoreInRange(StoreLocatorRequestDto storeLocatorRequestDto) {
        StoreItemSearchInRangeResponse storeItemSearchResponse = new StoreItemSearchInRangeResponse();
        List<StoreItmSearchAttributeResponse> storeListInRange = new ArrayList<>();
        double userLatitude = storeLocatorRequestDto.getLatitude();
        double userLongitude = storeLocatorRequestDto.getLongitude();
        List<Store> allStores = storeLocatorDao.getAllStores();
        if (null != allStores) {
            allStores.forEach(store -> {
                long distanceInKm = DistanceUtility.distance(userLatitude, store.getLatitude(), userLongitude, store.getLongitude());
                // Old Impl start
                /*if (distanceInKm <= 3) {
                    StoreSearchItemData storeSearchItemData = checkItemAvailability(store.getMerchant().getMerchantId(), storeLocatorRequestDto.getItemName());
                    if (null != storeSearchItemData) {
                        //ProfileImage storeProfileImage = storeLocatorDao.getStoreProfileImageById(store.getMerchant().getMerchantId());
                        StoreItmSearchAttributeResponse prepareSearchStoreData = prepareSearchStoreData(store);
                        //prepareSearchStoreData.setProfilePic(storeProfileImage.getProfilePic());
                        prepareSearchStoreData.setItemId(storeSearchItemData.getItemId());
                        prepareSearchStoreData.setItemCategory(storeSearchItemData.getItemCategory());
                        prepareSearchStoreData.setItemImage(storeSearchItemData.getItemImage());
                        prepareSearchStoreData.setItemName(storeSearchItemData.getItemName());
                        prepareSearchStoreData.setItemPrice(storeSearchItemData.getItemPrice());
                        prepareSearchStoreData.setItemWeight(storeSearchItemData.getItemWeight());
                        prepareSearchStoreData.setMerchantId(storeSearchItemData.getMerchantId());
                        storeListInRange.add(prepareSearchStoreData);
                    }
                }*/
                // Old Impl end
                //New Impl start
                if (distanceInKm <= 3) {
                    List<StoreSearchItemData> storeSearchItemDataList = checkItemAvailabilityInStore(store, store.getMerchant().getMerchantId(), storeLocatorRequestDto.getItemName());
                    if (0 != storeSearchItemDataList.size()) {
                        StoreItmSearchAttributeResponse prepareSearchStoreData = prepareSearchStoreData(store);
                        prepareSearchStoreData.setStoreSearchItemData(storeSearchItemDataList);
                        storeListInRange.add(prepareSearchStoreData);
                    }
                }
                //New Impl end
            });
            if (0 != storeListInRange.size()) {
                storeItemSearchResponse.setStoreList(storeListInRange);
                storeItemSearchResponse.setResponseCode(UserConstants.SUCCESS_RESPONSE_CODE);
                storeItemSearchResponse.setResponseMessage("Item available in stores within 3 KM range");
            } else {
                storeItemSearchResponse.setResponseCode(UserConstants.FAILED_RESPONSE_CODE);
                storeItemSearchResponse.setResponseMessage("Item not available in any store within 3 KM range");
            }
        }
        return storeItemSearchResponse;
    }

    @Override
    public StoreItemSearchResponse findStoreItem(StoreLocatorRequestDto storeLocatorRequestDto) {
        StoreItemSearchResponse storeItemSearchResponse = new StoreItemSearchResponse();
        Store store = storeLocatorDao.getStoreById(storeLocatorRequestDto.getStoreId());
        if(null != store){
            StoreSearchItemData storeSearchItemData = checkItemAvailability(store.getMerchant().getMerchantId(), storeLocatorRequestDto.getItemName());
            if(null != storeSearchItemData){
                Offer offer = storeLocatorDao.getItemsOffer(storeSearchItemData.getMerchantId(), storeSearchItemData.getItemId());
                storeItemSearchResponse = prepareSearchStoreItemResponse(store, storeSearchItemData, offer);
            }else{
                storeItemSearchResponse.setResponseCode(UserConstants.FAILED_RESPONSE_CODE);
                storeItemSearchResponse.setResponseMessage("Item not available in the store");
            }
        }else{
            storeItemSearchResponse.setResponseCode(UserConstants.FAILED_RESPONSE_CODE);
            storeItemSearchResponse.setResponseMessage("Item not available in the store");
        }
        return storeItemSearchResponse;
    }

    @Override
        public SearchAllStoreItemResponse findAllItemsInStore(StoreLocatorRequestDto storeLocatorRequestDto) {
        SearchAllStoreItemResponse searchAllStoreItemResponse = new SearchAllStoreItemResponse();
        List<StoreSearchItemData> storeSearchItemDataList = new ArrayList<>();
        Store store = storeLocatorDao.getStoreById(storeLocatorRequestDto.getStoreId());
        if(null == store){
            searchAllStoreItemResponse.setResponseCode(UserConstants.FAILED_RESPONSE_CODE);
            searchAllStoreItemResponse.setResponseMessage("Store Not available, Please register the store first");
            return searchAllStoreItemResponse;
        }
        List<ItemsOfMerchant> itemsOfMerchantList;
        try {
            itemsOfMerchantList = storeLocatorDao.getAllItemsFromStore(store.getMerchant().getMerchantId());
            if(0 != itemsOfMerchantList.size()){
                itemsOfMerchantList.forEach(itemsOfMerchant -> {
                    StoreSearchItemData storeSearchItemData = prepareStoreItemData(itemsOfMerchant);
                    storeSearchItemData.setStoreId(store.getStoreId());
                    storeSearchItemData.setStoreName(store.getStoreName());
                    storeSearchItemDataList.add(storeSearchItemData);
                });
                searchAllStoreItemResponse.setStoreSearchItemDataList(storeSearchItemDataList);
                searchAllStoreItemResponse.setResponseCode(UserConstants.SUCCESS_RESPONSE_CODE);
                searchAllStoreItemResponse.setResponseMessage("Store Item get Success");
            }else{
                searchAllStoreItemResponse.setResponseCode(UserConstants.FAILED_RESPONSE_CODE);
                searchAllStoreItemResponse.setResponseMessage("No item available in the store");
            }
        }catch(Exception e){
            LOGGER.info(e.getMessage());
            return null;
        }
        return searchAllStoreItemResponse;
    }

    @Override
    public SearchAllStoreItemResponse findAllItemsInStoreByCategory(StoreLocatorRequestDto storeLocatorRequestDto) {
        SearchAllStoreItemResponse searchAllStoreItemResponse = new SearchAllStoreItemResponse();
        List<StoreSearchItemData> storeSearchItemDataList = new ArrayList<>();
        Store store = storeLocatorDao.getStoreById(storeLocatorRequestDto.getStoreId());
        if(null == store){
            searchAllStoreItemResponse.setResponseCode(UserConstants.FAILED_RESPONSE_CODE);
            searchAllStoreItemResponse.setResponseMessage("Store Not available, Please register the store first");
            return searchAllStoreItemResponse;
        }
        List<ItemsOfMerchant> itemsOfMerchantList;
        try {
            itemsOfMerchantList = storeLocatorDao.getAllItemsFromStoreByCategory(store.getMerchant().getMerchantId(), storeLocatorRequestDto.getItemCategoryId());
            if(0 != itemsOfMerchantList.size()){
                itemsOfMerchantList.forEach(itemsOfMerchant -> {
                    StoreSearchItemData storeSearchItemData = prepareInStoreItemData(itemsOfMerchant, store);
                    storeSearchItemDataList.add(storeSearchItemData);
                });
                searchAllStoreItemResponse.setStoreSearchItemDataList(storeSearchItemDataList);
                searchAllStoreItemResponse.setResponseCode(UserConstants.SUCCESS_RESPONSE_CODE);
                searchAllStoreItemResponse.setResponseMessage("Store Item by Category get Success");
            }else{
                searchAllStoreItemResponse.setResponseCode(UserConstants.FAILED_RESPONSE_CODE);
                searchAllStoreItemResponse.setResponseMessage("No item available in the store");
            }
        }catch(Exception e){
            LOGGER.info(e.getMessage());
            return null;
        }
        return searchAllStoreItemResponse;
    }

    private StoreItemSearchResponse prepareSearchStoreItemResponse(Store store, StoreSearchItemData storeSearchItemData, Offer offer) {
        StoreItemSearchResponse storeItemSearchResponse = new StoreItemSearchResponse();
        storeItemSearchResponse.setResponseCode(UserConstants.SUCCESS_RESPONSE_CODE);
        storeItemSearchResponse.setResponseMessage("Search store item get success");
        //store
        storeItemSearchResponse.setStoreId(store.getStoreId());
        storeItemSearchResponse.setLatitude(store.getLongitude());
        storeItemSearchResponse.setLongitude(store.getLongitude());
        storeItemSearchResponse.setDeliveryDuration(store.getDeliveryDuration());
        storeItemSearchResponse.setStoreRating(store.getStoreRating());
        storeItemSearchResponse.setStoreName(store.getStoreName());
        //item
        storeItemSearchResponse.setItemName(storeSearchItemData.getItemName());
        storeItemSearchResponse.setItemImage(storeSearchItemData.getItemImage());
        storeItemSearchResponse.setItemPrice(storeSearchItemData.getItemPrice());
        storeItemSearchResponse.setItemWeight(storeSearchItemData.getItemWeight());
        storeItemSearchResponse.setItemCategory(storeSearchItemData.getItemCategory());
        //offer
        if (null != offer) {
            storeItemSearchResponse.setActualPrice(offer.getActualPrice());
            storeItemSearchResponse.setDiscountPercentage(offer.getDiscountPercentage());
            storeItemSearchResponse.setDiscountAmount(offer.getDiscountAmount());
            storeItemSearchResponse.setPriceAfterDiscount(offer.getPriceAfterDiscount());
        }
        return storeItemSearchResponse;
    }

    private StoreSearchItemData checkItemAvailability(Long merchantId, String itemName) {
        StoreSearchItemData storeSearchItemData;
        ItemsOfMerchant itemsOfMerchant;
        try {
            itemsOfMerchant = storeLocatorDao.getItemFromStoreInRange(merchantId, itemName);
            storeSearchItemData = prepareStoreItemData(itemsOfMerchant);
        } catch (Exception e) {
            LOGGER.info(e.getMessage());
            return null;
        }
        return storeSearchItemData;
    }

    private List<StoreSearchItemData> checkItemAvailabilityInStore(Store store, Long merchantId, String itemName) {
        List<StoreSearchItemData> storeSearchItemDataList = new ArrayList<>();
        try {
            List<ItemsOfMerchant> itemsOfMerchants = storeLocatorDao.getItemsListFromStoreInRange(merchantId, itemName);
            itemsOfMerchants.forEach(itemsOfMerchant -> {
                StoreSearchItemData storeSearchItemData = prepareInStoreItemData(itemsOfMerchant, store);
                storeSearchItemDataList.add(storeSearchItemData);
            });
        } catch (Exception e) {
            LOGGER.info(e.getMessage());
            return null;
        }
        return storeSearchItemDataList;
    }

    private StoreSearchItemData prepareInStoreItemData(ItemsOfMerchant itemsOfMerchant, Store store) {
        StoreSearchItemData storeSearchItemData = new StoreSearchItemData();
        storeSearchItemData.setItemMerchantId(itemsOfMerchant.getItemMerchantId());
        storeSearchItemData.setStoreId(store.getStoreId());
        storeSearchItemData.setStoreName(store.getStoreName());
        Item item = storeLocatorDao.getItemById(itemsOfMerchant.getItemId());
        storeSearchItemData.setItemImage(item.getItemImage());
        storeSearchItemData.setItemName(item.getItemName());
        storeSearchItemData.setItemPrice(itemsOfMerchant.getItemPrice());
        storeSearchItemData.setItemWeight(item.getItemWeight());
        ItemCategory itemCategory = storeLocatorDao.getItemCategory(itemsOfMerchant.getItemCategory().getItemCategoryId());
        storeSearchItemData.setItemCategory(itemCategory.getItemCategoryName());
        storeSearchItemData.setItemId(itemsOfMerchant.getItemId());
        storeSearchItemData.setMerchantId(itemsOfMerchant.getMerchant().getMerchantId());
        storeSearchItemData.setItemMRP(item.getItemPrice());
        return storeSearchItemData;
    }

    private StoreSearchItemData prepareStoreItemData(ItemsOfMerchant itemsOfMerchant) {
        StoreSearchItemData storeSearchItemData = new StoreSearchItemData();
        storeSearchItemData.setItemMerchantId(itemsOfMerchant.getItemMerchantId());
        Item item = storeLocatorDao.getItemById(itemsOfMerchant.getItemId());
        storeSearchItemData.setItemImage(item.getItemImage());
        storeSearchItemData.setItemName(item.getItemName());
        storeSearchItemData.setItemPrice(itemsOfMerchant.getItemPrice());
        storeSearchItemData.setItemWeight(item.getItemWeight());
        ItemCategory itemCategory = storeLocatorDao.getItemCategory(itemsOfMerchant.getItemCategory().getItemCategoryId());
        storeSearchItemData.setItemCategory(itemCategory.getItemCategoryName());
        storeSearchItemData.setItemId(itemsOfMerchant.getItemId());
        storeSearchItemData.setMerchantId(itemsOfMerchant.getMerchant().getMerchantId());
        storeSearchItemData.setItemMRP(item.getItemPrice());
        return storeSearchItemData;
    }

    private StoreAttributeResponse prepareStoreData(Store store) {
        StoreAttributeResponse storeAttributeResponse = new StoreAttributeResponse();
        storeAttributeResponse.setAddress(store.getAddress());
        storeAttributeResponse.setStoreId(store.getStoreId());
        storeAttributeResponse.setMerchantId(store.getMerchant().getMerchantId());
        storeAttributeResponse.setDeliveryDuration(store.getDeliveryDuration());
        storeAttributeResponse.setWorkingHourFrom(store.getWorkingHourFrom());
        storeAttributeResponse.setWorkingHourTo(store.getWorkingHourTo());
        storeAttributeResponse.setDeliveryRange(store.getDeliveryRange());
        storeAttributeResponse.setStoreName(store.getStoreName());
        storeAttributeResponse.setLatitude(store.getLatitude());
        storeAttributeResponse.setLongitude(store.getLongitude());
        storeAttributeResponse.setCity(store.getCity());
        storeAttributeResponse.setState(store.getState());
        storeAttributeResponse.setCountry(store.getCountry());
        storeAttributeResponse.setPinCode(store.getPinCode());
        storeAttributeResponse.setFlag(store.getFlag());
        return storeAttributeResponse;
    }

    private StoreItmSearchAttributeResponse prepareSearchStoreData(Store store) {
        StoreItmSearchAttributeResponse storeAttributeResponse = new StoreItmSearchAttributeResponse();
        storeAttributeResponse.setStoreId(store.getStoreId());
        storeAttributeResponse.setDeliveryDuration(store.getDeliveryDuration());
        storeAttributeResponse.setLatitude(store.getLatitude());
        storeAttributeResponse.setLongitude(store.getLongitude());
        storeAttributeResponse.setStoreRating(store.getStoreRating());
        storeAttributeResponse.setStoreName(store.getStoreName());
        return storeAttributeResponse;
    }
}
