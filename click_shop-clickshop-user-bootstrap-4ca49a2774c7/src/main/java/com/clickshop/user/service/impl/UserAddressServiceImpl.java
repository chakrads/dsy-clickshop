package com.clickshop.user.service.impl;

import com.clickshop.user.constants.UserConstants;
import com.clickshop.user.dao.UserAddressDao;
import com.clickshop.user.dto.UserShippingAddressListResponse;
import com.clickshop.user.dto.UserShippingAddressResponse;
import com.clickshop.user.dto.request.UserShippingAddressRequest;
import com.clickshop.user.entity.User;
import com.clickshop.user.entity.UserShippingAddress;
import com.clickshop.user.service.UserAddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Component
public class UserAddressServiceImpl implements UserAddressService {

    @Autowired
    private UserAddressDao userAddressDao;

    @Transactional
    @Override
    public UserShippingAddressResponse saveUserShippingAddress(UserShippingAddressRequest userShippingAddressRequest) {
        UserShippingAddressResponse userShippingAddressResponse = new UserShippingAddressResponse();
        UserShippingAddress userShippingAddress = prepareUserShippingAddressRequest(userShippingAddressRequest);
        UserShippingAddress userShippingAddressData = userAddressDao.saveUserShippingAddress(userShippingAddress);
        if (null != userShippingAddressData) {
            userShippingAddressResponse.setResponseCode(UserConstants.SUCCESS_RESPONSE_CODE);
            userShippingAddressResponse.setResponseMessage("User shipping address save success");
            userShippingAddressResponse.setUserShippingAddressId(userShippingAddressData.getUserShippingAddressId());
            userShippingAddressResponse.setAddress(userShippingAddressData.getAddress());
            userShippingAddressResponse.setMobileNumber(userShippingAddressData.getMobileNumber());
            userShippingAddressResponse.setUserId(userShippingAddressData.getUser().getUserId());
        } else {
            userShippingAddressResponse.setResponseCode(UserConstants.FAILED_RESPONSE_CODE);
            userShippingAddressResponse.setResponseMessage("User shipping address save failed");
        }
        return userShippingAddressResponse;
    }

    private UserShippingAddress prepareUserShippingAddressRequest(UserShippingAddressRequest userShippingAddressRequest) {
        UserShippingAddress userShippingAddress = new UserShippingAddress();
        User user = new User();
        user.setUserId(userShippingAddressRequest.getUserId());
        userShippingAddress.setUserShippingAddressId(userShippingAddressRequest.getUserShippingAddressId());
        userShippingAddress.setAddress(userShippingAddressRequest.getAddress());
        userShippingAddress.setMobileNumber(userShippingAddressRequest.getMobileNumber());
        userShippingAddress.setUser(user);
        return userShippingAddress;
    }

    @Override
    public UserShippingAddressListResponse getUserShippingAddress(UserShippingAddressRequest userShippingAddressRequest) {
        UserShippingAddressListResponse userShippingAddressListResponse = new UserShippingAddressListResponse();
        List<UserShippingAddress> userShippingAddressList = userAddressDao.getUserShippingAddressListById(userShippingAddressRequest.getUserId());
        if (userShippingAddressList.size() != 0) {
            List<UserShippingAddressRequest> shippingAddressList = new ArrayList<>();
            userShippingAddressList.forEach(userShippingAddress -> {
                UserShippingAddressRequest userShippingAddressData = new UserShippingAddressRequest();
                userShippingAddressData.setUserShippingAddressId(userShippingAddress.getUserShippingAddressId());
                userShippingAddressData.setUserId(userShippingAddress.getUser().getUserId());
                userShippingAddressData.setAddress(userShippingAddress.getAddress());
                userShippingAddressData.setMobileNumber(userShippingAddress.getMobileNumber());
                shippingAddressList.add(userShippingAddressData);
            });
            userShippingAddressListResponse.setResponseCode(UserConstants.SUCCESS_RESPONSE_CODE);
            userShippingAddressListResponse.setResponseMessage("User shipping address list get success");
            userShippingAddressListResponse.setUserShippingAddressList(shippingAddressList);
        } else {
            userShippingAddressListResponse.setResponseCode(UserConstants.FAILED_RESPONSE_CODE);
            userShippingAddressListResponse.setResponseMessage("No shipping address available, Please add your shipping address to continue");
        }
        return userShippingAddressListResponse;
    }

    @Override
    public UserShippingAddressListResponse deleteUserShippingAddress(UserShippingAddressRequest userShippingAddressRequest) {
        UserShippingAddressListResponse userShippingAddressListResponse = new UserShippingAddressListResponse();
        List<UserShippingAddress> userShippingAddressList = userAddressDao.deleteUserShippingAddressById(prepareUserShippingAddressRequest(userShippingAddressRequest));
        if (userShippingAddressList.size() != 0) {
            List<UserShippingAddressRequest> shippingAddressList = new ArrayList<>();
            userShippingAddressList.forEach(userShippingAddress -> {
                UserShippingAddressRequest userShippingAddressData = new UserShippingAddressRequest();
                userShippingAddressData.setUserShippingAddressId(userShippingAddress.getUserShippingAddressId());
                userShippingAddressData.setUserId(userShippingAddress.getUser().getUserId());
                userShippingAddressData.setAddress(userShippingAddress.getAddress());
                userShippingAddressData.setMobileNumber(userShippingAddress.getMobileNumber());
                shippingAddressList.add(userShippingAddressData);
            });
            userShippingAddressListResponse.setResponseCode(UserConstants.SUCCESS_RESPONSE_CODE);
            userShippingAddressListResponse.setResponseMessage("User shipping address delete success");
            userShippingAddressListResponse.setUserShippingAddressList(shippingAddressList);
        } else if (0 == userShippingAddressList.size()) {
            userShippingAddressListResponse.setResponseCode(UserConstants.SUCCESS_RESPONSE_CODE);
            userShippingAddressListResponse.setResponseMessage("User shipping address list is empty");
        } else if (null == userShippingAddressList) {
            userShippingAddressListResponse.setResponseCode(UserConstants.FAILED_RESPONSE_CODE);
            userShippingAddressListResponse.setResponseMessage("User shipping address delete failed");
        }
        return userShippingAddressListResponse;
    }
}
