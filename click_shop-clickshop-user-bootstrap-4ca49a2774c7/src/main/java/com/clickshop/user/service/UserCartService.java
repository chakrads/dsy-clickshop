package com.clickshop.user.service;

import com.clickshop.user.dto.*;
import org.springframework.stereotype.Component;

@Component
public interface UserCartService {

    public UserCartResponseDto saveUserCart(UserCartRequestDto userCartRequestDto);

    public UserCartResponseDto getUserCart(UserCartRequestDto userCartRequestDto);

    public UserCartDeleteResponseDto deleteUserCart(UserCartRequestDto userCartRequestDto);

    public UserCartResponseDto addItemUserCart(UserCartAddItemRequestDto userCartAddItemRequestDto);

    public UserCartDeleteResponseDto deleteUserSelectedStore(UserCartRequestDto userCartRequestDto);

    public UserCartDeleteResponseDto deleteUserCartItem(UserCartAddItemRequestDto userCartAddItemRequestDto);

    public UserSelectedStoreResponseDto saveUserSelectedStore(UserCartAddItemRequestDto userCartAddItemRequestDto);

}
