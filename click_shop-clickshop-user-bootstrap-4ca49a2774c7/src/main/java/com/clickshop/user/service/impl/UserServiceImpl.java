package com.clickshop.user.service.impl;

import com.clickshop.user.constants.SecurityConstants;
import com.clickshop.user.constants.UserConstants;
import com.clickshop.user.dao.UserDao;
import com.clickshop.user.dao.UserPasswordDao;
import com.clickshop.user.dto.UserRequestDto;
import com.clickshop.user.dto.UserSignOutResponseDto;
import com.clickshop.user.dto.request.UserAddressTypeRequestDto;
import com.clickshop.user.dto.response.UserAddressTypeResponseDto;
import com.clickshop.user.dto.UserResponseDto;
import com.clickshop.user.entity.User;
import com.clickshop.user.entity.UserAddressType;
import com.clickshop.user.entity.UserDevice;
import com.clickshop.user.entity.UserLoginHistory;
import com.clickshop.user.service.UserService;
import com.clickshop.user.utils.PasswordUtility;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserServiceImpl implements UserService {

    public static final Logger LOGGER = LoggerFactory.getLogger(UserServiceImpl.class);

    @Autowired
    private UserDao userDao;

    @Autowired
    private UserPasswordDao userPasswordDao;

    @Override
    public UserResponseDto signUpUser(UserRequestDto userRequestDto) {
        UserResponseDto userResponseDto = new UserResponseDto();
        User checkUserData = userDao.getUserByMobileNumber(userRequestDto.getMobileNumber());
        if (null == checkUserData) {
            // User save
            String secureMerchantPassword = PasswordUtility.generateSecurePassword(userRequestDto.getPassword(), SecurityConstants.PASSWORD_ENCRYPTION_SALT);
            User user = setUserRequest(userRequestDto);
            user.setPassword(secureMerchantPassword);
            User userData = userDao.saveUser(user);
            if (null != userData) {
                // User device save
                UserDevice userDevice = setUserDeviceRequest(userRequestDto, user);
                UserDevice userDeviceData = userDao.saveUserDevice(userDevice);
                if (null != userDeviceData) {
                    // User login history save
                    UserLoginHistory userLoginHistory = setUserLoginHistoryRequest(userRequestDto, userData, userDeviceData);
                    UserLoginHistory userLoginHistoryData = userDao.saveUserLoginHistory(userLoginHistory);
                    userResponseDto = prepareUserResponse(userData);
                    userResponseDto.setSessionId(userLoginHistoryData.getSessionId());
                    userResponseDto.setDeviceUniqueId(userDeviceData.getDeviceUniqueId());
                }
            }
        } else {
            userResponseDto.setResponseCode(UserConstants.FAILED_RESPONSE_CODE);
            userResponseDto.setResponseMessage("User already exist, please continue login with valid credential");
        }
        return userResponseDto;
    }

    @Override
    public UserResponseDto signInUser(UserRequestDto userRequestDto) {
        UserResponseDto userResponseDto = new UserResponseDto();
        User checkUserData = userDao.getUserByMobileNumber(userRequestDto.getMobileNumber());
        if (null != checkUserData) {
            String sessionId = getSessionId(userRequestDto.getMobileNumber(), userRequestDto.getDeviceUniqueId(), userRequestDto.getDeviceType());
            UserDevice userDevice = userDao.getUserDeviceByType(checkUserData.getUserId(), userRequestDto.getDeviceUniqueId());
            if ("-1".equals(sessionId)) {
                //User user = userDao.getUserByMobileNumber(userRequestDto.getMobileNumber());
                //UserDevice userDevice = userDao.getUserDeviceByType(checkUserData.getUserId(), userRequestDto.getDeviceUniqueId());
                String newSessionId = String.valueOf(System.nanoTime());
                int result = userDao.updateSessionId(userDevice.getUserDeviceId(), newSessionId);
                if (1 == result) {
                    LOGGER.info("Session updated successfully!!!");
                }
            }
            boolean verifyUserPassword = PasswordUtility.verifyPassword(userRequestDto.getPassword(), checkUserData.getPassword(),
                    SecurityConstants.PASSWORD_ENCRYPTION_SALT);
            if (verifyUserPassword) {
                String latestSessionId = userDao.getSessionIdByUserDeviceId(userDevice.getUserDeviceId());
                userResponseDto = prepareUserResponse(checkUserData);
                userResponseDto.setDeviceUniqueId(userRequestDto.getDeviceUniqueId());
                userResponseDto.setSessionId(latestSessionId);
                userResponseDto.setResponseMessage("Login successful!!!");
                userDao.updateFirebaseToken(userRequestDto.getFirebaseToken(), checkUserData.getUserId());
            } else {
                userResponseDto.setResponseCode(UserConstants.FAILED_RESPONSE_CODE);
                userResponseDto.setResponseMessage("User credentials doesn't match, please continue login with valid credential");
            }
        } else {
            userResponseDto.setResponseCode(UserConstants.FAILED_RESPONSE_CODE);
            userResponseDto.setResponseMessage("User doesn't exist, please register first before login");
        }
        return userResponseDto;
    }

    @Override
    public UserSignOutResponseDto signOutUser(UserRequestDto userRequestDto) {
        UserSignOutResponseDto userSignOutResponseDto = new UserSignOutResponseDto();
        User user = userPasswordDao.getUserByMobileNumber(userRequestDto.getMobileNumber());
        if (null != user) {
            UserDevice userDevice = userDao.getUserDeviceByType(user.getUserId(), userRequestDto.getDeviceUniqueId());
            int result = userDao.updateSessionId(userDevice.getUserDeviceId(), "-1");
            if (1 == result) {
                userSignOutResponseDto.setResponseCode(UserConstants.SUCCESS_RESPONSE_CODE);
                userSignOutResponseDto.setResponseMessage("Logging out...");
            } else {
                userSignOutResponseDto.setResponseCode(UserConstants.FAILED_RESPONSE_CODE);
                userSignOutResponseDto.setResponseMessage("Logging out failed...");
            }
        }
        return userSignOutResponseDto;
    }

    private UserLoginHistory setUserLoginHistoryRequest(UserRequestDto userRequestDto, User userData, UserDevice userDeviceData) {
        UserLoginHistory userLoginHistory = new UserLoginHistory();
        userLoginHistory.setLatitude(Double.valueOf(userRequestDto.getLatitude()));
        userLoginHistory.setLongitude(Double.valueOf(userRequestDto.getLongitude()));
        userLoginHistory.setSessionId(String.valueOf(System.nanoTime()));
        userLoginHistory.setUser(userData);
        userLoginHistory.setUserDevice(userDeviceData);
        return userLoginHistory;
    }

    private UserDevice setUserDeviceRequest(UserRequestDto userRequestDto, User user) {
        UserDevice userDevice = new UserDevice();
        userDevice.setDeviceType(userRequestDto.getDeviceType());
        userDevice.setDeviceUniqueId(userRequestDto.getDeviceUniqueId());
        userDevice.setUser(user);
        userDevice.setIsActive("1");
        userDevice.setIsBlocked("1");
        return userDevice;
    }

    private User setUserRequest(UserRequestDto userRequestDto) {
        User user = new User();
        user.setMobileNumber(userRequestDto.getMobileNumber());
        user.setMobileVerified("1");
        user.setEmail(userRequestDto.getEmail());
        user.setEmailVerified("1");
        user.setPassword(userRequestDto.getPassword());
        user.setFirstName(userRequestDto.getFirstName());
        user.setLastName(userRequestDto.getLastName());
        user.setFirebaseToken(userRequestDto.getFirebaseToken());
        user.setActive("1");
        user.setReferralCode(userRequestDto.getReferralCode());
        return user;
    }

    @Override
    public UserAddressTypeResponseDto saveAddressType(UserAddressTypeRequestDto userAddressTypeRequestDto) {
        UserAddressTypeResponseDto userAddressTypeResponseCheck = getAddressType(userAddressTypeRequestDto);
        if(userAddressTypeResponseCheck.getResponseCode() == 400)
            userAddressTypeResponseCheck = null;

        UserAddressTypeResponseDto userAddressTypeResponseDto = new UserAddressTypeResponseDto();
        if (null == userAddressTypeResponseCheck) {
            UserAddressType userAddressType = prepareUserAddressTypeRequest(userAddressTypeRequestDto, null);
            UserAddressType userAddressTypeData = userDao.saveUserAddressType(userAddressType);
            if (null != userAddressTypeData) {
                userAddressTypeResponseDto = prepareUserAddressTypeResponse(userAddressTypeData);
            } else {
                userAddressTypeResponseDto.setResponseCode(UserConstants.FAILED_RESPONSE_CODE);
                userAddressTypeResponseDto.setResponseMessage("User Address save failed");
            }
        } else if (null != userAddressTypeResponseCheck){
            UserAddressType userAddressType = prepareUserAddressTypeRequest(userAddressTypeRequestDto, userAddressTypeResponseCheck.getUserAddressTypeId());
            UserAddressType userAddressTypeData = userDao.updateUserAddressType(userAddressType);
            if (null != userAddressTypeData) {
                userAddressTypeResponseDto = prepareUserAddressTypeResponse(userAddressTypeData);
            } else {
                userAddressTypeResponseDto.setResponseCode(UserConstants.FAILED_RESPONSE_CODE);
                userAddressTypeResponseDto.setResponseMessage("User Address update failed");
            }
        }
        return userAddressTypeResponseDto;
    }

    @Override
    public UserAddressTypeResponseDto getAddressType(UserAddressTypeRequestDto userAddressTypeRequestDto) {
        UserAddressType userAddressTypeData = userDao.getUserAddressType(userAddressTypeRequestDto);
        UserAddressTypeResponseDto userAddressTypeResponseDto = new UserAddressTypeResponseDto();
        if (null != userAddressTypeData) {
            userAddressTypeResponseDto = prepareUserAddressTypeResponse(userAddressTypeData);
            userAddressTypeResponseDto.setResponseMessage("User Address get success");
        } else {
            userAddressTypeResponseDto.setResponseCode(UserConstants.FAILED_RESPONSE_CODE);
            userAddressTypeResponseDto.setResponseMessage("User Address get failed");
        }
        return userAddressTypeResponseDto;
    }

    private UserAddressTypeResponseDto prepareUserAddressTypeResponse(UserAddressType userAddressTypeData) {
        UserAddressTypeResponseDto userAddressTypeResponseDto = new UserAddressTypeResponseDto();
        userAddressTypeResponseDto.setResponseCode(UserConstants.SUCCESS_RESPONSE_CODE);
        userAddressTypeResponseDto.setResponseMessage("User Address save success");
        userAddressTypeResponseDto.setUserAddressTypeId(userAddressTypeData.getUserAddressTypeId());
        userAddressTypeResponseDto.setAddress(userAddressTypeData.getAddress());
        //userAddressTypeResponseDto.setAddressType(userAddressTypeData.getAddressType());
        userAddressTypeResponseDto.setLatitude(userAddressTypeData.getLatitude());
        userAddressTypeResponseDto.setLongitude(userAddressTypeData.getLongitude());
        userAddressTypeResponseDto.setUserId(userAddressTypeData.getUserId().getUserId());
        userAddressTypeResponseDto.setCity(userAddressTypeData.getCity());
        userAddressTypeResponseDto.setState(userAddressTypeData.getState());
        userAddressTypeResponseDto.setCountry(userAddressTypeData.getCountry());
        userAddressTypeResponseDto.setPinCode(userAddressTypeData.getPinCode());
        return userAddressTypeResponseDto;
    }

    private UserAddressType prepareUserAddressTypeRequest(UserAddressTypeRequestDto userAddressTypeRequestDto, Long userAddressTypeId) {
        User user = new User();
        user.setUserId(userAddressTypeRequestDto.getUserId());
        UserAddressType userAddressType = new UserAddressType();
        userAddressType.setAddress(userAddressTypeRequestDto.getAddress());
        //userAddressType.setAddressType(userAddressTypeRequestDto.getAddressType());
        userAddressType.setUserAddressTypeId(userAddressTypeId);
        userAddressType.setLatitude(userAddressTypeRequestDto.getLatitude());
        userAddressType.setLongitude(userAddressTypeRequestDto.getLongitude());
        userAddressType.setUserId(user);
        userAddressType.setCity(userAddressTypeRequestDto.getCity());
        userAddressType.setState(userAddressTypeRequestDto.getState());
        userAddressType.setCountry(userAddressTypeRequestDto.getCountry());
        userAddressType.setPinCode(userAddressTypeRequestDto.getPinCode());
        return userAddressType;
    }

    private UserResponseDto prepareUserResponse(User userData) {
        UserResponseDto userResponseDto = new UserResponseDto();
        if (null != userData) {
            userResponseDto.setResponseCode(UserConstants.SUCCESS_RESPONSE_CODE);
            userResponseDto.setResponseMessage(UserConstants.LOGIN_REGISTRATION_SUCCESS_MESSAGE);
            userResponseDto.setUserId(userData.getUserId());
            userResponseDto.setEmail(userData.getEmail());
            userResponseDto.setFirstName(userData.getFirstName());
            userResponseDto.setLastName(userData.getLastName());
            userResponseDto.setMobileNumber(userData.getMobileNumber());
            userResponseDto.setReferralCode(userData.getReferralCode());
        }
        return userResponseDto;
    }

    private String getSessionId(String mobileNumber, String deviceUniqueId, String deviceType){
        String sessionId;
        User user = userDao.getUserByMobileNumber(mobileNumber);
        UserDevice userDevice = userDao.getUserDeviceByType(user.getUserId(), deviceUniqueId);
        if (null == userDevice) {
            sessionId = getSessionIdUserNewDevice(deviceUniqueId, deviceType, user);
        } else {
            sessionId = userDao.getSessionIdByUserDeviceId(userDevice.getUserDeviceId());
        }
        return sessionId;
    }

    private String getSessionIdUserNewDevice(String deviceUniqueId, String deviceType, User user) {
        UserDevice userDeviceRequestData = new UserDevice();
        userDeviceRequestData.setDeviceType(deviceType);
        userDeviceRequestData.setDeviceUniqueId(deviceUniqueId);
        userDeviceRequestData.setUser(user);
        userDeviceRequestData.setIsActive("1");
        userDeviceRequestData.setIsBlocked("1");
        UserDevice userDeviceResData = userDao.saveUserDevice(userDeviceRequestData);
        UserLoginHistory userLoginHistory = new UserLoginHistory();
        userLoginHistory.setLatitude(Double.valueOf("0.0"));
        userLoginHistory.setLongitude(Double.valueOf("0.0"));
        userLoginHistory.setSessionId(String.valueOf(System.nanoTime()));
        userLoginHistory.setUser(user);
        userLoginHistory.setUserDevice(userDeviceResData);
        UserLoginHistory userLoginHistoryData = userDao.saveUserLoginHistory(userLoginHistory);
        String sessionId = userLoginHistoryData.getSessionId();
        return sessionId;
    }

}
