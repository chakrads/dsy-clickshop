package com.clickshop.user.service.impl;

import com.clickshop.user.constants.UserConstants;
import com.clickshop.user.dao.OrderDao;
import com.clickshop.user.dao.StoreLocatorDao;
import com.clickshop.user.dao.UserCartDao;
import com.clickshop.user.dto.OrderItemDto;
import com.clickshop.user.dto.OrderRequestDto;
import com.clickshop.user.dto.OrderResponseDto;
import com.clickshop.user.dto.request.FindOrderRequestDto;
import com.clickshop.user.dto.request.OrderSearchRequestDto;
import com.clickshop.user.dto.request.OrderStatusRequestDto;
import com.clickshop.user.dto.response.*;
import com.clickshop.user.entity.*;
import com.clickshop.user.fcm.SendOrderNotifs;
import com.clickshop.user.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class OrderServiceImpl implements OrderService {

    @Autowired
    private OrderDao orderDao;

    @Autowired
    private StoreLocatorDao storeLocatorDao;

    @Autowired
    private UserCartDao userCartDao;

    @Override
    public OrderResponseDto createOrder(OrderRequestDto orderRequestDto) {
        OrderResponseDto orderResponseDto = new OrderResponseDto();
        Order order = prepareOrderRequest(orderRequestDto);
        Order orderData = orderDao.createOrder(order);
        if (null != orderData.getOrderId()) {
            orderRequestDto.getItemList().forEach(orderItemDto -> {
                OrderDetails orderDetails = prepareOrderDetailsRequest(orderData, orderItemDto);
                orderDao.saveOrderDetails(orderDetails);
            });
            //Clear cart and userSelectedStore
            userCartDao.deleteUserCart(orderRequestDto.getUserId());
            userCartDao.deleteUserSelectedStore(orderRequestDto.getUserId());
            //Send New Order Notifs to Merchant start
            Long merchantId = orderData.getMerchant().getMerchantId();
            String firebaseToken = orderDao.getMerchantFcmToken(merchantId);
            if (null != firebaseToken) {
                SendOrderNotifs orderNotifs = new SendOrderNotifs();
                orderNotifs.sendOrderNotifsToMerchant(firebaseToken, orderData.getOrderId());
            }
            //Send New Order Notifs to Merchant start
            orderResponseDto.setOrderId(orderData.getOrderId());
            orderResponseDto.setOrderStatus(orderData.getOrderStatus());
            orderResponseDto.setResponseCode(UserConstants.SUCCESS_RESPONSE_CODE);
            orderResponseDto.setResponseMessage("Order placed successfully, kindly wait for store confirmation!");
        } else {
            orderResponseDto.setOrderId(orderData.getOrderId());
            orderResponseDto.setResponseCode(UserConstants.FAILED_RESPONSE_CODE);
            orderResponseDto.setResponseMessage("Order Failed, kindly make the order again after sometime!");
        }
        return orderResponseDto;
    }

    public OrderDetails prepareOrderDetailsRequest(Order orderData, OrderItemDto orderItemDto) {
        OrderDetails orderDetails = new OrderDetails();
        orderDetails.setOrder(orderData);
        /*orderDetails.setItemId(orderItemDto.getItemId());
        orderDetails.setItemName(orderItemDto.getItemName());
        orderDetails.setItemImage(orderItemDto.getItemImage());*/
        /*ItemsOfMerchant itemsOfMerchant = new ItemsOfMerchant();
        itemsOfMerchant.setItemMerchantId(orderItemDto.getItemMerchantId());*/
        orderDetails.setItemMerchantId(orderItemDto.getItemMerchantId());
        orderDetails.setItemPrice(orderItemDto.getItemPrice());
        orderDetails.setItemQuantityRequested(orderItemDto.getItemQuantityRequested());
        orderDetails.setItemQuantityApproved(orderItemDto.getItemQuantityRequested());
        orderDetails.setItemPrice(orderItemDto.getItemPrice());
        orderDetails.setTotalPrice(orderItemDto.getTotalPrice());
        //orderDetails.setItemWeight(orderItemDto.getItemWeight());
        return orderDetails;
    }

    private Order prepareOrderRequest(OrderRequestDto orderRequestDto) {
        User user = new User();
        user.setUserId(orderRequestDto.getUserId());
        Order order = new Order();
        Store storeData = orderDao.getStoreById(orderRequestDto.getStoreId());
        Merchant merchant = new Merchant();
        merchant.setMerchantId(storeData.getMerchant().getMerchantId());
        order.setMerchant(merchant);
        order.setOrderAmount(orderRequestDto.getOrderAmount());
        order.setPaymentMode(orderRequestDto.getPaymentMode());
        order.setShippingAddress(orderRequestDto.getShippingAddress());
        order.setMobileNumber(orderRequestDto.getMobileNumber());
        order.setUser(user);
        order.setOrderStatus(UserConstants.USER_ORDER_STATUS_NEW);
        order.setDeliveryCharge(orderRequestDto.getDeliveryCharge());
        return order;
    }

    @Override
    public OrderSearchResponceDto findOrder(OrderSearchRequestDto orderSearchRequestDto) {
        OrderSearchResponceDto orderSearchResponceDto = new OrderSearchResponceDto();
        List<OrderDetailsListResponseDto> orderDetailsListResponseDto = new ArrayList<>();
        Order order = orderDao.findOrder(orderSearchRequestDto.getOrderId());
        if (null != order) {
            List<OrderDetails> orderDetailsList = orderDao.getOrderDetails(orderSearchRequestDto.getOrderId());
            if (null != orderDetailsList) {
                orderDetailsList.forEach(orderDetails -> {
                    OrderDetailsListResponseDto orderListResponseDto = new OrderDetailsListResponseDto();
                    ItemsOfMerchant itemsOfMerchant = storeLocatorDao.getMerchantItemById(orderDetails.getItemMerchantId());
                    Item item = storeLocatorDao.getItemById(itemsOfMerchant.getItemId());
                    orderListResponseDto.setItemId(item.getItemId());
                    orderListResponseDto.setOrderId(orderDetails.getOrder().getOrderId());
                    orderListResponseDto.setItemPrice(orderDetails.getItemPrice());
                    orderListResponseDto.setItemQuantity(orderDetails.getItemQuantityRequested());
                    orderListResponseDto.setItemWeight(item.getItemWeight());
                    orderListResponseDto.setTotalPrice(orderDetails.getTotalPrice());
                    orderListResponseDto.setItemName(item.getItemName());
                    orderListResponseDto.setItemImage(item.getItemImage());
                    orderDetailsListResponseDto.add(orderListResponseDto);
                });
            }
            orderSearchResponceDto.setResponseCode(UserConstants.SUCCESS_RESPONSE_CODE);
            orderSearchResponceDto.setResponseMessage("Order details get success");
            orderSearchResponceDto.setOrderId(order.getOrderId());
            orderSearchResponceDto.setOrderStatus(order.getOrderStatus());
            Store store = orderDao.getStoreByMerchantId(order.getMerchant().getMerchantId());
            orderSearchResponceDto.setStoreId(store.getStoreId());
            orderSearchResponceDto.setOrderAmount(order.getOrderAmount());
            orderSearchResponceDto.setPaymentMode(order.getPaymentMode());
            orderSearchResponceDto.setShippingAddress(order.getShippingAddress());
            orderSearchResponceDto.setMobileNumber(order.getMobileNumber());
            orderSearchResponceDto.setUserId(order.getUser().getUserId());
            orderSearchResponceDto.setDeliveryCharge(order.getDeliveryCharge());
            orderSearchResponceDto.setOrderDetailsList(orderDetailsListResponseDto);
        } else {
            orderSearchResponceDto.setResponseCode(UserConstants.FAILED_RESPONSE_CODE);
            orderSearchResponceDto.setResponseMessage("Order details get failed");
        }
        return orderSearchResponceDto;
    }

    @Override
    public OrderStatusResponseDto findOrderStatus(OrderStatusRequestDto orderStatusRequestDto) {
        OrderStatusResponseDto orderStatusResponse = new OrderStatusResponseDto();
        Order orderData = orderDao.findOrderStatus(orderStatusRequestDto);
        if (null != orderData) {
            orderStatusResponse.setOrderId(orderData.getOrderId());
            orderStatusResponse.setMerchantId(orderData.getMerchant().getMerchantId());
            orderStatusResponse.setOrderAmount(orderData.getOrderAmount());
            orderStatusResponse.setOrderStatus(orderData.getOrderStatus());
            orderStatusResponse.setPaymentMode(orderData.getPaymentMode());
            orderStatusResponse.setUserId(orderData.getUser().getUserId());
            orderStatusResponse.setShippingAddress(orderData.getShippingAddress());
            orderStatusResponse.setMobileNumber(orderData.getMobileNumber());
            orderStatusResponse.setDeliveryCharge(orderData.getDeliveryCharge());
            orderStatusResponse.setResponseCode(UserConstants.SUCCESS_RESPONSE_CODE);
            orderStatusResponse.setResponseMessage("Order status get success");
        } else {
            orderStatusResponse.setResponseCode(UserConstants.FAILED_RESPONSE_CODE);
            orderStatusResponse.setResponseMessage("Order details get failed");
        }
        return orderStatusResponse;
    }

    @Override
    public OrderSearchResponseDto getUserCurrentOrders(OrderStatusRequestDto orderStatusRequestDto) {
        OrderSearchResponseDto orderSearchResponseDto = new OrderSearchResponseDto();
        List<OrderSearchListResponseDto> orderSearchList = new ArrayList<>();
        List<Order> orderList = orderDao.getUserCurrentOrders(orderStatusRequestDto.getUserId());
        if (orderList.size() != 0) {
            orderList.forEach(order -> {
                OrderSearchListResponseDto orderSearchListResponseDto = new OrderSearchListResponseDto();
                orderSearchListResponseDto.setMerchantId(order.getMerchant().getMerchantId());
                orderSearchListResponseDto.setOrderId(order.getOrderId());
                orderSearchListResponseDto.setOrderStatus(order.getOrderStatus());
                orderSearchListResponseDto.setOrderAmount(order.getOrderAmount());
                orderSearchListResponseDto.setPaymentMode(order.getPaymentMode());
                orderSearchListResponseDto.setShippingAddress(order.getShippingAddress());
                orderSearchListResponseDto.setMobileNumber(order.getMobileNumber());
                orderSearchListResponseDto.setUserId(order.getUser().getUserId());
                orderSearchListResponseDto.setDeliveryCharge(order.getDeliveryCharge());
                orderSearchList.add(orderSearchListResponseDto);
            });
            orderSearchResponseDto.setResponseCode(UserConstants.SUCCESS_RESPONSE_CODE);
            orderSearchResponseDto.setResponseMessage("User current order get success");
            orderSearchResponseDto.setOrderSearchList(orderSearchList);
        } else {
            orderSearchResponseDto.setResponseCode(UserConstants.FAILED_RESPONSE_CODE);
            orderSearchResponseDto.setResponseMessage("User current order get failed");
        }
        return orderSearchResponseDto;
    }

    @Override
    public FindOrderResponseDto findOrderList(FindOrderRequestDto findOrderRequestDto) {
        FindOrderResponseDto findOrderResponseDto = new FindOrderResponseDto();
        List<OrderSearchListResponseDto> orderFindList = new ArrayList<>();
        List<Order> orders = orderDao.getOrderList(findOrderRequestDto.getUserId());
        if (orders.size() != 0) {
            orders.forEach(orderOfUser -> {
                OrderSearchListResponseDto orderFindListResponseDto = new OrderSearchListResponseDto();
                orderFindListResponseDto.setMerchantId(orderOfUser.getMerchant().getMerchantId());
                Store store = orderDao.getStoreByMerchantId(orderOfUser.getMerchant().getMerchantId());
                orderFindListResponseDto.setStoreName(store.getStoreName());
                Merchant merchant = orderDao.getMerchantById(orderOfUser.getMerchant().getMerchantId());
                orderFindListResponseDto.setStoreMobileNumber(merchant.getMobileNumber());
                orderFindListResponseDto.setOrderId(orderOfUser.getOrderId());
                orderFindListResponseDto.setUserId(orderOfUser.getUser().getUserId());
                orderFindListResponseDto.setOrderAmount(orderOfUser.getOrderAmount());
                orderFindListResponseDto.setPaymentMode(orderOfUser.getPaymentMode());
                orderFindListResponseDto.setOrderStatus(orderOfUser.getOrderStatus());
                orderFindListResponseDto.setShippingAddress(orderOfUser.getShippingAddress());
                orderFindListResponseDto.setMobileNumber(orderOfUser.getMobileNumber());
                orderFindListResponseDto.setDeliveryCharge(orderOfUser.getDeliveryCharge());
                orderFindListResponseDto.setOrderTime(orderOfUser.getOrderTime());
                orderFindList.add(orderFindListResponseDto);
            });
            findOrderResponseDto.setResponseCode(UserConstants.SUCCESS_RESPONSE_CODE);
            findOrderResponseDto.setResponseMessage("User list of order get success");
            findOrderResponseDto.setOrderList(orderFindList);
        } else {
            findOrderResponseDto.setResponseCode(UserConstants.FAILED_RESPONSE_CODE);
            findOrderResponseDto.setResponseMessage("User list of order get failed");
        }
        return findOrderResponseDto;
    }

    @Override
    public OrderDeliveryChargeResponseDto getOrderDeliveryCharge(OrderRequestDto orderRequestDto) {
        OrderDeliveryChargeResponseDto orderDeliveryChargeResponseDto = new OrderDeliveryChargeResponseDto();
        Store store = orderDao.getStoreById(orderRequestDto.getStoreId());
        if (null != store) {
            orderDeliveryChargeResponseDto.setResponseCode(UserConstants.SUCCESS_RESPONSE_CODE);
            orderDeliveryChargeResponseDto.setResponseMessage("Store delivery charge get success");
            orderDeliveryChargeResponseDto.setDeliveryCharge(store.getDeliveryCharge());
            orderDeliveryChargeResponseDto.setDeliveryFreeAbove(store.getDeliveryFreeAbove());
        } else {
            orderDeliveryChargeResponseDto.setResponseCode(UserConstants.FAILED_RESPONSE_CODE);
            orderDeliveryChargeResponseDto.setResponseMessage("Store delivery charge get failed");
        }
        return orderDeliveryChargeResponseDto;
    }
}


