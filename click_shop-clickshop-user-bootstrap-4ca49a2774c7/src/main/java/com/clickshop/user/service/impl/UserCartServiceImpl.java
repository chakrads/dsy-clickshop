package com.clickshop.user.service.impl;

import com.clickshop.user.constants.UserConstants;
import com.clickshop.user.dao.StoreLocatorDao;
import com.clickshop.user.dao.UserCartDao;
import com.clickshop.user.dto.*;
import com.clickshop.user.entity.*;
import com.clickshop.user.service.UserCartService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class UserCartServiceImpl implements UserCartService {

    @Autowired
    private UserCartDao userCartDao;

    @Autowired
    private StoreLocatorDao storeLocatorDao;


    @Override
    public UserCartResponseDto saveUserCart(UserCartRequestDto userCartRequestDto) {
        UserCartResponseDto userCartResponseDto = new UserCartResponseDto();
        UserSelectedStore userSelectedStoreData = userCartDao.getUserSelectedStore(userCartRequestDto.getUserId());
        if (null == userSelectedStoreData) {
            UserSelectedStore userSelectedStoreRequest = setUserSelectedStoreRequest(userCartRequestDto);
            UserSelectedStore userSelectedStore = userCartDao.saveUserSelectedStore(userSelectedStoreRequest);
            List<UserCart> userCartSavedList = new ArrayList<>();
            if (null != userSelectedStore) {
                List<UserCart> userCartList = setUserCartRequest(userCartRequestDto);
                userCartList.forEach(userCart -> {
                    UserCart userCartSavedData = userCartDao.saveUserCart(userCart);
                    if (null != userCartSavedData)
                        userCartSavedList.add(userCartSavedData);
                });
            }
            if (0 != userCartSavedList.size()) {
                List<UserCartItemListResponseDto> userCartItemList = setUserCartResponse(userCartSavedList);
                userCartResponseDto.setResponseCode(UserConstants.SUCCESS_RESPONSE_CODE);
                userCartResponseDto.setResponseMessage("User Cart save success");
                userCartResponseDto.setUserCartItemList(userCartItemList);
            } else {
                userCartResponseDto.setResponseCode(UserConstants.FAILED_RESPONSE_CODE);
                userCartResponseDto.setResponseMessage("User Cart save failed");
            }
        } else {
            // Send item one by one to add to user cart
            userCartRequestDto.getUserCartItemList().forEach(userCartItemListRequestDto -> {
                UserCartAddItemRequestDto userCartAddItemRequestDto = setUserCartInputRequest(userCartRequestDto, userCartItemListRequestDto);
                addItemUserCart(userCartAddItemRequestDto);
                userCartResponseDto.setResponseCode(UserConstants.SUCCESS_RESPONSE_CODE);
                userCartResponseDto.setResponseMessage("User Cart save success");
            });
        }
        return userCartResponseDto;
    }

    private UserCartAddItemRequestDto setUserCartInputRequest(UserCartRequestDto userCartRequestDto, UserCartItemListRequestDto userCartItemListRequestDto) {
        UserCartAddItemRequestDto userCartAddItemRequestDto = new UserCartAddItemRequestDto();
        userCartAddItemRequestDto.setUserId(userCartRequestDto.getUserId());
        userCartAddItemRequestDto.setStoreId(userCartRequestDto.getStoreId());
        userCartAddItemRequestDto.setDeviceUniqueId(userCartRequestDto.getDeviceUniqueId());
        userCartAddItemRequestDto.setSessionId(userCartRequestDto.getSessionId());
        userCartAddItemRequestDto.setItemMerchantId(userCartItemListRequestDto.getItemMerchantId());
        userCartAddItemRequestDto.setItemPrice(userCartItemListRequestDto.getItemPrice());
        userCartAddItemRequestDto.setItemQuantity(userCartItemListRequestDto.getItemQuantity());
        userCartAddItemRequestDto.setTotalPrice(userCartItemListRequestDto.getTotalPrice());
        return userCartAddItemRequestDto;
    }

    private UserSelectedStore setUserSelectedStoreRequest(UserCartRequestDto userCartRequestDto) {
        UserSelectedStore userSelectedStore = new UserSelectedStore();
        userSelectedStore.setStoreId(userCartRequestDto.getStoreId());
        userSelectedStore.setUserId(userCartRequestDto.getUserId());
        return userSelectedStore;
    }

    private UserSelectedStore setUserSelectedStoreInputRequest(UserCartAddItemRequestDto userCartAddItemRequestDto) {
        UserSelectedStore userSelectedStore = new UserSelectedStore();
        userSelectedStore.setStoreId(userCartAddItemRequestDto.getStoreId());
        userSelectedStore.setUserId(userCartAddItemRequestDto.getUserId());
        return userSelectedStore;
    }

    @Override
    public UserCartResponseDto getUserCart(UserCartRequestDto userCartRequestDto) {
        UserCartResponseDto userCartResponseDto = new UserCartResponseDto();
        List<UserCart> userCartList = userCartDao.getUserCartList(userCartRequestDto.getUserId());
        UserSelectedStore userSelectedStore = userCartDao.getUserSelectedStore(userCartRequestDto.getUserId());
        if (0 != userCartList.size()) {
            userCartResponseDto.setResponseCode(UserConstants.SUCCESS_RESPONSE_CODE);
            userCartResponseDto.setResponseMessage("User Cart get success");
            userCartResponseDto.setStoreId(userSelectedStore.getStoreId());
            List<UserCartItemListResponseDto> userCartItemList = setUserCartResponse(userCartList);
            userCartResponseDto.setUserCartItemList(userCartItemList);
        } else {
            userCartResponseDto.setStoreId(0L);
            userCartResponseDto.setResponseCode(UserConstants.FAILED_RESPONSE_CODE);
            userCartResponseDto.setResponseMessage("User Cart get failed");
        }
        return userCartResponseDto;
    }

    @Override
    public UserCartDeleteResponseDto deleteUserCart(UserCartRequestDto userCartRequestDto) {
        UserCartDeleteResponseDto userCartDeleteResponseDto = new UserCartDeleteResponseDto();
        int deleteSelectedStore = userCartDao.deleteUserSelectedStore(userCartRequestDto.getUserId());
        if (1 == deleteSelectedStore) {
            int result = userCartDao.deleteUserCart(userCartRequestDto.getUserId());
            if (result > 0) {
                userCartDeleteResponseDto.setResponseCode(UserConstants.SUCCESS_RESPONSE_CODE);
                userCartDeleteResponseDto.setResponseMessage("User cart delete success");
            } else {
                userCartDeleteResponseDto.setResponseCode(UserConstants.FAILED_RESPONSE_CODE);
                userCartDeleteResponseDto.setResponseMessage("User cart delete failed");
            }
        }
        return userCartDeleteResponseDto;
    }

    @Override
    public UserCartResponseDto addItemUserCart(UserCartAddItemRequestDto userCartAddItemRequestDto) {
        UserCartResponseDto userCartResponseDto = new UserCartResponseDto();
        UserSelectedStore userSelectedStore = userCartDao.getUserSelectedStore(userCartAddItemRequestDto.getUserId());
        if (null != userSelectedStore) {
            UserCart userCart = userCartDao.getUserCartItem(userCartAddItemRequestDto.getUserId(), userCartAddItemRequestDto.getItemMerchantId(), userCartAddItemRequestDto.getStoreId());
            if (null == userCart) {
                UserCart userCartItem = setUserCartItemRequest(userCartAddItemRequestDto);
                UserCart userCartAddItem = userCartDao.saveUserCart(userCartItem);
                if (null != userCartAddItem) {
                    List<UserCart> userCartList = userCartDao.getUserCartList(userCartAddItemRequestDto.getUserId());
                    List<UserCartItemListResponseDto> userCartItemList = setUserCartResponse(userCartList);
                    userCartResponseDto.setUserCartItemList(userCartItemList);
                    userCartResponseDto.setResponseCode(UserConstants.SUCCESS_RESPONSE_CODE);
                    userCartResponseDto.setResponseMessage("User cart item add success");
                } else {
                    userCartResponseDto.setResponseCode(UserConstants.FAILED_RESPONSE_CODE);
                    userCartResponseDto.setResponseMessage("User cart item add failed");
                }
            } else if (StringUtils.isNotEmpty(userCartAddItemRequestDto.getItemQuantity())
                    && !"0".equals(userCartAddItemRequestDto.getItemQuantity())) {
                UserCart userCartData = setUserCartExistingItemRequest(userCartAddItemRequestDto);
                int result = userCartDao.updateUserCartItem(userCartData);
                if (1 == result) {
                    List<UserCart> userCartList = userCartDao.getUserCartList(userCartAddItemRequestDto.getUserId());
                    List<UserCartItemListResponseDto> userCartItemList = setUserCartResponse(userCartList);
                    userCartResponseDto.setUserCartItemList(userCartItemList);
                    userCartResponseDto.setResponseCode(UserConstants.SUCCESS_RESPONSE_CODE);
                    userCartResponseDto.setResponseMessage("User cart item add success");
                } else {
                    userCartResponseDto.setResponseCode(UserConstants.FAILED_RESPONSE_CODE);
                    userCartResponseDto.setResponseMessage("User cart item add failed");
                }
            } else if (StringUtils.isEmpty(userCartAddItemRequestDto.getItemQuantity())
                    || "0".equals(userCartAddItemRequestDto.getItemQuantity())) {
                int result = userCartDao.deleteUserCartItem(userCartAddItemRequestDto);
                if (1 == result) {
                    List<UserCart> userCartList = userCartDao.getUserCartList(userCartAddItemRequestDto.getUserId());
                    List<UserCartItemListResponseDto> userCartItemList = setUserCartResponse(userCartList);
                    userCartResponseDto.setUserCartItemList(userCartItemList);
                    userCartResponseDto.setResponseCode(UserConstants.SUCCESS_RESPONSE_CODE);
                    userCartResponseDto.setResponseMessage("User cart item update success");
                } else {
                    userCartResponseDto.setResponseCode(UserConstants.FAILED_RESPONSE_CODE);
                    userCartResponseDto.setResponseMessage("User cart item update failed");
                }
            }
        } else {
            UserCart userCart = userCartDao.getUserCartItem(userCartAddItemRequestDto.getUserId(), userCartAddItemRequestDto.getItemMerchantId(), userCartAddItemRequestDto.getStoreId());
            if (null == userCart) {
                UserCart userCartItem = setUserCartItemRequest(userCartAddItemRequestDto);
                UserCart userCartAddItem = userCartDao.saveUserCart(userCartItem);
                UserSelectedStore userSelectedStoreRequestData = setUserSelectedStoreInputRequest(userCartAddItemRequestDto);
                UserSelectedStore userSelectedStoreSavedData = userCartDao.saveUserSelectedStore(userSelectedStoreRequestData);
                if (null != userCartAddItem && null != userSelectedStoreSavedData) {
                    List<UserCart> userCartList = userCartDao.getUserCartList(userCartAddItemRequestDto.getUserId());
                    List<UserCartItemListResponseDto> userCartItemList = setUserCartResponse(userCartList);
                    userCartResponseDto.setUserCartItemList(userCartItemList);
                    userCartResponseDto.setResponseCode(UserConstants.SUCCESS_RESPONSE_CODE);
                    userCartResponseDto.setResponseMessage("User cart item add success");
                } else {
                    userCartResponseDto.setResponseCode(UserConstants.FAILED_RESPONSE_CODE);
                    userCartResponseDto.setResponseMessage("User cart item add failed");
                }
            }
        }
        return userCartResponseDto;
    }

    @Override
    public UserCartDeleteResponseDto deleteUserSelectedStore(UserCartRequestDto userCartRequestDto) {
        UserCartDeleteResponseDto userCartDeleteResponseDto = new UserCartDeleteResponseDto();
        //if (0 == userCartRequestDto.getUserCartItemList().size()) {
            int deleteSelectedStore = userCartDao.deleteUserSelectedStore(userCartRequestDto.getUserId());
            if (1 == deleteSelectedStore) {
                userCartDeleteResponseDto.setResponseCode(UserConstants.SUCCESS_RESPONSE_CODE);
                userCartDeleteResponseDto.setResponseMessage("User selected store delete success");
            } else {
                userCartDeleteResponseDto.setResponseCode(UserConstants.FAILED_RESPONSE_CODE);
                userCartDeleteResponseDto.setResponseMessage("User selected store delete failed");
            }
        //}
        return userCartDeleteResponseDto;
    }

    @Override
    public UserCartDeleteResponseDto deleteUserCartItem(UserCartAddItemRequestDto userCartAddItemRequestDto) {
        UserCartDeleteResponseDto userCartDeleteResponseDto = new UserCartDeleteResponseDto();
        int result = userCartDao.deleteUserCartItem(userCartAddItemRequestDto);
        if (1 == result) {
            userCartDeleteResponseDto.setResponseCode(UserConstants.SUCCESS_RESPONSE_CODE);
            userCartDeleteResponseDto.setResponseMessage("User cart item delete success");
        } else {
            userCartDeleteResponseDto.setResponseCode(UserConstants.DELETE_FAILED_RESPONSE_CODE);
            userCartDeleteResponseDto.setResponseMessage("User cart item delete failed, because user already selected another store");
        }
        return userCartDeleteResponseDto;
    }

    @Override
    public UserSelectedStoreResponseDto saveUserSelectedStore(UserCartAddItemRequestDto userCartAddItemRequestDto) {
        UserSelectedStoreResponseDto userSelectedStoreResponseDto = new UserSelectedStoreResponseDto();
        List<UserCart> userCartList = userCartDao.getUserCartList(userCartAddItemRequestDto.getUserId());
        UserSelectedStore userSelectedStoreGet = userCartDao.getUserSelectedStore(userCartAddItemRequestDto.getUserId());
        if (0 == userCartList.size()) {
            if (null == userSelectedStoreGet) {
                UserSelectedStore userSelectedStore = setUserSelectedStoreInputRequest(userCartAddItemRequestDto);
                UserSelectedStore userSelectedStoreSavedData = userCartDao.saveUserSelectedStore(userSelectedStore);
                if (null != userSelectedStoreSavedData) {
                    userSelectedStoreResponseDto.setStoreId(userSelectedStoreSavedData.getStoreId());
                    userSelectedStoreResponseDto.setUserId(userSelectedStoreSavedData.getUserId());
                    userSelectedStoreResponseDto.setUserSelectedStoreId(userSelectedStoreSavedData.getUserSelectedStoreId());
                    userSelectedStoreResponseDto.setResponseCode(UserConstants.SUCCESS_RESPONSE_CODE);
                    userSelectedStoreResponseDto.setResponseMessage("User selected store save success");
                } else {
                    userSelectedStoreResponseDto.setResponseCode(UserConstants.FAILED_RESPONSE_CODE);
                    userSelectedStoreResponseDto.setResponseMessage("User selected store save failed");
                }
            } else if (!userCartAddItemRequestDto.getStoreId().equals(userSelectedStoreGet.getStoreId())) {
                int result = userCartDao.updateUserSelectedStoreId(userCartAddItemRequestDto, userSelectedStoreGet.getUserSelectedStoreId());
                if (1 == result) {
                    userCartDao.clearUserCartByUserId(userCartAddItemRequestDto.getUserId());
                    UserSelectedStore userSelectedStoreUpdateData = userCartDao.getUserSelectedStore(userCartAddItemRequestDto.getUserId());
                    userSelectedStoreResponseDto.setStoreId(userSelectedStoreUpdateData.getStoreId());
                    userSelectedStoreResponseDto.setUserId(userSelectedStoreUpdateData.getUserId());
                    userSelectedStoreResponseDto.setUserSelectedStoreId(userSelectedStoreUpdateData.getUserSelectedStoreId());
                    userSelectedStoreResponseDto.setResponseCode(UserConstants.SUCCESS_RESPONSE_CODE);
                    userSelectedStoreResponseDto.setResponseMessage("User selected store update success");
                } else {
                    userSelectedStoreResponseDto.setResponseCode(UserConstants.FAILED_RESPONSE_CODE);
                    userSelectedStoreResponseDto.setResponseMessage("User selected store update failed");
                }
            } else if (userCartAddItemRequestDto.getStoreId().equals(userSelectedStoreGet.getStoreId())) {
                UserSelectedStore userSelectedStoreUpdateData = userCartDao.getUserSelectedStore(userCartAddItemRequestDto.getUserId());
                userSelectedStoreResponseDto.setStoreId(userSelectedStoreUpdateData.getStoreId());
                userSelectedStoreResponseDto.setUserId(userSelectedStoreUpdateData.getUserId());
                userSelectedStoreResponseDto.setUserSelectedStoreId(userSelectedStoreUpdateData.getUserSelectedStoreId());
                userSelectedStoreResponseDto.setResponseCode(UserConstants.SUCCESS_RESPONSE_CODE);
                userSelectedStoreResponseDto.setResponseMessage("User selected store get success");
            }
        } else if (userCartAddItemRequestDto.getStoreId().equals(userSelectedStoreGet.getStoreId())) {
            UserSelectedStore userSelectedStoreUpdateData = userCartDao.getUserSelectedStore(userCartAddItemRequestDto.getUserId());
            userSelectedStoreResponseDto.setStoreId(userSelectedStoreUpdateData.getStoreId());
            userSelectedStoreResponseDto.setUserId(userSelectedStoreUpdateData.getUserId());
            userSelectedStoreResponseDto.setUserSelectedStoreId(userSelectedStoreUpdateData.getUserSelectedStoreId());
            userSelectedStoreResponseDto.setResponseCode(UserConstants.SUCCESS_RESPONSE_CODE);
            userSelectedStoreResponseDto.setResponseMessage("User selected store get success");
        } else {
            userSelectedStoreResponseDto.setResponseCode(UserConstants.FAILED_RESPONSE_CODE);
            userSelectedStoreResponseDto.setResponseMessage("Cart already has items from another store, please clear the cart before proceeding for other store!");
        }
        return userSelectedStoreResponseDto;
    }

    private UserCart setUserCartExistingItemRequest(UserCartAddItemRequestDto userCartAddItemRequestDto) {
        UserCart userCartItemData = new UserCart();
        ItemsOfMerchant itemsOfMerchant = new ItemsOfMerchant();
        itemsOfMerchant.setItemMerchantId(userCartAddItemRequestDto.getItemMerchantId());
        userCartItemData.setItemMerchant(itemsOfMerchant);
        User user = new User();
        user.setUserId(userCartAddItemRequestDto.getUserId());
        userCartItemData.setUser(user);
        userCartItemData.setStoreId(userCartAddItemRequestDto.getStoreId());
        userCartItemData.setItemPrice(userCartAddItemRequestDto.getItemPrice());
        userCartItemData.setItemQuantity(userCartAddItemRequestDto.getItemQuantity());
        userCartItemData.setTotalPrice(userCartAddItemRequestDto.getTotalPrice());
        return userCartItemData;
    }

    private UserCart setUserCartItemRequest(UserCartAddItemRequestDto userCartAddItemRequestDto) {
        UserCart userCart = new UserCart();
        ItemsOfMerchant itemsOfMerchant = new ItemsOfMerchant();
        itemsOfMerchant.setItemMerchantId(userCartAddItemRequestDto.getItemMerchantId());
        userCart.setItemMerchant(itemsOfMerchant);
        userCart.setItemPrice(userCartAddItemRequestDto.getItemPrice());
        userCart.setItemQuantity(userCartAddItemRequestDto.getItemQuantity());
        userCart.setTotalPrice(userCartAddItemRequestDto.getTotalPrice());
        User user = new User();
        user.setUserId(userCartAddItemRequestDto.getUserId());
        userCart.setUser(user);
        userCart.setStoreId(userCartAddItemRequestDto.getStoreId());
        return userCart;
    }

    private List<UserCartItemListResponseDto> setUserCartResponse(List<UserCart> userCartSavedList) {
        List<UserCartItemListResponseDto> userCartItemList = new ArrayList<>();
        userCartSavedList.forEach(userCartItem -> {
            UserCartItemListResponseDto userCartItemListResponseDto = new UserCartItemListResponseDto();
            ItemsOfMerchant itemsOfMerchant = userCartDao.getMerchantItemData(userCartItem.getItemMerchant().getItemMerchantId());
            Item item = storeLocatorDao.getItemById(itemsOfMerchant.getItemId());
            userCartItemListResponseDto.setItemMerchantId(userCartItem.getItemMerchant().getItemMerchantId());
            userCartItemListResponseDto.setItemId(item.getItemId());
            userCartItemListResponseDto.setItemName(item.getItemName());
            userCartItemListResponseDto.setItemImage(item.getItemImage());
            userCartItemListResponseDto.setItemWeight(item.getItemWeight());
            userCartItemListResponseDto.setItemPrice(userCartItem.getItemPrice());
            userCartItemListResponseDto.setItemQuantity(userCartItem.getItemQuantity());
            userCartItemListResponseDto.setTotalPrice(userCartItem.getTotalPrice());
            userCartItemList.add(userCartItemListResponseDto);
        });
        return userCartItemList;
    }

    private List<UserCart> setUserCartRequest(UserCartRequestDto userCartRequestDto) {
        List<UserCart> userCartList = new ArrayList<>();
        userCartRequestDto.getUserCartItemList().forEach(userCartItemData -> {
            UserCart userCart = new UserCart();
            ItemsOfMerchant itemsOfMerchant = new ItemsOfMerchant();
            itemsOfMerchant.setItemMerchantId(userCartItemData.getItemMerchantId());
            userCart.setItemMerchant(itemsOfMerchant);
            userCart.setItemPrice(userCartItemData.getItemPrice());
            userCart.setItemQuantity(userCartItemData.getItemQuantity());
            userCart.setTotalPrice(userCartItemData.getTotalPrice());
            User user = new User();
            user.setUserId(userCartRequestDto.getUserId());
            userCart.setUser(user);
            userCart.setStoreId(userCartRequestDto.getStoreId());
            userCartList.add(userCart);
        });
        return userCartList;
    }
}
