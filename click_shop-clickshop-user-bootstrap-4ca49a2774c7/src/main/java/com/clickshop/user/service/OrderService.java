package com.clickshop.user.service;

import com.clickshop.user.dto.OrderRequestDto;
import com.clickshop.user.dto.OrderResponseDto;
import com.clickshop.user.dto.request.FindOrderRequestDto;
import com.clickshop.user.dto.request.OrderSearchRequestDto;
import com.clickshop.user.dto.request.OrderStatusRequestDto;
import com.clickshop.user.dto.response.*;
import org.springframework.stereotype.Component;

@Component
public interface OrderService {

    public OrderResponseDto createOrder(OrderRequestDto orderRequestDto);

    public OrderSearchResponceDto findOrder(OrderSearchRequestDto orderSearchRequestDto);

    public OrderStatusResponseDto findOrderStatus(OrderStatusRequestDto orderStatusRequestDto);

    public OrderSearchResponseDto getUserCurrentOrders(OrderStatusRequestDto orderStatusRequestDto);

    public FindOrderResponseDto findOrderList(FindOrderRequestDto findOrderRequestDto);

    public OrderDeliveryChargeResponseDto getOrderDeliveryCharge(OrderRequestDto orderRequestDto);

}
