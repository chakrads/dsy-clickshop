package com.clickshop.user.service;

import com.clickshop.user.dto.request.StoreLocatorRequestDto;
import com.clickshop.user.dto.response.*;
import org.springframework.stereotype.Component;

@Component
public interface StoreLocatorService {

    public StoreLocatorResponseDto findStoreInRange(StoreLocatorRequestDto storeLocatorRequestDto);

    public StoreItemSearchInRangeResponse findItemInStoreInRange(StoreLocatorRequestDto storeLocatorRequestDto);

    public StoreItemSearchResponse findStoreItem(StoreLocatorRequestDto storeLocatorRequestDto);

    public SearchAllStoreItemResponse findAllItemsInStore(StoreLocatorRequestDto storeLocatorRequestDto);

    public SearchAllStoreItemResponse findAllItemsInStoreByCategory(StoreLocatorRequestDto storeLocatorRequestDto);

    public SearchItemInGalleryByCategoryResponse searchItemInGalleryByCategory(StoreLocatorRequestDto storeLocatorRequestDto);
}
