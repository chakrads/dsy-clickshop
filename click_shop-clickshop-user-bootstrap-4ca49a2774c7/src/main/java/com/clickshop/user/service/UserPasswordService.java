package com.clickshop.user.service;

import com.clickshop.user.dto.request.UserPasswordRequestDto;
import com.clickshop.user.dto.response.UserPasswordResponseDto;
import org.springframework.stereotype.Component;

@Component
public interface UserPasswordService {
    public UserPasswordResponseDto createNewPassword(UserPasswordRequestDto userPasswordRequestDto);

    public UserPasswordResponseDto updateNewPassword(UserPasswordRequestDto userPasswordRequestDto);
}
