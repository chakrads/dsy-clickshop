package com.clickshop.user.service;

import com.clickshop.user.dto.UserRequestDto;
import com.clickshop.user.dto.UserSignOutResponseDto;
import com.clickshop.user.dto.request.UserAddressTypeRequestDto;
import com.clickshop.user.dto.response.UserAddressTypeResponseDto;
import com.clickshop.user.dto.UserResponseDto;
import com.clickshop.user.entity.User;
import org.springframework.stereotype.Component;

@Component
public interface UserService {

    public UserResponseDto signUpUser(UserRequestDto userRequestDto);

    public UserResponseDto signInUser(UserRequestDto userRequestDto);

    public UserSignOutResponseDto signOutUser(UserRequestDto userRequestDto);

    public UserAddressTypeResponseDto saveAddressType(UserAddressTypeRequestDto userAddressTypeRequestDto);

    public UserAddressTypeResponseDto getAddressType(UserAddressTypeRequestDto userAddressTypeRequestDto);

}
