package com.clickshop.user.service;

import com.clickshop.user.dto.UserShippingAddressListResponse;
import com.clickshop.user.dto.UserShippingAddressResponse;
import com.clickshop.user.dto.request.UserShippingAddressRequest;
import org.springframework.stereotype.Component;

@Component
public interface UserAddressService {

    public UserShippingAddressResponse saveUserShippingAddress(UserShippingAddressRequest userShippingAddressRequest);

    public UserShippingAddressListResponse getUserShippingAddress(UserShippingAddressRequest userShippingAddressRequest);

    public UserShippingAddressListResponse deleteUserShippingAddress(UserShippingAddressRequest userShippingAddressRequest);

}
