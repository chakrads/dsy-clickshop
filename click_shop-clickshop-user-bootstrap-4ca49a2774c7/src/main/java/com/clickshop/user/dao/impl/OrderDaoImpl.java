package com.clickshop.user.dao.impl;

import com.clickshop.user.dao.OrderDao;
import com.clickshop.user.dto.request.OrderStatusRequestDto;
import com.clickshop.user.entity.Merchant;
import com.clickshop.user.entity.Order;
import com.clickshop.user.entity.OrderDetails;
import com.clickshop.user.entity.Store;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Component
public class OrderDaoImpl implements OrderDao {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserDaoImpl.class);

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private SessionFactory sessionFactory;

    private Session getSession() {
        return sessionFactory.getCurrentSession();
    }

    @Transactional
    @Override
    public Order createOrder(Order order) {
        Session session = getSession();
        session.save(order);
        return order;
    }

    @Transactional
    @Override
    public OrderDetails saveOrderDetails(OrderDetails orderDetails) {
        Session session = getSession();
        session.save(orderDetails);
        return orderDetails;
    }

    @Override
    public Order findOrder(String orderId) {
        Order order;
        try {
            order = (Order) this.entityManager.createQuery("from Order where order_id=:orderId")
                    .setParameter("orderId", orderId)
                    .getSingleResult();
        } catch (Exception e) {
            LOGGER.info(e.getMessage());
            return null;
        }
        return order;
    }

    @Override
    public List<OrderDetails> getOrderDetails(String orderId) {
        List<OrderDetails> orderDetailsList;
        try {
            orderDetailsList = (List<OrderDetails>) this.entityManager.createQuery("from OrderDetails where order_id=:orderId")
                    .setParameter("orderId", orderId)
                    .getResultList();
        } catch (Exception e) {
            LOGGER.info(e.getMessage());
            return null;
        }
        return orderDetailsList;
    }

    @Override
    public Store getStoreById(Long storeId) {
        Store storeData;
        try {
            String hqlQueryString = "from Store where store_Id=:storeId";
            storeData = (Store) this.entityManager.createQuery(hqlQueryString)
                    .setParameter("storeId", storeId).getSingleResult();
        } catch (Exception exception) {
            LOGGER.info(exception.getMessage());
            return null;
        }
        return storeData;
    }

    @Override
    public Store getStoreByMerchantId(Long merchantId) {
        Store storeData;
        try {
            String hqlQueryString = "from Store where merchant_Id=:merchantId";
            storeData = (Store) this.entityManager.createQuery(hqlQueryString)
                    .setParameter("merchantId", merchantId).getSingleResult();
        } catch (Exception exception) {
            LOGGER.info(exception.getMessage());
            return null;
        }
        return storeData;
    }

    @Override
    public Order findOrderStatus(OrderStatusRequestDto orderStatusRequestDto){
        Order order;
        try {
            order = (Order) this.entityManager.createQuery("from Order where user_id=:userId and order_status=:orderStatus")
                    .setParameter("userId", orderStatusRequestDto.getUserId())
                    .setParameter("orderStatus", orderStatusRequestDto.getOrderStatus())
                    .getSingleResult();
        } catch (Exception e) {
            LOGGER.info(e.getMessage());
            return null;
        }
        return order;
    }

    @Override
    public List<Order> getUserCurrentOrders(Long userId) {
        List<Order> orderList;
        try {
            orderList = (List<Order>) this.entityManager.createQuery("from Order where user_id=:userId and order_status=:orderStatus")
                    .setParameter("userId", userId)
                    .setParameter("orderStatus", "Progress")
                    .getResultList();
        } catch (Exception e) {
            LOGGER.info(e.getMessage());
            return null;
        }
        return orderList;
    }

    @Override
    public List<Order> getOrderList(Long userId){
        List<Order> orders;
        try{
            orders = (List<Order>) this.entityManager.createQuery("from Order where user_id=:userId")
                    .setParameter("userId",userId).getResultList();
        }catch (Exception e) {
            LOGGER.info(e.getMessage());
            return null;
        }
        return orders;

    }

    @Override
    public String getMerchantFcmToken(Long merchantId) {
        String firebaseToken;
        try{
            Merchant merchant = (Merchant) this.entityManager.createQuery("from Merchant where merchant_id=:merchantId")
                    .setParameter("merchantId",merchantId).getSingleResult();
            firebaseToken = merchant.getFirebaseToken();
        }catch (Exception e) {
            LOGGER.info(e.getMessage());
            return null;
        }
        return firebaseToken;
    }
    
    @Override
    public Merchant getMerchantById(Long merchantId) {
        Merchant merchantData = null;
        try {
            String hqlQueryString = "from Merchant where merchant_Id=:merchantId";
            merchantData = (Merchant) this.entityManager.createQuery(hqlQueryString)
                    .setParameter("merchantId", merchantId).getSingleResult();
        } catch (Exception exception) {
            LOGGER.info(exception.getMessage());
        }
        return merchantData;
    }

}
