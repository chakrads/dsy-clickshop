package com.clickshop.user.dao;

import com.clickshop.user.entity.Offer;
import com.clickshop.user.entity.Store;

public interface UserOfferDao {

    public Offer getItemsOffer(Long merchantId, Long itemId);

}
