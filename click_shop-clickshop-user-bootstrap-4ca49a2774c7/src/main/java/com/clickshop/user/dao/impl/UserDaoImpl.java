package com.clickshop.user.dao.impl;

import com.clickshop.user.dao.UserDao;
import com.clickshop.user.dto.request.UserAddressTypeRequestDto;
import com.clickshop.user.entity.User;
import com.clickshop.user.entity.UserAddressType;
import com.clickshop.user.entity.UserDevice;
import com.clickshop.user.entity.UserLoginHistory;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Component
public class UserDaoImpl implements UserDao {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserDaoImpl.class);

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private SessionFactory sessionFactory;

    private Session getSession() {
        return sessionFactory.getCurrentSession();
    }

    @Transactional
    @Override
    public User saveUser(User user) {
        Session session = getSession();
        session.save(user);
        return user;
    }

    @Transactional
    @Override
    public UserDevice saveUserDevice(UserDevice userDevice) {
        Session session = getSession();
        session.save(userDevice);
        return userDevice;
    }

    @Transactional
    @Override
    public UserLoginHistory saveUserLoginHistory(UserLoginHistory userLoginHistory) {
        Session session = getSession();
        session.save(userLoginHistory);
        return userLoginHistory;
    }

    @Override
    public User getUserByMobileNumber(String mobileNumber) {
        User userData;
        try {
            String hqlQueryString = "from User where mobile_number=:mobileNumber";
            userData = (User) this.entityManager.createQuery(hqlQueryString)
                    .setParameter("mobileNumber", mobileNumber).getSingleResult();
        } catch (Exception exception) {
            LOGGER.info(exception.getMessage());
            return null;
        }
        return userData;
    }

    @Transactional
    @Override
    public UserAddressType saveUserAddressType(UserAddressType userAddressType) {
        Session session = getSession();
        session.save(userAddressType);
        return userAddressType;
    }

    @Transactional
    @Override
    public UserAddressType updateUserAddressType(UserAddressType userAddressType) {
        Session session = getSession();
        session.update(userAddressType);
        return userAddressType;
    }

    @Transactional
    @Override
    public int updateFirebaseToken(String firebaseToken, Long userId) {
        Session session = getSession();
        Query query = session.createQuery("update User set firebase_token = :firebaseToken" +
                " where user_id = :userId");
        query.setParameter("firebaseToken", firebaseToken);
        query.setParameter("userId", userId);
        int result = query.executeUpdate();
        session.flush();
        return result;
    }

    @Override
    public UserDevice getUserDeviceByType(Long userId, String deviceUniqueId) {
        UserDevice userDevice;
        try {
            String hqlQueryString = "from UserDevice where user_Id=:userId and device_unique_id=:deviceUniqueId";
            userDevice = (UserDevice) this.entityManager.createQuery(hqlQueryString)
                    .setParameter("userId", userId)
                    .setParameter("deviceUniqueId", deviceUniqueId)
                    .getSingleResult();
        } catch (Exception exception) {
            LOGGER.info(exception.getMessage());
            return null;
        }
        return userDevice;
    }

    @Transactional
    @Override
    public int updateSessionId(Long userDeviceId, String sessionId) {
        Session session = getSession();
        Query query = session.createQuery("update UserLoginHistory set session_id=:sessionId" +
                " where user_device_id=:userDeviceId");
        query.setParameter("sessionId", sessionId);
        query.setParameter("userDeviceId", userDeviceId);
        int result = query.executeUpdate();
        if (1 == result) {
            UserLoginHistory userLoginHistory = getUserLoginHistoryById(userDeviceId);
            session.refresh(userLoginHistory);
        }
        session.flush();
        return result;
    }

    private UserLoginHistory getUserLoginHistoryById(Long userDeviceId) {
        UserLoginHistory userLoginHistory;
        try {
            String hqlQueryString = "from UserLoginHistory where user_device_id=:userDeviceId";
            userLoginHistory = (UserLoginHistory) this.entityManager.createQuery(hqlQueryString)
                    .setParameter("userDeviceId", userDeviceId).getSingleResult();
        } catch (Exception exception) {
            LOGGER.info(exception.getMessage());
            return null;
        }
        return userLoginHistory;
    }

    @Override
    public String getSessionIdByUserDeviceId(Long userDeviceId) {
        UserLoginHistory userLoginHistory;
        String sessionId;
        try {
            String hqlQueryString = "from UserLoginHistory where user_device_id=:userDeviceId";
            userLoginHistory = (UserLoginHistory) this.entityManager.createQuery(hqlQueryString)
                    .setParameter("userDeviceId", userDeviceId)
                    .getSingleResult();
            sessionId = userLoginHistory.getSessionId();
        } catch (Exception exception) {
            LOGGER.info(exception.getMessage());
            return null;
        }
        return sessionId;
    }

    @Override
    public UserAddressType getUserAddressType(UserAddressTypeRequestDto userAddressTypeRequestDto) {
        UserAddressType userAddressTypeData;
        try{
            User user = new User();
            user.setUserId(userAddressTypeRequestDto.getUserId());
            String hqlQueryString = "from UserAddressType where user_id=:userId";
            userAddressTypeData = (UserAddressType) this.entityManager.createQuery(hqlQueryString)
                    .setParameter("userId", user)
                    .getSingleResult();
        }catch (Exception e){
            LOGGER.info(e.getMessage());
            return null;
        }
        return userAddressTypeData;
    }

}
