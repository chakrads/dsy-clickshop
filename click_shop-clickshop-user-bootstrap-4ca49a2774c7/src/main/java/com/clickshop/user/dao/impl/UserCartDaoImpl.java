package com.clickshop.user.dao.impl;

import com.clickshop.user.dao.UserCartDao;
import com.clickshop.user.dto.UserCartAddItemRequestDto;
import com.clickshop.user.entity.ItemsOfMerchant;
import com.clickshop.user.entity.UserCart;
import com.clickshop.user.entity.UserSelectedStore;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Component
public class UserCartDaoImpl implements UserCartDao {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserCartDaoImpl.class);

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private SessionFactory sessionFactory;

    private Session getSession() {
        return sessionFactory.getCurrentSession();
    }

    @Transactional
    @Override
    public UserCart saveUserCart(UserCart userCartItem) {
        Session session = getSession();
        session.save(userCartItem);
        return userCartItem;
    }

    @Override
    public ItemsOfMerchant getMerchantItemData(Long itemMerchantId) {
        ItemsOfMerchant itemsOfMerchant;
        try {
            String hqlQueryString = "from ItemsOfMerchant where item_merchant_id=:itemMerchantId";
            itemsOfMerchant = (ItemsOfMerchant) this.entityManager.createQuery(hqlQueryString)
                    .setParameter("itemMerchantId", itemMerchantId)
                    .getSingleResult();
        } catch (Exception exception) {
            LOGGER.info(exception.getMessage());
            return null;
        }
        return itemsOfMerchant;
    }

    @Override
    public List<UserCart> getUserCartList(Long userId) {
        List<UserCart> userCartList;
        try {
            String hqlQueryString = "from UserCart where user_id=:userId";
            userCartList = (List<UserCart>) this.entityManager.createQuery(hqlQueryString)
                    .setParameter("userId", userId)
                    .getResultList();
        } catch (Exception exception) {
            LOGGER.info(exception.getMessage());
            return null;
        }
        return userCartList;
    }

    @Transactional
    @Override
    public int deleteUserCart(Long userId) {
        int result = 0;
        try {
            String hqlQueryString = "delete from UserCart where user_Id=:userId";
            result = this.entityManager.createQuery(hqlQueryString)
                    .setParameter("userId", userId)
                    .executeUpdate();
        } catch (Exception exception) {
            LOGGER.info(exception.getMessage());
        }
        return result;
    }

    @Override
    public UserCart getUserCartItem(Long userId, Long itemMerchantId, Long storeId) {
        UserCart userCart;
        try {
            String hqlQueryString = "from UserCart where user_id=:userId and item_merchant_id=:itemMerchantId and store_id=:storeId";
            userCart = (UserCart) this.entityManager.createQuery(hqlQueryString)
                    .setParameter("userId", userId)
                    .setParameter("itemMerchantId", itemMerchantId)
                    .setParameter("storeId", storeId)
                    .getSingleResult();
        } catch (Exception exception) {
            LOGGER.info(exception.getMessage());
            return null;
        }
        return userCart;
    }

    @Transactional
    @Override
    public int updateUserCartItem(UserCart userCart) {
        Session session = getSession();
        Query query = session.createQuery("update UserCart set item_price = :itemPrice," + " item_quantity = :itemQuantity," + " total_price = :totalPrice" +
                " where item_merchant_id = :itemMerchantId" + " and user_id = :userId" + " and store_id = :storeId");
        query.setParameter("itemPrice", userCart.getItemPrice());
        query.setParameter("itemQuantity", userCart.getItemQuantity());
        query.setParameter("totalPrice", userCart.getTotalPrice());
        query.setParameter("itemMerchantId", userCart.getItemMerchant().getItemMerchantId());
        query.setParameter("userId", userCart.getUser().getUserId());
        query.setParameter("storeId", userCart.getStoreId());
        int result = query.executeUpdate();
        if (1 == result) {
            UserCart userCartUpdatedData = getUserCartItem(userCart.getUser().getUserId(), userCart.getItemMerchant().getItemMerchantId(), userCart.getStoreId());
            session.refresh(userCartUpdatedData);
        }
        session.flush();
        return result;
    }

    @Transactional
    @Override
    public int deleteUserCartItem(UserCartAddItemRequestDto userCartAddItemRequestDto) {
        int result;
        try {
            String hqlQueryString = "delete from UserCart where user_id=:userId and item_merchant_id=:itemMerchantId and store_id=:storeId";
            result = this.entityManager.createQuery(hqlQueryString)
                    .setParameter("userId", userCartAddItemRequestDto.getUserId())
                    .setParameter("itemMerchantId", Long.valueOf(userCartAddItemRequestDto.getItemMerchantId()))
                    .setParameter("storeId", userCartAddItemRequestDto.getStoreId())
                    .executeUpdate();
        } catch (Exception exception) {
            LOGGER.info(exception.getMessage());
            return 0;
        }
        return result;
    }

    @Transactional
    @Override
    public UserSelectedStore saveUserSelectedStore(UserSelectedStore userSelectedStore) {
        Session session = getSession();
        session.save(userSelectedStore);
        return userSelectedStore;
    }

    /*@Override
    public UserSelectedStore getUserSelectedStore(Long userId, Long storeId) {
        UserSelectedStore userSelectedStore;
        try {
            String hqlQueryString = "from UserSelectedStore where user_id=:userId and store_id=:storeId";
            userSelectedStore = (UserSelectedStore) this.entityManager.createQuery(hqlQueryString)
                    .setParameter("userId", userId)
                    .setParameter("storeId", storeId)
                    .getSingleResult();
        } catch (Exception exception) {
            LOGGER.info(exception.getMessage());
            return null;
        }
        return userSelectedStore;
    }*/

    @Override
    public UserSelectedStore getUserSelectedStore(Long userId) {
        UserSelectedStore userSelectedStore;
        try {
            String hqlQueryString = "from UserSelectedStore where user_id=:userId";
            userSelectedStore = (UserSelectedStore) this.entityManager.createQuery(hqlQueryString)
                    .setParameter("userId", userId)
                    .getSingleResult();
        } catch (Exception exception) {
            LOGGER.info(exception.getMessage());
            return null;
        }
        return userSelectedStore;
    }

    @Transactional
    @Override
    public int deleteUserSelectedStore(Long userId) {
        int result;
        try {
            String hqlQueryString = "delete from UserSelectedStore where user_id=:userId";
            result = this.entityManager.createQuery(hqlQueryString)
                    .setParameter("userId", userId)
                    .executeUpdate();
        } catch (Exception exception) {
            LOGGER.info(exception.getMessage());
            return 0;
        }
        return result;
    }

    @Transactional
    @Override
    public int updateUserSelectedStoreId(UserCartAddItemRequestDto userCartAddItemRequestDto, Long userSelectedStoreId) {
        Session session = getSession();
        Query query = session.createQuery("update UserSelectedStore set store_id=:storeId where user_selected_store_id=:userSelectedStoreId");
        query.setParameter("storeId", userCartAddItemRequestDto.getStoreId());
        query.setParameter("userSelectedStoreId", userSelectedStoreId);
        int result = query.executeUpdate();
        if (1 == result) {
            UserSelectedStore userSelectedStoreData = getUserSelectedStore(userCartAddItemRequestDto.getUserId());
            session.refresh(userSelectedStoreData);
        }
        session.flush();
        return result;
    }

    @Transactional
    @Override
    public int clearUserCartByUserId(Long userId) {
        int result;
        try {
            String hqlQueryString = "delete from UserCart where user_id=:userId";
            result = this.entityManager.createQuery(hqlQueryString)
                    .setParameter("userId", userId)
                    .executeUpdate();
        } catch (Exception exception) {
            LOGGER.info(exception.getMessage());
            return 0;
        }
        return result;
    }
}
