package com.clickshop.user.dao.impl;

import com.clickshop.user.dao.UserAddressDao;
import com.clickshop.user.entity.UserShippingAddress;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Component
public class UserAddressDaoImpl implements UserAddressDao {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserDaoImpl.class);

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private SessionFactory sessionFactory;

    private Session getSession() {
        return sessionFactory.getCurrentSession();
    }

    @Transactional
    @Override
    public UserShippingAddress saveUserShippingAddress(UserShippingAddress userShippingAddress) {
        Session session = getSession();
        session.save(userShippingAddress);
        return userShippingAddress;
    }

    @Override
    public List<UserShippingAddress> getUserShippingAddressListById(Long userId) {
        List<UserShippingAddress> userShippingAddressList;
        try{
            String hqlQueryString = "from UserShippingAddress where user_id=:userId";
            userShippingAddressList = (List<UserShippingAddress>) this.entityManager.createQuery(hqlQueryString)
                    .setParameter("userId", userId)
                    .getResultList();
        }catch (Exception e){
            LOGGER.info(e.getMessage());
            return null;
        }
        return userShippingAddressList;
    }

    @Transactional
    @Override
    public List<UserShippingAddress> deleteUserShippingAddressById(UserShippingAddress userShippingAddress) {
        int result = 0;
        try {
            String hqlQueryString = "delete from UserShippingAddress where user_shipping_address_id=:userShippingAddressId and user_id=:userId";
            result = this.entityManager.createQuery(hqlQueryString)
                    .setParameter("userShippingAddressId", userShippingAddress.getUserShippingAddressId())
                    .setParameter("userId", userShippingAddress.getUser().getUserId())
                    .executeUpdate();
        } catch (Exception e) {
            LOGGER.info(e.getMessage());
            return null;
        }
        List<UserShippingAddress> userShippingAddressList = null;
        if(result == 1){
            userShippingAddressList = getUserShippingAddressListById(userShippingAddress.getUser().getUserId());
        }
        return userShippingAddressList;
    }
}
