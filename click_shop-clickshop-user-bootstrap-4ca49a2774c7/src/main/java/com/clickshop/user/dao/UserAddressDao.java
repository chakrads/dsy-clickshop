package com.clickshop.user.dao;

import com.clickshop.user.entity.UserShippingAddress;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface UserAddressDao {

    public UserShippingAddress saveUserShippingAddress(UserShippingAddress userShippingAddress);

    public List<UserShippingAddress> getUserShippingAddressListById(Long userId);

    public List<UserShippingAddress> deleteUserShippingAddressById(UserShippingAddress userShippingAddress);

}
