package com.clickshop.user.dao.impl;

import com.clickshop.user.dao.UserOfferDao;
import com.clickshop.user.entity.Offer;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Component
public class UserOfferDaoImpl implements UserOfferDao {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserDaoImpl.class);

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private SessionFactory sessionFactory;

    private Session getSession() {
        return sessionFactory.getCurrentSession();
    }

    @Override
    public Offer getItemsOffer(Long merchantId, Long itemId) {

        Offer itemData;
        try {
            itemData = (Offer) this.entityManager.createQuery("from Offer where merchant_id=:merchantId" + " and item_Id = :itemId")
                    .setParameter("merchantId",merchantId)
                    .setParameter("itemId", itemId)
                    .getSingleResult();
        } catch (Exception exception) {
            LOGGER.info(exception.getMessage());
            return null;
        }
        return itemData;
    }
}
