package com.clickshop.user.dao.impl;

import com.clickshop.user.constants.SecurityConstants;
import com.clickshop.user.dao.UserPasswordDao;
import com.clickshop.user.dto.request.UserPasswordRequestDto;
import com.clickshop.user.entity.User;
import com.clickshop.user.utils.PasswordUtility;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Component
public class UserPasswordDaoImpl implements UserPasswordDao {
    private static final Logger LOGGER = LoggerFactory.getLogger(UserPasswordDaoImpl.class);


    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private SessionFactory sessionFactory;

    private Session getSession() {
        return sessionFactory.getCurrentSession();
    }

    @Override
    public User getUserByMobileNumber(String mobileNumber) {
        User userData = null;
        try {
            userData = (User) this.entityManager
                    .createQuery("from User where mobile_number=:mobileNo")
                    .setParameter("mobileNo", mobileNumber)
                    .getSingleResult();
        } catch (Exception exception) {
            LOGGER.info(exception.getMessage());
        }
        return userData;
    }

    @Transactional
    @Override
    public int createUserNewPassword(UserPasswordRequestDto userPasswordRequestDto) {
        int result;
        try {
            Session session = getSession();
            String secureUserPassword = PasswordUtility.generateSecurePassword(userPasswordRequestDto.getNewPassword(), SecurityConstants.PASSWORD_ENCRYPTION_SALT);
            userPasswordRequestDto.setNewPassword(secureUserPassword);
            String newUserPassword = userPasswordRequestDto.getNewPassword();
            Query query = session.createQuery("update User set password = :newPassword" +
                    " where mobile_number = :mobileNumber");
            query.setParameter("newPassword", newUserPassword);
            query.setParameter("mobileNumber", userPasswordRequestDto.getMobileNumber());
            result = query.executeUpdate();
            session.flush();
        } catch (Exception e) {
            LOGGER.info(e.getMessage());
            return 0;
        }
        return result;
    }
}
