package com.clickshop.user.dao.impl;

import com.clickshop.user.dao.StoreLocatorDao;
import com.clickshop.user.entity.*;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Component
public class StoreLocatorDaoImpl implements StoreLocatorDao {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserDaoImpl.class);

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private SessionFactory sessionFactory;

    private Session getSession() {
        return sessionFactory.getCurrentSession();
    }

    @Override
    public List<Store> getAllStores() {
        List<Store> storeList;
        try {
            storeList = (List<Store>) this.entityManager.createQuery("from Store").getResultList();
        } catch (NullPointerException e) {
            LOGGER.info(e.getMessage());
            return null;
        }
        return storeList;
    }

    @Override
    public Store getStoreById(Long storeId) {
        Store store;
        try {
            store = (Store) this.entityManager.createQuery("from Store where store_id=:storeId")
                    .setParameter("storeId",storeId )
                    .getSingleResult();
        } catch (Exception e) {
            LOGGER.info(e.getMessage());
            return null;
        }
        return store;
    }

    @Override
    public ProfileImage getStoreProfileImageById(Long merchantId) {
        ProfileImage profileImageGetData;
        System.out.println("PVR:Entering"+":"+merchantId);
        try {
            String hqlQueryString = "from ProfileImage where merchant_Id=:merchantId";
            profileImageGetData = (ProfileImage) this.entityManager.createQuery(hqlQueryString)
                    .setParameter("merchantId", merchantId).getSingleResult();
        } catch (Exception e) {
            LOGGER.info(e.getMessage());
            return null;
        }
        System.out.println("PVR:"+profileImageGetData.getMerchantId()+":"+profileImageGetData.getProfilePicUrl());
        return profileImageGetData;
    }

    @Override
    public ItemsOfMerchant getItemFromStoreInRange(Long merchantId, String itemName) {
        ItemsOfMerchant itemsOfMerchant;
        try {
            Merchant merchant = new Merchant();
            merchant.setMerchantId(merchantId);
            String hqlQueryString = "from ItemsOfMerchant where merchant_Id=:merchantId and item_name like :itemName";
            itemsOfMerchant = (ItemsOfMerchant) this.entityManager.createQuery(hqlQueryString)
                    .setParameter("merchantId", merchant)
                    .setParameter("itemName", '%'+itemName+'%')
                    .getSingleResult();
        } catch (Exception exception) {
            LOGGER.info(exception.getMessage());
            return null;
        }
        return itemsOfMerchant;
    }

    @Override
    public List<ItemsOfMerchant> getItemsListFromStoreInRange(Long merchantId, String itemName) {
        List<ItemsOfMerchant> itemsOfMerchants;
        try {
            Merchant merchant = new Merchant();
            merchant.setMerchantId(merchantId);
            String hqlQueryString = "from ItemsOfMerchant where merchant_Id=:merchantId and item_name like :itemName";
            itemsOfMerchants = (List<ItemsOfMerchant>) this.entityManager.createQuery(hqlQueryString)
                    .setParameter("merchantId", merchant)
                    .setParameter("itemName", '%'+itemName+'%')
                    .getResultList();
        } catch (Exception exception) {
            LOGGER.info(exception.getMessage());
            return null;
        }
        return itemsOfMerchants;
    }

    @Override
    public List<ItemsOfMerchant> getAllItemsFromStore(Long merchantId) {
        List<ItemsOfMerchant> itemsOfMerchantList;
        try {
            Merchant merchant = new Merchant();
            merchant.setMerchantId(merchantId);
            String hqlQueryString = "from ItemsOfMerchant where merchant_Id=:merchantId";
            itemsOfMerchantList = (List<ItemsOfMerchant>) this.entityManager.createQuery(hqlQueryString)
                    .setParameter("merchantId", merchant)
                    .getResultList();
        } catch (Exception exception) {
            LOGGER.info(exception.getMessage());
            return null;
        }
        return itemsOfMerchantList;
    }

    @Override
    public List<ItemsOfMerchant> getAllItemsFromStoreByCategory(Long merchantId, Long itemCategoryId) {
        List<ItemsOfMerchant> itemsOfMerchantList;
        try {
            Merchant merchant = new Merchant();
            merchant.setMerchantId(merchantId);
            String hqlQueryString = "from ItemsOfMerchant where merchant_Id=:merchantId and item_category_id=:itemCategoryId";
            itemsOfMerchantList = (List<ItemsOfMerchant>) this.entityManager.createQuery(hqlQueryString)
                    .setParameter("merchantId", merchant)
                    .setParameter("itemCategoryId", itemCategoryId)
                    .getResultList();
        } catch (Exception exception) {
            LOGGER.info(exception.getMessage());
            return null;
        }
        return itemsOfMerchantList;
    }

    @Override
    public Item getItemById(Long itemId) {
        Item itemData = null;

        try {
            itemData = (Item) this.entityManager.createQuery("from Item where item_id=:itemId")
                    .setParameter("itemId", itemId).getSingleResult();
        } catch (Exception exception) {
            LOGGER.info(exception.getMessage());
        }
        return itemData;
    }

    @Override
    public List<Item> getItemByCategory(String itemCategory) {
        List<Item> itemDataList;

        try {
            itemDataList = (List<Item>) this.entityManager.createQuery("from Item where item_category like :itemCategory")
                    .setParameter("itemCategory", '%'+itemCategory+'%').getResultList();
        } catch (Exception exception) {
            LOGGER.info(exception.getMessage());
            return null;
        }
        return itemDataList;
    }

    @Override
    public Offer getItemsOffer(Long merchantId, Long itemId) {

        Offer itemData = null;
        try {
            itemData = (Offer) this.entityManager.createQuery("from Offer where merchant_id=:merchantId" + " and item_Id = :itemId")
                    .setParameter("merchantId", merchantId)
                    .setParameter("itemId", itemId)
                    .getSingleResult();
        } catch (Exception exception) {
            LOGGER.info(exception.getMessage());
            return null;
        }
        return itemData;
    }

    @Override
    public ItemCategory getItemCategory(Long itemCategoryId) {
        ItemCategory itemCategory;
        try {
            itemCategory = (ItemCategory) this.entityManager.createQuery("from ItemCategory where item_category_id=:itemCategoryId")
                    .setParameter("itemCategoryId", itemCategoryId)
                    .getSingleResult();
        } catch (Exception exception) {
            LOGGER.info(exception.getMessage());
            return null;
        }
        return itemCategory;
    }

    @Override
    public ItemsOfMerchant getMerchantItemById(Long itemMerchantId) {
        ItemsOfMerchant itemsOfMerchant;
        try {
            String hqlQueryString = "from ItemsOfMerchant where item_merchant_Id=:itemMerchantId";
            itemsOfMerchant = (ItemsOfMerchant) this.entityManager.createQuery(hqlQueryString)
                    .setParameter("itemMerchantId", itemMerchantId)
                    .getSingleResult();
        } catch (Exception exception) {
            LOGGER.info(exception.getMessage());
            return null;
        }
        return itemsOfMerchant;
    }

}
