package com.clickshop.user.dao;

import com.clickshop.user.dto.UserCartAddItemRequestDto;
import com.clickshop.user.entity.ItemsOfMerchant;
import com.clickshop.user.entity.UserCart;
import com.clickshop.user.entity.UserSelectedStore;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface UserCartDao {

    public UserCart saveUserCart(UserCart userCartItem);

    public ItemsOfMerchant getMerchantItemData(Long itemMerchantId);

    public List<UserCart> getUserCartList(Long userId);

    public int deleteUserCart(Long userId);

    public UserCart getUserCartItem(Long userId, Long itemMerchantId, Long storeId);

    public int updateUserCartItem(UserCart userCart);

    public int deleteUserCartItem(UserCartAddItemRequestDto userCartAddItemRequestDto);

    public UserSelectedStore saveUserSelectedStore(UserSelectedStore userSelectedStore);

    //public UserSelectedStore getUserSelectedStore(Long userId, Long storeId);

    public int deleteUserSelectedStore(Long userId);

    public int updateUserSelectedStoreId(UserCartAddItemRequestDto userCartAddItemRequestDto, Long userSelectedStoreId);

    public UserSelectedStore getUserSelectedStore(Long userId);

    public int clearUserCartByUserId(Long userId);

}
