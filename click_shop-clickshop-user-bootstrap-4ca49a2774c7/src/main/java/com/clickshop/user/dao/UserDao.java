package com.clickshop.user.dao;

import com.clickshop.user.dto.request.UserAddressTypeRequestDto;
import com.clickshop.user.entity.User;
import com.clickshop.user.entity.UserAddressType;
import com.clickshop.user.entity.UserDevice;
import com.clickshop.user.entity.UserLoginHistory;
import org.springframework.stereotype.Component;

@Component
public interface UserDao {

    public User saveUser(User user);

    public UserDevice saveUserDevice(UserDevice userDevice);

    public UserLoginHistory saveUserLoginHistory(UserLoginHistory userLoginHistory);

    public User getUserByMobileNumber(String mobileNumber);

    public UserAddressType saveUserAddressType(UserAddressType userAddressType);

    public UserAddressType getUserAddressType(UserAddressTypeRequestDto userAddressTypeRequestDto);

    public UserAddressType updateUserAddressType(UserAddressType userAddressType);

    public int updateFirebaseToken(String firebaseToken, Long userId);

    public UserDevice getUserDeviceByType(Long userId, String deviceUniqueId);

    public int updateSessionId(Long userDeviceId, String sessionId);

    public String getSessionIdByUserDeviceId(Long userDeviceId);

}
