package com.clickshop.user.dao;

import com.clickshop.user.dto.request.UserPasswordRequestDto;
import com.clickshop.user.entity.User;
import org.springframework.stereotype.Component;

@Component
public interface UserPasswordDao {
    public User getUserByMobileNumber(String mobileNumber);

    public int createUserNewPassword(UserPasswordRequestDto userPasswordRequestDto);
}
