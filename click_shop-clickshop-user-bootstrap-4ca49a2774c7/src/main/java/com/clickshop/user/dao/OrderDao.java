package com.clickshop.user.dao;

import com.clickshop.user.dto.request.OrderStatusRequestDto;
import com.clickshop.user.entity.Merchant;
import com.clickshop.user.entity.Order;
import com.clickshop.user.entity.OrderDetails;
import com.clickshop.user.entity.Store;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface OrderDao {

    public Order createOrder(Order order);

    public OrderDetails saveOrderDetails(OrderDetails orderDetails);

    public Order findOrder(String orderId);

    public List<OrderDetails> getOrderDetails(String orderId);

    public Store getStoreById(Long storeId);

    public Store getStoreByMerchantId(Long merchantId);

    public Order findOrderStatus(OrderStatusRequestDto orderStatusRequestDto);

    public List<Order> getUserCurrentOrders(Long userId);

    public List<Order> getOrderList(Long userId);

    public String getMerchantFcmToken(Long merchantId);
    
    public Merchant getMerchantById(Long merchantId);
}
