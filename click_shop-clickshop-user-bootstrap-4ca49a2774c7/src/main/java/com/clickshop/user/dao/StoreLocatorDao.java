package com.clickshop.user.dao;

import com.clickshop.user.entity.*;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface StoreLocatorDao {

    public List<Store> getAllStores();

    public Store getStoreById(Long storeId);

    public ProfileImage getStoreProfileImageById(Long merchantId);

    public ItemsOfMerchant getItemFromStoreInRange(Long merchantId, String itemName);

    public List<ItemsOfMerchant> getItemsListFromStoreInRange(Long merchantId, String itemName);

    public Item getItemById(Long ItemId);

    public List<Item> getItemByCategory(String itemCategory);

    public Offer getItemsOffer(Long merchantId, Long itemId);

    public List<ItemsOfMerchant> getAllItemsFromStore(Long merchantId);

    public List<ItemsOfMerchant> getAllItemsFromStoreByCategory(Long merchantId, Long itemCategoryId);

    public ItemCategory getItemCategory(Long itemCategoryId);

    public ItemsOfMerchant getMerchantItemById(Long itemMerchantId);

}
