package com.clickshop.user;

import org.hibernate.SessionFactory;
import org.hibernate.jpa.HibernateEntityManagerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

/**
 * Created by Dell on 7/18/2018.
 */

@ComponentScan
@SpringBootApplication
@EnableAutoConfiguration
public class ClickShopUserApplication {

    public static void main(String[] args) {
        SpringApplication.run(ClickShopUserApplication.class,args);
    }

    @Bean
    public SessionFactory sessionFactory(HibernateEntityManagerFactory hemf) {
        return hemf.getSessionFactory();
    }

}
