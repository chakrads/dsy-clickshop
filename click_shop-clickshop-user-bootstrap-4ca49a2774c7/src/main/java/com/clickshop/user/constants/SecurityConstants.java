package com.clickshop.user.constants;

public class SecurityConstants {

    public static String PASSWORD_ENCRYPTION_SALT = "ClickShopPasswordSalt";
    public static String PASSWORD_ENCRYPTION_ALGORITHM_TYPE = "PBKDF2WithHmacSHA1";

}
