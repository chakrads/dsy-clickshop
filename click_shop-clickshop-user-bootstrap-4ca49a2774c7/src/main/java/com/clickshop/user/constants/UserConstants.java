package com.clickshop.user.constants;

public class UserConstants {

    public static String LOGIN_REGISTRATION_SUCCESS_MESSAGE = "Login registration Successful";
    public static Long SUCCESS_RESPONSE_CODE = 200L;
    public static Long FAILED_RESPONSE_CODE = 400L;
    public static Long DELETE_FAILED_RESPONSE_CODE = 402L;
    public static String USER_ORDER_STATUS_NEW = "New";
    public static Long INVALID_SESSION_CODE = 101L;
    public static String INVALID_SESSION_MESSAGE = "Invalid Session!";

}
